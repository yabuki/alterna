<?php
// alterna/inc/alterna-functions.php line-446//////
//	if(taxonomy_exists('portfolio_categories')) {
//		global $term;
//		$output = $term->name;
//	}
// show portfolio category link line-374

function child_styles_scripts() {
	//get template directory url
	$dir = get_stylesheet_directory_uri();
	//get theme version
	$theme_data = wp_get_theme();
	$ver = $theme_data['Version'];
	wp_enqueue_style( 'prettify', $dir . '/code/jquery.ex-code-prettify.css' , array() , $ver );
	wp_enqueue_script( 'google-code' , $dir . '/code/google-code-prettify/prettify.js' , array('jquery') , $ver , true);
	wp_enqueue_script( 'ex-code' , $dir . '/code/jquery.ex-code-prettify.js' , array('jquery') , $ver , true);
	wp_enqueue_script( 'my-code' , $dir . '/code/my-code.js' , array('ex-code') , $ver , true);
	}
add_action('wp_enqueue_scripts', 'child_styles_scripts');

/**
* Add additional fields to the taxonomy add view
* e.g. /wp-admin/edit-tags.php?taxonomy=category
*/
function taxonomy_metadata_add( $tag ) {
// Only allow users with capability to publish content
if ( current_user_can( 'publish_posts' ) ): ?>
	<div class="form-field">
		<label for="meta_title"><?php _e('Search Engine Title'); ?></label>
		<input name="meta_title" id="meta_title" type="text" value="" size="40" />
		<p class="description"><?php _e('Title display in search engines is limited to 70 chars'); ?></p>
	</div>
	<div class="form-field">
		<label for="meta_description"><?php _e('Search Engine Description'); ?></label>
		<textarea name="meta_description" id="meta_description" rows="5" cols="40"></textarea>
		<p class="description"><?php _e('The meta description will be limited to 156 chars by search engines.'); ?></p>
	</div>
<?php endif;
}

/**
* Add additional fields to the taxonomy edit view
* e.g. /wp-admin/edit-tags.php?action=edit&taxonomy=category&tag_ID=27&post_type=post
*/
function taxonomy_metadata_edit( $tag ) {
// Only allow users with capability to publish content
if ( current_user_can( 'publish_posts' ) ): ?>
	<tr class="form-field">
	<th scope="row" valign="top">
		<label for="meta_title"><?php _e('Search Engine Title'); ?></label>
	</th>
	<td>
		<input name="meta_title" id="meta_title" type="text" value="<?php echo get_term_meta($tag->term_id, 'meta_title', true); ?>" size="40" />
		<p class="description"><?php _e('Title display in search engines is limited to 70 chars'); ?></p>
	</td>
	</tr>
	<tr class="form-field">
	<th scope="row" valign="top">
		<label for="meta_description">
			<?php _e('Search Engine Description'); ?>
		</label>
	</th>
	<td>
	<textarea name="meta_description" id="meta_description" rows="5" cols="40"><?php echo get_term_meta($tag->term_id, 'meta_description', true); ?></textarea>
	<p class="description"><?php _e('Title display in search engines is limited to 70 chars'); ?></p>
	</td>
	</tr>
<?php endif;
}

/**
* Save taxonomy metadata
*
* Currently the Taxonomy Metadata plugin is needed to add a few features to the WordPress core
* that allow us to store this information into a new database table
*
* http://wordpress.org/extend/plugins/taxonomy-metadata/
*/
function save_taxonomy_metadata( $term_id ) {
	if ( isset($_POST['meta_title']) )
		update_term_meta( $term_id,
		'meta_title', esc_attr($_POST['meta_title']) );
	if ( isset($_POST['meta_description']) )
		update_term_meta( $term_id,
		'meta_description', esc_attr($_POST['meta_description'])
	);
}

/**
* Add additional taxonomy fields to all public taxonomies
*/
function taxonomy_metadata_init() {
// Require the Taxonomy Metadata plugin
	if( !function_exists('update_term_meta') || !function_exists('get_term_meta') ) return false;
	// Get a list of all public custom taxonomies
	$taxonomies = get_taxonomies( array(
	'public' => true,
	'_builtin' => true
	), 
	'names', 'and');
	// Attach additional fields onto all custom,
	// public taxonomies
	if ( $taxonomies ) {
		foreach ( $taxonomies as $taxonomy ) {
		// Add fields to "add" and
		// "edit" term pages
		add_action("{$taxonomy}_add_form_fields", 'taxonomy_metadata_add', 10, 1);
		add_action("{$taxonomy}_edit_form_fields", 'taxonomy_metadata_edit', 10, 1);
		// Process and save the data
		add_action("created_{$taxonomy}", 'save_taxonomy_metadata', 10, 1);
		add_action("edited_{$taxonomy}", 'save_taxonomy_metadata', 10, 1);
		}
	}
}
add_action('admin_init', 'taxonomy_metadata_init');
?>
