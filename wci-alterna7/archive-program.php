<?php
/**
 * The Template for displaying service archive.
 *
 * @since alterna 7.0
 */
get_header(); 
?>
<div id="main" class="container">
	<div class="row">
        <div class="alterna-col col-lg-8 col-md-8 col-sm-8">
            	<section class="service-container row">
				<?php get_template_part( 'template/blog/blog', 'top-content' ); ?>
				<?php	get_template_part( 'template/program/program-content'); ?>
                </section>
                <?php alterna_content_pagination('nav-bottom' , 'pagination-centered'); ?>
        </div>
        <div class="alterna-col col-lg-4 col-md-4 col-sm-4"><?php generated_dynamic_sidebar(); ?></div>
    </div>
</div>
<?php get_footer(); ?>