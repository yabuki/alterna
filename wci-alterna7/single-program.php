<?php
/**
 * Single Program
 *
 * @since alterna 7.0
 */

get_header(); 
?>
<div id="main" class="container">
    <div class="row">
		<section class="single-program-section alterna-col col-lg-12 col-md-12 col-sm-12">
            <?php 
			if ( have_posts() ) {
				while ( have_posts() ){
					the_post();
					get_template_part( 'template/program/program-single' );
				}
			}else{ ?>
                <p><?php _e('Sorry, this page does not exist.' , 'alterna' ); ?></p>
            <?php } 
			alterna_single_content_nav('single-nav-bottom');
			?>
        </section>
    </div>
</div>
<?php get_footer(); ?>