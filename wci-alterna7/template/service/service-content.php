<?php
/**
 * Text Post Content
 *
 * @since alterna 7.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('service-archive-article entry-post ');?> itemscope itemtype="http://schema.org/Article">
	<!-- ***************************post content ***********************************************-->
<?php
$args = array(
'parent'       => 0,
'hierarchical' => 0,
'orderby'      => 'term_order', // Category Order and Taxonomy Terms Order を使用
'order'        => 'ASC'
);
    $taxonomy_name = 'service_type';
    $taxonomys = get_terms($taxonomy_name,$args);
    if(!is_wp_error($taxonomys) && count($taxonomys)):
        foreach($taxonomys as $taxonomy):
        $url = get_term_link($taxonomy->slug, $taxonomy_name);
        $tax_posts = get_posts(array(
            'post_type' => get_post_type(),
            'posts_per_page' => 5, // 表示させたい記事数
            'tax_query' => array(
                array(
                    'taxonomy'=>'service_type',
                    'terms'=>array( $taxonomy->slug ), 
                    'field'=>'slug',
                    'include_children'=>true,
                    'operator'=>'IN'
                    ),
                'relation' => 'AND'
                )
            ));
    if($tax_posts):
?>
    <section class="service-content-section entry-right-side col-md-12 col-sm-12">

<div class="alterna-sc-title left">
	<div class="row">
		<div class="col-md-12">
			<div class="alterna-sc-title-container">
				<h3 id="<?php echo esc_html($taxonomy->slug); ?>" class="alterna-sc-entry-title bold">
					<a href="<?php echo $url; ?>"><?php echo esc_html($taxonomy->name); ?></a>
				</h3>
				<div class="alterna-sc-title-line">
				</div>
			</div>
		</div>
	</div>
</div>

					
			<?php foreach($tax_posts as $tax_post): ?>
    	<header class="entry-header">
			<h4><a href="<?php the_permalink(); ?>"><?php echo get_the_title($tax_post->ID); ?></a></h4>
		</header><!-- .entry-header -->
        <div class="entry-summary" itemprop="articleSection">
        <?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
        <?php echo '<a class="more-link" href="'.esc_url( get_permalink() ).'">'.__('Read More','alterna').'</a>'; ?>
		<?php endforeach; ?>
    </section>
					<?php
					endif;
					endforeach;
					endif;
					?>	

</article>
        