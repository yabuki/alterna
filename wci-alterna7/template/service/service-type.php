<?php
/**
 * Text Post Content
 *
 * @since alterna 7.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('service-archive-article entry-post ');?> itemscope itemtype="http://schema.org/Article">
<div class="row">
	<!-- ***************************post content ***********************************************-->
    <section class="service-content-section entry-right-side col-md-12 col-sm-12">
        	<?php edit_post_link(__('Edit', 'alterna'), '<div class="post-edit"><i class="fa fa-edit"></i>', '</div>'); ?>
			
    	<header class="entry-header">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="post-meta">
                <div class="cat-links">
					<i class="fa fa-folder-open"></i>
					<span itemprop="genre"><?php echo get_the_term_list($post->ID,'service_type'); ?></span>
				</div>
            </div>
		</header><!-- .entry-header -->
        <div class="entry-summary" itemprop="articleSection">
        <?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
        <?php echo '<a class="more-link" href="'.esc_url( get_permalink() ).'">'.__('Read More','alterna').'</a>'; ?>
		
    </section>
	
</div>
</article>
        