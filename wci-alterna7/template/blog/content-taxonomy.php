<?php
/**
 * Text Post Content
 *
 * @since alterna 7.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('taxonomy-archive-article entry-post ');?> itemscope itemtype="http://schema.org/Article">
<div class="row">
	<!-- ***************************post content ***********************************************-->
    <section class="content-custom-section entry-right-side col-md-12 col-sm-12">
    	<header class="entry-header">
			<div class="date entry-date updated" itemprop="datePublished"><?php echo esc_html(get_the_date()); ?></div>
        	<?php edit_post_link(__('Edit', 'alterna'), '<div class="post-edit"><i class="fa fa-edit"></i>', '</div>'); ?>
        	<h3 class="entry-title" itemprop="name"><?php single_term_title(); ?></h3>
            <div class="post-meta">
                <div class="cat-links">
					<i class="fa fa-folder-open"></i>
					<span itemprop="genre"><?php the_taxonomies(); ?></span>
				</div>
            </div>
		</header><!-- .entry-header -->
        <div class="entry-summary" itemprop="articleSection">
        <?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
        <?php echo '<a class="more-link" href="'.esc_url( get_permalink() ).'">'.__('Read More','alterna').'</a>'; ?>
    </section>
</div>
</article>
        