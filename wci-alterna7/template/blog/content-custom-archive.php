<?php
/**
 * Text Post Content
 *
 * @since alterna 7.0
 */
global $blog_show_type;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('custom-archive-article entry-post '.(intval($blog_show_type) != 0 ? "blog-show-style-2" : ""));?> itemscope itemtype="http://schema.org/Article">
<div class="row">
	<!-- ***************************post content ***********************************************-->
    <section class="custom-archive-section entry-right-side col-md-12 col-sm-12">
    	<header class="entry-header">
        	<?php edit_post_link(__('Edit', 'alterna'), '<div class="post-edit"><i class="fa fa-edit"></i>', '</div>'); ?>
        	<?php the_title( '<h3 class="entry-title" itemprop="name"><a href="' . esc_url( get_permalink() ) . '" itemprop="url">', '</a></h3>' ); ?>
            <div class="post-meta">
				<div class="post-date">
					<i class="fa fa-calendar"></i><span class="entry-date updated" itemprop="datePublished"><?php echo esc_html(get_the_date()); ?></span>
				</div>
                <div class="cat-links">
					<i class="fa fa-folder-open"></i>
					<span itemprop="genre">
				<?php 
					$args = array(
						'post' => 0,
						'before' => '',
						'sep' => ' , ',
						'after' => '',
						'template' => '<div style="display:none;">%s: </div>%l'
					);
					the_taxonomies($args); 
				?> 
					</span>
				</div>
            </div>
		</header><!-- .entry-header -->
        <div class="entry-summary" itemprop="articleSection">
        <?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
        <?php echo '<a class="more-link" href="'.esc_url( get_permalink() ).'">'.__('Read More','alterna').'</a>'; ?>
    </section>
</div>
</article>
        