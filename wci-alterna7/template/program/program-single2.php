<?php
/**
 * Portfolio Single Page Style 2
 *
 * @since alterna 7.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('program-single-article post-entry'); ?> itemscope itemtype="http://schema.org/CreativeWork">
<div class="row">
	<div class="single-portfolio-left-content col-lg-8 col-md-6 col-sm-6" >

        <?php if(has_post_thumbnail(get_the_ID())) { ?>
            <?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbnail_size); ?>
            <?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
            <a href="<?php echo $full_image[0]; ?>" class="fancyBox">
            <div class="post-img">
                <img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo get_the_title(); ?>" />
            </div>
            </a>
        <?php } ?>
        <div class="entry-content" itemprop="text">
        <?php the_title( '<h3 class="entry-title" itemprop="name"><a href="' . esc_url( get_permalink() ) . '" itemprop="url">', '</a></h3>' ); ?>
		<p class="sub-title">- <?php the_field('program_Name'); ?></p>
        <?php edit_post_link(__('Edit', 'alterna'), '<div class="post-edit"><i class="fa fa-edit"></i>', '</div>'); ?>
		
				<?php if(get_post_meta( $post->ID, 'program_Summary', true )): ?>
					<h3><i class="font-icon-book-2"></i>プログラムの概要</h3>
					<p><?php the_field('program_Summary'); ?></p>
				<?php endif; ?>

				<?php if(get_post_meta( $post->ID, 'Features_Benefits', true )): ?>
					<h3><i class="font-icon-book-2"></i>特徴と利点</h3>
					<p><?php the_field('Features_Benefits'); ?></p>
				<?php endif; ?>
				
					<?php if(get_post_meta( $post->ID, 'c_Ccurriculum', true )): ?>
						<h3><i class="font-icon-book"></i>カリキュラム</h3>	
						<p><?php the_field('c_Ccurriculum'); ?></p>
					<?php endif; ?>
	
					<?php if(get_post_meta( $post->ID, 'entrance_requirement', true )): ?>
						<h3><i class="font-icon-book"></i>入学条件</h3>
						<p><?php the_field('entrance_requirement'); ?></p>
						<div class="alert alert-block alert-info fade in">
							本プログラムの参加には、他プログラムと同様に留学生ビザ（F-1ビザ）の取得が必要です。
					  	</div>
					<?php endif; ?>

					<?php if(get_post_meta( $post->ID, 'class_Schedule_text', true )): ?>
						<h3><i class="font-icon-book"></i>プログラムスケジュール</h3>	
						<p><?php the_field('class_Schedule_text'); ?></p>
					<?php endif; ?>

					<?php if(get_post_meta( $post->ID, 'P_Application_fee', true )): ?>
						<h3><i class="font-icon-book"></i>授業料</h3>	
						<ul>
							<li>$<?php the_field('P_Application_fee'); ?></li>
						</ul>
					<?php endif; ?>
	
					<?php if(get_post_meta( $post->ID, 'p_whet', true )): ?>
						<h3><i class="font-icon-book"></i>スケジュール</h3>	
						<p><?php the_field('p_whet'); ?></p>
					<?php endif; ?>
					
					<?php if(get_post_meta( $post->ID, 'p_period', true )): ?>
						<h3><i class="font-icon-book"></i>期間</h3>	
						<ul><li><?php the_field('p_period'); ?></li></ul>
					<?php endif; ?>
		
					<p class="textalignright"><a href="<?php bloginfo('url');  ?>/how-to" class="btn btn-info"><i class="icon-caret-right"></i>プログラムの参加方法はこちらから</a></p>
		
				
			<?php the_content(); ?>
            <?php wp_link_pages(); ?>
        </div>		
	</div>
	
    <div class="single-portfolio-right-content col-lg-4 col-md-6 col-sm-6">
        <ul class="single-portfolio-meta">
            <li>
                <div class="type"><i class="fa fa-calendar"></i><?php _e('Date','alterna'); ?></div>
                <div class="value entry-date updated" itemprop="datePublished"><?php echo esc_html(get_the_date()); ?></div>
            </li>
             <li>
                <div class="type"><i class="fa fa-folder-open"></i><?php _e('Categories','alterna'); ?></div>
                <div class="value" itemprop="genre"><?php echo alterna_get_custom_portfolio_category_links( alterna_get_custom_post_categories(get_the_ID(),'portfolio_categories',false)  , ' / '); ?></div>
            </li>
            <?php if(alterna_get_post_meta_key('portfolio-client') != "") { ?>
            <li>
                <div class="type"><i class="fa fa-user"></i>&nbsp;<?php _e('Client','alterna'); ?></div>
                <div class="value" itemprop="author"><?php echo esc_attr(alterna_get_post_meta_key('portfolio-client')); ?></div>
            </li>
            <?php } ?>
            <?php if(alterna_get_post_meta_key('portfolio-skills') != "") { ?>
            <li>
                <div class="type"><i class="fa fa-bolt"></i><?php _e('Skills','alterna'); ?></div>
                <div class="value"><?php echo esc_attr(alterna_get_post_meta_key('portfolio-skills')); ?></div>
            </li>
            <?php } ?>
            <?php if(alterna_get_post_meta_key('portfolio-colors') != "") { ?>
            <li>
                <div class="type"><i class="fa fa-adjust"></i><?php _e('Colors','alterna'); ?></div>
                <div class="value"><?php echo alterna_get_color_list(alterna_get_post_meta_key('portfolio-colors')); ?></div>
            </li>
            <?php } ?>
            <?php if(alterna_get_post_meta_key('portfolio-system') != "") { ?>
            <li>
                <div class="type"><i class="fa fa-desktop"></i><?php _e('Used System','alterna'); ?></div>
                <div class="value"><?php echo esc_attr(alterna_get_post_meta_key('portfolio-system')); ?></div>
            </li>
            <?php } ?>
            <?php if(alterna_get_post_meta_key('portfolio-price') != "") { ?>
            <li>
                <div class="type"><i class="fa fa-usd"></i><?php _e('Price','alterna'); ?></div>
                <div class="value"><?php echo esc_attr(alterna_get_post_meta_key('portfolio-price')); ?></div>
            </li>
            <?php } ?>
            
            <?php alterna_get_portfolio_custom_fields(alterna_get_post_meta_key('portfolio-custom-fields')); ?>
            
             <?php if(alterna_get_post_meta_key('portfolio-link') != ""){ ?>
             <li>
                <div class="type"><i class="fa fa-link"></i><?php _e('Link','alterna'); ?></div>
                <div class="value"><a href="<?php echo esc_url(alterna_get_post_meta_key('portfolio-link')); ?>"><?php echo esc_url(alterna_get_post_meta_key('portfolio-link')); ?></a></div>
            </li>
            <?php } ?>
        </ul>

        <?php if(alterna_get_options_key('portfolio-enable-share') == "on") { ?>
        <div class="portfolio-share">
			<?php echo  alterna_get_options_key('portfolio-share-code'); ?>
        </div>
        <?php } ?>
    </div>
</div>
</article>