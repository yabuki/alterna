<?php
/**
 * Program Single Page
 *
 * @since alterna 7.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('program-single-article post-entry'); ?> itemscope itemtype="http://schema.org/CreativeWork">
<div class="row">
	<div class="single-portfolio-left-content col-lg-8 col-md-6 col-sm-6" >
        <?php if(has_post_thumbnail(get_the_ID())) { ?>
            <?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbnail_size); ?>
            <?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
            <a href="<?php echo $full_image[0]; ?>" class="fancyBox">
            <div class="post-img">
                <img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo get_the_title(); ?>" />
            </div>
            </a>
        	<?php } ?>
        <div class="entry-content" itemprop="text">
        <?php edit_post_link(__('Edit', 'alterna'), '<div class="post-edit"><i class="fa fa-edit"></i>', '</div>'); ?>
			<?php the_content(); ?>
<?php if( get_field('codex_on') ) { ?>
<div class="ex-code-prettify">
	<textarea class="code" data-ex-code-prettify-param="{codeType:'<?php the_field('code_type'); ?>'}">
		<?php the_field('code'); ?>
	</textarea>
</div>	
<?php } else { ?>

    
<?php } ?>
		
		
			
			
			<?php wp_link_pages(); ?>
        </div><!-- / .entry-content -->
	</div>
	<div class="single-portfolio-right-content col-lg-4 col-md-6 col-sm-6">
		<?php echo do_shortcode('[title text="'.get_the_title().'"]'); ?>
		
		<p>Snipet Information</p>
	   
		<ul class="single-portfolio-meta row-fluid">
			<li>
				<div class="type"><i class="fa fa-calendar"></i><?php _e('Up Date','alterna'); ?></div>
				<div class="value"><?php the_modified_date(); ?></div>
			</li>
			 <li>
				<div class="type"><i class="fa fa-folder-open"></i><?php _e('Type','alterna'); ?></div>
				<div class="value"><?php echo get_the_term_list($post->ID, 'program_type'); ?></div>
			</li>
			<li>
				<div class="type"><i class="fa fa-cog"></i>&nbsp;<?php _e('Code','alterna'); ?></div>
				<div class="value"><?php echo get_the_term_list($post->ID, 'code_type'); ?></div>
			</li>

			<?php if(get_field('plugin_price')): ?>
			<li>
				<div class="type"><i class="fa fa-usd"></i>&nbsp;<?php _e('Plugin Price','alterna'); ?></div>
				<div class="value"><?php the_field('plugin_price'); ?></div>
			</li>
			<?php endif; ?>
			
			<?php if(get_field( 'plugin_name')): ?>
			<li>
				<div class="type"><i class="fa fa-cogs"></i>&nbsp;<?php _e('Add-ons','alterna'); ?></div>
				<div class="value"><?php the_field('add-on'); ?></div>
			</li>
			<?php endif; ?>						
			
			<?php if(get_field('via_site_name')): ?>
			<li>
				<div class="type"><i class="fa fa-usd"></i>&nbsp;<?php _e('via.','alterna'); ?></div>
				<div class="value"><?php the_field('via_site_name'); ?></div>
			</li>
			<?php endif; ?>
			
			<?php if(get_field('template_name')): ?>
			<li>
				<div class="type"><i class=" fa fa-code"></i>&nbsp;<?php _e('Customize','alterna'); ?></div>
				<div class="value"><?php the_field('template_name'); ?></div>
			</li>
			<?php endif; ?>

			<!-- show share -->
			<?php if(intval(alterna_get_options_key('portfolio-share-type')) != 0) : ?>
			<li>
				<div class="single-portfolio-share">
					<?php if(alterna_get_options_key('portfolio-share-type') == "1") : ?>
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_button_tweet"></a>
					<a class="addthis_button_pinterest_pinit"></a>
					<a class="addthis_counter addthis_pill_style"></a>
					</div>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5102b19361611ae7"></script>
					<!-- AddThis Button END -->
					<?php else : ?>
					<!-- Custom share plugin code -->
					<?php echo  alterna_get_options_key('portfolio-share-code'); ?>
					<?php endif; ?>
				</div>  
			</li>
			<?php endif; ?>
		</ul>
		<?php echo do_shortcode('[space line="yes"]'); ?>
	</div>
</div><!-- / .row -->
</article>
