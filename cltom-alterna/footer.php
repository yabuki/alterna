<?php
/**
 * The Footer for our theme.
 *
 * @since alterna 1.0
 */
?>
	</div><!-- end content-wrap -->

	<div class="footer-wrap">
    	<footer class="footer-content">
        	<div class="footer-top-content container">
            	<div class="row-fluid">
					<?php $cols = alterna_get_footer_widget_active_items(); ?>
                    <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('sidebar-footer-1')): ?>
                    <div class="<?php echo $cols; ?>">
                        <?php dynamic_sidebar('sidebar-footer-1') ?>
                    </div>
                    <?php endif; ?>
                   <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('sidebar-footer-2')): ?>
                    <div class="<?php echo $cols; ?>">
                        <?php dynamic_sidebar('sidebar-footer-2') ?>
                    </div>
                    <?php endif; ?>
                    <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('sidebar-footer-3')): ?>
                    <div class="<?php echo $cols; ?>">
                        <?php dynamic_sidebar('sidebar-footer-3') ?>
                    </div>
                    <?php endif; ?>
                    <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('sidebar-footer-4')): ?>
                    <div class="<?php echo $cols; ?>">
                        <?php dynamic_sidebar('sidebar-footer-4') ?>
                    </div>
                    <?php endif; ?>
                </div>
			</div>
            
        	<div class="footer-bottom-content">
            	<div class="container">
                	<div class="row-fluid">
            		<div class="footer-copyright"><?php echo alterna_get_options_key('footer-copyright-message'); ?></div>
            		<div class="footer-link"><?php echo alterna_get_options_key('footer-link-text'); ?></div>
                    </div>
                    <?php
						if(intval(alterna_get_options_key('global-responsive')) == 0 && alterna_get_options_key('global-mobile-enable') == "on") { 
							global $deviceType, $alterna_show_mode;
							if($deviceType == "phone"){
						?>
							<div id="alterna-show-mode" class="row-fluid footer-copyright">
								<?php 
								echo __('Show:','alterna');
								if($alterna_show_mode == "" || $alterna_show_mode == "mobile") {
									echo "<span>".__('Mobile','alterna')."</span>"; 
									echo __('|','alterna');
									echo "<a onclick='document.cookie=\"alterna_show_mode=desktop\";' href='?show_mode=desktop'>".__('Desktop','alterna')."</a>"; 
								}else{
									echo "<a onclick='document.cookie=\"alterna_show_mode=mobile\";' href='?show_mode=mobile'>".__('Mobile','alterna')."</a>";
									echo __('|','alterna');
									echo "<span>".__('Desktop','alterna')."</span>";  
								}
								?>
							</div>
						<?php
							}
						}
					?>
                </div>
        	</div>
    	</footer>
        <?php if(alterna_get_options_key('footer-banner-enable') == "on") : ?>
            <!-- header banner -->
            <div id="footer-banner" data-id="<?php echo alterna_get_options_key('footer-banner-id'); ?>">
                <div class="container">
                	<?php echo do_shortcode(alterna_get_options_key('footer-banner-content')); ?>
                    <a href="#" class="close-btn">×</a>
                </div>
            </div>
        <?php endif; ?>
	</div><!-- end footer-wrap -->
    </div><!-- end wrapper -->
    <!-- alert modal banner -->
    <?php if( alterna_get_options_key('global-modal-enable') == "on" &&  alterna_get_options_key('global-modal-content') != "") : ?>
    	<div id="alert-modal-banner" class="modal hide fade" data-id="<?php echo alterna_get_options_key('global-modal-id');?>">
        	<?php echo do_shortcode(alterna_get_options_key('global-modal-content')); ?>
		</div>
    <?php endif; ?>
	<?php wp_footer() ?>
</body>
</html>