<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @since alterna 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<?php 
global $page, $paged, $post, $deviceType, $alterna_show_mode;

// get the current page
$paged = 1;
if (get_query_var('paged')) {
	$paged = get_query_var('paged');
} else if (get_query_var('page')) {
	$paged = get_query_var('page');
}

?>
<title><?php wp_title( '|', true, 'right' ); ?></title>

<?php if(intval(alterna_get_options_key('global-responsive')) == 0 && alterna_get_options_key('global-mobile-enable') == "on") { 
	
	if(!class_exists('Mobile_Detect')){
		require_once("inc/tools/Mobile-Detect/Mobile_Detect.php");
	}
	
	$detect = new Mobile_Detect;
	$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
	$alterna_show_mode = isset($_COOKIE["alterna_show_mode"]) ? $_COOKIE["alterna_show_mode"] : "";
	
?>
	<?php if(($deviceType == "phone" && $alterna_show_mode != "desktop") || $deviceType != "phone"){ ?>
    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php } ?>
<?php } ?>

<link rel="shortcut icon" href="<?php echo (alterna_get_options_key('favicon') != "") ? alterna_get_options_key('favicon') : get_template_directory_uri()."/img/favicon.png";?>" />

<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!--[if IE 7]>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fontawesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<?php 
	$global_layout = 'boxed-layout'; 
	if(intval(alterna_get_options_key('global-responsive')) != 0){
		$global_layout = 'fixed-width';
	}else if(intval(alterna_get_options_key('global-layout')) != 0){
		$global_layout = 'full-width-layout';
	}
?>

<body <?php body_class($global_layout); ?>>
	<div class="wrapper">
    <div class="header-wrap">
    
    	<?php if(alterna_get_options_key('header-banner-enable') == "on") : ?>
            <!-- header banner -->
            <div id="header-banner" data-id="<?php echo alterna_get_options_key('header-banner-id'); ?>">
                <div class="container">
                	<?php echo do_shortcode(alterna_get_options_key('header-banner-content')); ?>
                    <a href="#" class="close-btn">×</a>
                </div>
            </div>
        <?php endif; ?>
        
        <?php if(alterna_get_options_key('header-topbar-enable') == "on") : ?>
        <!-- header top bar -->
		<?php if(intval(alterna_get_options_key('header-topbar-style-type')) == 0) { ?>
        <div class="header-top-content">
            <div class="container">
                <div class="header-information">
                <?php echo do_shortcode(alterna_get_options_key('header-alert-message'));  ?>
                <?php
				if(alterna_get_options_key('header-topbar-login-enable') == "on"){
                    if (class_exists( 'woocommerce' )) {
                        get_template_part('woocommerce/content-header');
                    }else if(alterna_get_options_key('header-topbar-custom-login-page') != ""){ ?>
                    <div class="custom-login-header">
                        <div class="custom-login">
                         <?php if ( is_user_logged_in() ) { ?>
                            <a href="<?php echo alterna_get_options_key('header-topbar-custom-login-page'); ?>" title="<?php _e( 'View your account', 'alterna' ); ?>"><?php 
                                global $current_user;
                                get_currentuserinfo();
                                if($current_user->user_firstname)
                                    echo __('Welcome, ','alterna') . $current_user->user_firstname;
                                elseif($current_user->display_name)
                                    echo __('Welcome, ','alterna') . $current_user->display_name;
                            ?></a>&nbsp;&nbsp;<?php _e('|','alterna'); ?>&nbsp;&nbsp;<a href="<?php echo wp_logout_url(get_permalink()); ?>"><?php _e('Log out','alterna'); ?></a>                                
                            <?php }	else { ?>
                            <a class="wc-login-in" href="<?php echo alterna_get_options_key('header-topbar-custom-login-page'); ?>" title="<?php _e( 'Login / Register', 'alterna' ); ?>"><?php _e( 'Login / Register', 'alterna' ); ?></a>
                            <?php } ?>
                        </div>
                    </div>	
                <?php	}
				}
                ?>
             </div>
         </div>
         </div>
        <?php }else{ ?>
        <div id="header-topbar" >
        <div class="container">
        <?php if(alterna_get_options_key('header-topbar-menu-enable') == "on") { ?>
            <div id="header-topbar-left-content">
                <?php 
					$alterna_topbar_menu = array(
						'theme_location'  	=> 'alterna_topbar_menu',
						'container'			=> '',
						'menu_class'    	=> 'alterna-topbar-menu',
						'fallback_cb'	  	=> 'alterna_show_setting_topbar_menu'
					); 
					wp_nav_menu($alterna_topbar_menu);
				?>
            </div>
		<?php } ?>
            <div id="header-topbar-right-content">
                <ul>
				<?php
					if(alterna_get_options_key('header-topbar-login-enable') == "on"){
						if (class_exists( 'woocommerce' )) {
							get_template_part('woocommerce/content-topbar-header');
						}else if( alterna_get_options_key('header-topbar-login-enable') == "on" && alterna_get_options_key('header-topbar-custom-login-page') != ""){
                ?>
                    	<li>
                    	<?php if ( is_user_logged_in() ) { ?>
                            <a href="<?php echo alterna_get_options_key('header-topbar-custom-login-page'); ?>" title="<?php _e( 'View your account', 'alterna' ); ?>"><?php 
                                global $current_user;
                                get_currentuserinfo();
                                if($current_user->user_firstname)
                                    echo __('Welcome, ','alterna') . $current_user->user_firstname;
                                elseif($current_user->display_name)
                                    echo __('Welcome, ','alterna') . $current_user->display_name;
                            ?></a>&nbsp;&nbsp;<?php _e('|','alterna'); ?>&nbsp;&nbsp;<a href="<?php echo wp_logout_url(get_permalink()); ?>"><?php _e('Log out','alterna'); ?></a>                                
                            <?php }else{ ?>
                            <a class="wc-login-in" href="<?php echo alterna_get_options_key('header-topbar-custom-login-page'); ?>" title="<?php _e( 'Login / Register', 'alterna' ); ?>"><?php _e( 'Login / Register', 'alterna' ); ?></a>
                            <?php } ?>
                         </li>
                    <?php
						}
					}
						
					if(alterna_get_options_key('header-topbar-social-enable') == "on"){
						echo alterna_get_social_list('',true); 
					}
					if(alterna_get_options_key('rss-feed') != ""){
						echo '<li class="social"><a href="'.alterna_get_options_key('rss-feed').'"><i class="icon-rss-sign"></i></a></li>';
					}
					if(alterna_get_options_key('header-topbar-wpml-enable') == "on"){
						echo alterna_get_wpml_switcher();
					}
					?>
                </ul>
            </div>
            <div id="alterna-topbar-nav" class="navbar">
                <div id="alterna-topbar-select" class="navbar-inverse">
                    <button type="button" class="btn btn-navbar collapsed">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <div class="collapse"><ul class="nav"></ul></div>
                </div>
            </div>
        </div>
        </div>
        <?php } ?>
        <?php endif; ?>

        <header>
            <?php get_template_part( 'header', 'style-'.(intval(alterna_get_options_key('header-style-type')) + 1) );?>
        </header>
        
    </div><!-- end header-wrap -->
    
    <?php 
		// current post, page id
		$post_id = ($post) ? $post->ID : '-1';
		if(is_home() && !is_front_page()) $post_id = get_option('page_for_posts');
		// show header layerslider
		if(!(is_category()|| is_tag() || is_404() || is_search() || is_date()) && intval(get_post_meta($post_id , 'slide-type', true)) != 0) :
	?>
    	<?php if(intval(get_post_meta($post_id , 'slide-type', true)) == 1 && intval(get_post_meta($post_id , 'layer-slide-id', true)) != 0) : ?>
        	<div id="layerslide-container" class="page-slider-container">
                <?php echo do_shortcode('[layerslider id="'.(intval(get_post_meta($post_id , 'layer-slide-id', true))).'"]'); ?>
        	</div>
        <?php elseif(intval(get_post_meta($post_id , 'slide-type', true)) == 2 && intval(get_post_meta($post_id , 'rev-slide-id', true)) != 0) : ?>
        	<div id="revslide-container" class="page-slider-container">
            	<?php echo do_shortcode('[rev_slider '.(intval(get_post_meta($post_id , 'rev-slide-id', true))).']'); ?>
        	</div>
        <?php endif; ?>
    <?php endif;  ?>
    
    <div class="page-header-wrap">

    <?php // show header title
		
		$current_tax = get_query_var('taxonomy');
		
		if((is_tax() && taxonomy_exists('product_cat') && $current_tax == "product_cat") || is_singular('product') || (class_exists( 'woocommerce' ) && is_shop() && is_page())) {
			echo '';
		} elseif ( ( ( (is_home() && !is_front_page()) || is_page() || is_single()) && alterna_get_currect_value(alterna_get_post_meta_key('title-show', $post_id))) || (is_home() && is_front_page()) ) { ?>
		<div id="page-header">
        	<div class="top-shadow"></div>
        	<div class="container">
        	<div class="page-header-content row-fluid">
            	<div class="title span12">
        			<?php 
						$title = get_post_meta($post_id, 'title-content', true);
						$breadcrumb =get_post_meta($post_id, 'title-breadcrumb', true); 
						if($title == '') $title = '<h2>'.get_the_title($post_id).'</h2>';
						echo $title; 
					?>
                </div>
            </div>
            </div>
        </div>
        <?php if(alterna_get_currect_value($breadcrumb)){ ?>
        <div id="page-breadcrumb">
        	<div class="container">
            <?php if ( alterna_get_options_key('global-breakcrumbs-enable') == 'on' &&  function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p class="yoast_breadcrumbs">','</p>');
			}else{ ?>
            <ul><?php echo alterna_page_links(); ?></ul>
            <?php } ?>
            </div>
        </div>
        <?php } ?>
    <?php } elseif ( (is_tax() && taxonomy_exists('portfolio_categories') && $current_tax == "portfolio_categories") || is_category() || is_tag() || is_404() || is_search() || is_date() || is_author()) {?>
		<div id="page-header">
        	<div class="top-shadow"></div>
        	<div  class="container">
        	<div class="page-header-content row-fluid">
            	<div class="title span12">
        			<h2><?php echo alterna_page_title();?></h2>
                </div>
            </div>
            </div>
        </div>
       	<div id="page-breadcrumb">
        	<div class="container">
             <?php if ( alterna_get_options_key('global-breakcrumbs-enable') == 'on' &&  function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p class="yoast_breadcrumbs">','</p>');
			}else{ ?>
            <ul>
            <?php echo alterna_page_links(); ?>
            <?php if(is_search()) : ?>
                <li><i class="icon-chevron-right"></i><span><?php printf( __( 'Search Results for "%s"', 'alterna' ), get_search_query() ); ?></span></li>
            <?php endif; ?>
            </ul>
            <?php } ?>
        	</div>
        </div>
    <?php } ?>
	
    </div><!-- end page-header-wrap -->
    
    <div class="content-wrap">