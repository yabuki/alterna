<?php
/**
 * Template Name: Blog Template with Custom Category
 *
 * @since alterna 2.0
 */

get_header(); ?>
	
   <?php
		global $paged, $blog_show_type;
		$layout = alterna_get_page_layout(); // get page layout 
		$per_page_num 	=	get_post_meta(get_the_ID() , 'blog-ajax-page-num', true);
		$page_cats	=	get_post_meta(get_the_ID() , 'blog-ajax-cat', true);
		$sidebar_name = get_post_meta(get_the_ID() , 'sidebar-type', true);
		$blog_show_type = get_post_meta(get_the_ID() , 'blog-show-type', true);
	?>
    
	<div id="main" class="container">
    	<div class="row-fluid">
            <?php if($layout == 2) : ?> 
            	<div class="span4"><?php generated_dynamic_sidebar($sidebar_name); ?></div>
            <?php endif; ?>
            
        	<div class="left-side <?php echo $layout == 1 ? 'span12' : 'span8'; ?>">
            
            	<?php 

                        if($paged == 0) $paged = 1;
                        
                        $args = array(	'post_type' => 'post',
                                        'post_status' => 'publish',
                                        'paged' => $paged,
                                        'posts_per_page'=> $per_page_num
                                     );
						if($page_cats != "") {
							
							$cats = explode("," , $page_cats);
							
							if(count($cats) > 0) {
								$args['category__in'] = $cats;
							}
						}
                        // The Query
						query_posts($args);
				?>
                
				<?php if ( have_posts() ) : ?>
					
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() );?>
    
					<?php endwhile; ?>

					<?php alterna_content_pagination('nav-bottom' , 'pagination-centered'); ?>
                   
                 	<?php wp_reset_postdata(); ?>
				<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'alterna' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'alterna' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #left-side -->
             <?php if($layout == 3) : ?> 
            	<div class="span4"><?php generated_dynamic_sidebar($sidebar_name); ?></div>
            <?php endif; ?>
		</div><!-- #row-fluid -->
    </div><!-- #container -->
        
<?php get_footer(); ?>