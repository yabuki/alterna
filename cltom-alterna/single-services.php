<?php
/**
 * Single Service 
 * @since alterna 1.0
 */

get_header(); ?>

	<div id="main" class="container">
    
    	<div id="single-service" class="row-fluid">
        
        		<?php
                if ( have_posts() ) : while ( have_posts() ) : the_post(); 
                ?>
            	<div class="span8">
                	<div class="single-service-element row-fluid" >

                        	<?php if(has_post_thumbnail(get_the_ID())) : ?>
								<?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'portfolio-single-thumbs'); ?>
                                <?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
                                <a href="<?php echo $full_image[0]; ?>" class="fancyBox">
                                <div class="post-img">
                                    <img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo get_the_title(); ?>" />
                                    <div class="post-tip"><div class="bg"></div><div class="link no-bg"><i class="big-icon-preview"></i></div></div>
                                </div>
                                </a>
						<?php endif; ?>
                    	<?php the_content(); ?>			
                     	 <?php edit_post_link(__('Edit', 'alterna'), '<div class="post-edit"><i class="icon-edit"></i>', '</div>'); ?>
                    <?php
                    	$tags = get_the_tags(); 
						if($tags && count($tags) > 0) {
					?>
                    <!-- show tags -->
                    <div class="post-meta post-tags row-fluid">
                    	<div class="post-tags-icon"><i class="icon-tags icon-white"></i></div>
                        <span><?php _e('Tagged: ', 'alterna' ); ?><?php the_tags('',' , ','');?></span>
                    </div>
                    <?php } ?>
						 
                	</div>
        		</div>
                <div class="single-portfolio-information span4">
                    <?php echo do_shortcode('[title text="'.get_the_title().'"]'); ?>
                   
                    <ul class="single-portfolio-meta row-fluid">
                        <li>
                            <div class="type"><i class="icon-calendar"></i><?php _e('Date','alterna'); ?></div>
                            <div class="value"><?php echo get_the_date(); ?></div>
                        </li>
                         <li>
                            <div class="type"><i class="icon-folder-open"></i><?php _e('Categories','alterna'); ?></div>
                            <div class="value"><?php wp_list_categories('taxonomy=service_categories&title_li=&style=none'); ?> </div>
                        </li>
						<?php if(get_post_meta( $post->ID, 'plugin_name', true )): ?>
                        <li>
                            <div class="type"><i class="icon-user"></i>&nbsp;<?php _e('Plugin Name','alterna'); ?></div>
                            <div class="value"><a href="<?php the_field('plugin_url'); ?>"><?php the_field('plugin_name'); ?></a></div>
                        </li>
						<?php endif; ?>

						<?php if(get_field( $post->ID, 'plugin_price', true )): ?>
                        <li>
                            <div class="type"><i class="icon-user"></i>&nbsp;<?php _e('Plugin Price','alterna'); ?></div>
                            <div class="value"><?php the_field('plugin_price'); ?></div>
                        </li>
						<?php endif; ?>

                        <!-- show share -->
                   		<?php if(intval(alterna_get_options_key('portfolio-share-type')) != 0) : ?>
                        <li>
                        	<div class="single-portfolio-share">
                            	<?php if(alterna_get_options_key('portfolio-share-type') == "1") : ?>
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_pinterest_pinit"></a>
                                <a class="addthis_counter addthis_pill_style"></a>
                                </div>
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5102b19361611ae7"></script>
                                <!-- AddThis Button END -->
                                <?php else : ?>
                                <!-- Custom share plugin code -->
                           		<?php echo  alterna_get_options_key('portfolio-share-code'); ?>
                                <?php endif; ?>
                            </div>  
                        </li>
                        <?php endif; ?>
                        
					</ul>
					<?php echo do_shortcode('[space line="yes"]'); ?>

                    
                </div>
                
                <?php endwhile; else: ?>
                    <p><?php _e('Sorry, this page does not exist.' , 'alterna' ); ?></p>
                <?php endif; ?>
        </div>
	</div>

<?php get_footer(); ?>