<?php
/**
 * Template Name: Sitemap Template
 *
 * @since alterna 2.3
 */

get_header(); 

		// get page layout 
		$layout = alterna_get_page_layout();           

?>

    <div id="main" class="container">
    	<div class="row-fluid">
        	<?php if($layout == 2) : ?> 
            	<div class="span4"><?php generated_dynamic_sidebar(); ?></div>
            <?php endif; ?>
            
        	<div class="<?php echo $layout == 1 ? 'span12' : 'span8'; ?>">
            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="row-fluid">
                          <?php the_content(); ?>
                    </div>
                <?php endwhile;endif; ?>
				<div class="row-fluid">
                	<div class="span4">
                    	<div class="alterna-title sitemap-title row-fluid"><h3><?php _e('Pages','alterna'); ?><i class=" icon-double-angle-down"></i></h3><div class="full-line"></div></div>
						<ul class="sitemap-ul">
						<?php wp_list_pages('title_li='); ?>
                        </ul>
                    </div>
                    
                    <div class="span4">
                    	<div class="alterna-title sitemap-title row-fluid"><h3><?php _e('Posts','alterna'); ?><i class=" icon-double-angle-down"></i></h3><div class="full-line"></div></div>
                        <ul class="sitemap-ul">
						<?php
							$cats = get_categories();
							foreach ($cats as $cat) {
							  echo '<li class="sitemap-cat"><a href="'.get_category_link($cat->cat_ID).'">'.$cat->cat_name.'</a>';
							  echo "<ul>";
							  query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
							  while(have_posts()) {
								the_post();
								  echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
							  }
							  echo "</ul>";
							  echo "</li>";
							}
							wp_reset_postdata();
                        ?>
                        </ul>
                    </div>
                    
                    <div class="span4">
                    	<div class="alterna-title sitemap-title row-fluid"><h3><?php _e('Portfolios','alterna'); ?><i class=" icon-double-angle-down"></i></h3><div class="full-line"></div></div>
                        <ul class="sitemap-ul">
						<?php
                        $cats = alterna_get_custom_all_categories('portfolio_categories');
                        foreach ($cats as $cat) {
                          echo '<li class="sitemap-cat"><a href="'.get_term_link($cat->slug, 'portfolio_categories' ).'">'.$cat->name.'</a>';
                          echo "<ul>";
                          
                          $args=array(
                                  'post_type' => 'portfolio',
                                  'post_status' => 'publish',
                                  'posts_per_page' => '-1',
                                  'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'portfolio_categories',
                                                    'field' => 'id',
                                                    'terms' => $cat->term_id
                                                )
                                        )
                                  );
                          
                          query_posts($args);
                          while(have_posts()) {
                            the_post();
                            $category = alterna_get_custom_post_categories(get_the_ID(), "portfolio_categories");
                            echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
                          }
                          echo "</ul>";
                          echo "</li>";
                        }
						wp_reset_postdata();
                        ?>
                        </ul>
                        
                    </div>
                </div>
            </div>
            <?php if($layout == 3) : ?> 
            	<div class="span4"><?php generated_dynamic_sidebar(); ?></div>
            <?php endif; ?>
            
        </div><!-- end row-fluid -->
    </div><!-- end container -->
    
<?php get_footer(); ?>