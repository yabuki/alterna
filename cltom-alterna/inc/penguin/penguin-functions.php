<?php

/**
	Penguin Framework

	Copyright (c) 2009-2014 ThemeFocus

	@url http://penguin.themefocus.co
	@package Penguin
	@version 4.0
**/


//========================================================
//  	PLUGIN FUNCTION METHODS
//========================================================

/**
 * Get option  for layerslider
 */
function penguin_get_layerslider(){
	$layerslider_slides = array();
	$layerslider_slides[0] = __('Select a slider',Penguin::$THEME_NAME);
	
	 // Get WPDB Object
    global $wpdb;
 
    // Table name
    $table_name = $wpdb->prefix . "layerslider";

	
	$sql = "show tables like '$table_name'";
	
	$table = $wpdb->get_var($sql);

	// have no rev slider 
	if($table != $table_name) return $layerslider_slides;
 
    // Get sliders
    $sliders = $wpdb->get_results( "SELECT * FROM $table_name
                                        WHERE flag_hidden = '0' AND flag_deleted = '0'
                                        ORDER BY date_c ASC LIMIT 100" );
 
    // Iterate over the sliders
    foreach($sliders as $key => $item) {
 		$layerslider_slides[$item->id] = '#'.$item->id . ' - ' .$item->name;
    }
	
	return $layerslider_slides;
}

/**
 * Get option  for revslider
 */
function penguin_get_revslider(){
	$revslider_slides = array();
	$revslider_slides[0] = __('Select a slider',Penguin::$THEME_NAME);
	
	 // Get WPDB Object
    global $wpdb;

    // Table name
    $table_name = $wpdb->prefix . "revslider_sliders";
	
	$sql = "show tables like '$table_name'";
	
	$table = $wpdb->get_var($sql);

	// have no rev slider 
	if($table != $table_name) return $revslider_slides;
	
    // Get sliders
    $sliders = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY id LIMIT 100" );
 
    // Iterate over the sliders
    foreach($sliders as $key => $item) {
 		$revslider_slides[$item->id] = '#'.$item->id . ' - ' .$item->title;
    }
	
	return $revslider_slides;
}

/**
 * Get option  for wpml
 */
function penguin_get_wpml_switcher(){
	if (function_exists( 'icl_get_languages' ) && function_exists('icl_disp_language')) {
		$languages = icl_get_languages('skip_missing=1');
		
		if(!empty($languages)){
			
			$lang_active = '';
			$lang_list = '<ul class="sub-menu">';
			
			foreach($languages as $l){

				if($l['active']){ $lang_active = '<a href="#"><i class="icon-globe"></i>'.$l['native_name'].'</a>';				}
				$lang_list .= '<li>';

				if(!$l['active']){
					$lang_list .= '<a href="'.$l['url'].'">';
				}else{
					$lang_list .=  '<span>';
				}
				
				if($l['country_flag_url']){
					$lang_list .= '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';				
				}
				$lang_list .=  $l['native_name'];
			
				if(!$l['active']){
					$lang_list .= '</a>';
				}else{
					$lang_list .=  '</span>';
				}
				$lang_list .= '</li>';
			}
			$lang_list .= '</ul>';
			
			return '<li class="wpml">'.$lang_active.$lang_list.'</li>';
		}
	}
	return '';
}