
<?php
wp_enqueue_script('jquery');
global $wp_scripts;

$effect_list = array("none","bounce","flash","pulse","shake","swing","tada","wobble","bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flip","flipInX","flipInY","flipOutX","flipOutY","lightSpeedIn","lightSpeedOut","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideInDown","slideInLeft","slideInRight","slideOutLeft","slideOutRight","slideOutUp","hinge","rollIn","rollOut");

//input
function penguin_shortcode_input($name = '', $key = '', $desc = '', $default = ''){
	return '<div class="penguin-table-tr" data-type="input" data-key="'.$key.'">
				<div class="penguin-table-title">'.$name.'<div class="penguin-page-content-desc">'.$desc.'</div></div>
				<div class="penguin-table-content"><input class="penguin-input-text" value="'.$default.'" type="text"></div>
			</div>';
}

//number
function penguin_shortcode_number($name = '', $key = '', $desc = '', $default = ''){
	return '<div class="penguin-table-tr" data-type="input" data-key="'.$key.'">
				<div class="penguin-table-title">'.$name.'<div class="penguin-page-content-desc">'.$desc.'</div></div>
				<div class="penguin-table-content"><input class="penguin-input-text" value="'.$default.'" type="number"></div>
			</div>';
}

//select
function penguin_shortcode_select($name = '', $key = '', $desc = '', $options = array(), $default = ''){
	$output =  '<div class="penguin-table-tr" data-type="select" data-key="'.$key.'">
				<div class="penguin-table-title">'.$name.'<div class="penguin-page-content-desc">'.$desc.'</div></div>
				<div class="penguin-table-content"><select class="penguin-select">';
				foreach($options as $option){
					$output .=  '<option '.($default == $option ? 'selected' : '' ).' value='.$option.'>'.$option.'</option>';
				}
	$output .= '</select></div></div>';
	return $output;
}

//color
function penguin_shortcode_color($name = '', $key = '', $desc = '', $default = ''){
	if($default == ""){
		$default = '#f5f5f5';
	}
	$output =  '<div class="penguin-table-tr" data-type="color" data-key="'.$key.'">
				<div class="penguin-table-title">'.$name.'<div class="penguin-page-content-desc">'.$desc.'</div></div>
				<div class="penguin-table-content"><input class="color penguin-color-picker" value="'.$default.'"></div></div>';
	return $output;
}

//textarea
function penguin_shortcode_textarea($name = '', $key = '', $desc = '', $default = ''){
	$output =  '<div class="penguin-table-tr" data-type="textarea" data-key="'.$key.'">
				<div class="penguin-table-title">'.$name.'<div class="penguin-page-content-desc">'.$desc.'</div></div>
				<div class="penguin-table-content"><textarea class="penguin-textarea">'.$default.'</textarea></div></div>';
	return $output;
}


?>
<!DOCTYPE html>
	<head>
        <title>Alterna Theme Shortcodes</title>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php echo get_option('blog_charset'); ?>" />
        <link rel='stylesheet' id='bootstrap-css'  href='<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='fontawesome-css'  href='<?php echo get_template_directory_uri(); ?>/fontawesome/css/font-awesome.min.css' type='text/css' media='all' />
        <link rel='stylesheet' id='penguinshortcodes-css'  href='<?php echo get_template_directory_uri() . '/inc/penguin/tinymce/penguinshortcodes_tinymce.css'; ?>' type='text/css' media='all' />
       	<script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>/wp-includes/js/jquery/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>/wp-includes/js/tinymce/utils/form_utils.js"></script>
        <script language="javascript" type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/bootstrap/js/bootstrap.min.js'></script>
        <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() . '/inc/penguin/tinymce/jscolor.js'; ?>"></script>
        <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() . '/inc/penguin/tinymce/penguinshortcodes_tinymce.js'; ?>"></script>
        <base target="_self" />
    </head>
    <body id="link">
        <section id="penguin-shortcode-contents">
            <div class="penguin-shortcode-header">
				<div class="penguin-shortcode-title"><?php _e("Current Element:", 'alterna'); ?></div>
                <div class="penguin-shortcode-select">
                	<select id="shortcode-select">
                    	<optgroup label="Complex" >
                        <option value="0"><?php _e("Choose Insert Shortcode", 'alterna'); ?></option>
                        <option value="101">New Title</option>
                        <option value="102">Title</option>
                        <option value="103">Space</option>
                        <option value="104">Button</option>
                        <option value="105">Blockquote</option>
                        <option value="106">Dropcap</option>
                        <option value="107">Icon</option>
                        <option value="108">Bullets</option>
                        <option value="109">Alert Message</option>
                        <option value="110">Accordion</option>
                        <option value="111">Toggle</option>
                        <option value="112">Tabs</option>
                        <option value="113">SideTabs</option>
                        <option value="114">Features</option>
                        <option value="115">Services</option>
                        <option value="116">New Services</option>
                        <option value="117">History</option>
                        <option value="118">Team</option>
                        <option value="119">Call to action</option>
                        <option value="120">Call to action bar</option>
                        <option value="121">Price Table</option>
                        <option value="122">Price Slider</option>
                        <option value="123">Testimonials</option>
                        <option value="124">Map</option>
                        <option value="125">FlexSlider</option>
                        <option value="126">Carousel</option>
                        <option value="127">Social</option>
                        <option value="128">Skills</option>
                        <option value="129">Clients</option>
                        </optgroup>
                        
                        <optgroup label="Media" >
                        <option value="201">Youtube</option>
                        <option value="202">Vimeo</option>
                        <option value="203">Soundcloud</option>
                        </optgroup>
                        
                        <optgroup label="Module" >
                        <option value="301">Big Blog List</option>
                        <option value="302">Blog List</option>
                        <option value="303">Portflio List</option>
                        </optgroup>
                        
                        <optgroup label="Columns & Content" >
                        <option value="401">Wide Background</option>
                        <option value="402">One</option>
                        <option value="403">Inner One</option>
                        <option value="404">One Half</option>
                        <option value="405">One Third</option>
                        <option value="406">Two Third</option>
                        <option value="407">One Fourth</option>
                        <option value="408">Two Fourth</option>
                        <option value="409">Three Fourth</option>
                        </optgroup>
                    </select>
            	</div>
            </div>
            
            <div class="shortcodes-container">
  
                <div id="shortcodes-element-101" data-shortcode="newtitle" class="shortcodes-element">
                	<?php 
					echo penguin_shortcode_input(__('Class name','alterna'),'class',__('Custom title class name.(Options)','alterna'));
                    echo penguin_shortcode_select(__('Type','alterna'),'type','', array('text','icon'));
                    echo penguin_shortcode_select(__('Align','alterna'),'align','', array('left','center','right'));
                	echo penguin_shortcode_input(__('Text','alterna'),'value',__('Title text value.','alterna'),'Title');
                    echo penguin_shortcode_select(__('Text size','alterna'),'size','', array('h1','h2','h3','h4','h5','h6'),'h3');
                    echo penguin_shortcode_select(__('Text uppercase','alterna'),'uppercase','', array('yes','no'));
                    echo penguin_shortcode_select(__('Text bold','alterna'),'bold','', array('yes','no'));
                    echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-flag (Options)','alterna'),'icon-flag');
                    echo penguin_shortcode_select(__('Icon size','alterna'),'icon_size','', array('normal','big' , 'min'));
                	echo penguin_shortcode_select(__('Show text background','alterna'),'show_bg','', array('yes','no'));
                	echo penguin_shortcode_select(__('Show line','alterna'),'line','', array('yes','no'));
                    echo penguin_shortcode_select(__('Line align','alterna'),'line_align','', array('center','top','bottom'));
                    echo penguin_shortcode_color(__('Text or Icon color','alterna'),'color','','#666666');
                    echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#ffffff');
                    echo penguin_shortcode_color(__('Line color','alterna'),'line_color','','#e8e8e8');
                    echo penguin_shortcode_color(__('Icon type text color ','alterna'),'extra_color','','#666666');
                    echo penguin_shortcode_input(__('Style','alterna'),'style',__("Icon or Text Style CSS",'alterna'));
					?>
                </div>
                
                <div id="shortcodes-element-102" data-shortcode="title" class="shortcodes-element">
                	<?php 
                	echo penguin_shortcode_input(__('Text','alterna'),'text',__('Title text value.','alterna'),'Title');
                    echo penguin_shortcode_select(__('Text size','alterna'),'size','', array('h1','h2','h3','h4','h5','h6'),'h3');
                    echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-flag (Options)','alterna'));
                    
                	echo penguin_shortcode_select(__('Show line','alterna'),'line','', array('yes','no'));
					echo penguin_shortcode_select(__('Align','alterna'),'align','', array('left','center','right'));
					?>
				</div>

				<div id="shortcodes-element-103" data-shortcode="space" class="shortcodes-element">
                	<?php 
					echo penguin_shortcode_select(__('Show line','alterna'),'line','', array('yes','no'));
                	echo penguin_shortcode_select(__('Space size','alterna'),'size','', array('normal','small','big'));
                    echo penguin_shortcode_select(__('Line style','alterna'),'style','', array('solid','dashed')); 
					?>
                </div>     
                
                <div id="shortcodes-element-104" data-shortcode="button" class="shortcodes-element">
                	<?php 
					echo penguin_shortcode_input(__('Text','alterna'),'text');
					echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-flag (Options)','alterna'));
                    echo penguin_shortcode_select(__('Type','alterna'),'type','', array('btn-primary' , 'btn-info' , 'btn-success' , 'btn-warning' , 'btn-danger' , 'btn-inverse' , 'btn-link' , 'btn-custom'));
					
					echo penguin_shortcode_select(__('Size','alterna'),'size','', array('default', 'btn-large' , 'btn-small' , 'btn-mini'));
                    echo penguin_shortcode_input(__('Link','alterna'),'url','','#');
                    echo penguin_shortcode_select(__('Link target','alterna'),'target','', array('_self', '_blank'),'_blank');
					
                    echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#cc3333');
                    echo penguin_shortcode_color(__('Background hover color','alterna'),'bg_hover_color','','#242424');
					echo penguin_shortcode_color(__('Font color','alterna'),'txt_color','','#ffffff');
                    echo penguin_shortcode_color(__('Font hover color','alterna'),'txt_hover_color','','#ffffff');
					?>
                </div>  
                
                <div id="shortcodes-element-105" data-shortcode="blockquote" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_color(__('Border color','alterna'),'border_color','','#eeeeee');
					echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#ffffff');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>  

                <div id="shortcodes-element-106" data-shortcode="dropcap" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Text','alterna'),'text');
					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('default', 'text') );
					echo penguin_shortcode_color(__('Text color','alterna'),'txt_color','','#ffffff');
					echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#242424');
					?>
                </div>   
                
                <div id="shortcodes-element-107" data-shortcode="icon" class="shortcodes-element">
                	<?php 
					echo penguin_shortcode_input(__('Icon name','alterna'),'icon_name',__('FontAwesome icon name e.g. icon-flag','alterna'),'icon-flag');
                	echo penguin_shortcode_select(__('Icon size','alterna'),'icon_size','', array('default', 'icon-large' , 'icon-2x', 'icon-3x', 'icon-4x' , 'icon-5x'));
                    echo penguin_shortcode_color(__('Icon color','alterna'),'icon_color',__('Custom icon color.','alterna'),'#cc3333');
                    echo penguin_shortcode_input(__('Icon style','alterna'),'icon_style',__('Custom icon style.(Options) pull-left , pull-right , icon-spin , icon-muted , icon-border','alterna'));
					?>
                </div>

               <div id="shortcodes-element-108" data-shortcode="bullets" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('ok', 'error', 'custom') );
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_color(__('Bullets color','alterna'),'color','','#000000');
					echo penguin_shortcode_color(__('Text color','alterna'),'txt_color','','#333333');
					?>
                    <div data-shortcode="bullet" class="shortcodes-child">
                    	<h3><?php _e('Bullet Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_select(__('Custom icon','alterna'),'custom','', array('yes', 'no'),'no' );
							echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-flag','alterna'));
							echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
							?>
						</div>
                    </div>
                </div>
    
                <div id="shortcodes-element-109" data-shortcode="alert" class="shortcodes-element">
                	<?php 
                    echo penguin_shortcode_select(__('Alert Type','alterna'),'type','', array('success' , 'danger' , 'error' , 'info'));
					echo penguin_shortcode_select(__('Alert Message Close','alterna'),'close','', array('yes', 'no'));
					echo penguin_shortcode_input(__('Alert Title','alterna'),'title');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>       
               
				<div id="shortcodes-element-110" data-shortcode="accordion" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                    <div data-shortcode="accordion_item" class="shortcodes-child">
                    	<h3><?php _e('Accordion Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_input(__('Title','alterna'),'title');
							echo penguin_shortcode_select(__('Open','alterna'),'open','', array('yes', 'no'),'no');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
							?>
						</div>
                    </div>
                </div>

                <div id="shortcodes-element-111" data-shortcode="toggle" class="shortcodes-element">
                	<?php
		  			echo penguin_shortcode_input(__('Title','alterna'),'title');
					echo penguin_shortcode_select(__('Open','alterna'),'open','', array('yes', 'no'),'no');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>
                
                <div id="shortcodes-element-112" data-shortcode="tabs" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                    <div data-shortcode="tabs_item" class="shortcodes-child">
                    	<h3><?php _e('Tab Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
		  					echo penguin_shortcode_input(__('Title','alterna'),'title');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                </div>
                
               <div id="shortcodes-element-113" data-shortcode="sidetabs" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                    <div data-shortcode="sidetabs_item" class="shortcodes-child">
                    	<h3><?php _e('Sidetabs Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
		  					echo penguin_shortcode_input(__('Title','alterna'),'title');
							echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-flag (Options)','alterna'));
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                </div>

                <div id="shortcodes-element-114" data-shortcode="features" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Title','alterna'),'title');
					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('icon', 'img'));
					echo penguin_shortcode_input(__('Icon name Or Image src','alterna'),'src',__('FontAwesome icon name e.g. icon-flag','alterna'));
					echo penguin_shortcode_select(__('Show Background','alterna'),'show_bg','', array('yes', 'no'));
					echo penguin_shortcode_select(__('Border','alterna'),'border','', array('yes', 'no'), 'no');
					echo penguin_shortcode_input(__('Link','alterna'),'link','','');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>
                
                <div id="shortcodes-element-115" data-shortcode="services" class="shortcodes-element">
                	<?php
					
					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('icon', 'img'));
					echo penguin_shortcode_input(__('Icon name Or Image src','alterna'),'src',__('FontAwesome icon name e.g. icon-flag','alterna'));
					echo penguin_shortcode_select(__('Show Background','alterna'),'show_bg','', array('yes', 'no'));
					echo penguin_shortcode_select(__('Border','alterna'),'border','', array('yes', 'no'), 'no');
					echo penguin_shortcode_input(__('Link','alterna'),'link','','');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>

                <div id="shortcodes-element-116" data-shortcode="newservices" class="shortcodes-element">
                	<?php
					
					echo penguin_shortcode_select(__('Layout','alterna'),'layout','', array('left', 'center'));
					echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-flag','alterna'));
					echo penguin_shortcode_input(__('Title','alterna'),'title','','');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>
                
                <div id="shortcodes-element-117" data-shortcode="history" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Title','alterna'),'title');
					echo penguin_shortcode_input(__('Date','alterna'),'day');
					echo penguin_shortcode_input(__('Image Src','alterna'),'src',__('Options','alterna'));
					echo penguin_shortcode_select(__('Start Point','alterna'),'start','', array('yes','no'),'no');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-118" data-shortcode="team" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Name','alterna'),'name');
					echo penguin_shortcode_input(__('Job','alterna'),'job');
					echo penguin_shortcode_input(__('Image url','alterna'),'src');
					echo penguin_shortcode_input(__('Link','alterna'),'link');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                    <div data-shortcode="team_social" class="shortcodes-child">
                    	<h3><?php _e('Socials','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_input(__('Icon name','alterna'),'icon',__('FontAwesome icon name e.g. icon-twitter','alterna'));
							echo penguin_shortcode_input(__('Link','alterna'),'link');
							echo penguin_shortcode_select(__('Link target','alterna'),'target','', array('_self', '_blank'),'_blank');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                    <div data-shortcode="team_content" class="shortcodes-child">
                    	<h3><?php _e('Person Details','alterna'); ?></h3>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-119" data-shortcode="call_to_action" class="shortcodes-element">
                	<?php
		  			echo penguin_shortcode_input(__('Title','alterna'),'title');
					echo penguin_shortcode_select(__('Size','alterna'),'size','', array('big', 'small'));
					echo penguin_shortcode_input(__('Button Title','alterna'),'btn_title');
					echo penguin_shortcode_select(__('Button Type','alterna'),'btn_type','', array('btn-primary' , 'btn-info' , 'btn-success' , 'btn-warning' , 'btn-danger' , 'btn-inverse' , 'btn-link' , 'btn-custom'));
					echo penguin_shortcode_select(__('Button Size','alterna'),'btn_size','', array('default', 'btn-large' , 'btn-small' , 'btn-mini'));
					echo penguin_shortcode_input(__('Link','alterna'),'url','','#');
					echo penguin_shortcode_select(__('Link target','alterna'),'target','', array('_self', '_blank'),'_blank');
					echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#cc3333');
                    echo penguin_shortcode_color(__('Background hover color','alterna'),'bg_hover_color','','#242424');
					echo penguin_shortcode_color(__('Font color','alterna'),'txt_color','','#ffffff');
                    echo penguin_shortcode_color(__('Font hover color','alterna'),'txt_hover_color','','#ffffff');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>
                
                <div id="shortcodes-element-120" data-shortcode="call_to_action_bar" class="shortcodes-element">
                	<?php
		  			echo penguin_shortcode_input(__('Title','alterna'),'title');
					echo penguin_shortcode_select(__('Size','alterna'),'size','', array('big', 'small'));
					echo penguin_shortcode_input(__('Button Title','alterna'),'btn_title');
					echo penguin_shortcode_select(__('Button Type','alterna'),'btn_type','', array('btn-primary' , 'btn-info' , 'btn-success' , 'btn-warning' , 'btn-danger' , 'btn-inverse' , 'btn-link' , 'btn-custom'));
					echo penguin_shortcode_select(__('Button Size','alterna'),'btn_size','', array('default', 'btn-large' , 'btn-small' , 'btn-mini'));
					echo penguin_shortcode_input(__('Link','alterna'),'url','','#');
					echo penguin_shortcode_select(__('Link target','alterna'),'target','', array('_self', '_blank'),'_blank');
					echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#cc3333');
                    echo penguin_shortcode_color(__('Background hover color','alterna'),'bg_hover_color','','#242424');
					echo penguin_shortcode_color(__('Font color','alterna'),'txt_color','','#ffffff');
                    echo penguin_shortcode_color(__('Font hover color','alterna'),'txt_hover_color','','#ffffff');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
					?>
                </div>
                
                <div id="shortcodes-element-121" data-shortcode="price" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('standard','free','recommend','custom'));
					echo penguin_shortcode_input(__('Title','alterna'),'title');
					echo penguin_shortcode_input(__('Price','alterna'),'price');
					echo penguin_shortcode_input(__('Plan','alterna'),'plan');
					echo penguin_shortcode_color(__('Background color','alterna'),'background_color','','#787878');
                    echo penguin_shortcode_color(__('Border color','alterna'),'border_color','','#646464');
					echo penguin_shortcode_color(__('Content background color','alterna'),'content_bg_color','','#F4F4F4');
					echo penguin_shortcode_select(__('Content align','alterna'),'content_align','', array('left', 'center','right'),'center');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                    <div data-shortcode="price_item" class="shortcodes-child">
                    	<h3><?php _e('Price Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
		  					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('text','btn'));
		  					echo penguin_shortcode_input(__('Button text','alterna'),'btn_text');
							echo penguin_shortcode_select(__('Button Size','alterna'),'btn_size','', array('default', 'btn-large' , 'btn-small' , 'btn-mini'));
							echo penguin_shortcode_input(__('Link','alterna'),'btn_url','','#');
							echo penguin_shortcode_select(__('Link target','alterna'),'btn_target','', array('_self', '_blank'),'_blank');
							echo penguin_shortcode_color(__('Background color','alterna'),'btn_bg_color','','#cc3333');
							echo penguin_shortcode_color(__('Background hover color','alterna'),'btn_bg_hover_color','','#242424');
							echo penguin_shortcode_color(__('Font color','alterna'),'btn_txt_color','','#ffffff');
							echo penguin_shortcode_color(__('Font hover color','alterna'),'btn_txt_hover_color','','#ffffff');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-122" data-shortcode="priceslider" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Height','alterna'),'height','','200');
                    echo penguin_shortcode_color(__('Nav button color','alterna'),'nav_color','','#646464');
					?>
                    <div data-shortcode="priceslider_item" class="shortcodes-child">
                    	<h3><?php _e('Price Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
		  					echo penguin_shortcode_input(__('Image url','alterna'),'img');
							echo penguin_shortcode_input(__('Plan','alterna'),'plan','',esc_html('<i class="icon-truck"></i> Regular License'));
							echo penguin_shortcode_input(__('Currency','alterna'),'currency','','$');
							echo penguin_shortcode_color(__('Color','alterna'),'color','','#7AB80E');
		  					echo penguin_shortcode_input(__('Button Text','alterna'),'btn_text');
							echo penguin_shortcode_select(__('Button Size','alterna'),'btn_size','', array('default', 'btn-large' , 'btn-small' , 'btn-mini'));
							echo penguin_shortcode_input(__('Link','alterna'),'btn_url','','#');
							echo penguin_shortcode_select(__('Link target','alterna'),'btn_target','', array('_self', '_blank'),'_blank');
							echo penguin_shortcode_color(__('Background color','alterna'),'btn_bg_color','','#cc3333');
							echo penguin_shortcode_color(__('Background hover color','alterna'),'btn_bg_hover_color','','#242424');
							echo penguin_shortcode_color(__('Font color','alterna'),'btn_txt_color','','#ffffff');
							echo penguin_shortcode_color(__('Font hover color','alterna'),'btn_txt_hover_color','','#ffffff');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-123" data-shortcode="testimonials" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('text', 'wide','avatar'));
					echo penguin_shortcode_select(__('Auto Play','alterna'),'autoplay','', array('yes', 'no'),'no');
					echo penguin_shortcode_number(__('Delay Time','alterna'),'delay','','6000');
					echo penguin_shortcode_select(__('Show Nav Button','alterna'),'show_nav','', array('yes', 'no'),'no');
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                    <div data-shortcode="testimonials_item" class="shortcodes-child">
                    	<h3><?php _e('Testimonials Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_input(__('Name','alterna'),'name');
							echo penguin_shortcode_input(__('Job','alterna'),'job');
							echo penguin_shortcode_input(__('Image url','alterna'),'img');
							echo penguin_shortcode_input(__('Link','alterna'),'link');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input content.','alterna')); 
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-124" data-shortcode="map" class="shortcodes-element">
                	<?php 
					echo penguin_shortcode_input(__('LatLng','alterna'),'latlng',__("The map LatLng value from google map.e.g. 36.597889,-86.234436",'alterna'));
					echo penguin_shortcode_input(__('Map width','alterna'),'width','','100%');
					echo penguin_shortcode_input(__('Map height','alterna'),'height','','300');
					echo penguin_shortcode_number(__('Map Zoom','alterna'),'zoom','','13');
                    echo penguin_shortcode_select(__('Draggable','alterna'),'draggable','', array('yes' , 'no'));
					echo penguin_shortcode_select(__('Scroll Wheel','alterna'),'scrollwheel','', array('yes', 'no'));
					echo penguin_shortcode_select(__('Show Marker','alterna'),'show_marker','', array('yes', 'no'));
					echo penguin_shortcode_select(__('Show Address Information','alterna'),'show_info',__("The map show info box of address.",'alterna'), array('yes', 'no'));
					echo penguin_shortcode_number(__('Information Width','alterna'),'info_width',__("The map info address box width.",'alterna'),'260');
					
					echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input address content.','alterna')); 
					?>
                </div>
                
                <div id="shortcodes-element-125" data-shortcode="flexslider" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Auto Play','alterna'),'auto','', array('yes', 'no'),'no');
					echo penguin_shortcode_number(__('Delay Time','alterna'),'delay','','6000');
					?>
                    <div data-shortcode="flexslider_item" class="shortcodes-child">
                    	<h3><?php _e('Flexslider Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
		  					echo penguin_shortcode_select(__('Type','alterna'),'type','', array('image', 'video'));
		  					echo penguin_shortcode_input(__('Image url','alterna'),'src');
							echo penguin_shortcode_input(__('Link','alterna'),'link');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content','',__('Input video content when video type.','alterna')); 
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-126" data-shortcode="carousel" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_select(__('Auto Play','alterna'),'auto','', array('yes', 'no'),'no');
					echo penguin_shortcode_number(__('Delay Time','alterna'),'delay','','6000');
					?>
                    <div data-shortcode="carousel_item" class="shortcodes-child">
                    	<h3><?php _e('Carousel Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
		  					echo penguin_shortcode_input(__('Image url','alterna'),'src');
							echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-127" data-shortcode="social" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#e8e8e8');
					echo penguin_shortcode_select(__('Show Tooltip','alterna'),'tooltip','', array('yes', 'no'),'no');
					echo penguin_shortcode_select(__('Tooltip Placement','alterna'),'tooltip_placement','', array('left','right','top','bottom'),'top');
					?>
					<div data-shortcode="social_item" class="shortcodes-child">
                    	<h3><?php _e('Social Item','alterna'); ?></h3>
                    	<button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
						<div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_select(__('Social type','alterna'),'type','',array('twitter','facebook','google-plus','dribbble','pinterest','flickr','skype','youtube','vimeo','linkedin', 'digg','deviantart','behance','forrst','lastfm','xing','instagram','stumbleupon','picasa','email'));
							echo penguin_shortcode_input(__('Title','alterna'),'title');
							echo penguin_shortcode_input(__('Link','alterna'),'link');
							echo penguin_shortcode_select(__('Link target','alterna'),'target','', array('_self', '_blank'),'_blank');
							?>
						</div>
					</div>
                </div>
                
                
                <div id="shortcodes-element-128" data-shortcode="skills" class="shortcodes-element">
                    <div data-shortcode="skill" class="shortcodes-child">
                    	<h3><?php _e('Skill Item','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_input(__('Title','alterna'),'name',__('Skill Name','alterna'));
							echo penguin_shortcode_input(__('Percent','alterna'),'percent','','50%');
							echo penguin_shortcode_input(__('Custom Text','alterna'),'text', __('Custom text replace percent number (Options)','alterna'));
							echo penguin_shortcode_color(__('Background color','alterna'),'bg_color','','#f8f8f8');
							echo penguin_shortcode_color(__('Title color','alterna'),'color','','#333333');
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-129" data-shortcode="clients" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Columns','alterna'),'col','', '6');
					?>
                    <div data-shortcode="client" class="shortcodes-child">
                    	<h3><?php _e('Client','alterna'); ?></h3>
                        <button class="btn btn-warning shortcodes-child-add"><i class="icon-plus"></i></button>
                        <div class="shortcodes-child-element">
                        	<?php
							echo penguin_shortcode_input(__('Title','alterna'),'title');
							echo penguin_shortcode_input(__('Image url','alterna'),'src');
							echo penguin_shortcode_input(__('Link','alterna'),'link');
							echo penguin_shortcode_select(__('Link target','alterna'),'target','', array('_self', '_blank'),'_self');
							?>
						</div>
                    </div>
                </div>
                
                <div id="shortcodes-element-201" data-shortcode="youtube" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Youtube ID','alterna'),'id',__("Enter video ID (eg.6htyfxPkYDU).",'alterna'));
					echo penguin_shortcode_input(__('Width','alterna'),'width','','100%');
					echo penguin_shortcode_input(__('Height','alterna'),'height','','360');
					?>
                </div>
                
                <div id="shortcodes-element-202" data-shortcode="vimeo" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Vimeo ID','alterna'),'id',__("Enter video ID (eg.54578415).",'alterna'));
					echo penguin_shortcode_input(__('Width','alterna'),'width','','100%');
					echo penguin_shortcode_input(__('Height','alterna'),'height','','360');
					?>
                </div>
                
                <div id="shortcodes-element-203" data-shortcode="soundcloud" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('Soundcloud URL','alterna'),'url',__("Enter soundcloud url like http://api.soundcloud.com/tracks/38987054.",'alterna'));
					echo penguin_shortcode_input(__('Width','alterna'),'width','','100%');
					echo penguin_shortcode_input(__('Height','alterna'),'height','','166');
					?>
                </div>
                
                <div id="shortcodes-element-301" data-shortcode="blog_bigcontent" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_number(__('Number','alterna'),'number','','4');
					echo penguin_shortcode_select(__('Type','alterna'),'type','',array('none','gallery','image','quote','video','audio'),'none');
					echo penguin_shortcode_input(__('Cats','alterna'),'related_slug',__('Show related blog post category id items use ",". (Options)','alterna'));
					echo penguin_shortcode_input(__('Tags','alterna'),'tag__in',__('Show related blog post tag id items use ",". (Options)','alterna'));
					echo penguin_shortcode_input(__('Post not in','alterna'),'post__not_in',__('(Options)','alterna'));
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                </div>
                
                <div id="shortcodes-element-302" data-shortcode="blog_list" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_number(__('Number','alterna'),'number','','4');
					echo penguin_shortcode_select(__('Columns','alterna'),'columns','',array('2','3','4'),'4');
					echo penguin_shortcode_select(__('Style','alterna'),'show_style','',array('0','1'));
					echo penguin_shortcode_select(__('Type','alterna'),'show_type',__('Show item type, 0: recent; 1: related','alterna'),array('0','1'));
					echo penguin_shortcode_input(__('Categories ids','alterna'),'related_slug',__('Show related blog post category id items use ",". (Options)','alterna'));
					echo penguin_shortcode_input(__('Tags ids','alterna'),'tag__in',__('Show post with which tags, here input tag slug name.If more than 1 tag use ",". (Options)','alterna'));
					echo penguin_shortcode_input(__('Post not in','alterna'),'post__not_in',__('ID (Options)','alterna'));
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                </div>
                
                <div id="shortcodes-element-303" data-shortcode="portfolio_list" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_number(__('Number','alterna'),'number','','4');
					echo penguin_shortcode_select(__('Columns','alterna'),'columns','',array('2','3','4'),'4');
					echo penguin_shortcode_select(__('Style','alterna'),'show_style','',array('0','1'));
					echo penguin_shortcode_select(__('Type','alterna'),'show_type',__('Show item type, 0: recent; 1: related','alterna'),array('0','1'));
					echo penguin_shortcode_input(__('Categories slug','alterna'),'related_slug',__('Show related portfolio category slug items use ",". (Options)','alterna'));
					echo penguin_shortcode_input(__('Post not in','alterna'),'post__not_in',__('ID (Options)','alterna'));
					echo penguin_shortcode_select(__('Effect','alterna'),'effect','', $effect_list, 'none');
					?>
                </div>

                <div id="shortcodes-element-401" data-shortcode="wide" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_input(__('ID','alterna'),'id');
					echo penguin_shortcode_input(__('Class name','alterna'),'class');
					echo penguin_shortcode_input(__('Style','alterna'),'style');
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-402" data-shortcode="one" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-403" data-shortcode="inner_one" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-404" data-shortcode="one_half" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-405" data-shortcode="one_third" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-406" data-shortcode="two_third" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-407" data-shortcode="one_fourth" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-408" data-shortcode="two_fourth" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>
                
                <div id="shortcodes-element-409" data-shortcode="three_fourth" class="shortcodes-element">
                	<?php
					echo penguin_shortcode_textarea(__('Content','alterna'),'content'); 
					?>
                </div>

            </div>
			
            <div>
                <div style="float: left">
                    <button class="btn btn-info" onClick="tinyMCEPopup.close();"><?php _e("Cancel", 'alterna'); ?></button>
                </div>

                <div style="float: right">
                    <button type="submit" class="btn btn-success" onClick="insertpenguinshortcode();"><?php _e("Insert", 'alterna'); ?></button>
                </div>
            </div>
        </section>
    </body>
</html>