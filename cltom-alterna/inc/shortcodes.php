<?php
$effect_list = array("none","bounce","flash","pulse","shake","swing","tada","wobble","bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flip","flipInX","flipInY","flipOutX","flipOutY","lightSpeedIn","lightSpeedOut","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideInDown","slideInLeft","slideInRight","slideOutLeft","slideOutRight","slideOutUp","hinge","rollIn","rollOut");

include_once ('penguin/tinymce/tinymce.php');
require_once ('penguin/tinymce/ajax.php');

//=============================
// Header Alert Message
//=============================
function alterna_headeralert_func($atts, $content = null){
	global $alterna_headeralert_items;
	$alterna_headeralert_item = array();
	do_shortcode($content);
	$count = count($alterna_headeralert_items);
	$output = '';
	if($count > 0){
		foreach($alterna_headeralert_items as $alterna_headeralert_item){
			$output .= $alterna_headeralert_item;
			if($count != 1) $output .= '<div class="header-information-element header-information-line">|</div>';
			$count--;
		}
	}
	
	return $output;
}
add_shortcode('headeralert', 'alterna_headeralert_func');

function alterna_headeralert_item_func($atts, $content = null){
	global $alterna_headeralert_items;
	
	$alterna_headeralert_items[] = '<div class="header-information-element">'.do_shortcode($content).'</div>';
	
	return "";
}
add_shortcode('headeralert_item', 'alterna_headeralert_item_func');

//=============================
// Title
//=============================
function alterna_title_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'text'	=> '',
		  'size' => 'h3',
		  'line' => 'true',
		  'icon' => '',
		  'align'	=> ''
		  ), $atts ) );
	
	$title_content = '<'.$size.' '.($align !='' ? 'style="text-align:'.$align.';"' : "").'>'.($icon != '' ? '<i class="'.$icon.'"></i>' : '' ).$text.'</'.$size.'>';
	if($line == 'true' || $line == 'yes') 
		$line = '<div class="line"></div>';
	else
		$line = '';
	
	return '<div class="alterna-title row-fluid">'.$title_content.$line.'</div>';
}
add_shortcode('title', 'alterna_title_func');

//=============================
// Alterna New Title v4.0.4
//=============================
function alterna_newtitle_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'class'		=>	'',
		  'type'		=>	'text',
		  'icon'		=>	'',
		  'value'		=>	'',
		  'size' 		=>	'h3',
		  'icon_size' 	=>	'',
		  'uppercase'	=>	'yes',
		  'bold'		=>	'yes', 
		  'style'		=>	'',
		  'show_bg'		=>	'yes',
		  'line' 		=>	'yes',
		  'align'		=>	'left',
		  'line_align'	=>	'center',
		  'color'		=>	'',
		  'bg_color'	=>	'',
		  'line_color'	=>	'',
		  'extra_color'		=>	'',
		  ), $atts ) );
		
		$content =  '';
		$extra_content =  '';
		if($color != ""){
			$color = 'color:'.$color.';';
		}
		if($bg_color != ""){
			$bg_color = 'style="background:'.$bg_color.'"';
		}
		
		if($line_color != ""){
			$line_color = 'style="background:'.$line_color.'"';
		}
		if($extra_color != ""){
			$extra_color = 'style="color:'.$extra_color.'"';
		}
		if($type == "text"){
			$content =  '<'.$size.' class="'.($uppercase == "yes" ? 'uppercase ' : '').($bold == "yes" ? 'bold ' : '').'" style="'.$color.$style.'">'.$value.'</'.$size.'>';
		}else{
			if($size == "h3") {
				$size = "";
			}
			$content =  '<i class="'.$icon.'" style="'.$color.$style.'"></i>';
			if($value != "") {
				$extra_content = '<span class="new-title-extra '.$icon_size.($uppercase == "yes" ? ' uppercase' : '').($bold == "yes" ? ' bold' : '').'" '.$extra_color.'>'.$value.'</span>';
			}
		}
		
		
	
		return '<div class="alterna-new-title row-fluid '.$class.' '.$align.' line-'.$line_align.'">'.($line == "yes" ? '<div class="new-line" '.$line_color.'></div>' : '').'<div class="new-title-container '.($show_bg == "yes" ? 'show-bg' : '').' '.($type != "text" ? 'icon '.$icon_size : '').'" '.($show_bg == "yes" ? $bg_color : '').'>'.$content.'</div>'.$extra_content.'</div>';
}
add_shortcode('newtitle', 'alterna_newtitle_func');

//=============================
// Icons
//=============================
function alterna_icon_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'icon_name' => '',
		  'icon_size' => '',
		  'icon_style' => '',
		  'icon_color' => ''
		  ), $atts ) );
	if($icon_name == '') return "";
	return '<i class="'.$icon_name.' '.$icon_size.' '.$icon_style.'" '.($icon_color != '' ? ' style="color:'.$icon_color.'"' :'').'></i>';
}
add_shortcode('icon', 'alterna_icon_func');

//=============================
// Social
//=============================
function alterna_social_func($atts, $content = null){
	global $alterna_icon_items, $alterna_icon_default_color, $alterna_icon_default_params;
	$alterna_icon_items = array();
	extract( shortcode_atts( array(
			'bg_color' => '',
			'tooltip' => 'no',
			'tooltip_placement' => 'top'
		  ), $atts ) );
	
	$alterna_icon_default_color = $bg_color;
	$alterna_icon_default_params = array('tooltip' => $tooltip, 'placement' => $tooltip_placement);
	
	$output = '<ul class="inline alterna-social">';
	do_shortcode($content);
	
	if(count($alterna_icon_items) > 0) {
		foreach($alterna_icon_items as $alterna_icon_item) {
			$output .= '<li>'.$alterna_icon_item.'</li>';
		}
	}
	
	$output .= '</ul>';
	
	return $output;
}

add_shortcode('social', 'alterna_social_func');

function alterna_social_item_func($atts, $content = null){
	global $alterna_icon_items, $alterna_icon_default_color, $alterna_icon_default_params;
	
	extract( shortcode_atts( array(
			'type' => 'twitter',
			'link' => '#',
			'target' => '_blank',
			'title' => ''
		  ), $atts ) );
	
	if($alterna_icon_default_params['tooltip'] == "yes" && $title == ""){
		$title = $type;
	}
	
	$alterna_icon_items[] = '<a '.($alterna_icon_default_color != "" ? 'style="background-color:'.$alterna_icon_default_color.'"' : '').' href="'.$link.'" target="'.$target.'" '.($alterna_icon_default_params['tooltip'] == "yes" ? 'title="'.$title.'" data-placement="'.$alterna_icon_default_params['placement'].'"' : '').' class="alterna-icon-'.$type.' '.($alterna_icon_default_params['tooltip'] == "yes" ? "show-tooltip" : "").'"></a>';
	
	return "";
}
add_shortcode('social_item', 'alterna_social_item_func');

//=============================
// Map
//=============================
function alterna_map_func($atts, $content = null){
	global $alterna_map_id;
	if(isset($alterna_map_id)){
		$alterna_map_id++;
	}else{
		$alterna_map_id = 1;
	}
	extract( shortcode_atts( array(
		  'zoom'	=> '13',
		  'scrollwheel' => 'yes',
		  'draggable'	=> 'yes',
		  'latlng' => '',
		  'width' => '300',
		  'height' => '200',
		  'show_marker' => 'no',
		  'show_info' =>'no',
		  'info_width' => '260',
		  ), $atts ) );
		
	$id = 'alterna_map_'.$alterna_map_id;
	
	if($width != "100%") $width = $width.'px';
	if($height != "100%") $height = $height.'px';
		
	$output = '<div id="'.$id.'" class="map_canvas" style="float:left;width:'.$width.';height:'.$height.';"  data-zoom="'.$zoom.'" data-latlng="'.$latlng.'" data-scrollwheel="'.$scrollwheel.'" data-draggable="'.$draggable.'" ';
	
	if($show_marker == 'yes'){
		
		$output .= 'data-showmarker="'.$show_marker.'" ';
		
		if($show_info == "yes"){
			$output .= 'data-showinfo="'.$show_info.'" ';
			$output .= 'data-infowidth="'.$info_width.'" ';
			$output .= 'data-infobg="'.get_template_directory_uri().'/img/tipbox.png" >';
			$output .= '</div>';
			$output .= '<div id="'.$id.'-map-info" style="display:none;">'.$content.'</div>';
			
			return $output;
		}
		$output .= '></div>';
		return $output;
	}
	$output .= '></div>';
	return $output;
	
}
add_shortcode('map', 'alterna_map_func');

//=============================
// FlexSlider
//=============================
function alterna_flexslider_func($atts, $content = null){
	global $alterna_flexslider_items;
	
	$alterna_flexslider_items = array();
	extract( shortcode_atts( array(
		  'auto' => 'no',
		  'delay' => '5000'
		  ), $atts ) );
		  
	$output = '<div class="flexslider alterna-fl post-gallery" '.($auto == "yes" ? 'data-delay="'.$delay.'"' : '').' ><ul class="slides">';
	
	do_shortcode($content);
	if(count($alterna_flexslider_items) > 0){
		foreach($alterna_flexslider_items as $alterna_flexslider_item){
			$output .= '<li>'.$alterna_flexslider_item.'</li>';
		}
	}
	
	$output .= '</ul></div>';
	
	return $output;
}
add_shortcode('flexslider', 'alterna_flexslider_func');

function alterna_flexslider_item_func($atts, $content = null){
	global $alterna_flexslider_items;
	extract( shortcode_atts( array(
		  'type' 	=>	'image',
		  'src'	 	=>	'',
		  'link'	=>	''
		  ), $atts ) );
	switch($type){
		case 'image' :
			if($link != ""){
				$alterna_flexslider_items[] = '<a href="'.$link.'"><img src="'.$src.'" alt="" ></a>';
			}else{
				$alterna_flexslider_items[] = '<img src="'.$src.'" alt="" >';
			}
			break;
		case 'video' :
			$alterna_flexslider_items[] = do_shortcode($content);
			break;
	}
	return "";
}
add_shortcode('flexslider_item', 'alterna_flexslider_item_func');

//=============================
// Carousel
//=============================
function alterna_carousel_func($atts, $content = null){
	global $alterna_carousel_items, $alterna_carousel_id;
	if(isset($alterna_carousel_id)){
		$alterna_carousel_id++;
	}else{
		$alterna_carousel_id = 1;
	}
	$alterna_carousel_items = array();
	
	extract( shortcode_atts( array(
		  'auto' => 'no',
		  'delay' => '5000'
		  ), $atts ) );
	$id = 'alterna-carousel-'.$alterna_carousel_id;
	
	$output =  '<div id="'.$id.'" class="carousel slide '.($auto == "yes" ? 'carousel-autoplay' : '').'" '.($auto == "yes" ? 'data-delay="'.$delay.'"' : '').'>';
	$output .= '<div class="carousel-inner">';
	
	do_shortcode($content);
	$c = 0;
	foreach($alterna_carousel_items as $alterna_carousel_item){
		$output .= '<div class="item '.($c == 0 ? 'active' : '').'">'.$alterna_carousel_item.'</div>';
		$c++;
	}
	
	$output .= '</div>';
	$output .= '<a class="carousel-control left" href="#'.$id.'" data-slide="prev">&lsaquo;</a>';
	$output .= '<a class="carousel-control right" href="#'.$id.'" data-slide="next">&rsaquo;</a>';
	$output .= '</div>';
	
	return $output;
}
add_shortcode('carousel', 'alterna_carousel_func');

function alterna_carousel_item_func($atts, $content = null){
	global $alterna_carousel_items;
	extract( shortcode_atts( array(
		  'src'	 => ''
		  ), $atts ) );
	$output = '<img src="'.$src.'" alt="" >';
	$output .= '<div class="carousel-caption">'.$content.'</div>';
	$alterna_carousel_items[] = $output;
		
	return "";
}
add_shortcode('carousel_item', 'alterna_carousel_item_func');

//=============================
// Buttons
//=============================
function alterna_button_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'text' => 'Button Name',
		  'icon' => '',
		  'type' => '',
		  'size' => '',
		  'url'	 => '#',
		  'target' => '_blank',
		  'bg_color' => '',
		  'bg_hover_color' => '',
		  'txt_color' => '',
		  'txt_hover_color' => ''
		  ), $atts ) );
	if($type == 'btn-custom') $type = 'btn-custom ';
	$output = '<a class="btn '.$type.' '.$size.'" href="'.$url.'" target="'.$target.'"';
	
	if($bg_color != '') $output .=' data-bgcolor="'.$bg_color.'"';
	if($bg_hover_color != '') $output .=' data-bghovercolor="'.$bg_hover_color.'"';
	if($txt_color != '') $output .=' data-txtcolor="'.$txt_color.'"';
	if($txt_hover_color != '') $output .=' data-txthovercolor="'.$txt_hover_color.'"';
	if($icon != '') {
		$icon = '<i class="'.$icon.'"></i>';
	}
	$output .= ' >'.$icon.$text.'</a>';
	
	return $output;
}
add_shortcode('button', 'alterna_button_func');

//=============================
// Alert Message
//=============================
function alterna_alert_func($atts, $content = null){
	global $alterna_carousel_items;
	$alterna_carousel_items = array();
	
	extract( shortcode_atts( array(
		  'type' => '',
		  'close' => 'yes',
		  'title' => '',
		  ), $atts ) );
		  
	$output =  '<div class="alert '.($close == "yes" ? 'alert-block ' : '').($type == '' ? '' : 'alert-'.$type ).' fade in">';
	//if($close == 'yes') $output .= '<a class="close" data-dismiss="alert" href="#">&times;</a>';
	if($close == 'yes') $output .= '<div class="close" data-dismiss="alert">&times;</div>';
	if($title != '') $output .= '<h4 class="alert-heading">'.$title.'</h4>';
	$output .= do_shortcode($content);
    $output .= '</div>';
	
	return $output;
}
add_shortcode('alert', 'alterna_alert_func');

//=============================
// Dropcap
//=============================
function alterna_dropcap_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'text' 		=> '',
		  'type' 		=> '',
		  'txt_color'	=> '#000000',
		  'bg_color'	=> '#ff0000'
		  ), $atts ) );
	
	$output = '<span class="dropcap';
	switch($type){
		case "text":
			$output .= ' dropcap-text" style="color:'.$txt_color.'"';
			break;
		default :
			$output .= ' dropcap-default" style="background:'.$bg_color.';color:'.$txt_color.'"';
			
	}
	$output .= '>';
	$output .= $text.'</span>';
	$output .= do_shortcode($content);
	
	return $output;
}
add_shortcode('dropcap', 'alterna_dropcap_func');

//=============================
// Blockquote
//=============================
function alterna_blockquote_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'border_color'	=> '#eeeeee',
		  'bg_color'		=> '#ffffff'
		  ), $atts ) );
	
	$output = '<blockquote style="padding:10px 10px 10px 15px;border-left: 5px solid '.$border_color.';'.($bg_color != '' ? 'background:'.$bg_color.';' : '').'">'.do_shortcode($content).'</blockquote>';

	return $output;
}
add_shortcode('blockquote', 'alterna_blockquote_func');

//=============================
// Bullets
//=============================
function alterna_bullets_func($atts, $content = null){
	global $alterna_bullets_items,$alterna_bullets_type,$alterna_bullets_color,$alterna_bullets_effect;
	$alterna_bullets_items = array();
	
	extract( shortcode_atts( array(
			'type'	=> 'ok',
			'color' => '#000000',
			'txt_color' => '',
			'effect'	=>	''
		  ), $atts ) );
	
	if($effect != "" && $effect != "none"){
		$output = '<ul class="bullets animate-list" '.($txt_color != '' ? ' style="color:'.$txt_color.'"' : '').'>';
	}else{
		$output = '<ul class="bullets" '.($txt_color != '' ? ' style="color:'.$txt_color.'"' : '').'>';
		
	}
	$alterna_bullets_effect = $effect;
	
	$alterna_bullets_type = $type;
	$alterna_bullets_color = $color;
	
	do_shortcode($content);
	
	foreach($alterna_bullets_items as $alterna_bullets_item){
		$output .= $alterna_bullets_item;
	}
	
	$output .= '</ul>';
	return $output;
}
add_shortcode('bullets', 'alterna_bullets_func');

function alterna_bullet_func($atts, $content = null){
	global $alterna_bullets_items,$alterna_bullets_type,$alterna_bullets_color,$alterna_bullets_effect;
	
	extract( shortcode_atts( array(
			'custom'	=> 'no',
			'icon'		=>	'',
			'txt_color' => ''
		  ), $atts ) );
	
	if($alterna_bullets_effect != ""){
		$output = '<li '.($txt_color != '' ? ' style="color:'.$txt_color.'"' : '').' class="animate-item" data-effect="'.esc_attr($alterna_bullets_effect).'">';
	}else{
		$output = '<li '.($txt_color != '' ? ' style="color:'.$txt_color.'"' : '').'>';
		
	}

	if($custom == 'yes' ){
		if($icon != ""){
			$output .= do_shortcode('[icon icon_name="'.$icon.'" icon_color="'.$alterna_bullets_color.'"]') ;
		}
	}else{
		if($alterna_bullets_type == "ok"){
			$output .= do_shortcode('[icon icon_name="icon-ok" icon_color="'.$alterna_bullets_color.'"]') ;
		}else if($alterna_bullets_type == "number"){
			$output .= '<span style="background-color:'.$alterna_bullets_color.';">'.(count($alterna_bullets_items) + 1).'</span>';
		}else if($alterna_bullets_type == "error"){
			$output .= do_shortcode('[icon icon_name="icon-remove" icon_color="'.$alterna_bullets_color.'"]') ;
		}
	}
	
	$output .= do_shortcode($content);
	
	$output .= '</li>';
	
	$alterna_bullets_items[] = $output;
	
	return "";
}
add_shortcode('bullet', 'alterna_bullet_func');

//=============================
// PriceTable
//=============================
function alterna_price_func($atts, $content = null){
	global $alterna_price_items,$alterna_price_type;
	$alterna_price_items = array();
	extract( shortcode_atts( array(
		  'type'	=> 'standard',
		  
		  'title'	=> '',
		  'price'	=> '',
		  'plan'	=> '',
		  
		  'background_color' 	=> '',
		  'border_color'		=> '',
		  
		  'content_bg_color'		=> '',
		  'content_align'			=> 'center',
		  
		  'effect'			=>	''
		  
		  ), $atts ) );
	//recommend , free , standard
	if($type == "custom"){
		if($border_color == ""){
			$border_color = '#c7c7c7';
		}
	}else{
		switch($type){
			case 'recommend':
				$border_color = '#C7A807';
				break;
			case 'free':
				$border_color = '#5E9401';
				break;
			default :
				$border_color = '#c7c7c7';
		}
	}
	
	if($type == "custom"){
		if($background_color == ""){
			$background_color = '#e2e2e2';
		}
	}else{
		switch($type){
			case 'recommend':
				$background_color = '#F1D027';
				break;
			case 'free':
				$background_color = '#82C906';
				break;
			default :
				$background_color = '#e2e2e2';
		}
	}
	
	if($type == "custom"){
		if($content_bg_color == ""){
			$content_bg_color = '#F4F4F4';
		}
	}else{
		switch($type){
			case 'recommend':
				$content_bg_color = '#fff9d9';
				break;
			case 'free':
				$content_bg_color = '#EFFED6';
				break;
			default :
				$content_bg_color = '#F4F4F4';
		}
	}
	
	$alterna_price_type = $type;
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="price '.($type).' animate" data-effect="'.esc_attr($effect).'"><div class="price-header" style="border: 1px '.$border_color.' solid;background: '.$background_color.';">';
	}else{
		$output = '<div class="price '.($type).'"><div class="price-header" style="border: 1px '.$border_color.' solid;background: '.$background_color.';">';
	}
	
	
	
	$output .= '<div class="price-title"><h5>'.$title.'</h5></div>';
	
	$output .= '<div class="price-price-plan">';
	$output .= '<div class="price-num">'.$price.'</div>';
	$output .= '<div class="price-plan">'.$plan.'</div>';
	$output .= '</div>';
	
	$output .= '</div><ul class="price-content" style="text-align:'.$content_align.';background: '.$content_bg_color.';">';
	
	do_shortcode($content);
	
	foreach($alterna_price_items as $alterna_price_item){
		$output .= $alterna_price_item;
	}
	
	$output .= '</ul></div>';
	
	return $output;
}
add_shortcode('price', 'alterna_price_func');

function alterna_price_item_func($atts, $content = null){
	global $alterna_price_items, $alterna_price_type;
	
	extract( shortcode_atts( array(
		  'type'	 	=> 'text',
		  'btn_text'	=> 'Sign Up',
		  'btn_url'		=>	'#',
		  'btn_target'	=> 	'_self',
		  'btn_size'	=>	'default',
		  'btn_bg_color' => '',
		  'btn_bg_hover_color' => '',
		  'btn_txt_color'	=> '',
		  'btn_txt_hover_color'	=> ''
		  ), $atts ) );
	
	if($alterna_price_type == 'custom'){
		if($btn_bg_color == ''){ $btn_bg_color = '#666666';}
		if($btn_bg_hover_color == ''){ $btn_bg_hover_color = '#333333';}
		if($btn_txt_color == '') $btn_txt_color = '#ffffff';
		if($btn_txt_hover_color == ''){ $btn_txt_hover_color = '#ffffff';}
	}else{
		switch($alterna_price_type){
			case 'recommend' :
				$btn_bg_color = '#F1D027';
				$btn_bg_hover_color = '#C7A807';
				$btn_txt_color = '#ffffff';
				$btn_txt_hover_color = '#ffffff';
				break;
			case 'free' :
				$btn_bg_color = '#82C906';
				$btn_bg_hover_color = '#5E9401';
				$btn_txt_color = '#ffffff';
				$btn_txt_hover_color = '#ffffff';
				break;
			default :
				$btn_bg_color = '#666666';
				$btn_bg_hover_color = '#333333';
				$btn_txt_color = '#ffffff';
				$btn_txt_hover_color = '#ffffff';
		}
	}
	
	switch($type){
		case 'btn':
			$btn = '[button text="'.$btn_text.'" type="btn-custom" size="'.$btn_size.'" url="'.$btn_url.'"  target="'.$btn_target.'" bg_color="'.$btn_bg_color.'" bg_hover_color="'.$btn_bg_hover_color.'" txt_color="'.$btn_txt_color.'" txt_hover_color="'.$btn_txt_hover_color.'"]';
			$alterna_price_items[] = '<li class="price-item price-btn">'.do_shortcode($btn).'</li>';
			break;
		default :
			$alterna_price_items[] = '<li class="price-item">'.do_shortcode($content).'</li>';
	}

	return "";
	
}
add_shortcode('price_item', 'alterna_price_item_func');

//=============================
// PriceSlide
//=============================

function alterna_priceslider_func($atts, $content = null){
	global $priceslider_contents;
	
	$priceslider_contents = array();
	
	extract( shortcode_atts( array(
				'height' => '' , 
				'nav_color' => ''
		  ), $atts ) );
	
	do_shortcode($content);
	
	$output = '<div class="price-slider" '.($height != '' ? 'style="height:'.$height.'px;"' : '').'>';
	$output .= '<div class="price-slider-images">';
		foreach($priceslider_contents as $priceslider_content){
			$output .= $priceslider_content['img'];
		}	
	$output	.= '</div>';
	$output .= '<div class="price-slider-content">';
		foreach($priceslider_contents as $priceslider_content){
			$output .= $priceslider_content['element'];
		}
	$output .= '</div>';
	$output .= '<div class="price-slider-btns">';
		$output .= '<a class="price-prev" '.($nav_color != '' ? 'style="background-color:'.$nav_color.'"' : '').'><i class="icon-chevron-left"></i></a>';
		$output .= '<a class="price-next" '.($nav_color != '' ? 'style="background-color:'.$nav_color.'"' : '').'><i class="icon-chevron-right"></i></a>';
	$output	.= '</div>';
	
	$output .= '</div>';
	
	return $output;	
}

add_shortcode('priceslider', 'alterna_priceslider_func');

function alterna_priceslider_item_func($atts, $content = null){
	global $priceslider_contents;
	extract( shortcode_atts( array(
				'img'	 	=> '',
				'plan'		=>	'<i class="icon-truck"></i> Regular License',
				'currency'	=> '$',
				'price'		=>	'0',
				'color'		=> '#7AB80E',
				'btn_text'	=> 'Sign Up',
			  	'btn_url'		=>	'#',
			  	'btn_target'	=> 	'_self',
			  	'btn_size'	=>	'default',
			  	'btn_bg_color' => '#7AB80E',
			  	'btn_bg_hover_color' => '#5b8f00',
			  	'btn_txt_color'	=> '#ffffff',
			  	'btn_txt_hover_color'	=> '#ffffff'
		  ), $atts ) );
	$output_data = array('img'=>'','element'=>'');
	$output_data['img'] = '<div class="price-img"><img src="'.$img.'" alt=""></div>';
	
	$element = '<div class="price-slider-element">';
		$element .= '<div class="price-plan" style="background-color:'.$color.';"><h4>'.$plan.'</h4></div>';
		$element .= '<div class="price-value" style="color:'.$color.';"><sup>'.$currency.'</sup>'.$price.'</div>';
		$element .= '<div class="price-slider-description">'.$content.'</div>';
		$element .= '<div class="price-slider-signup">';
			$element .='[button text="'.$btn_text.'" type="btn-custom" size="'.$btn_size.'" url="'.$btn_url.'"  target="'.$btn_target.'" bg_color="'.$btn_bg_color.'" bg_hover_color="'.$btn_bg_hover_color.'" txt_color="'.$btn_txt_color.'" txt_hover_color="'.$btn_txt_hover_color.'"]';
		$element	.= '</div>';
		$element	.= '</div>';
	
	$output_data['element'] = do_shortcode($element);
	
	$priceslider_contents[] = $output_data;
	
	return "";
}
add_shortcode('priceslider_item', 'alterna_priceslider_item_func');


//=============================
// Call To Action
//=============================
function alterna_call_to_action_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'title' => '',
		  'desc' => '',
		  'size' => 'big',
		  'btn_title' => '',
		  'btn_type' => 'btn-primary',
		  'btn_size' => 'btn-large',
		  'url'	 => '#',
		  'target' => '_self',
		  'bg_color' => '',
		  'bg_hover_color' => '',
		  'txt_color' => '',
		  'txt_hover_color' => '',
		  'effect' => ''
		  ), $atts ) );
	
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="call-to-action animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="call-to-action">';
	}
	
	if($size == 'big')
		$output .= '<h1>'.$title.'</h1>';
	else if($size == '' || $size == 'small')
		$output .= '<h3>'.$title.'</h3>';
	else 
		$output .= '<'.$size.'>'.$title.'</'.$size.'>';
	
	if($desc != ''){
		$output .= '<p class="desc '.$size.'">'.$desc.'</p>';
	}else{
		$output .= '<p class="desc '.$size.'">'.do_shortcode($content).'</p>';
	}
	
	if($btn_title != '') {
		if($btn_type == 'btn-custom') $btn_type = 'btn-custom ';
		$output .= '<p><a class="btn '.$btn_type.' '.$btn_size.'" href="'.$url.'" target="'.$target.'"';
		
		if($bg_color != '') $output .=' data-bgcolor="'.$bg_color.'"';
		if($bg_hover_color != '') $output .=' data-bghovercolor="'.$bg_hover_color.'"';
		if($txt_color != '') $output .=' data-txtcolor="'.$txt_color.'"';
		if($txt_hover_color != '') $output .=' data-txthovercolor="'.$txt_hover_color.'"';
		
		$output .= ' >'.$btn_title.'</a></p>';
	}
	
	$output .= '</div>';
	
	return $output;
}
add_shortcode('call_to_action', 'alterna_call_to_action_func');

function alterna_call_to_action_bar_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'title' => '',
		  'desc' => '',
		  'size' => 'big',
		  'btn_title' => '',
		  'btn_type' => 'btn-primary',
		  'btn_size' => 'btn-large',
		  'url'	 => '#',
		  'target' => '_self',
		  'bg_color' => '',
		  'bg_hover_color' => '',
		  'txt_color' => '',
		  'txt_hover_color' => '',
		  'effect' => ''
		  ), $atts ) );

	
	if($effect != "" && $effect != "none"){
		$output = '<div class="call-to-action-bar animate" data-effect="'.esc_attr($effect).'"><div class="call-to-action-bar-content">';
	}else{
		$output = '<div class="call-to-action-bar"><div class="call-to-action-bar-content">';
	}
	
	if($size == 'big')
		$output .= '<h1>'.$title.'</h1>';
	else if($size == '' || $size == 'small')
		$output .= '<h3>'.$title.'</h3>';
	else 
		$output .= '<'.$size.'>'.$title.'</'.$size.'>';
	
	if($desc != ''){
		$output .= '<p class="desc '.$size.'">'.$desc.'</p>';
	}else{
		$output .= '<p class="desc '.$size.'">'.do_shortcode($content).'</p>';
	}
	
	$output .= '</div>';
	if($btn_title != '') {
		if($btn_type == 'btn-custom') $btn_type = 'btn-custom ';
		$output .= '<p><a class="btn '.$btn_type.' '.$btn_size.'" href="'.$url.'" target="'.$target.'"';
		
		if($bg_color != '') $output .=' data-bgcolor="'.$bg_color.'"';
		if($bg_hover_color != '') $output .=' data-bghovercolor="'.$bg_hover_color.'"';
		if($txt_color != '') $output .=' data-txtcolor="'.$txt_color.'"';
		if($txt_hover_color != '') $output .=' data-txthovercolor="'.$txt_hover_color.'"';
		
		$output .= ' >'.$btn_title.'</a></p>';
	}
	
	$output .= '</div>';
	
	return $output;
}
add_shortcode('call_to_action_bar', 'alterna_call_to_action_bar_func');

//=============================
// Accordion
//=============================
function alterna_accordion_func($atts, $content = null){
	global $alterna_accordion_items,$alterna_accordion_id,$alterna_accordion_cid,$alterna_accordion_item_effect;;

	if(isset($alterna_accordion_id)){
		$alterna_accordion_id++;
	}else{
		$alterna_accordion_id = 1;
	}
	
	$alterna_accordion_cid = 1;
	
	extract( shortcode_atts( array(
		  'effect' => '',
		  ), $atts ) );
	$alterna_accordion_item_effect = $effect;
	$alterna_accordion_items = array();
	$id = 'alterna-accordion-'.$alterna_accordion_id;
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="accordion alterna-accordion animate-list" id="'.$id.'">';
	}else{
		$output = '<div class="accordion alterna-accordion" id="'.$id.'">';
	}
	
	
	do_shortcode($content);
	foreach($alterna_accordion_items as $alterna_accordion_item){
		$output .= $alterna_accordion_item;
	}
	$output .= '</div>';
	
	return $output;
}
add_shortcode('accordion', 'alterna_accordion_func');

function alterna_accordion_item_func($atts, $content = null){
	global $alterna_accordion_items,$alterna_accordion_id, $alterna_accordion_cid, $alterna_accordion_item_effect;
	if(isset($alterna_accordion_cid)){
		$alterna_accordion_cid++;
	}else{
		$alterna_accordion_cid = 1;
	}
	extract( shortcode_atts( array(
		  'title' => '',
		  'open' => 'no'
		  ), $atts ) );
	
	$pid = 'alterna-accordion-'.$alterna_accordion_id;
	$id = $pid.'-'.$alterna_accordion_cid;
	
	if($alterna_accordion_item_effect != ""){
		$output = '<div class="accordion-group animate-item" data-effect="'.esc_attr($alterna_accordion_item_effect).'">';
	}else{
		$output = '<div class="accordion-group">';
	}
	
	 $output .= '<div class="accordion-heading"><a class="accordion-toggle '.($open == "true" || $open == "yes" ? '' : 'collapsed').'" data-toggle="collapse" data-parent="#'.$pid.'" href="#'.$id.'"><i class="icon-minus"></i><i class="icon-plus"></i>'.$title.'</a></div><div id="'.$id.'" class="accordion-body collapse '.($open == "true" || $open == "yes" ? "in" : "").'"><div class="accordion-inner">'.do_shortcode($content).'</div></div></div>';
	
	$alterna_accordion_items[] = $output;
	return "";
}
add_shortcode('accordion_item', 'alterna_accordion_item_func');

//=============================
// Toggle
//=============================
function alterna_toggle_func($atts, $content = null){
	global $alterna_toggle_id;
	
	if(isset($alterna_toggle_id)){
		$alterna_toggle_id++;
	}else{
		$alterna_toggle_id = 1;
	}
	
	extract( shortcode_atts( array(
		  'title' => '',
		  'open'  => '',
		  'effect' => ''
		  ), $atts ) );
		  
	$id = 'alterna-toggle-'.$alterna_toggle_id;
	if($effect != "" && $effect != "none"){
		$output = '<div class="accordion animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="accordion">';
	}
	$output .= '<div class="accordion-group"><div class="accordion-heading"><a class="accordion-toggle '.($open == "true" || $open == "yes" ? '' : 'collapsed').'" data-toggle="collapse" href="#'.$id.'"><i class="icon-minus"></i><i class="icon-plus"></i>'.$title.'</a></div>';
	$output .= '<div id="'.$id.'" class="accordion-body collapse '.($open == "true" || $open == "yes" ? 'in' : '').'"><div class="accordion-inner">'.do_shortcode($content).'</div></div></div>';
	$output .= '</div>';
	
	return $output;
}
add_shortcode('toggle', 'alterna_toggle_func');

//=============================
// Tabs
//=============================
function alterna_tabs_func($atts, $content = null){
	global $tabs_title_array,$tabs_content_array;
	$tabs_title_array 	= array();
	$tabs_content_array = array();
	extract( shortcode_atts( array(
			'effect'	=>	''
		  ), $atts ) );
	if($effect != "" && $effect != "none"){
		$output = '<div class="tabs animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="tabs">';
	}
	do_shortcode($content);
	$output .= '<ul class="tabs-nav">';
	foreach($tabs_title_array as $tabs_title){
		$output .= $tabs_title;
	}
	$output .= '</ul>';
	$output .= '<div class="tabs-container">';
	foreach($tabs_content_array as $tabs_content){
		$output .= $tabs_content;
	}
	$output .='</div></div>';
	return $output;
}
add_shortcode('tabs', 'alterna_tabs_func');

function alterna_tabs_item_func($atts, $content = null){
	global $tabs_title_array,$tabs_content_array;
	
	extract( shortcode_atts( array(
		  'title' => 'No title!'
		  ), $atts ) );
		  
	$tabs_title_array[] = '<li>'.$title.'</li>';
	$tabs_content_array[]	= '<div class="tabs-content">'.do_shortcode($content).'</div>';
	return "";
}
add_shortcode('tabs_item', 'alterna_tabs_item_func');

//=============================
// SideTabs
//=============================
function alterna_sidetabs_func($atts, $content = null){
	global $sidetabs_title_array,$sidetabs_content_array;
	$sidetabs_title_array 	= array();
	$sidetabs_content_array = array();
	
	extract( shortcode_atts( array(
			'effect'	=>	'',
		  ), $atts ) );
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="sidetabs animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="sidetabs">';
	}
	do_shortcode($content);
	$output .= '<ul class="sidetabs-nav the-icons">';
	foreach($sidetabs_title_array as $sidetabs_title){
		$output .= $sidetabs_title;
	}
	$output .= '</ul>';
	$output .= '<div class="sidetabs-container">';
	foreach($sidetabs_content_array as $sidetabs_content){
		$output .= $sidetabs_content;
	}
	$output .='</div></div>';
	return $output;
}
add_shortcode('sidetabs', 'alterna_sidetabs_func');

function alterna_sidetabs_item_func($atts, $content = null){
	global $sidetabs_title_array,$sidetabs_content_array;
	
	extract( shortcode_atts( array(
		  'title' => 'No title!',
		  'icon' => ''
		  ), $atts ) );
		  
	$sidetabs_title_array[] = '<li>'.($icon != '' ? '<i class="'.$icon.'"></i>' : '' ).$title.'</li>';
	$sidetabs_content_array[]	= '<div class="sidetabs-content">'.do_shortcode($content).'</div>';
	return "";
}
add_shortcode('sidetabs_item', 'alterna_sidetabs_item_func');

//=============================
// Testimonials
//=============================
function alterna_testimonials_func($atts, $content = null) {
	global $alterna_testimonials_items,$alterna_testimonials_type;
	
	$alterna_testimonials_items = array();
	
	extract( shortcode_atts( array(
		  	'type'		=> '',
			'autoplay'	=> 'no',
			'delay'		=>	'6000',
			'show_nav'	=>	'yes',
			'effect'	=>	''
		  ), $atts ) );
	
	$alterna_testimonials_type = $type;
	$testimonials_type = '';
	
	if($type == 'avatar'){
		 $testimonials_type = "testimonials-avatar";
	}else if($type == 'wide'){
		$testimonials_type = "testimonials-wide";
		if($show_nav == 'yes'){
			$testimonials_type = "testimonials-wide testimonials-show-nav";
		}
	}
	
	if($effect != "" && $effect != "none"){
		$output ='<div class="testimonials '.$testimonials_type.($autoplay == 'yes' ? " testimonials-auto" : "").' animate" data-effect="'.esc_attr($effect).'" data-delay="'.$delay.'">';
	}else{
		$output ='<div class="testimonials '.$testimonials_type.($autoplay == 'yes' ? " testimonials-auto" : "").'" data-delay="'.$delay.'">';
	}
	
	if($type == 'wide'){
		$output .= '<i class="icon-quote-left"></i>';
	}
	
	do_shortcode($content);
	
	foreach($alterna_testimonials_items as $alterna_testimonials_item){
		$output .= $alterna_testimonials_item;
	}
	
	if($show_nav == 'yes'){
		$output .='<a class="testimonials-prev"><i class="icon-angle-left"></i></a><a class="testimonials-next"><i class="icon-angle-right"></i></a>';
	}
	
	$output .= '</div>';
	return $output;
}
add_shortcode('testimonials', 'alterna_testimonials_func');

function alterna_testimonials_item_func($atts, $content = null){
	global $alterna_testimonials_items , $alterna_testimonials_type;
	
	extract( shortcode_atts( array(
		  	'name'	=>  '',
			'job'	=>	'',
			'img'	=>	'',
			'link'	=>	'',
		  ), $atts ) );
	
	
	$output = '<div class="testimonials-item">';
	if($alterna_testimonials_type == 'avatar'){
		if($img == "") $img = get_template_directory_uri() . '/img/testimonials-client.png';
		$output .= '<div class="testimonials-avatar"><img src="'.$img.'" alt="" ></div>';
	}
	
	if($alterna_testimonials_type == 'wide'){
		$output .= '<div class="testimonials-content">';
		$output .= $content.'</div><div class="testimonials-name"><span>';
		if($link != ''){
			$output .= '<a href="'.$link.'" target="_blank">'.$name.'</a>';
		}else{
			$output .= $name;
		}
		$output .= '</span>'.( $job != '' ? '<span class="testimonials-job">- '.$job : "" ).'</span></div></div>';
	}else{
		$output .= '<div class="testimonials-content">';
		$output .= '<i class="icon-quote-left"></i>'.$content.'<i class="icon-quote-right"></i><span class="testimonials-arraw"></span></div><div class="testimonials-name"><div class="testimonials-icon" ><span>';
		if($link != ''){
			$output .= '<a href="'.$link.'" target="_blank">'.$name.'</a>';
		}else{
			$output .= $name;
		}
		$output .= '</span>'.( $job != '' ? '<span class="testimonials-job">- '.$job : "" ).'</span></div></div></div>';
	}
	$alterna_testimonials_items[] = $output;
	
	return "";
}
add_shortcode('testimonials_item', 'alterna_testimonials_item_func');

//=============================
// Services
//=============================
function alterna_services_func($atts, $content = null){
	extract( shortcode_atts( array(
			'type' => 'icon',
			'src'	=> '',
			'show_bg' => 'yes',
			'border' => 'no',
			'link'	=> '',
			'effect'	=>	''
		  ), $atts ) );
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="services '.($link != "" ? 'fsbox-link ' : '').($show_bg == "yes" ? 'show-bg' : '').($border == "yes" ? ' border' : '').' animate" data-effect="'.esc_attr($effect).'" '.($link != "" ? 'data-link="'.$link.'"' : '').'><div class="services-image">';
	}else{
		$output = '<div class="services '.($link != "" ? 'fsbox-link ' : '').($show_bg == "yes" ? 'show-bg' : '').($border == "yes" ? ' border' : '').'" '.($link != "" ? 'data-link="'.$link.'"' : '').'><div class="services-image">';
	}
		  
	if($type == "icon"){
		$output .= '<div class="services-icon"><i class="'.$src.'"></i></div>';
	}else{
		$output .= '<div class="services-icon"><img src="'.$src.'" alt=""></div>';
	}
	$output .= '</div>';
	$output .= do_shortcode($content);
	$output .= '</div>';
	
	return $output;
}
add_shortcode('services', 'alterna_services_func');

//=============================
// Features
//=============================
function alterna_features_func($atts, $content = null){
	extract( shortcode_atts( array(
			'type' => 'icon',
			'src'	=> '',
			'title' => '',
			'show_bg' => 'yes',
			'border' => 'no',
			'link'	=> '',
			'effect'	=>	''
		  ), $atts ) );
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="features '.($link != "" ? 'fsbox-link ' : '').($show_bg == "yes" ? 'show-bg' : '').($border == "yes" ? ' border' : '').' animate" data-effect="'.esc_attr($effect).'" '.($link != "" ? 'data-link="'.$link.'"' : '').'>';
	}else{
		$output = '<div class="features '.($link != "" ? 'fsbox-link ' : '').($show_bg == "yes" ? 'show-bg' : '').($border == "yes" ? ' border' : '').'" '.($link != "" ? 'data-link="'.$link.'"' : '').'>';
	}
	
	
	if($type == "icon"){
		$output .= '<div class="features-icon"><i class="'.$src.'"></i></div>';
	}else{
		$output .= '<div class="features-icon"><img src="'.$src.'" alt=""></div>';
	}
	$output .= '<div class="features-title"><h3>'.$title.'</h3><div class="features-content">';
	$output .= do_shortcode($content);
	$output .= '</div></div></div>';
	
	return $output;
}
add_shortcode('features', 'alterna_features_func');


//=============================
// New Services
//=============================
function alterna_newservices_func($atts, $content = null){
	extract( shortcode_atts( array(
			'layout'	=> 'left',
			'icon' 		=> 'icon-flag',
			'title'		=>	'',
			'effect'	=>	''
		  ), $atts ) );
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="newservices '.$layout.' animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="newservices '.$layout.'">';
	}

	$output .= '<div class="service-icon"><i class="'.$icon.'"></i></div><div class="service-content"><h3 class="service-title">'.$title.'</h3>'.do_shortcode($content).'</div></div>';

	
	return $output;
}
add_shortcode('newservices', 'alterna_newservices_func');

//=============================
// Team
//=============================
function alterna_team_func($atts, $content = null){
	global $alterna_team_content , $alterna_team_socials;
	$alterna_team_content = '';
	$alterna_team_socials = array();
	
	extract( shortcode_atts( array(
			'name' 	=> '',
			'job'	=> '',
			'src'	=> '',
			'link'	=>	'',
			'effect'=> ''
		  ), $atts ) );
		  
	
	if($effect != "" && $effect != "none"){
		$output = '<div class="team animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="team">';
	}
	
	$output	.='<div class="team-avatar"><img src="'.$src.'" alt="" /></div>';
	$output .='<div class="team-user">';
	if($link != ""){
		$output .='<a href="'.$link.'"><h4 class="team-title">'.$name.'</h4></a>';
	}else{
		$output .='<h4 class="team-title">'.$name.'</h4>';
	}
	$output	.='<div class="team-job">'.$job.'</div>';
	$output	.='</div>';
	
	do_shortcode($content);
	
	$output	.='<div class="team-information">'.$alterna_team_content.'</div>';
	$output	.='<div class="team-social">';
	$count = 0;
	foreach($alterna_team_socials as $alterna_team_social){
		$output	.=$alterna_team_social;
		$count++;
	}
	$output	.='</div></div>';
	
	return $output;
}
add_shortcode('team', 'alterna_team_func');

function alterna_team_content_func($atts, $content = null){
	global $alterna_team_content ;
	$alterna_team_content = do_shortcode($content);
	return $alterna_team_content;
}
add_shortcode('team_content', 'alterna_team_content_func');

function alterna_team_social_func($atts, $content = null){
	global $alterna_team_socials;
	
	extract( shortcode_atts( array(
			'link'  => '#',
			'target'=> '_blank',
			'icon'	=>	''
		  ), $atts ) );
	$alterna_team_socials[] = '<a href="'.$link.'" target="'.$target.'"><i class="'.$icon.'"></i></a>';
	
	return "";
}
add_shortcode('team_social', 'alterna_team_social_func');
		  
//=============================
// History
//=============================
function alterna_history_func($atts, $content = null){
	extract( shortcode_atts( array(
			'day'	=>	'',
			'src'	=>	'',
			'width' =>	'',
			'start' => 'no',
			'title' => '',
			'effect' => ''
		  ), $atts ) );
	if($effect != "" && $effect != "none"){
		$output = '<div class="history animate" data-effect="'.esc_attr($effect).'">';
	}else{
		$output = '<div class="history">';
	}  
	
	$output .= '<div class="history-date"><div class="day">'.$day.'</div></div><div class="history-line"></div><div class="history-hor-line"></div>';
	if($start == "yes") $output .='<div class="history-start-point"></div>';
	$output .='<div class="history-container">';
	if($src != '') $output .='<div class="history-img"><img src="'.$src.'" alt="" '.($width != '' ? 'style="width:'.$width.';"' : '').' ></div>';
	$output .='<div class="history-content '.($src == "" ? "" : "history-hasimg" ).'">';
	if($title != '') $output .='<h4>'.$title.'</h4>';
	$output .=do_shortcode($content).'</div></div></div>';

	return $output;
}
add_shortcode('history', 'alterna_history_func');

//=============================
// Skrills
//=============================
function alterna_skills_func($atts, $content = null){
	global $skill_items;
	$skill_items = array();
	
	$output = '<ul class="skills">';
	
	do_shortcode($content);
	
	if(count($skill_items) > 0){
		foreach($skill_items as $skill_item){
			$output .= '<li>'.$skill_item.'</li>';
		}
	}
	
	$output .= '</ul>';
	return $output;
}
add_shortcode('skills', 'alterna_skills_func');

function alterna_skill_func($atts, $content = null){
	global $skill_items;
	
	extract( shortcode_atts( array(
			'name'	=> 'No Name',
			'percent' => '50%',
			'text' => '',
			'bg_color' => '',
			'color'	=> ''
		  ), $atts ) );
	
	$skill_items[] = '<span class="skill-bg" data-percent="'.$percent.'" '.($bg_color != '' ? 'style="background:'.$bg_color.'";' : '').'></span><span class="skill-name" '.($color != '' ? 'style="color:'.$color.'";' : '').'>'.$name.'</span><span class="skill-progress" '.($color != '' ? 'style="color:'.$color.'";' : '').'>'.($text == "" ? $percent : $text).'</span>';
	
	return "";
}
add_shortcode('skill', 'alterna_skill_func');

//=============================
// Clients
//=============================
function alterna_clients_func($atts, $content = null){
	global $client_items;
	$client_items = array();
	
	extract( shortcode_atts( array(
			'col'		=> '6'
		  ), $atts ) );
		  
	$output = '<ul class="clients clients-col-'.$col.'">';
	
	do_shortcode($content);
	
	if(count($client_items) > 0){
		foreach($client_items as $client_item){
			$output .= '<li>'.$client_item.'</li>';
		}
	}
	
	$output .= '</ul>';
	return $output;
}
add_shortcode('clients', 'alterna_clients_func');

function alterna_client_func($atts, $content = null){
	global $client_items;
	
	extract( shortcode_atts( array(
			'title'	=> '',
			'link' => '',
			'src' => '',
			'target' => '_self'
		  ), $atts ) );
	
	$output = '';
	if($link != '') { 
		$output .= '<a href="'.$link.'" title="'.$title.'" target="'.$target.'">';
	}else{
		$output .= '<span>';
	}
	$output .= '<img src="'.$src.'" alt="">';
	if($link != '') { 
		$output .= '</a>';
	}else{
		$output .= '</span>';
	}
	$client_items[] = $output;
	
	return '';
}
add_shortcode('client', 'alterna_client_func');

//=============================
// Columns
//=============================
function alterna_space_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'line'	=> 'no',
		  'style'	=>	'solid',
		  'size'	=>	'normal'
		  ), $atts ) );
	return '<div class="row-fluid alterna-space '.$size.' '.($line =="yes" ? 'alterna-line '.$style : '').'">'.do_shortcode($content).'</div>';
}
add_shortcode('space', 'alterna_space_func');

function alterna_wide_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'id'		=>	'',
		  'style'	=> '',
		  'class'	=> ''
		  ), $atts ) );
	if($id != ""){
		$id = 'id="'.$id.'"';
	}
	return '<div '.$id.' class="row-fluid wide-background '.$class.'" style="'.$style.'">'.do_shortcode($content).'</div>';
}
add_shortcode('wide', 'alterna_wide_func');

function alterna_one_func($atts, $content = null){
	return '<div class="row-fluid">'.do_shortcode($content).'</div>';
}
add_shortcode('one', 'alterna_one_func');

function alterna_inner_one_func($atts, $content = null){
	return '<div class="row-fluid">'.do_shortcode($content).'</div>';
}
add_shortcode('inner_one', 'alterna_inner_one_func');

function alterna_one_half_func($atts, $content = null){
	return '<div class="span6">'.do_shortcode($content).'</div>';
}
add_shortcode('one_half', 'alterna_one_half_func');

function alterna_one_third_func($atts, $content = null){
	return '<div class="span4">'.do_shortcode($content).'</div>';
}
add_shortcode('one_third', 'alterna_one_third_func');

function alterna_two_third_func($atts, $content = null){
	return '<div class="span8">'.do_shortcode($content).'</div>';
}
add_shortcode('two_third', 'alterna_two_third_func');

function alterna_one_fourth_func($atts, $content = null){
	return '<div class="span3">'.do_shortcode($content).'</div>';
}
add_shortcode('one_fourth', 'alterna_one_fourth_func');

function alterna_two_fourth_func($atts, $content = null){
	return '<div class="span6">'.do_shortcode($content).'</div>';
}
add_shortcode('two_fourth', 'alterna_two_fourth_func');

function alterna_three_fourth_func($atts, $content = null){
	return '<div class="span9">'.do_shortcode($content).'</div>';
}
add_shortcode('three_fourth', 'alterna_three_fourth_func');


//=============================
// Youtube Video Player
//=============================
function alterna_youtube_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'id' => '',
		  'width' => '600',
		  'height' => '360',
		  ), $atts ) );
	if($width == "100%") {
		$out_width = 'class="full-width-show" width="600"';
	}else{
		$out_width = 'width="'.$width.'"';
	}
	
	$output = '<div class="video-youtube"><iframe title="YouTube Video Player" src="http://www.youtube.com/embed/' . $id . '?html5=1" '.$out_width.' height="' . $height . '" allowfullscreen></iframe></div>';
		
	return $output;
}
add_shortcode('youtube', 'alterna_youtube_func');

//=============================
// Vimeo Video Player
//=============================
function alterna_vimeo_func($atts, $content = null){
	extract( shortcode_atts( array(
		  'id' => '',
		  'width' => '600',
		  'height' => '360',
		  ), $atts ) );
		  
	if($width == "100%") {
		$out_width = 'class="full-width-show" width="600"';
	}else{
		$out_width = 'width="'.$width.'"';
	}
		
	$output = '<div class="video-vimeo"><iframe title="Vimeo Video Player" src="http://player.vimeo.com/video/' . $id . '" '.$out_width.' height="' . $height . '" ></iframe></div>';
		
	return $output;
}
add_shortcode('vimeo', 'alterna_vimeo_func');

//=============================
// Soundcloud Audio Player
//=============================
function alterna_soundcloud_func($atts, $content = null){
	extract( shortcode_atts( array(
		  	'url' => '',
			'iframe' => 'true',
			'width' => '100%',
			'height' => 166,
			'auto_play' => 'true',
			'show_comments' => 'true',
			'color' => 'ff7700',
			'theme_color' => 'ff6699',
		  ), $atts ) );
	
	// use iframe
	if($iframe == 'true'){
		$url = 'http://w.soundcloud.com/player?' . http_build_query($atts);
		if($width == "100%") {
			$out_width = 'class="full-width-show" width="600"';
		}else{
			$out_width = 'width="'.$width.'"';
		}
		return '<div class="sound-sl"><iframe '.$out_width.' height="'.$height.'" scrolling="no" src="'.$url.'"></iframe></div>';
	}else{
	// use flash
		$url = 'http://player.soundcloud.com/player.swf?' . http_build_query($atts);
		return '<div class="sound-sl"><object width="'.$width.'" height="'.$height.'">
                                <param name="movie" value="'.$url.'"></param>
                                <param name="allowscriptaccess" value="always"></param>
                                <embed width="'.$width.'" height="'.$height.'" src="'.$url.'" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>
                              </object></div>';
	}
	return "";
}

add_shortcode('soundcloud', 'alterna_soundcloud_func');





/*-------------------------------------------------------------
 			THEME CONTENT WITH SHORTCODE
-------------------------------------------------------------*/

//=============================
// Big content blog 
//=============================
function alterna_blog_bigcontent_func($atts, $content = null){
	extract( shortcode_atts( array(
		  	'type' => '',
			'number' => '3',
			'related_slug'	=>	'',
			'tag__in'		=>	'',
			'post__not_in'	=>	'',
			'effect'	=>	''
		  ), $atts ) );
	
	$args = array(	'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => $number,
                  );

	if($type != ''){
		$args['tax_query'] = array(	array(
								'taxonomy' => 'post_format',
								'field' => 'slug',
								'terms' => array('post-format-'.$type),
							));
	}
	if($related_slug != ""){
		$post_cats = explode("," , $related_slug);
		if(count($post_cats) != 0){
			$args['category__in'] = $post_cats;
		}
	}
	if($tag__in != ""){
		$post_tags = explode("," , $tag__in);
		if(count($post_tags) != 0){
			$args['tag__in'] = $post_tags;
		}
	}
	if($post__not_in != ""){
		$post__not_in = explode("," , $post__not_in);
		$args['post__not_in'] = $post__not_in;
	}
	
	$output = '';
				  
	$blog = new WP_Query($args);
	if($blog->have_posts()) :
		while($blog->have_posts()) : $blog->the_post();
		
			$contents = '';
			$informations = '';
			$btns = '';
			$icon_type = '';
			$content_is_null = false;
			
			if($effect != "" && $effect != "none"){
				$output .= '<div class="blog-big-widget animate" data-effect="'.esc_attr($effect).'">';
			}else{
				$output .= '<div class="blog-big-widget">';
			}
	
			
			
			switch(get_post_format()){
				case 'video': $icon_type = 'big-icon-video'; break;
				case 'audio': $icon_type = 'big-icon-music'; break;
				case 'image': $icon_type = 'big-icon-picture'; break;
				case 'gallery': $icon_type = 'big-icon-slideshow'; break;
				case 'quote': $icon_type = 'big-icon-quote'; break;
				default : $icon_type = 'big-icon-file';
			}
		
			$contents .= '<div class="blog-big-widget-element">';
			if(get_post_format() == "image"){
				if(has_post_thumbnail(get_the_ID())){
				
					$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-thumbnail');
                    $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
					
					$contents .= '<a href="'.$full_image[0].'" class="fancyBox"><div class="post-img">';
					$contents .= '<img src="'.$attachment_image[0].'" alt="'.get_the_title().'" /><div class="post-tip"><div class="bg"></div><div class="link no-bg"><i class="big-icon-preview"></i></div></div></div></a>';

				}
			}else if(get_post_format() == "gallery"){
				$contents .= '<div class="flexslider alterna-fl post-gallery"><ul class="slides">';
				
				$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-thumbnail');
				$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
				
				$contents .= '<li><a href="'.$full_image[0].'" class="fancyBox"><img src="'.$attachment_image[0].'" alt="" ></a></li>';
				$contents .= alterna_get_gallery_list(get_the_ID() ,'post-thumbnail');
                 
				$contents .= '</ul></div>';
					
			}else if(get_post_format() == "video"){
				$video_type = get_post_meta(get_the_ID(), 'video-type', true);
				$video_content 	= get_post_meta(get_the_ID(), 'video-content', true);
				if($video_content && $video_content != ''){
					if(intval($video_type) == 0){
						$contents .= do_shortcode('[youtube id="'.$video_content.'" width="100%" height="400"]');
					}else if(intval($video_type) == 1){
						$contents .= do_shortcode('[vimeo id="'.$video_content.'" width="100%" height="400"]');
					}else{
					   $contents .= $video_content;
					}
				}
			}else if(get_post_format() == "audio"){
				$audio_type 	= get_post_meta(get_the_ID(), 'audio-type', true);
				$audio_content 	= get_post_meta(get_the_ID(), 'audio-content', true);
				if($audio_content && $audio_content != ''){
				   if(intval($audio_type) == 0){
					 $contents .= do_shortcode('[soundcloud url="'.$audio_content.'"]');
				   }else{
					   $contents .= $audio_content;
				   }
				}
			}else if(get_post_format() == "quote"){
				$contents .= '<div class="post-quote-entry"><div class="post-quote-icon"></div>'.get_the_content().'</div>';
			}else {
				$content_is_null = true;
			}
			$contents .= '</div>';
			
			$informations .= '<div class="blog-big-widget-information"><div class="post-type"><i class="'.$icon_type.' "></i></div><div class="post-content"><h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
			
            $informations .= '<div class="post-meta row-fluid"><div class="post-date"><i class="icon-calendar"></i><a href="'.get_month_link(get_the_date('Y'), get_the_date('M')).'" title="'.get_the_date('M').' , '.get_the_date('Y').'">'.get_the_date().'</a></div><div class="post-category"><i class="icon-folder-open"></i><span>';
			$categories = get_the_category();
			$seperator = ' , ';
			$str = '';
			if($categories){
				foreach($categories as $category) {
					$str .= '<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s",'alterna'), $category->name ) ) . '">'.$category->cat_name.'</a>'.$seperator;
				}
				$informations .= trim($str, $seperator);
			}
			
			$num_comments = get_comments_number();
			
			if ( $num_comments == 0 ) {
				$comments = __('No Comments' , 'alterna');
			} elseif ( $num_comments > 1 ) {
				$comments = $num_comments . __(' Comments' , 'alterna');
			} else {
				$comments = __('1 Comment' , 'alterna');
			}
		
			$informations .= '</span></div><div class="post-comments"><i class="icon-comments"></i><a href="'.get_permalink(get_the_ID()).'#comments'.'">'.$comments.'</a></div></div>';
			
			$informations .= '</div><div class="post-excerpt">'.string_limit_words(get_the_excerpt(), 20).'</div><a class="more-link" href="'.get_permalink().'">'.__('Read More &raquo;','alterna').'</a></div>';

			//image,video etc conent
			if(!$content_is_null) $output .= $contents;
	
			//desc
			$output .= $informations;
			
			$output .= '</div>';
		endwhile;
	endif;
	wp_reset_postdata();
	return $output;  
}

add_shortcode('blog_bigcontent', 'alterna_blog_bigcontent_func');


//=============================
// Blog List
//=============================
function alterna_blog_list_func($atts, $content = null){
	/**
	 *	show_type 0:recent , 1:related 
	 */
	extract( shortcode_atts( array(
		  	'number' 	=> '4',
			'columns'	=>	'4',
			'show_type' => '0',
			'show_style'	=> '0',
			'related_slug'	=> '',
			'tag__in'		=>	'',
			'post__not_in'	=> '',
			'effect'		=>	''
		  ), $atts ) );
	
	switch($show_type){
		case '0': 
				$blog_posts = alterna_get_blog_widget_post('recent',$number,'',true,0,$post__not_in,$tag__in);
				break;
		case '1': 
				$blog_posts = alterna_get_blog_widget_post('related',$number,$related_slug,true,0,$post__not_in,$tag__in);
				break;
	}

	$output = '';
	if($blog_posts->have_posts()){
		
		$thumbs_size = 'post-thumbs';
		
		if($effect != "" && $effect != "none"){
			$output .= '<div class="alterna-related-posts row-fluid related-column-'.$columns.' animate-list">';
		}else{
			$output .= '<div class="alterna-related-posts row-fluid related-column-'.$columns.'">';
		}
		while($blog_posts->have_posts()) : $blog_posts->the_post();
			
			$custom = alterna_get_custom_post_meta(get_the_ID(),'portfolio');
			
			if($effect != "" && $effect != "none"){
				$output .= '<article class="shortcode-post-element span'.(12/$columns).' animate-item" data-effect="'.esc_attr($effect).'">';
			}else{
				$output .= '<article class="shortcode-post-element span'.(12/$columns).'">';
			}
			
			
				if($show_style == "0") {
					$output .= '<div class="post-img"><a href="'.get_permalink(get_the_ID()).'" >';
					if( has_post_thumbnail(get_the_ID())) {
						$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbs_size);	
						$output .= '<img src="'.$attachment_image[0].'" alt="" >';
					}else{
						 $output .= '<img src="'.get_template_directory_uri().'/img/portfolio-no-thumbs.png" alt="">' ;
					}
					$output .= '<div class="post-cover"></div><h5>'.get_the_title().'</h5></a>';
					$output	.= '<div class="date"><div class="day">'.get_the_date('d').'</div><div class="month">'.get_the_date('M').'</div><div class="year">'.get_the_date('Y').'</div></div>';
					
					$num_comments = get_comments_number(get_the_ID());

					$output .= '<div class="post-comments"><a href="'.get_permalink(get_the_ID()).'#comments'.'"><i class="icon-comments"></i>'.$num_comments.'</a></div>';
					
					$output .= '</div>';
					$output .= '<div class="post-title"><h4><a href="'.get_permalink(get_the_ID()).'" >'.get_the_title().'</a></h4></div>';
					$output .= '<div class="post-content">'.string_limit_words(get_the_excerpt()).'</div><a class="more-link" href="'.get_permalink().'">'.__('Read More &raquo;','alterna').'</a>';
				}else {
					$output .= '<div class="post-img"><a href="'.get_permalink(get_the_ID()).'" >';
					if( has_post_thumbnail(get_the_ID())) {
						$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbs_size);
						$output .= '<img src="'.$attachment_image[0].'" alt="" >';
					}else{
						 $output .= '<img src="'.get_template_directory_uri().'/img/portfolio-no-thumbs.png" alt="">' ;
					}
					$output .= '<div class="post-cover"></div><h5>'.get_the_title().'</h5></a>';
					$output	.= '<div class="date"><div class="day">'.get_the_date('d').'</div><div class="month">'.get_the_date('M').'</div><div class="year">'.get_the_date('Y').'</div></div>';
					
					$num_comments = get_comments_number(get_the_ID());

					$output .= '<div class="post-comments"><a href="'.get_permalink(get_the_ID()).'#comments'.'"><i class="icon-comments"></i>'.$num_comments.'</a></div>';
					
					$output .= '</div>';
					$output .= '<div class="post-title"><h4><a href="'.get_permalink(get_the_ID()).'" >'.get_the_title().'</a></h4></div>';
				}
			$output .= '</article>';
			
		endwhile;
		
		$output .= '</div>';
		
	}
	
	wp_reset_postdata();
	
	return $output;
	
		  
		  
}
add_shortcode('blog_list', 'alterna_blog_list_func');

//=============================
// Portfolio List
//=============================
function alterna_portfolio_list_func($atts, $content = null){
	
	/**
	 *	show_type 0:recent , 1:related 
	 */
	extract( shortcode_atts( array(
		  	'number' 	=> '4',
			'columns'	=>	'4',
			'show_type' => '0',
			'show_style'	=> '0',
			'related_slug'	=> '',
			'post__not_in' => '',
			'effect'	=>	''
		  ), $atts ) );
	
	switch($show_type){
		case '0': 
				$portfolios = alterna_get_portfolio_widget_post('recent',$number,'',true,0,$post__not_in);
				break;
		case '1': 
				$portfolios = alterna_get_portfolio_widget_post('related',$number,$related_slug,true,0,$post__not_in);
				break;
	}
	
	$output = '';
	
	if($portfolios->have_posts()){
		
		$thumbs_size	=	'portfolio-four-thumbs';
		switch(intval($columns)){
			case 2: $thumbs_size = 'portfolio-two-thumbs';break;
			case 3: $thumbs_size = 'portfolio-three-thumbs';break;
			default :
		}
		if($effect != "" && $effect != "none"){
			$output .= '<div class="alterna-related-posts row-fluid related-column-'.$columns.' animate-list">';
		}else{
			$output .= '<div class="alterna-related-posts row-fluid related-column-'.$columns.'">';
		}
		while($portfolios->have_posts()) : $portfolios->the_post();
			$custom = alterna_get_custom_post_meta(get_the_ID(),'portfolio');
			
			if($effect != "" && $effect != "none"){
				$output .= '<article class="portfolio-element portfolio-style-'.(intval($show_style)+1).' span'.(12/$columns).' animate-item" data-effect="'.esc_attr($effect).'">';
			}else{
				$output .= '<article class="portfolio-element portfolio-style-'.(intval($show_style)+1).' span'.(12/$columns).'">';
			}
			
				if(intval($show_style) == 0) {
					$output .= '<div class="portfolio-wrap">';
						 			// show gallery
								if(intval($custom['portfolio-type']) == 1) {
									$output .= '<div class="flexslider alterna-fl">';
										$output	.=	'<ul class="slides">';
														if( has_post_thumbnail(get_the_ID())) :
                                                        	$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbs_size);
                                                        	$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); 
															$output .= '<li><a href="'.$full_image[0].'" class="fancybox-thumb" rel="fancybox-thumb['.get_the_ID().']"><img src="'.$attachment_image[0].'" alt="" ></a></li>';
														endif;
															$output .= alterna_get_gallery_list(get_the_ID() ,$thumbs_size);
										$output	.=	'</ul>';
									$output .= '</div>';
									// show video with youtube or vimeo
								} else  if(intval($custom['portfolio-type']) == 2 && $custom['video-content'] != '') {
										$output .= do_shortcode('['.(intval($custom['video-type']) == 0 ? 'youtube' : 'vimeo').' id="'.$custom['video-content'].'" width="100%" height="300"]');
												
								} else {
									$output .= '<div class="portfolio-img">';
											if( has_post_thumbnail(get_the_ID())) {
												$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbs_size);
												$output .= '<img src="'.$attachment_image[0].'" alt="" >';
											}else{
												 $output .= '<img src="'.get_template_directory_uri().'/img/portfolio-no-thumbs.png" alt="">' ;
											}
									$output .= '</div>';
                                    $output .= '<div class="post-tip">';
										$output .= '<div class="bg"></div>';
										$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');		
                                       	$output .= '<div class="link left-link"><a href="'.get_permalink().'"><i class="big-icon-link"></i></a></div>';
										$output	.= '<div class="link right-link"><a href="'.$full_image[0].'" class="fancyBox"><i class="big-icon-preview"></i></a></div>';
									$output .= '</div>';
								}
								$output .= '</div>';
                                     $output .= '<div class="portfolio-content">';
                                     $output .= '<div class="portfolio-title">';
                                                	if(intval($columns) == 0) : 
                                                    $output .= '<h4><a href="'.get_permalink(get_the_ID()).'" >'.get_the_title().'</a></h4>	';										
													else :
                                                    $output .= '<h5><a href="'.get_permalink(get_the_ID()).'" >'.get_the_title().'</a></h5>';
                                                    endif;
                                                    $output .= '<span class="portfolio-categories">'.alterna_get_custom_portfolio_category_links( alterna_get_custom_post_categories(get_the_ID(),'portfolio_categories',false) , ' / ').'</span>';
									$output .= '</div>';
									$output .= '</div>';
                                            
				} else if(intval($show_style) == 1){
                    $output .= '<div class="portfolio-wrap">';
						$output .= '<a href="'.get_permalink(get_the_ID()).'" >';
								$output .= '<div class="portfolio-img">';
											if( has_post_thumbnail(get_the_ID())) {
												$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbs_size);
											$output .= '<img src="'.$attachment_image[0].'" alt="" >';
											}else{
												$output .= '<img src="'.get_template_directory_uri().'/img/portfolio-no-thumbs.png" alt="">';
											}
                                 $output .= '</div>';
                                 $output .= '<div class="post-tip">';
                                        $output .= '<div class="bg"></div>';
                                        $output .= '<div class="post-tip-info">';
                                        	$output .= '<h4>'.get_the_title().'</h4>';
                                            $output .= '<p>'.string_limit_words(get_the_excerpt());
											if($custom['portfolio-client'] != "") {
												$output .= '<span class="portfolio-client"><strong>'.__('Client: ','alterna').'</strong>'.$custom['portfolio-client'].'</span>';
											}
											$output .= '</p>';
                                         $output .= '</div>';
                                   $output .= '</div>';
                           $output .= '</a>';
                       $output .= '</div>';
                }else{
					if(has_post_thumbnail(get_the_ID())) {
        				$output .= '<a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail(get_the_ID(),$thumbs_size , array('alt' => get_the_title(),'title' => '')).'</a>';
					}
					$output .= '<div class="alterna-related-content">';
					$output .= '<h5 class="entry-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h5>';
					$output .= '</div>';
				}
				
			$output .= '</article>';
			
		endwhile;
		
		$output .= '</div>';
		
	}
	wp_reset_postdata();
	return $output;
}

add_shortcode('portfolio_list', 'alterna_portfolio_list_func');

function alterna_clean_shortcodes($content){   
    $array = array (
        '<p>[' => '[', 
        ']</p>' => ']', 
        ']<br />' => ']'
    );
    $content = strtr($content, $array);
    return $content;
}
add_filter('the_content', 'alterna_clean_shortcodes');
add_filter('widget_text', 'alterna_clean_shortcodes');

/* Visual Composer */

if (function_exists('wpb_map')){
	
	/* wide background */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Wide Background",'alterna'),
		"base" => "wide",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("class",'alterna'),
			 "param_name" => "class",
			 "value" => '',
			 "description" => __("The wide background custom css class name.",'alterna')
		  ),
		  array(
			 "type" => "textarea",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("style",'alterna'),
			 "param_name" => "style",
			 "value" => '',
			 "description" => __("Input wide background basic style.",'alterna')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* space */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Space",'alterna'),
		"base" => "space",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"admin_enqueue_css" => array(get_template_directory_uri().'/vc_extend/alterna.css'),
		"params" => array(
			   array(
				 "type" => "dropdown",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Show Line",'alterna'),
				 "param_name" => "line",
				 "value" => array('no','yes')
			  ),
			  array(
				 "type" => "dropdown",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Space Size",'alterna'),
				 "param_name" => "size",
				 "value" => array('normal','small','big')
			  ),
			  array(
				 "type" => "dropdown",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Show Line Style",'alterna'),
				 "param_name" => "style",
				 "value" => array('solid','dashed')
			  )
		  )
	));
  
	/* title */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Title",'alterna'),
		"base" => "title",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title",'alterna'),
			 "param_name" => "text",
			 "value" => __("Default title",'alterna'),
			 "description" => __("Enter title name.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Size",'alterna'),
			 "param_name" => "size",
			 "value" => 'h3',
			 "description" => __("Title Size with h1-h6.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Line",'alterna'),
			 "param_name" => "line",
			 "value" => array('true','false')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Icon",'alterna'),
			 "param_name" => "icon",
			 "value" => '',
			 "description" => __("Title with icon, here you just input icon name.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Align",'alterna'),
			 "param_name" => "align",
			 "value" => '',
			 "description" => __("Title align, here default algin is left.",'alterna')
		  )
		)
	) );
	
	/* new title */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna New Title",'alterna'),
		"base" => "newtitle",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Class",'alterna'),
				"param_name" => "class",
				"description" => __("Custom title class name.",'alterna')
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Show Title Type",'alterna'),
				"param_name" => "type",
				"value" => array('text','icon')
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Title Align",'alterna'),
				"param_name" => "align",
				"value" => array('center','left','right')
			),
		  	array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text Value",'alterna'),
				"param_name" => "value",
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text Size",'alterna'),
				"param_name" => "size",
				"value" => array('h1','h2','h3','h4','h5','h6')
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text Uppercase",'alterna'),
				"param_name" => "uppercase",
				"value" => array('yes','no')
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text Bold",'alterna'),
				"param_name" => "bold",
				"value" => array('yes','no')
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Icon Class Name",'alterna'),
				"param_name" => "icon",
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Icon Size",'alterna'),
				"param_name" => "icon_size",
				"value" => array('normal','min','big')
			),
		  	array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Icon or Text Style CSS",'alterna'),
				"param_name" => "style",
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Show Background",'alterna'),
				"param_name" => "show_bg",
				"value" => array('yes','no')
			),
		  	array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Show Line",'alterna'),
				"param_name" => "line",
				"value" => array('yes','no')
			),
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Line Align",'alterna'),
				"param_name" => "line_align",
				"value" => array('center','top','bottom')
			),
			array(
				"type" => "colorpicker",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text or Icon Color",'alterna'),
				"param_name" => "color",
				"value" => '#666666',
			),
			array(
				"type" => "colorpicker",
				"holder" => "div",
				"class" => "",
				"heading" => __("Background Color",'alterna'),
				"param_name" => "bg_color",
				"value" => '#ffffff',
			),
			array(
				"type" => "colorpicker",
				"holder" => "div",
				"class" => "",
				"heading" => __("Line Color",'alterna'),
				"param_name" => "line_color",
				"value" => '#e8e8e8',
			),
			array(
				"type" => "colorpicker",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text Color when Icon Type",'alterna'),
				"param_name" => "extra_color",
				"value" => '#666666',
			),
		)
	) );
	
	/* icon */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Icon",'alterna'),
		"base" => "icon",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Icon Name",'alterna'),
			 "param_name" => "icon_name",
			 "value" => "",
			 "description" => __("Enter icon name like icon-adjust.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Icon Size",'alterna'),
			 "param_name" => "icon_size",
			 "value" => array('', 'icon-large' , 'icon-2x', 'icon-3x', 'icon-4x'),
			 "description" => __("The icon size, default don't need input.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("The icon style",'alterna'),
			 "param_name" => "icon_style",
			 "value" => array('', 'pull-left' , 'pull-right' , 'icon-spin' , 'icon-muted' ,'icon-border' ),
			 "description" => __("The icon style, default don't need input.",'alterna')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Icon",'alterna'),
			 "param_name" => "icon_color",
			 "value" => '#ff0000',
			 "description" => __("The icon default color.",'alterna')
		  )
		)
	) );
	
	/* google map */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Map",'alterna'),
		"base" => "map",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Map ID",'alterna'),
			 "param_name" => "id",
			 "value" => ""
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("LatLng",'alterna'),
			 "param_name" => "latlng",
			 "value" => '',
			 "description" => __("The map LatLng value from google map.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Map Height",'alterna'),
			 "param_name" => "height",
			 "value" => '200',
			 "description" => __("The map show height.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Map Width",'alterna'),
			 "param_name" => "width",
			 "value" => '300',
			 "description" => __("The map show width.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Map Zoom",'alterna'),
			 "param_name" => "zoom",
			 "value" => '13',
			 "description" => __("The map default show zoom.",'alterna')
		  ),
		   array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Draggable",'alterna'),
			 "param_name" => "draggable",
			 "value" => array('yes' ,'no' ),
			 "description" => __("The map can drag.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Scroll Wheel",'alterna'),
			 "param_name" => "scrollwheel",
			 "value" => array('yes' ,'no' ),
			 "description" => __("The map can scroll wheel zoom.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Marker",'alterna'),
			 "param_name" => "show_marker",
			 "value" => array('yes' ,'no' ),
			 "description" => __("The map show marker.",'alterna')
		  ),
		   array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Address Information",'alterna'),
			 "param_name" => "show_info",
			 "value" => array('yes' ,'no' ),
			 "description" => __("The map show info box of address.",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Information Width",'alterna'),
			 "param_name" => "info_width",
			 "value" => '260',
			 "description" => __("The map info address box width.",'alterna')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>MAP ADDRESS TEXT.</p>",'alterna'),
			 "description" => __("Enter your address.",'alterna')
		  )
		)
	) );
	
	/* buttons */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Button",'alterna'),
		"base" => "button",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Title",'alterna'),
			 "param_name" => "text",
			 "value" => __("Button",'alterna'),
			 "description" => __("Enter button title.",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Icon Name",'alterna'),
			 "param_name" => "icon",
			 "value" => 'icon-flag',
			 "description" => __("Enter icon use fontawesome",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Type",'alterna'),
			 "param_name" => "type",
			 "value" => 'btn-custom',
			 "description" => __("Enter button type like btn-custom, btn-primary , btn-info , btn-success , btn-warning , btn-danger , btn-inverse , btn-link , disabled.",'alterna')
		  ),
		   array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Size",'alterna'),
			 "param_name" => "size",
			 "value" => array('', 'btn-large' , 'btn-small' , 'btn-mini'),
			 "description" => __("Button size, default don't need input.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Link",'alterna'),
			 "param_name" => "url",
			 "value" => '',
			 "description" => __("Click button open link.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Link Target",'alterna'),
			 "param_name" => "target",
			 "value" => array('_self', '_blank'),
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Background Color",'alterna'),
			 "param_name" => "bg_color",
			 "value" => '#7AB80E',
			 "description" => __("The button background color.",'alterna')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Background Hover Color",'alterna'),
			 "param_name" => "bg_hover_color",
			 "value" => '#559500',
			 "description" => __("The button background hover color .",'alterna')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Font Color",'alterna'),
			 "param_name" => "txt_color",
			 "value" => '#ffffff',
			 "description" => __("The button font color.",'alterna')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Button Font Hover Color",'alterna'),
			 "param_name" => "txt_hover_color",
			 "value" => '#000000',
			 "description" => __("The button font hover color .",'alterna')
		  )
		 )
	) );
	
	/* alert message */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Alert Message",'alterna'),
		"base" => "alert",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Alert Message Type",'alterna'),
			 "param_name" => "type",
			 "value" => array('success' , 'danger' , 'error' , 'info'),
			 "description" => __("The alert message type.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Alert Message Close",'alterna'),
			 "param_name" => "close",
			 "value" => array('yes' , 'no'),
			 "description" => __("The alert can been close.",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Alert Message Title",'alterna'),
			 "param_name" => "title",
			 "value" => '',
			 "description" => __("The alert title.",'alterna')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input alert message content.</p>",'alterna')
		  )
		 )
	));
	
	/* dropcap */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Dropcap",'alterna'),
		"base" => "dropcap",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Alert Dropcap Text",'alterna'),
			 "param_name" => "text",
			 "value" => '1',
			 "description" => __("The dropcap text.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Alert Dropcap Type",'alterna'),
			 "param_name" => "type",
			 "value" => array('default','text'),
			 "description" => __("The dropcap type , default is text with background",'alterna')
		  ),
		   array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Text Color",'alterna'),
			 "param_name" => "txt_color",
			 "value" => '#ffffff',
			 "description" => __("The dropcap text color.",'alterna')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Background Color",'alterna'),
			 "param_name" => "bg_color",
			 "value" => '#000000',
			 "description" => __("The dropcap background color.",'alterna')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* blockquote */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Blockquote",'alterna'),
		"base" => "blockquote",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		   array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Border Color",'alterna'),
			 "param_name" => "border_color",
			 "value" => '#eeeeee',
			 "description" => __("The blockquote border color.",'alterna')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Background Color",'alterna'),
			 "param_name" => "bg_color",
			 "value" => '#ffffff',
			 "description" => __("The blockquote background color.",'alterna')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* toggle */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Toggle",'alterna'),
		"base" => "toggle",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Toggle ID",'alterna'),
			 "param_name" => "id",
			 "value" => '',
			 "description" => __("The toggle unique id.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Toggle Title",'alterna'),
			 "param_name" => "title",
			 "value" => __("Input toggle title.",'alterna'),
			 "description" => __("The toggle title.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Toggle Open",'alterna'),
			 "param_name" => "open",
			 "value" => array('yes','no'),
			 "description" => __("The toggle content default show or hide.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* service */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Services",'alterna'),
		"base" => "services",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Type",'alterna'),
			 "param_name" => "type",
			 "value" => array('icon','image'),
			 "description" => __("The service show icon type.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Icon/Image src",'alterna'),
			 "param_name" => "src",
			 "value" => '',
			 "description" => __("Input icon name or image src.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Show Background",'alterna'),
			 "param_name" => "show_bg",
			 "value" => array('yes','no'),
			 "description" => __("The service show background.",'alterna')
		  ),
		   array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Show Border",'alterna'),
			 "param_name" => "border",
			 "value" => array('yes','no'),
			 "description" => __("The service show border.",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Box Link",'alterna'),
			 "param_name" => "link",
			 "value" => ''
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* new service */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna New Services",'alterna'),
		"base" => "newservices",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Type",'alterna'),
			 "param_name" => "layout",
			 "value" => array('left','center'),
			 "description" => __("The service show icon type.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Icon",'alterna'),
			 "param_name" => "icon",
			 "value" => '',
			 "description" => __("Input icon name use fontawesome.",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Service Title",'alterna'),
			 "param_name" => "title",
			 "value" => ''
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* features */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Features",'alterna'),
		"base" => "features",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Feature Type",'alterna'),
			 "param_name" => "type",
			 "value" => array('icon','image'),
			 "description" => __("The feature show icon type.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Feature Icon/Image src",'alterna'),
			 "param_name" => "src",
			 "value" => '',
			 "description" => __("Input icon name or image src.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Feature Title",'alterna'),
			 "param_name" => "title",
			 "value" => __("Feature Title",'alterna'),
			 "description" => __("Input icon name or image src.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Feature Show Background",'alterna'),
			 "param_name" => "show_bg",
			 "value" => array('yes','no'),
			 "description" => __("The feature show background.",'alterna')
		  ),
		   array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Feature Show Border",'alterna'),
			 "param_name" => "border",
			 "value" => array('yes','no'),
			 "description" => __("The feature show border.",'alterna')
		  ),
		   array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Feature Box Link",'alterna'),
			 "param_name" => "link",
			 "value" => ''
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	
	/* history */
	/*
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna History",'alterna'),
		"base" => "history",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Date",'alterna'),
			 "param_name" => "day",
			 "value" => '',
			 "description" => __("Input date like Jan 11, 2013.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("History Title",'alterna'),
			 "param_name" => "title",
			 "value" => ''
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("History image url",'alterna'),
			 "param_name" => "src",
			 "value" => ''
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Image Width",'alterna'),
			 "param_name" => "width",
			 "value" => '',
			 "description" => __("Setting image width.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Start Point",'alterna'),
			 "param_name" => "start",
			 "value" => array('no','yes')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Content",'alterna'),
			 "param_name" => "content",
			 "value" => __("<p>Input content.</p>",'alterna')
		  )
		 )
	));
	*/
	
	/* big blog style posts */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Blog Posts (Big Style)",'alterna'),
		"base" => "blog_bigcontent",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Post Type",'alterna'),
			 "param_name" => "type",
			 "value" => array('','gallery','image','quote','video','audio'),
			 "description" => __("Show post format type, default all.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Posts Number",'alterna'),
			 "param_name" => "number",
			 "value" => '3',
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  )
		 )
	));
	
	/* blog list */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Blog List",'alterna'),
		"base" => "blog_list",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Posts Number",'alterna'),
			 "param_name" => "number",
			 "value" => '3',
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Columns",'alterna'),
			 "param_name" => "columns",
			 "value" => '3',
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Type",'alterna'),
			 "param_name" => "show_type",
			 "value" => array('0','1'),
			 "description" => __("Show item type, 0: recent; 1: related.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Style",'alterna'),
			 "param_name" => "show_style",
			 "value" => array('0','1'),
			 "description" => __("Show item style.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Related Category",'alterna'),
			 "param_name" => "related_slug",
			 "value" => '',
			 "description" => __("Show related portfolio category slug items use \",\".",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Posts Except Them",'alterna'),
			 "param_name" => "post__not_in",
			 "value" => '',
			 "description" => __("Show related items, except post id.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  )
		 )
	));
	
	/* portfolio list */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Portfolio List",'alterna'),
		"base" => "portfolio_list",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Posts Number",'alterna'),
			 "param_name" => "number",
			 "value" => '3',
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Columns",'alterna'),
			 "param_name" => "columns",
			 "value" => '3',
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Type",'alterna'),
			 "param_name" => "show_type",
			 "value" => array('0','1'),
			 "description" => __("Show item type, 0: recent; 1: related.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Style",'alterna'),
			 "param_name" => "show_style",
			 "value" => array('0','1'),
			 "description" => __("Show item style.",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Related Category",'alterna'),
			 "param_name" => "related_slug",
			 "value" => '',
			 "description" => __("Show related portfolio category slug items use \",\".",'alterna')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Posts Except Them",'alterna'),
			 "param_name" => "post__not_in",
			 "value" => '',
			 "description" => __("Show related items, except post id.",'alterna')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Element show with effect",'alterna'),
			 "param_name" => "effect",
			 "value" => $effect_list
		  )
		 )
	));
	
	/* youtube */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Youtube",'alterna'),
		"base" => "youtube",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		 	array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Youtube ID",'alterna'),
			 "param_name" => "id",
			 "value" => '',
			 "description" => __("Enter video ID (eg.6htyfxPkYDU).",'alterna')
		  	),
			array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Width",'alterna'),
			 "param_name" => "width",
			 "value" => '600',
			 "description" => __("Youtube default show width.",'alterna')
		  	),
			array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Height",'alterna'),
			 "param_name" => "height",
			 "value" => '360',
			 "description" => __("Youtube default show width.",'alterna')
		  	)
		)
	));
	
	/* vimeo */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Vimeo",'alterna'),
		"base" => "vimeo",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		 	array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Vimeo ID",'alterna'),
			 "param_name" => "id",
			 "value" => '',
			 "description" => __("Enter video ID (eg.54578415).",'alterna')
		  	),
			array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Width",'alterna'),
			 "param_name" => "width",
			 "value" => '600',
			 "description" => __("Vimeo default show width.",'alterna')
		  	),
			array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Height",'alterna'),
			 "param_name" => "height",
			 "value" => '360',
			 "description" => __("Vimeo default show width.",'alterna')
		  	)
		)
	));
	
	/* Soundcloud */
	wpb_map( array(
		"icon" => "icon-wpb-alterna",
		"name" => __("Alterna Soundcloud",'alterna'),
		"base" => "soundcloud",
		"class" => "",
		"controls" => "full",
		"category" => __('Alterna Content','alterna'),
		"params" => array(
		 	array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Soundcloud URL",'alterna'),
			 "param_name" => "url",
			 "value" => '',
			 "description" => __("Enter soundcloud url like http://api.soundcloud.com/tracks/38987054.",'alterna')
		  	),
			array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show With iframe",'alterna'),
			 "param_name" => "iframe",
			 "value" => array('true','false')
		  	),
			array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Width",'alterna'),
			 "param_name" => "width",
			 "value" => '100%',
			 "description" => __("Soundcloud default show width.",'alterna')
		  	),
			array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Height",'alterna'),
			 "param_name" => "height",
			 "value" => '166',
			 "description" => __("Soundcloud default show width.",'alterna')
		  	),
			array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Auto Play",'alterna'),
			 "param_name" => "auto_play",
			 "value" => array('true','false')
		  	),
			array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Show Comments",'alterna'),
			 "param_name" => "show_comments",
			 "value" => array('true','false')
		  	),
			array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Color",'alterna'),
			 "param_name" => "color",
			 "value" => '#ff7700'
			),
			array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Theme Color",'alterna'),
			 "param_name" => "theme_color",
			 "value" => '#ff6699'
			)
		)
	));
}