<?php

//========================================================
//  	THEME METHODS
//========================================================

/**
 * Display navigation to pagination pages numbers when applicable
 *
 * @since alterna 1.0
 */
function alterna_content_pagination($pagination_id, $pagination_class  = '' , $max_show_number = 2 , $query = '') {
	global $wp_query;
	if($query == '') $query = $wp_query;

	if ( $query->max_num_pages > 1 ) {
		// get the current page
		$paged = 1;
		if (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} else if (get_query_var('page')) {
			$paged = get_query_var('page');
		}
		
		?>
        <div id="<?php echo $pagination_id; ?>" class="pagination <?php echo $pagination_class; ?>">
            <ul>
            <?php
                $max_number = $query->max_num_pages;
                //prev button
                if($paged > 1){
					echo '<li><a href="'. get_pagenum_link($paged-1) .'">'.__('&laquo','alterna').'</a></li>';
					if($paged - $max_show_number > 1) echo '<li><a href="'. get_pagenum_link(1) .'">1</a></li>';
				}
				
                if($paged - $max_show_number > 2) echo  '<li><span>...</span></li>';
				
                for($k= $paged - $max_show_number; $k <= ($paged+$max_show_number) & $k <= $max_number; $k++){
                    if($k < 1) continue;
                    if($k == $paged) 
                        echo '<li><span class="disabled">'.$k.'</span></li>';
                    else
                        echo '<li><a href="'.get_pagenum_link( $k).'">'.$k.'</a></li>';
                }
                if($paged + $max_show_number < $max_number) {
                     if($paged + $max_show_number < ($max_number-1)) echo  '<li><span>...</span></li>';
                     echo '<li><a href="'.get_pagenum_link( $max_number ).'">'.$max_number.'</a></li>';
                }
                //next button
                if($paged < $max_number) echo '<li><a href="'.get_pagenum_link($paged+1).'">'.__('&raquo;','alterna').'</a></li>';
				
            ?>
            </ul>
         </div>
        <?php
	}
}

/**
 * get show have no menu information
 *
 * @since alterna 1.0
 */
function alterna_show_setting_primary_menu(){
	echo '<h5 style="float:left;color: #ffffff;margin-left: 10px;line-height: 20px;">'.__('Please open Admin -&gt; Appearance -&gt; Menus Setting','alterna').'</h5>';
}

function alterna_show_setting_topbar_menu(){
	echo __('Please open Admin -&gt; Appearance -&gt; Menus Setting','alterna');
}



/**
 * get Custom Font For google font	
 *
 * @since alterna 1.0
 */
function alterna_get_custom_font(){
	global $alterna_options,$google_fonts,$google_load_fonts,$google_custom_fonts;
	
	$google_load_fonts = "";
	$google_custom_fonts = array();
	
 	$general_font 				= 'Source Sans Pro';
	$general_font_size 			= '14px';
	$menu_font					= 'Oswald';
	$menu_font_size				= '13px';
	$title_font					= 'Open Sans';
	
	$font_names = array();
	
	if(alterna_get_options_key('custom-enable-font') == "on"){

		$array = explode("|",$google_fonts);
		
		if( alterna_get_options_key('custom-general-font') !="0"){
			$font_name = $array[intval($alterna_options['custom-general-font'])-1];
			$general_font = alterna_get_current_font_name($font_name);
			array_push($font_names,$font_name.':'.alterna_get_options_key('custom-general-font-weight'));
		}else{
			array_push($font_names,'Source+Sans+Pro:400,400italic,700,700italic');
		}
			
		if( alterna_get_options_key('custom-menu-font') !="0"){
			$font_name = $array[intval($alterna_options['custom-menu-font'])-1];
			$menu_font = alterna_get_current_font_name($font_name);
			array_push($font_names,$font_name.':'.alterna_get_options_key('custom-menu-font-weight'));
		}else{
			array_push($font_names,'Oswald:400');
		}
		
		if( alterna_get_options_key('custom-title-font') !="0"){
			$font_name = $array[intval($alterna_options['custom-title-font'])-1];
			$title_font = alterna_get_current_font_name($font_name);
			array_push($font_names,$font_name.':'.alterna_get_options_key('custom-title-font-weight'));
		}else{
			array_push($font_names,'Open+Sans:400,400italic,300,300italic,700,700italic');
		}
	}else{
		array_push($font_names,'Source+Sans+Pro:400,400italic,700,700italic');
		array_push($font_names,'Oswald:400');
		array_push($font_names,'Open+Sans:400,400italic,300,300italic,700,700italic');
	}

	$google_custom_fonts['general_font']				= $general_font;
	$google_custom_fonts['menu_font']					= $menu_font;
	$google_custom_fonts['title_font']					= $title_font;
	
	$google_load_fonts = implode("|",array_unique($font_names));
	
}

/**
 * Get current font name
 *
 * @since Alterna 1.0
 */
function alterna_get_current_font_name($font_name){
	$arr = explode(":", str_replace("+"," ",$font_name) );
	return $arr[0];
}

/**
 * Display navigation to next/previous pages when applicable
 *
 * @since alterna 1.0
 */
function alterna_content_nav( $nav_id ,$nav_class = '' ) {
	global $wp_query;
	if ( $wp_query->max_num_pages > 1 ) : ?>
        <nav id="<?php echo $nav_id; ?>"  class="posts-nav <?php echo $nav_class; ?>">
			<div class="nav-prev"><?php next_posts_link( __( '&larr; Older posts', 'alterna' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'alterna' ) ); ?>
		</nav>
	<?php endif;
}

/**
 * Display single post navigation to next/previous post when applicable
 *
 * @since alterna 1.0
 */
function alterna_single_content_nav( $nav_id ,$nav_class = '' ) {
?>
	<nav class="single-pagination row-fluid">
		<?php previous_post_link('%link', __('&laquo; %title', 'alterna')); ?>
		<?php next_post_link('%link ', __('%title &raquo;', 'alterna')); ?>
	</nav>
<?php
}

function alterna_get_currect_value($value){
	if($value == "on" || ($value != "off" && intval($value) == 0)){
		return true;
	}
	return false;
}

/**
 * Get Page Layout
 * @since alterna 1.0
 */
function alterna_get_page_layout($id = "-1") {
	if($id != 'global'){
		$layout = intval(get_post_meta($id != "-1" ? $id : get_the_ID(), 'layout-type', true));
		if($layout != 0) return $layout;
	}
	switch(intval(alterna_get_options_key('global-sidebar-layout'))){
		case 1: return 3;
		case 2: return 1;
	}
	return 2;
}

/**
 * Get Page Header Links
 *
 * @since alterna 1.0
 */
function alterna_page_links() {
	
	$output = '';
	// page is not front page show first link with "home" page;
    if( !is_front_page() ) {
       $output .= '<li><i class="icon-home"></i><a href="'.home_url().'" title="'.__('Home','alterna').'">'.__('HOME','alterna').'</a></li>';
    }
    
	// page is used home page as posts
	if((is_home() || is_category() || is_tag() || is_date() || is_single()) && !is_front_page()){
		
		$single_type = get_post_type(get_the_ID());
		
		if(is_single() && $single_type == "portfolio") {
			global $portfolio_default_page_id;
		
			// show default portfolio page
			$portfolio_default_page_id  = alterna_get_default_portfolio_page();
			$portfolio_page = get_page( $portfolio_default_page_id );
			
			$output .= '<li><i class="icon-chevron-right"></i><a href="'.get_permalink($portfolio_default_page_id).'" title="'.$portfolio_page->post_title.'">'.$portfolio_page->post_title.'</a></li>';
		} else {
			if(intval(get_option('page_for_posts')) > 0) {
				$page = get_page( get_option('page_for_posts') );
				$output .= '<li><i class="icon-chevron-right"></i><a href="'.get_permalink(get_option('page_for_posts')).'" title="'.$page->post_title.'">'.$page->post_title.'</a></li>';
			}
		}
	}
	
	// page is category
	if(is_category()){
		$cat = get_category( get_query_var( 'cat' ) );
		$output .= '<li><i class="icon-chevron-right"></i><span>'.__('Category Archive for "','alterna').$cat->name.'"</span></li>';
	}
	
	// show portfolio category link
	if(taxonomy_exists('portfolio_categories') && is_tax()) {
		global $alterna_options,$term,$portfolio_default_page_id;
		
		// show default portfolio page
		$portfolio_default_page_id  = alterna_get_default_portfolio_page();
		$portfolio_page = get_page( $portfolio_default_page_id );
		
		$output .= '<li><i class="icon-chevron-right"></i><a href="'.get_permalink($portfolio_default_page_id).'" title="'.$portfolio_page->post_title.'">'.$portfolio_page->post_title.'</a></li>';
		// show category name
		$output .= '<li><i class="icon-chevron-right"></i><span>'.$term->name.'</span></li>';
	}
	
	// show page title
	if(is_page() || is_single()){
		global $post;
		if ( is_page() && $post->post_parent ) {
      		$parent_id  = $post->post_parent;
      		$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<li><i class="icon-chevron-right"></i><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				$output .= $breadcrumbs[$i];
			}
    	}
		
		//add category link for post
		if(is_single()){
			if( is_singular('post') && alterna_get_options_key('blog-enable-breadchrumb') == 'on'){
				$categories = get_the_category();
				if($categories){
					$cat_first = true;
					$output .= '<li><i class="icon-chevron-right"></i>';
					foreach($categories as $category) {
						if(!$cat_first){ $output .= ' , ';}
						$output .= '<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s",'alterna'), $category->name ) ) . '">'.$category->cat_name.'</a>';
						$cat_first = false;
					}
					$output .= '</li>';
				}
			}else if( is_singular('portfolio') && alterna_get_options_key('portfolio-enable-breadchrumb') == 'on'){
				$categories = alterna_get_custom_portfolio_category_links( alterna_get_custom_post_categories(get_the_ID(),'portfolio_categories',false)  , ' , ');
				if($categories != ""){ $output .= '<li><i class="icon-chevron-right"></i>'.$categories.'</li>'; }
			}
		}
		
		$output .= '<li><i class="icon-chevron-right"></i><span>'.get_the_title().'</span></li>';
	}
	
	// tag page
	if(is_tag()) {

		$output .= '<li><i class="icon-chevron-right"></i><span>'.__('Posts Tagged "','alterna').single_tag_title('', false).'"</span></li>';
	}
	
	// search page
	if(is_search()) {
		//$output .= '<li><i class="icon-chevron-right"></i><span>'.__('Search' , 'alterna').'</span></li>';
	}
	
	// 404 page
	if(is_404()){
		$output .= '<li><i class="icon-chevron-right"></i><span>'.__('404 Error' , 'alterna').'</span></li>';
	}
	
	// date page
	if(is_date()){
		$output .= '<li><i class="icon-chevron-right"></i><span>'.__('Date Archives for "','alterna').get_the_time('Y-M').'"</span></li>';
	}
	
	// author page
	if(is_author()){
		global $author_name;
		$output .= '<li><i class="icon-chevron-right"></i><span>'.__('Author "','alterna').$author_name.'"</span></li>';
	}
	return $output;
}

/**
 * Get Page Title
 *
 * @since alterna 1.0
 */
function alterna_page_title(){
	$output = '';
	
	// category page
	if(is_category()) $output = single_cat_title('', false);
	
	// tag page
	if(is_tag()) $output = single_tag_title('', false);
	
	// search page
	if(is_search()) $output = __('Search' , 'alterna');
	
	// 404 page
	if(is_404()) $output = __('Page Not Found' , 'alterna');
	
	// date page
	if(is_date())  $output = get_the_time('Y-M');
	
	// author page
	if(is_author()) $output = __('Author' , 'alterna');
	
	if(taxonomy_exists('portfolio_categories') && is_tax()) {
		global $term;
		$output = $term->name;
	}
	
	return $output;
}

/**
 * Get Page Custom Options CSS
 *
 * @since alterna 4.0
 */
function alterna_get_page_custom_options_css(){
	if((class_exists( 'woocommerce') && is_shop()) || is_page() || is_single() || is_home() || is_front_page()){
		
		if(class_exists( 'woocommerce') && is_shop()){
			$post_id = woocommerce_get_page_id( 'shop');
		}else if(is_home() && !is_front_page() && intval(get_option('page_for_posts')) > 0){
			$post_id = get_option('page_for_posts');
		}else{
			$post_id = get_the_ID();
		}
		
		$page_bg_type 				= intval(get_post_meta($post_id , 'page-background-type', true));
		$page_content_bg_type 		= intval(get_post_meta($post_id , 'page-content-background-type', true));
		$page_header_bg_type 		= intval(get_post_meta($post_id , 'page-header-area-background-type', true));
		$page_title_bg_type 		= intval(get_post_meta($post_id , 'page-title-background-type', true));
		$page_title_align			= intval(get_post_meta($post_id , 'title-align', true));
		
		if($page_bg_type != 0 || $page_content_bg_type != 0 || $page_header_bg_type != 0 || $page_title_bg_type != 0 || $page_title_align != 0 || alterna_get_post_meta_key('post-css-style',$post_id) != ""){

		?>
        <style id="alterna-custom-page-css" type="text/css">
			<?php if($page_bg_type == 1 && intval(get_post_meta($post_id , 'page-background-width', true)) != 0 && intval(get_post_meta($post_id , 'page-background-height', true)) != 0 && get_post_meta($post_id , 'page-background-pattern', true) != "") { ?>
				body {
					background-size:<?php echo get_post_meta($post_id , 'page-background-width', true);?>px <?php echo get_post_meta($post_id , 'page-background-height', true);?>px;
					background-repeat: repeat;
					background-image:url(<?php echo get_post_meta($post_id , 'page-background-pattern', true);?>);
				 }
			<?php } else if($page_bg_type == 2 && get_post_meta($post_id , 'page-background-img', true) != "") {?>
				body { 
					background: url(<?php echo get_post_meta($post_id , 'page-background-img', true); ?>) no-repeat center center fixed; 
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					background-size: cover;
				}
			<?php } else if($page_bg_type == 3) { 
				$color = str_replace("#","",alterna_get_post_meta_key('page-background-color', $post_id));
			?>
				body {background:#<?php echo $color; ?>;}
			<?php } ?>
			
			<?php if($page_content_bg_type == 1 && intval(get_post_meta($post_id , 'page-content-background-width', true)) != 0 && intval(get_post_meta($post_id , 'page-content-background-height', true)) != 0 && get_post_meta($post_id , 'page-content-background-pattern', true) != "") { ?>
				.content-wrap {
					background-size:<?php echo get_post_meta($post_id , 'page-content-background-width', true);?>px <?php echo get_post_meta($post_id , 'page-content-background-height', true);?>px;
					background-repeat: repeat;
					background-image:url(<?php echo get_post_meta($post_id , 'page-content-background-pattern', true);?>);
				 }
			<?php } else if($page_content_bg_type == 2 && get_post_meta($post_id , 'page-content-background-img', true) != "") {?>
				.content-wrap { 
					background: url(<?php echo get_post_meta($post_id , 'page-content-background-img', true); ?>) no-repeat center center fixed; 
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					background-size: cover;
				}
			<?php } else if($page_content_bg_type == 3) { 
				$color = str_replace("#","",alterna_get_post_meta_key('page-content-background-color', $post_id));
			?>
				.content-wrap {background:#<?php echo $color; ?>;}
			<?php } ?>
			
			<?php if($page_header_bg_type == 1 && intval(get_post_meta($post_id , 'page-header-area-background-width', true)) != 0 && intval(get_post_meta($post_id , 'page-header-area-background-height', true)) != 0 && get_post_meta($post_id , 'page-header-area-background-pattern', true) != "") { ?>
				#alterna-header {
					background-size:<?php echo get_post_meta($post_id , 'page-header-area-background-width', true);?>px <?php echo get_post_meta($post_id , 'page-header-area-background-height', true);?>px;
					background-repeat: repeat;
					background-image:url(<?php echo get_post_meta($post_id , 'page-header-area-background-pattern', true);?>);
				 }
			<?php } else if($page_header_bg_type == 2 && get_post_meta($post_id , 'page-header-area-background-img', true) != "") {?>
				#alterna-header { 
					background: url(<?php echo get_post_meta($post_id , 'page-header-area-background-img', true); ?>) repeat center center scroll; 
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					background-size: cover;
				}
			<?php } else if($page_header_bg_type == 3){ 
				$color = str_replace("#","",alterna_get_post_meta_key('page-header-area-background-color', $post_id));
			?>
				#alterna-header {background:#<?php echo $color;?>	}
			<?php } ?>
			
			<?php if($page_title_bg_type == 1 && intval(get_post_meta($post_id , 'page-title-background-width', true)) != 0 && intval(get_post_meta($post_id , 'page-title-background-height', true)) != 0 && get_post_meta($post_id , 'page-title-background-pattern', true) != "") { ?>
				#page-header {
					background-size:<?php echo get_post_meta($post_id , 'page-title-background-width', true);?>px <?php echo get_post_meta($post_id , 'page-title-background-height', true);?>px;
					background-repeat: repeat;
					background-image:url(<?php echo get_post_meta($post_id , 'page-title-background-pattern', true);?>);
				 }
			<?php } else if($page_title_bg_type == 2 && get_post_meta($post_id , 'page-title-background-img', true) != "") {?>
				#page-header { 
					background: url(<?php echo get_post_meta($post_id , 'page-title-background-img', true); ?>) repeat center center scroll; 
					-webkit-background-size: cover;
					-moz-background-size: cover;
					-o-background-size: cover;
					background-size: cover;
				}
			<?php } else if($page_title_bg_type == 3){ 
				$color = str_replace("#","",alterna_get_post_meta_key('page-title-background-color', $post_id));
			?>
				#page-header {background:#<?php echo $color; ?>;	}
			<?php } ?>
			
			<?php if($page_title_align != 0) { ?>
				#page-header .title {text-align:<?php echo $page_title_align == 1 ? 'center' : 'right'; ?>;}
			<?php } ?>
			<?php 
			if(alterna_get_post_meta_key('post-css-style',$post_id) != ""){
				echo alterna_get_post_meta_key('post-css-style',$post_id);
			}
			?>
			
			<?php if($page_bg_type == 1 || $page_content_bg_type == 1|| $page_header_bg_type == 1 || $page_title_bg_type == 1 || alterna_get_post_meta_key('post-css-retina-style',$post_id)) { ?>
			 @media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
				only screen and (-moz-min-device-pixel-ratio: 1.5),
				only screen and (-o-min-device-pixel-ratio: 3/2),
				only screen and (min-device-pixel-ratio: 1.5) {
					<?php if($page_bg_type == 1 && get_post_meta($post_id , 'page-background-retina-pattern', true) != ""){ ?>
						body { background-image:url(<?php echo get_post_meta($post_id , 'page-background-retina-pattern', true);?>); }
					<?php } ?>
					<?php if($page_content_bg_type == 1 && get_post_meta($post_id , 'page-content-background-retina-pattern', true) != ""){ ?>
						.content-wrap { background-image:url(<?php echo get_post_meta($post_id , 'page-content-background-retina-pattern', true);?>); }
					<?php } ?>
					<?php if($page_header_bg_type == 1 && get_post_meta($post_id , 'page-header-area-background-retina-pattern', true) != "") { ?>
						#alterna-header { background-image:url(<?php echo get_post_meta($post_id , 'page-header-area-background-retina-pattern', true);?>); }
					<?php } ?>
					<?php if($page_title_bg_type == 1 && get_post_meta($post_id , 'page-title-background-retina-pattern', true) != "") { ?>
						#page-header { background-image:url(<?php echo get_post_meta($post_id , 'page-title-background-retina-pattern', true);?>); }
					<?php } 
					
					if(alterna_get_post_meta_key('post-css-retina-style',$post_id) != ""){
						echo alterna_get_post_meta_key('post-css-retina-style',$post_id);
					}
					
					?>
				}
			<?php } ?>
		</style>
        <?php 
		}
	}
}

if ( ! function_exists( 'alterna_get_page_custom_options_scripts' ) ) :
/**
 * Get Page Custom Scripts
 *
 * @since Alterna 6.0
 */
function alterna_get_page_custom_options_scripts(){
	if((class_exists( 'woocommerce') && is_shop()) || is_page() || is_single() || is_home() || is_front_page()){
		if(class_exists( 'woocommerce') && is_shop()){
			$post_id = woocommerce_get_page_id( 'shop');
		}else if(is_home() && !is_front_page() && intval(get_option('page_for_posts')) > 0){
			$post_id = get_option('page_for_posts');
		}else{
			$post_id = get_the_ID();
		}
		if(alterna_get_post_meta_key('post-custom-scripts',$post_id) != ""){
			return alterna_get_post_meta_key('post-custom-scripts',$post_id);
		}
	}
	return '';
}
endif;

/**
 * Get All Portfolio Type Pages
 *
 * @since alterna 1.0
 */
function alterna_get_all_portfolio_type_pages($re_id = false) {
	$portfolio_pages = array();
	
	$p_types = array('page-portfolio.php', 'page-portfolio-ajax.php');
	
	foreach($p_types as $p_type){
		$args = array(
			'meta_key' => '_wp_page_template',
			'meta_value' => $p_type,
			'post_type' => 'page',
			'post_status' => 'publish',
			'sort_order' => 'ASC',
			'sort_column' => 'ID',
		); 
		$pages = get_pages($args); 
		if(!empty($pages)) {
			foreach($pages as $page){
				if($re_id){
					$portfolio_pages[] = $page->ID;
				}else {
					$portfolio_pages[$page->ID] = $page->post_title;
				}
			}
		}
	}
	
	return $portfolio_pages;
}

/**
 * Get Default Portfolio Page
 *
 * @since alterna 1.0
 */
function alterna_get_default_portfolio_page() {
	global $portfolio_default_page_id;
	
	$default_page_id = intval( alterna_get_options_key('portfolio-default-page') );
	
	$pages = alterna_get_all_portfolio_type_pages(true);

	if(isset($pages[$default_page_id])) {
		$default_page_id = $pages[$default_page_id];
		$template = get_post_meta( $default_page_id , '_wp_page_template', true );
		if($template == 'page-portfolio.php' || $template == 'page-portfolio-ajax.php' ) {
			$portfolio_default_page_id = $default_page_id;
			return $portfolio_default_page_id;
		}
		
	}

	foreach($pages as $key=>$value){
		$portfolio_default_page_id = $key;
		break;
	}
	return $portfolio_default_page_id;
}

/**
 * Get All Waterfall Flux Blog Type Pages
 *
 * @since alterna 2.2
 */
function alterna_get_all_blog_waterfall_type_pages($re_id = false) {
	$blog_waterfall_pages = array();
	
	$args = array(
		'meta_key' => '_wp_page_template',
		'meta_value' => 'page-blog-ajax.php',
		'post_type' => 'page',
		'post_status' => 'publish',
		'sort_order' => 'ASC',
		'sort_column' => 'ID',
	); 
	$b_pages = get_pages($args); 
	
	if(!empty($b_pages)) {
		foreach($b_pages as $page){
			if($re_id){
				$blog_waterfall_pages[] = $page->ID;
			}else {
				$blog_waterfall_pages[$page->ID] = $page->post_title;
			}
		}
	}
	
	return $blog_waterfall_pages;
}

/**
 * Get Default Blog Waterfall Flux Page
 *
 * @since alterna 2.2
 */
function alterna_get_default_blog_waterfaull_page() {

	$default_page_id = intval( alterna_get_options_key('blog-waterfall-page') );

	$b_pages = alterna_get_all_blog_waterfall_type_pages(true);

	if(isset($b_pages[$default_page_id])) {
		return $b_pages[$default_page_id];
	}
	
	if(count($b_pages)>0){
		return $b_pages[0];
	}
	return 0;
}

/**
 * Get all categories
 * @bool = true return <li> list with name
 */
function alterna_get_custom_all_categories($taxonomies,$bool = false){
	$categories = get_terms($taxonomies);
	$output = "";
	// return <li> html code
	if($bool){ 
		foreach($categories as $category){
			$output .= '<li>'.strtoupper($category->name).'</li>';
		}
	} else {
		return $categories;
	}
	return $output;
}

/**
 * Get Portfolio categories
 */
function alterna_get_portfolio_categories(){
	$output = '<ul>';
	$categories = alterna_get_custom_all_categories('portfolio_categories');
	if(count($categories) > 0){
		foreach($categories as $category){
			if(intval($category->parent) != 0) continue;
			$subcategories = get_terms('portfolio_categories',array('child_of' => $category->term_id));

			$output .='<li><a href="'.esc_attr(get_term_link($category, 'portfolio_categories')).'">'.$category->name.' ( '.alterna_get_portfolio_list_by_categories(array($category->term_id),true).' ) '.'</a>';
			if(count($subcategories)>0){
				$output .='<ul>';
				foreach($subcategories as $subcategory){
					$output .='<li><a href="'.esc_attr(get_term_link($subcategory, 'portfolio_categories')).'">'.$subcategory->name.' ( '.alterna_get_portfolio_list_by_categories(array($subcategory->term_id),true).' ) '.'</a></li>';
				}
				$output .='</ul>';
			}
			$output .= '</li>';
		}
	}
    $output .='</ul>';
	return $output;
}


/**
 * Get Portfolio categories
 */
function alterna_get_portfolio_list_by_categories($slugs = array(),$re_count = false){
	$args = array(
			'post_type' => 'portfolio',
			'posts_per_page' => -1, 
			'tax_query' => array(
				array(
					'taxonomy' => 'portfolio_categories',
					'field' => 'term',
					'terms' => $slugs)
				)
			);
	$the_query = new WP_Query($args);
	
	if($re_count){
		if($the_query->have_posts()) return $the_query->post_count;
		return 0;
	}
	return $the_query;
}

/**
 * Get current post categories
 * @bool = true return "," string name
 */
function alterna_get_custom_post_categories($id,$taxonomies,$bool = false,$sep=' , ' , $type = 'name' , $exter = ''){
	$categories = get_the_terms($id,$taxonomies);
	$output = "";
	// return <li> html code
	if($bool && !empty($categories)){
		$first = true;
		foreach($categories as $category){
			if(!$first)
				$output .=$sep;
			else
				$first = false;
				
			$output .= $exter.$category->$type;
		}
	} else {
		return $categories;
	}
	return $output;
}

/**
 * Get custom portfolio category links
 *
 * @since alterna 1.0
 */
function alterna_get_custom_portfolio_category_links($categories , $sep ='' , $taxonomies = "portfolio_categories"){
	
	$output = '';
	if( !empty($categories) ){
		$bool = false;
		foreach($categories as $category){
			if($bool) $output .= $sep;
			$output .= '<a href="'.get_term_link($category->slug, $taxonomies ).'">'.$category->name.'</a>';
			$bool = true;
		}
	}
	if($output == '') $output = __('No Category','alterna');
	return $output;
}

/**
 * get custom post meta list
 *
 * @since alterna 1.0
 */
function alterna_get_custom_post_meta($id,$type = 'post') {

	$custom = get_post_custom($id);
	$output = array();
	$check_array = array();
	switch($type){
		case "post": // Post
			$check_array = array('show-related-post','show-related-post-number');
			break;
		case "portfolio": // Portfolio
			$check_array = array('portfolio-type','video-type','video-content','portfolio-colors','portfolio-skills','portfolio-client','portfolio-system','portfolio-price','portfolio-link','show-related-portfolio','show-related-portfolio-number','show-related-portfolio-style');
			break;
	}
	foreach($check_array as $value){
		$output[$value] = isset($custom[$value])  ? $custom[$value][0] : "";
	}
	
	return $output;
}

/**
 * Get social account from alterna setting
 *
 * @since alterna 1.0
 */
function alterna_get_social_list($extra_name='',$topbar = false){
	global $alterna_options;
	
	$str = "";
	$social_list = array(	array('twitter','Twitter',1) ,
							array('facebook', 'Facebook',1) ,
							array('google-plus', 'Google Plus',1) ,
							array('dribbble', 'Dribbble',1) ,
							array('pinterest', 'Pinterest',1) ,
							array('flickr', 'Flickr',1) ,
							array('skype', 'Skype',1) ,
							array('youtube', 'Youtube',1) ,
							array('vimeo', 'Vimeo',0) ,
							array('linkedin', 'Linkedin',1),
							array('digg', 'Digg',0) ,
							array('deviantart', 'Deviantart',0) ,
							array('behance', 'Behance',0) ,
							array('forrst', 'Forrst',0) ,
							array('lastfm', 'Lastfm',0) ,
							array('xing', 'XING',1),
							array('instagram', 'instagram',1),
							array('stumbleupon', 'StumbleUpon',0),
							array('picasa', 'Picasa',0),
							array('email','Email',2,'envelope')
						);
	foreach($social_list as $social_item){
		
		if(alterna_get_options_key('social-'.$social_item[0]) != '') {
			if(!$topbar) {
				$str .=  '<li><a title="'.$social_item[1].'" href="'.alterna_get_options_key('social-'.$social_item[0]).'" target="_blank" class="alterna-icon-'.$social_item[0].'"></a></li>';
			}else if($social_item[2] != 0){
				$str .=  ' <li class="social"><a href="'.alterna_get_options_key('social-'.$social_item[0]).'" target="_blank" ><i class="icon-'.($social_item[2] == 1 ? $social_item[0] : $social_item[3]).'"></i></a></li>';
			}
			
		}
	}
	
	return $str;
}

/**
 * Get color list
 *
 * @since alterna 1.0
 */
function alterna_get_color_list($string){
	$output = '';
	$colors = explode(",",$string);
	if(count($colors) > 0){
		foreach($colors as $color){
			if($color != '') $output .= '<div class="circle-color show-tooltip" title="'.$color.'" style="background-color:'.$color.'"></div>';
		}
	}
	return $output;
}

if ( ! function_exists( 'alterna_get_gallery_list' ) ) :
/**
 * Get Page,Post custom gallery ids
 *
 * @since alterna 1.0
 */
function alterna_get_gallery_list($item_id, $thumbs_size){
	$gallery_images = alterna_get_post_meta_key('gallery-images', $item_id);
	$img_list = alterna_get_post_gallery_ids($gallery_images);
	$out_html = '';
	if(count($img_list) > 0){
		foreach($img_list as $item_id){
			$attachment_image = wp_get_attachment_image_src($item_id, $thumbs_size); 
			$full_image = wp_get_attachment_image_src($item_id, 'full'); 
			$out_html .= '<li><a href="'.esc_url($full_image[0]).'" class="fancybox-thumb" rel="fancybox-thumb['.$item_id.']"><img src="'.esc_url($attachment_image[0]).'" alt=""></a></li>';
		}
	}
	
	return $out_html;
}
endif;

if ( ! function_exists( 'alterna_get_post_gallery_ids' ) ) :
/**
 * Get Page,Post custom gallery ids
 *
 * @since alterna 6.0
 */
function alterna_get_post_gallery_ids($gallery_images){
	$ids = array();
	$list = explode("{|}",$gallery_images);
	foreach($list as $item){
		$img_data = explode("<|>",$item);
		if(count($img_data) > 1 && isset($img_data[1])){
			$ids[] = $img_data[1];
		}
	}
	return $ids;
}
endif;

if ( ! function_exists( 'alterna_get_portfolio_custom_fields' ) ) :
/**
 * Get Portfolio custom gallery ids
 *
 * @since alterna 6.0
 */
function alterna_get_portfolio_custom_fields($current_data){
	$lists = explode("{|}",$current_data);
	if(count($lists) > 0){
		foreach($lists as $list_item){
			if($list_item == "") {continue;}
			$fileds = explode("[|]",$list_item);
			if(count($fileds) > 2){
				echo '<li><div class="type"><i class="'.esc_attr($fileds[1]).'"></i>'.esc_html($fileds[0]).'</div><div class="value">'.esc_html($fileds[2]).'</div></li>';
			}
		}
	}
}
endif;

/**
 * Get attachments
 * if li true return <li></li>
 * if link true return <a>
 * @since alterna 1.0
 */
function alter_get_attachments($id , $args , $show_size , $re_li = false , $link = false , $link_size = 'full' , $params = '') {
	
	$args = wp_parse_args( $args , array(
		'post_type' => 'attachment',
		'numberposts' => '10',
		'post_status' => null,
		'post_parent' => $id,
		'post_mime_type' => 'image',
		'exclude' => get_post_thumbnail_id($id)
	));
	
	$attachments = get_posts($args);
	
	if($re_li){
		$output = '';
		foreach($attachments as $attachment) { 								
			$attachment_image = wp_get_attachment_image_src($attachment->ID, $show_size );
			$full_image = wp_get_attachment_image_src($attachment->ID, $link_size );
			$attachment_data = wp_get_attachment_metadata($attachment->ID);
			if(is_array($attachment_image) && $attachment_image[0] != '') {
				$output .= '<li>';
				
				if($link == true) {
					$params = $params == '' ? 'class="fancybox-thumb" rel="fancybox-thumb['.$id.']"' : $params;
					$output .= '<a href="'.$full_image[0].'" '.$params.'>';
				}
				$output .= '<img src="'.$attachment_image[0].'" alt="'.$attachment->post_title.'" />';
				if($link == true){
					$output .= '</a>';
				}
				$output .='</li>';
			}
		}
		return $output;
	}

	return $attachments;
}

/**
 * Get comment form
 *
 * @since alterna 1.0
 */
function alterna_comment_form(){
	global $user_identity;
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	if ( comments_open() ) { ?>
		<div id="respond" class="row-fluid">
        	<div id="reply-title" class="alterna-title row-fluid">
            	<h3 class="font14"><?php comment_form_title(__('Leave A Comment', 'alterna'), __('Leave A Comment', 'alterna')); ?></h3>
            	<div class="line row-fluid"><span class="left-line"></span><span class="right-line"></span></div>
        	</div>
            <div class="row-fluid"><?php cancel_comment_reply_link(); ?></div>
			<form id="comment-form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
        	
            <?php if ( is_user_logged_in() ) : ?>

            <p><?php _e('Logged in as', 'alterna'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account"><?php _e('Log out &raquo;', 'alterna'); ?></a></p>
    		<div class="row-fluid">
            	<div id="comment-textarea" class="span6">
                	<textarea placeholder="<?php _e('Comment...', 'alterna'); ?>" name="comment" id="comment" cols="60" rows="5" tabindex="4" class="textarea-comment logged-in"></textarea>
            	</div>
            </div>
            <div id="comment-alert-error" class="alert alert-error">
                <span class="comment-alert-error-message"><?php _e('Please enter a message.', 'alterna'); ?></span>
            </div>
            <div class="form-submit">
                <p><div class=""><input name="submit" class="btn btn-custom" type="submit" id="submit" tabindex="5" value="<?php _e('Post Comment', 'alterna'); ?>" /></div></p>
                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', get_the_ID()); ?>
            </div>
		
		<?php else : ?>
        	<div class="row-fluid">
            	<div id="comment-input" class="span6">
                	<div class="placeholding-input">
                		<input type="text" name="author" id="author" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> class="input-name" />
                        <label for="author" class="placeholder"><?php _e('Name (required)', 'alterna'); ?></label>
                    </div>
                    <div class="placeholding-input">
                		<input type="text" name="email" id="email" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> class="input-email"  />
                        <label for="email" class="placeholder"><?php _e('Email (required)', 'alterna'); ?></label>
                    </div>
                    <div class="placeholding-input">
                		<input type="text" name="url" id="url" tabindex="3" class="input-website" />
                        <label for="url" class="placeholder"><?php _e('Website', 'alterna'); ?></label>
                    </div>
            	</div>
            
            	<div id="comment-textarea" class="span6">
                	<div class="placeholding-input">
                		<textarea name="comment" id="comment" cols="60" rows="5" tabindex="4" class="textarea-comment"></textarea>
                    	<label for="comment" class="comment-placeholder placeholder"><?php _e('Comment...', 'alterna'); ?></label>
                    </div>
            	</div>
            </div>
            <div id="comment-alert-error" class="alert alert-error">
            	<span class="comment-alert-error-name"><?php _e('Please enter your name.', 'alterna'); ?></span>
                <span class="comment-alert-error-email"><?php _e('Please enter an valid email address.', 'alterna'); ?></span>
                <span class="comment-alert-error-message"><?php _e('Please enter a message.', 'alterna'); ?></span>
            </div>
            <div id="comment-submit">
                <div><input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Post Comment', 'alterna'); ?>" class="btn btn-custom" /></div>
                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', get_the_ID()); ?>
            </div>
    
            <?php endif; ?>
        </form>
    </div>
	<?php
	}
}

/**
 * Get comments list
 *
 * @since alterna 1.0
 */
function alterna_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
		
	if ( 'div' == $args['style'] ) {
		//$tag = 'div';
		$add_below = 'comment';
	} else {
		//$tag = 'li';
		$add_below = 'div-comment';
	}
	?>
    
    <li id="comment-<?php comment_ID() ?>">
    	<article id="div-comment-<?php comment_ID() ?>" class="comment-item">
        	<div class="gravatar"><?php if ($args['avatar_size'] != 0) echo  get_avatar( $comment, $args['avatar_size']); ?></div>
        	
        	<div class="comment-content">
            	<div class="comment-meta"><span class="author-name"><?php echo comment_author_link($comment->comment_ID);?></span><span>&nbsp;&nbsp;</span><span class="comment-date"><?php echo get_comment_date(); ?> <?php _e('at', 'alterna'); ?> <?php echo get_comment_time(); ?></span></div>
            	<?php comment_text(); ?>
                <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				<?php if ($comment->comment_approved == '0') : ?>
                      <em class="comment-wait-approved"><?php _e('Your comment is awaiting approved.' , 'alterna') ?></em>
            	<?php endif; ?>
            
        	</div>
    	</article>
    <?php 
}


/**
 * Get portfolio post for sidebar widget
 */
function alterna_get_portfolio_widget_post($type = 'recent' , $per_page = '' , $post_value = '' , $re_list = false , $show_type = 0 , $post__not_in = '') {
	if($per_page == "") $per_page = 5;
	
	$args=array( 'post_type' => 'portfolio', 'post_status' => 'publish', 'posts_per_page' => $per_page );
				 
	switch($type){
		case 'featured':
				$post_ids = explode("," , $post_value);
				if(count($post_ids) == 0) return "";
				$args['post__in']= $post_ids;
				$args['posts_per_page']= count($post_ids);
			break;
		case 'recent':
				
			break;
		case 'related':
				$slugs = explode("," , $post_value);
				if(count($slugs) == 0) return "";
				$args['tax_query'] = array(	array('taxonomy' => 'portfolio_categories',
												'field' => 'slug',
												'terms' => $slugs));
			break;
	}
	
	if($post__not_in != ""){
		$post__not_in = explode("," , $post__not_in);
		$args['post__not_in'] = $post__not_in;
	}

	$portfolios = new WP_Query($args);
	
	if($re_list) return $portfolios;
	
	$output = "";
	
	if ($portfolios -> have_posts() ){
		while ($portfolios -> have_posts() ) : $portfolios-> the_post();
		
			$custom = alterna_get_custom_post_meta(get_the_ID(),'portfolio');
			
			$output .='<div class="sidebar-portfolio-recent">';
			
			if(intval($show_type) == 0){
			
				$output .= '<div class="sidebar-portfolio-img post-img">';
				if (has_post_thumbnail(get_the_ID()) ) { 
					$output .= get_the_post_thumbnail(get_the_ID(), "portfolio-four-thumbs",array('alt' => get_the_title(get_the_ID()),'title' => get_the_title(get_the_ID())));
				}else{
					$output .= '<img  src="'.get_template_directory_uri().'/img/portfolio-no-thumbs.png" alt="">';
				}
				 $output .= '<div class="post-tip"><div class="bg"></div><a href="'.get_permalink(get_the_ID()).'" ><div class="link center-link"><i class="big-icon-link"></i></div></a></div>';
				$output .='</div>';
				$output .='<div class="sidebar-portfolio-title"><a title="'.get_the_title().'" href="'.get_permalink(get_the_ID()).'">'.get_the_title().'</a>';
			}else if(intval($show_type) == 1){
				$icon_class = 'big-icon-picture';
				switch(intval($custom['portfolio-type'])){
					case 1: $icon_class = 'big-icon-slideshow'; break;
					case 2: $icon_class = 'big-icon-video'; break;
				}
				$output .= '<div class="portfolio-type"><i class="'.$icon_class.'"></i></div>';
				$output .= '<div class="sidebar-portfolio-title-cat "><a title="'.get_the_title().'" href="'.get_permalink(get_the_ID()).'">'.get_the_title().'</a>';
				$output .= '<span class="portfolio-categories">'.alterna_get_custom_portfolio_category_links( alterna_get_custom_post_categories(get_the_ID(),'portfolio_categories',false) , ' / ').'</span>';
			}else{
				$output .= '<div class="alterna-recent-thumb">';
				if(has_post_thumbnail(get_the_ID())) {
					$output .= '<a href="'.get_permalink().'" title="'.get_the_title().'">';
					$output .= get_the_post_thumbnail(get_the_ID(), 'thumbnail' , array('alt' => get_the_title(),'title' => ''));
					$output .= '</a>';
				}
				$output .= '</div><div class="alterna-recent-content">';
				$output .= '<h5 class="entry-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h5>';
				$output .= '<span class="portfolio-categories">'.alterna_get_custom_portfolio_category_links( alterna_get_custom_post_categories(get_the_ID(),'portfolio_categories',false) , ' / ').'</span>';
			}

			$output .='</div></div>';
			
		endwhile;
	}
	wp_reset_postdata();
	return $output;
}

/**
 * Get blog posts for sidebar widget
 */
function alterna_get_blog_widget_post($type = 'recent' , $per_page = '' , $post_value = '' , $re_list = false , $show_type = 0 , $post__not_in = '',$tag__in= '') {
	
	if($per_page == "") $per_page = 5;
	
	$args=array('post_type' => 'post', 'post_status' => 'publish' , 'posts_per_page' => $per_page);
				 
	switch($type){
		case 'featured':
				$post_ids = explode("," , $post_value);
				if(count($post_ids) == 0) return "";
				$args['post__in']= $post_ids;
				$args['posts_per_page']= count($post_ids);
			break;
		case 'popular':
				$args['orderby']= 'comment_count';
		case 'recent':
				
			break;
		case 'related':
				$post_cats = explode("," , $post_value);
				if(count($post_cats) == 0) return "";
				$args['category__in'] = $post_cats;
			break;
	}
	
	if($tag__in != ""){
		$post_tags = explode("," , $tag__in);
		if(count($post_tags) != 0){
			$args['tag__in'] = $post_tags;
		}
	}
	
	if($post__not_in != ""){
		$post__not_in = explode("," , $post__not_in);
		$args['post__not_in'] = $post__not_in;
	}
				
	$blog_posts = new WP_Query($args);

	if($re_list) return $blog_posts;
	
	$output = "";
	
	if ($blog_posts -> have_posts() ){
		while ($blog_posts -> have_posts() ) : $blog_posts-> the_post();
		
			$output .='<div class="sidebar-portfolio-recent">';
			
			if(intval($show_type) == 0){
				$output .= '<div class="sidebar-portfolio-img post-img">';
				if (has_post_thumbnail(get_the_ID()) ) { 
					$output .= get_the_post_thumbnail(get_the_ID(), "portfolio-four-thumbs",array('alt' => get_the_title(get_the_ID()),'title' => get_the_title(get_the_ID())));
				}else{
					$output .= '<img  src="'.get_template_directory_uri().'/img/portfolio-no-thumbs.png" alt="">';
				}
				 $output .= '<div class="post-tip"><div class="bg"></div><a href="'.get_permalink(get_the_ID()).'" ><div class="link center-link"><i class="big-icon-link"></i></div></a></div>';
				$output .='</div>';
				$output .='<div class="sidebar-portfolio-title"><a title="'.get_the_title().'" href="'.get_permalink(get_the_ID()).'">'.get_the_title().'</a>';
			}else if(intval($show_type) == 1){
				$icon_class = 'big-icon-file';
			 	switch(get_post_format()){
					case 'video': $icon_class =  'big-icon-video'; break;
					case 'audio': $icon_class =  'big-icon-music'; break;
					case 'image': $icon_class =  'big-icon-picture'; break;
					case 'gallery': $icon_class =  'big-icon-slideshow'; break;
					case 'quote': $icon_class =  'big-icon-quote'; break;
				}
				$output .= '<div class="portfolio-type"><i class="'.$icon_class.'"></i></div>';
				$output .= '<div class="sidebar-blog-title"><a title="'.get_the_title().'" href="'.get_permalink(get_the_ID()).'">'.get_the_title().'</a>';
				$output .= '<div class="sidebar-blog-meta"><span class="blog-date">'.get_the_date().'</span>';
				$num_comments = get_comments_number(get_the_ID()); 
				if ( $num_comments == 0 ) {
					$comments = __('No Comments','alterna');
				} elseif ( $num_comments > 1 ) {
					$comments = $num_comments . __(' Comments','alterna');
				} else {
					$comments = __('1 Comment','alterna');
				}
				$output .= '<a class="blog-comments" href="' . get_comments_link(get_the_ID()) .'"><i class="icon-comments"></i>'.$comments.'</a></div>';
			}else{
				$output .= '<div class="alterna-recent-thumb">';
				if(has_post_thumbnail(get_the_ID())) {
					$output .= '<a href="'.get_permalink().'" title="'.get_the_title().'">';
					$output .= get_the_post_thumbnail(get_the_ID(), 'thumbnail' , array('alt' => get_the_title(),'title' => ''));
					$output .= '</a>';
				}
				$output .= '</div><div class="alterna-recent-content">';
				$output .= '<h5 class="entry-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h5>';
				$output .= '<div class="post-date">'.__('Posted on ','alterna').get_the_date().'</div>';
				$num_comments = get_comments_number(get_the_ID()); 
				if ( $num_comments == 0 ) {
					$comments = __('0 Comments','alterna');
				} elseif ( $num_comments > 1 ) {
					$comments = $num_comments . __(' Comments','alterna');
				} else {
					$comments = __('1 Comment','alterna');
				}
					
				$output .= '<div class="post-meta post-comments"><a href="'.get_permalink(get_the_ID()).'#comments">'.$comments.'</a></div>';
			}

			$output .='</div></div>';
		endwhile;
	}
	
	wp_reset_postdata();
	
	return $output;
}


//========================================================
//  	COMMON METHODS
//========================================================

/**
 * Get custom option key value
 *
 */
function alterna_get_options_key($key,$options = '',$rebool = false,$default = ''){
	global $alterna_options;
	if($options == '') $options = $alterna_options;
	if(isset($options[$key])){
		if($rebool) return true;
		return $options[$key];
	}else{
		if($rebool) return false;
	}
	return $default;
}

if ( ! function_exists( 'alterna_get_post_meta_key' ) ) :
/**
 * Get post meta key value
 *
 * @since Alterna 6.0
 */
function alterna_get_post_meta_key($key, $ID = 0, $default = ''){
	if($ID == 0){ $ID = get_the_ID(); }
	if($ID <= 0){ return $default;}
	$result = get_post_meta($ID , $key , true);
	if($result){ return $result; }
	return $default;
}
endif;

/**
 * Generate Options CSS
 *
 */
function alterna_generate_options_css() {
	
	//get theme update history
	$options_update_name = 'alterna_options_update';
	$options_update = get_option($options_update_name);
	
	//get theme version
	$theme_old_version = get_option($options_update_name.'-ver');
	$theme_data = wp_get_theme();
	if(!$theme_old_version){
		$theme_old_version = $theme_data['Version'];
		update_option($options_update_name.'-ver', $theme_data['Version']);
	}
	
	$update_data;
	if(isset($options_update['update'])){
		if($options_update['update'] == 'yes'){
			if(version_compare($theme_data['Version'], $theme_old_version, '>')){
				update_option($options_update_name.'-ver', $theme_data['Version']);
			}else{
				return;
			}
		}
		$update_data = array('update'=>'yes','version'=> (intval($options_update['version']) + 1) );
	}else{
		$update_data = array('update'=>'yes','version'=> 0 );
	}
	
	/** Define some vars **/
	$uploads = wp_upload_dir();
	$css_dir = get_template_directory() . '/custom/'; // Shorten code, save 1 call
	
	/** Save on different directory if on multisite **/
	if(is_multisite()) {
		$uploads_dir = trailingslashit($uploads['basedir']);
	} else {
		$uploads_dir = $css_dir;
	}
	
	/** Capture CSS output **/
	ob_start();
	require($css_dir . 'custom-styles.php');
	$css = ob_get_clean();
	
	/** Write to options.css file **/
	WP_Filesystem();
	global $wp_filesystem;
	if ( ! $wp_filesystem->put_contents( $uploads_dir . 'custom-styles.css', $css, 0644) ) {
		return true;
	}
	update_option($options_update_name ,$update_data);
}

/**
 * Add custom image size for feature image crop
 */
function alterna_add_image_size($name,$w,$h,$crop = true){
	add_image_size( $name, $w, $h, $crop );
	update_option($name.'_size_w', $w);
	update_option($name.'_size_h', $h);
	update_option($name.'_crop', $crop ? 1 : 0);
}

/**
 * Get limit words
 */
function string_limit_words($str, $limit = 18 , $need_end = false) {
	$words = explode(' ', $str, ($limit + 1));
	if(count($words) > $limit) {
		array_pop($words);
		array_push($words,'...');
	}
	return implode(' ', $words);
}

/**
 * Relace All key string use value
 */
function alterna_replace_str($str,$key,$value) {
	$str = str_replace($key,$value,$str);
	return $str;
}

/** 
 * Get Preg replace string value
 */
function alterna_get_retina_preg_replace_str($string , $preg = '/\.\w+$/'){
	$return_str = '';
	
	preg_match($preg, $string , $match_result);
	
	if(count($match_result) > 0) {
		$return_str = preg_replace($preg, '@2x'.$match_result[0] , $string);
	}
	
	return $return_str;
}

/**
 * Get common hex to rgb
 */
function alterna_hex2RGB($color) {
	if ($color[0] == '#')
        $color = substr($color, 1);

    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    else
        return false;

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return array('r' => $r, 'g' => $g, 'b' => $b);
}

//========================================================
//  	PLUGIN FUNCTION METHODS
//========================================================

/**
 * Get option  for layerslider
 */
function alterna_get_layerslider(){
	$layerslider_slides = array();
	$layerslider_slides[0] = 'Select a slider';
	
	 // Get WPDB Object
    global $wpdb;
 
    // Table name
    $table_name = $wpdb->prefix . "layerslider";

	
	$sql = "show tables like '$table_name'";
	
	$table = $wpdb->get_var($sql);

	// have no rev slider 
	if($table != $table_name) return $layerslider_slides;
 
    // Get sliders
    $sliders = $wpdb->get_results( "SELECT * FROM $table_name
                                        WHERE flag_hidden = '0' AND flag_deleted = '0'
                                        ORDER BY date_c ASC LIMIT 100" );
 
    // Iterate over the sliders
    foreach($sliders as $key => $item) {
 		$layerslider_slides[$item->id] = '#'.$item->id . ' - ' .$item->name;
    }
	
	return $layerslider_slides;
}

/**
 * Get option  for revslider
 */
function alterna_get_revslider(){
	$revslider_slides = array();
	$revslider_slides[0] = 'Select a slider';
	
	 // Get WPDB Object
    global $wpdb;

    // Table name
    $table_name = $wpdb->prefix . "revslider_sliders";
	
	$sql = "show tables like '$table_name'";
	
	$table = $wpdb->get_var($sql);

	// have no rev slider 
	if($table != $table_name) return $revslider_slides;
	
    // Get sliders
    $sliders = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY id LIMIT 100" );
 
    // Iterate over the sliders
    foreach($sliders as $key => $item) {
 		$revslider_slides[$item->id] = '#'.$item->id . ' - ' .$item->title;
    }
	
	return $revslider_slides;
}

/**
 * Get option  for wpml
 */
function alterna_get_wpml_switcher(){
	if (function_exists( 'icl_get_languages' ) && function_exists('icl_disp_language')) {
		$languages = icl_get_languages('skip_missing=1');
		
		if(!empty($languages)){
			
			$lang_active = '';
			$lang_list = '<ul class="sub-menu">';
			
			foreach($languages as $l){

				if($l['active']){ $lang_active = '<span><i class="icon-globe"></i>'.$l['native_name'].'<i class="icon-angle-down"></i></span>';				}
				$lang_list .= '<li>';

				if(!$l['active']){
					$lang_list .= '<a href="'.$l['url'].'">';
				}else{
					$lang_list .=  '<span>';
				}
				
				if($l['country_flag_url']){
					$lang_list .= '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';				
				}
				$lang_list .=  $l['native_name'];
			
				if(!$l['active']){
					$lang_list .= '</a>';
				}else{
					$lang_list .=  '</span>';
				}
				$lang_list .= '</li>';
			}
			$lang_list .= '</ul>';
			
			return '<li class="wpml">'.$lang_active.$lang_list.'</li>';
		}
	}
	return '';
}