<?php
/**
 * Penguin Framework Config for penguin framework.
 *
 * @package Penguin
 * @version 1.0
 */
	global $google_fonts;
	require_once("penguin/penguin.php");
	require_once("penguin/penguin-functions.php");
	
	$dir = get_template_directory_uri();
	
	/**
	 * Google Web Font now 624
	 * http://www.google.com/webfonts
	 * <link href='http://fonts.googleapis.com/css?family=...'rel='stylesheet' type='text/css'>
	 */
	$google_fonts = 'ABeeZee|Abel|Abril+Fatface|Aclonica|Acme|Actor|Adamina|Advent+Pro|Aguafina+Script|Akronim|Aladin|Aldrich|Alegreya|Alegreya+SC|Alex+Brush|Alfa+Slab+One|Alice|Alike|Alike+Angular|Allan|Allerta|Allerta+Stencil|Allura|Almendra|Almendra+Display|Almendra+SC|Amarante|Amaranth|Amatic+SC|Amethysta|Anaheim|Andada|Andika|Annie+Use+Your+Telescope|Anonymous+Pro|Antic|Antic+Didone|Antic+Slab|Anton|Arapey|Arbutus|Arbutus+Slab|Architects+Daughter|Archivo+Black|Archivo+Narrow|Arimo|Arizonia|Armata|Artifika|Arvo|Asap|Asset|Astloch|Asul|Atomic+Age|Aubrey|Audiowide|Autour+One|Average|Average+Sans|Averia+Gruesa+Libre|Averia+Libre|Averia+Sans+Libre|Averia+Serif+Libre|Bad+Script|Balthazar|Bangers|Basic|Baumans|Belgrano|Belleza|BenchNine|Bentham|Berkshire+Swash|Bevan|Bigelow+Rules|Bigshot+One|Bilbo|Bilbo+Swash+Caps|Bitter|Black+Ops+One|Bonbon|Boogaloo|Bowlby+One|Bowlby+One+SC|Brawler|Bree+Serif|Bubblegum+Sans|Bubbler+One|Buda:300|Buenard|Butcherman|Butterfly+Kids|Cabin|Cabin+Condensed|Cabin+Sketch|Caesar+Dressing|Cagliostro|Calligraffitti|Cambo|Candal|Cantarell|Cantata+One|Cantora+One|Capriola|Cardo|Carme|Carrois+Gothic|Carrois+Gothic+SC|Carter+One|Caudex|Cedarville+Cursive|Ceviche+One|Changa+One|Chango|Chau+Philomene+One|Chela+One|Chelsea+Market|Cherry+Cream+Soda|Cherry+Swash|Chewy|Chicle|Chivo|Cinzel|Cinzel+Decorative|Clicker+Script|Coda|Coda+Caption:800|Codystar|Combo|Comfortaa|Coming+Soon|Concert+One|Condiment|Contrail+One|Convergence|Cookie|Copse|Corben|Courgette|Cousine|Coustard|Covered+By+Your+Grace|Crafty+Girls|Creepster|Crete+Round|Crimson+Text|Croissant+One|Crushed|Cuprum|Cutive|Cutive+Mono|Damion|Dancing+Script|Dawning+of+a+New+Day|Days+One|Delius|Delius+Swash+Caps|Delius+Unicase|Della+Respira|Denk+One|Devonshire|Didact+Gothic|Diplomata|Diplomata+SC|Domine|Donegal+One|Doppio+One|Dorsa|Dosis|Dr+Sugiyama|Droid+Sans|Droid+Sans+Mono|Droid+Serif|Duru+Sans|Dynalight|EB+Garamond|Eagle+Lake|Eater|Economica|Electrolize|Elsie|Elsie+Swash+Caps|Emblema+One|Emilys+Candy|Engagement|Englebert|Enriqueta|Erica+One|Esteban|Euphoria+Script|Ewert|Exo|Expletus+Sans|Fanwood+Text|Fascinate|Fascinate+Inline|Faster+One|Federant|Federo|Felipa|Fenix|Finger+Paint|Fjalla+One|Fjord+One|Flamenco|Flavors|Fondamento|Fontdiner+Swanky|Forum|Francois+One|Freckle+Face|Fredericka+the+Great|Fredoka+One|Fresca|Frijole|Fruktur|Fugaz+One|Gabriela|Gafata|Galdeano|Galindo|Gentium+Basic|Gentium+Book+Basic|Geo|Geostar|Geostar+Fill|Germania+One|Gilda+Display|Give+You+Glory|Glass+Antiqua|Glegoo|Gloria+Hallelujah|Goblin+One|Gochi+Hand|Gorditas|Goudy+Bookletter+1911|Graduate|Grand+Hotel|Gravitas+One|Great+Vibes|Griffy|Gruppo|Gudea|Habibi|Hammersmith+One|Hanalei|Hanalei+Fill|Handlee|Happy+Monkey|Headland+One|Henny+Penny|Herr+Von+Muellerhoff|Holtwood+One+SC|Homemade+Apple|Homenaje|IM+Fell+DW+Pica|IM+Fell+DW+Pica+SC|IM+Fell+Double+Pica|IM+Fell+Double+Pica+SC|IM+Fell+English|IM+Fell+English+SC|IM+Fell+French+Canon|IM+Fell+French+Canon+SC|IM+Fell+Great+Primer|IM+Fell+Great+Primer+SC|Iceberg|Iceland|Imprima|Inconsolata|Inder|Indie+Flower|Inika|Irish+Grover|Istok+Web|Italiana|Italianno|Jacques+Francois|Jacques+Francois+Shadow|Jim+Nightshade|Jockey+One|Jolly+Lodger|Josefin+Sans|Josefin+Slab|Joti+One|Judson|Julee|Julius+Sans+One|Junge|Jura|Just+Another+Hand|Just+Me+Again+Down+Here|Kameron|Karla|Kaushan+Script|Kavoon|Keania+One|Kelly+Slab|Kenia|Kite+One|Knewave|Kotta+One|Kranky|Kreon|Kristi|Krona+One|La+Belle+Aurore|Lancelot|Lato|League+Script|Leckerli+One|Ledger|Lekton|Lemon|Libre+Baskerville|Life+Savers|Lilita+One|Limelight|Linden+Hill|Lobster|Lobster+Two|Londrina+Outline|Londrina+Shadow|Londrina+Sketch|Londrina+Solid|Lora|Love+Ya+Like+A+Sister|Loved+by+the+King|Lovers+Quarrel|Luckiest+Guy|Lusitana|Lustria|Macondo|Macondo+Swash+Caps|Magra|Maiden+Orange|Mako|Marcellus|Marcellus+SC|Marck+Script|Margarine|Marko+One|Marmelad|Marvel|Mate|Mate+SC|Maven+Pro|McLaren|Meddon|MedievalSharp|Medula+One|Megrim|Meie+Script|Merienda|Merienda+One|Merriweather|Merriweather+Sans|Metal+Mania|Metamorphous|Metrophobic|Michroma|Milonga|Miltonian|Miltonian+Tattoo|Miniver|Miss+Fajardose|Modern+Antiqua|Molengo|Molle:400italic|Monda|Monofett|Monoton|Monsieur+La+Doulaise|Montaga|Montez|Montserrat|Montserrat+Alternates|Montserrat+Subrayada|Mountains+of+Christmas|Mouse+Memoirs|Mr+Bedfort|Mr+Dafoe|Mr+De+Haviland|Mrs+Saint+Delafield|Mrs+Sheppards|Muli|Mystery+Quest|Neucha|Neuton|New+Rocker|News+Cycle|Niconne|Nixie+One|Nobile|Norican|Nosifer|Nothing+You+Could+Do|Noticia+Text|Noto+Sans|Noto+Serif|Nova+Cut|Nova+Flat|Nova+Mono|Nova+Oval|Nova+Round|Nova+Script|Nova+Slim|Nova+Square|Numans|Nunito|Offside|Old+Standard+TT|Oldenburg|Oleo+Script|Oleo+Script+Swash+Caps|Open+Sans|Open+Sans+Condensed:300|Oranienbaum|Orbitron|Oregano|Orienta|Original+Surfer|Oswald|Over+the+Rainbow|Overlock|Overlock+SC|Ovo|Oxygen|Oxygen+Mono|PT+Mono|PT+Sans|PT+Sans+Caption|PT+Sans+Narrow|PT+Serif|PT+Serif+Caption|Pacifico|Paprika|Parisienne|Passero+One|Passion+One|Patrick+Hand|Patrick+Hand+SC|Patua+One|Paytone+One|Peralta|Permanent+Marker|Petit+Formal+Script|Petrona|Philosopher|Piedra|Pinyon+Script|Pirata+One|Plaster|Play|Playball|Playfair+Display|Playfair+Display+SC|Podkova|Poiret+One|Poller+One|Poly|Pompiere|Pontano+Sans|Port+Lligat+Sans|Port+Lligat+Slab|Prata|Press+Start+2P|Princess+Sofia|Prociono|Prosto+One|Puritan|Purple+Purse|Quando|Quantico|Quattrocento|Quattrocento+Sans|Questrial|Quicksand|Quintessential|Qwigley|Racing+Sans+One|Radley|Raleway|Raleway+Dots|Rambla|Rammetto+One|Ranchers|Rancho|Rationale|Redressed|Reenie+Beanie|Revalia|Ribeye|Ribeye+Marrow|Righteous|Risque|Roboto|Roboto+Condensed|Rochester|Rock+Salt|Rokkitt|Romanesco|Ropa+Sans|Rosario|Rosarivo|Rouge+Script|Ruda|Rufina|Ruge+Boogie|Ruluko|Rum+Raisin|Ruslan+Display|Russo+One|Ruthie|Rye|Sacramento|Sail|Salsa|Sanchez|Sancreek|Sansita+One|Sarina|Satisfy|Scada|Schoolbell|Seaweed+Script|Sevillana|Seymour+One|Shadows+Into+Light|Shadows+Into+Light+Two|Shanti|Share|Share+Tech|Share+Tech+Mono|Shojumaru|Short+Stack|Sigmar+One|Signika|Signika+Negative|Simonetta|Sintony|Sirin+Stencil|Six+Caps|Skranji|Slackey|Smokum|Smythe|Sniglet:800|Snippet|Snowburst+One|Sofadi+One|Sofia|Sonsie+One|Sorts+Mill+Goudy|Source+Code+Pro|Source+Sans+Pro|Special+Elite|Spicy+Rice|Spinnaker|Spirax|Squada+One|Stalemate|Stalinist+One|Stardos+Stencil|Stint+Ultra+Condensed|Stint+Ultra+Expanded|Stoke|Strait|Sue+Ellen+Francisco|Sunshiney|Supermercado+One|Swanky+and+Moo+Moo|Syncopate|Tangerine|Tauri|Telex|Tenor+Sans|Text+Me+One|The+Girl+Next+Door|Tienne|Tinos|Titan+One|Titillium+Web|Trade+Winds|Trocchi|Trochut|Trykker|Tulpen+One|Ubuntu|Ubuntu+Condensed|Ubuntu+Mono|Ultra|Uncial+Antiqua|Underdog|Unica+One|UnifrakturCook:700|UnifrakturMaguntia|Unkempt|Unlock|Unna|VT323|Vampiro+One|Varela|Varela+Round|Vast+Shadow|Vibur|Vidaloka|Viga|Voces|Volkhov|Vollkorn|Voltaire|Waiting+for+the+Sunrise|Wallpoet|Walter+Turncoat|Warnes|Wellfleet|Wendy+One|Wire+One|Yanone+Kaffeesatz|Yellowtail|Yeseva+One|Yesteryear|Zeyada';
     
	 
	/**
	 * Option page content
	 */
	$page_content = array();
	
	/* General Page */
	$page_content[] = array(
			'section' => 'general',
			'icon'	=>	'icon-cog',
			'name' => __('General','alterna'),
			'title' => __('General Setting','alterna'),
			'elements'	=> array(
				'theme_update'		=> array(
						'title' => __('Theme Auto Update Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'envato_name'		=> array(
								'name'	=> __('Username','alterna'),
								'type'	=>	'input',
								'property'	=> 'theme-name',
								'desc'	=> __('Envato market username.','alterna')
							),
							'envato_purchase_code'		=> array(
									'name'	=> __('Purchase Code','alterna'),
									'type'	=>	'input',
									'property'	=> 'theme-purchase-code',
									'desc'	=> __('Download License Certificate will find Item Purchase Code.','alterna')
								),
							'envato_api'		=> array(
									'name'	=> __('Envato API','alterna'),
									'type'	=>	'input',
									'property'	=> 'theme-api',
									'desc'	=> __('To obtain your API Key, visit "My Settings" page on any of the Envato Marketplaces.','alterna')
								),
							'enable_auto'	=>	array(
									'name'	=> __('Theme Auto Update','alterna'),
									'type'	=>	'pc',
									'desc' => __('Turn on auto update when have new version.','alterna'),
									'property'	=> 'theme-update-enable'
								)
						)
					),
				'favicon'		=> array(
						'name'	=> __('Favicon','alterna'),
						'title'	=> __('Favicon','alterna'),
						'type'	=>	'upload',
						'property'	=> 'favicon',
					),
				'rss_feed'		=> array(
						'name'	=> __('Feed url','alterna'),
						'title'	=> __('RSS Feed','alterna'),
						'type'	=>	'input',
						'property'	=> 'rss-feed',
					),
				'global'		=> array(
						'title' => __('Global Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'global_responsive'		=> array(
									'name'	=> __('Global Responsive','alterna'),
									'type'	=>	'radio',
									'property'	=> 'global-responsive',
									'radios'	=> array(0 => 'Responsive', 1 => 'Fixed Width')
								),
							'global_layout'		=> array(
									'name'	=> __('Global Layout','alterna'),
									'type'	=>	'radio',
									'property'	=> 'global-layout',
									'radios'	=> array(0 => 'Boxed', 1 => 'Full Width'),
									'enable-element' => 'yes',
									'enable-id'		=> '0-pe_global_layout',
									'enable-group'	=> 'pe_global_layout_group'
								),
							'global_boxed_padding'		=> array(
								'name'	=> __('Boxed Layout Padding','alterna'),
								'type'	=>	'number',
								'default'	=> 30, 
								'property'	=> 'global-layout-boxed-padding',
								'desc'	=> __('Setting Global Boxed Layout Top, Bottom Padding','alterna'),
								'enabled-id'		=> 'pe_global_layout',
								'enable-group'	=> 'pe_global_layout_group'
							),
							'global_mobile'		=> array(
									'name'	=> __('Mobile Responsive','alterna'),
									'type'	=>	'pc',
									'desc' => __('Turn on mobile responsive support.','alterna'),
									'property'	=> 'global-mobile-enable'
								),
							'global_sidebar_layout'		=> array(
									'name'	=> __('Global Sidebar Layout','alterna'),
									'type'	=>	'radio',
									'property'	=> 'global-sidebar-layout',
									'default'	=> 1,
									'radios'	=> array(0 => 'Left Sidebar', 1 => 'Right Sidebar', 2 => 'Full Width')
							),
							'global_breadcrumbs'		=> array(
									'name'	=> __('Enable Plugin Breadcrumbs','alterna'),
									'type'	=>	'pc',
									'desc' => __('Turn on enable Breadcrumbs With WordPress SEO by Yoast.','alterna'),
									'property'	=> 'global-breakcrumbs-enable',
							)
						)
					),
				'global_alert_modal'		=> array(
						'title' => __('Alter Modal Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'global_modal_type'		=> array(
								'name'	=> __('Enabled Modal','alterna'),
								'type'	=>	'pc',
								'property'	=> 'global-modal-enable',
								'enable-element' => 'yes',
								'enable-id'		=> '1-pe_modal_enable',
								'enable-group'	=> 'pe_modal_enable_group'
							),
							'global_modal_id'		=> array(
								'name'	=> __('Modal ID','alterna'),
								'type'	=>	'input',
								'property'	=> 'global-modal-id',
								'desc'	=> __('Use it will different before modal.','alterna'),
								'enabled-id'		=> 'pe_modal_enable',
								'enable-group'	=> 'pe_modal_enable_group'
							),
							'global_modal_content'		=> array(
								'name'	=> __('Modal Content','alterna'),
								'type'	=>	'textarea',
								'property'	=> 'global-modal-content',
								'desc'	=> __('Support html code for it, you can view more details from here http://goo.gl/lkoMWf -> Modals .','alterna'),
								'enabled-id'		=> 'pe_modal_enable',
								'enable-group'	=> 'pe_modal_enable_group'
							)
						)
					),
				'google_analytics'		=> array(
						'title'	=> __('Google Analytics','alterna'),
						'type'	=>	'moreline',
						'moreline'	=> array(
								'position' => array(
									'name'	=> __('Script position','alterna'),
									'type' 	=> 'radio',
									'property' => 'google_analytics-position',
									'radios'	=> array(
											'radios_1' => __('Header','alterna'),
											'radios_2' => __('Footer','alterna')
										)
									
									),
								'scripts' => array(
									'name'	=> __('Analytics script','alterna'),
									'type' 	=> 'textarea',
									'property' => 'google_analytics-text',
									'desc'	=> __('Paste your Google Analytics or other tracking code here.','alterna')
									)	
							)
					)
			),
		);
	
	/* WooCommerce Setting */
	$page_content[] = array(
			'section' => 'woocommerce',
			'icon'	=>	'icon-shopping-cart',
			'name' => __('WooCommerce','alterna'),
			'title' => __('WooCommerce Basic Setting','alterna'),
			'elements'	=> array(
			'woocommerce_setting'		=> array(
						'title'	=> __('WooCommerce Setting','alterna'),
						'type'	=>	'moreline',
						'moreline'	=> array(
								'shop_per_page' => array(
									'name'	=> __('Shop Per Page Number','alterna'),
									'type' 	=> 'number',
									'property' => 'shop-per-page',
									'after'	=>	'px',
									),
								'enable_product_search'	=>	array(
									'name'	=> __('Enabled Header Product Search','alterna'),
									'type'	=>	'pc',
									'desc' => __('Turn on enabled product search form.','alterna'),
									'property'	=> 'shop-product-search'
								),
							)
						)
					)
			);
			
	/* Background */	
	$page_content[] = array(
			'section' => 'background',
			'icon'	=>	'icon-certificate',
			'name' => __('Background','alterna'),
			'title' => __('Background Setting','alterna'),
			'elements'	=> array(
				'global_background'		=> array(
						'title' => __('Background Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'global_bg_type'		=> array(
								'name'	=> __('Background Image Type','alterna'),
								'type'	=>	'select',
								'property'	=> 'global-bg-type',
								'default'	=> 0,
								'options'	=> array(0 => 'Pattern', 1 => 'Image', 2 => 'Color'),
								'enable-element' => 'yes',
								'enable-id'		=> '0-global_bg_type_pattern:1-global_bg_type_image:2-global_bg_type_color',
								'enable-group'	=> 'global_bg_type_group'
							),
							'global_bg_color'		=> array(
								'name'	=> __('Background Color','alterna'),
								'type'	=>	'color',
								'property'	=> 'global-bg-color',
								'desc'	=> __('When background type select "Color" use it.','alterna'),
								'enabled-id'	=> 'global_bg_type_color',
								'enable-group'	=> 'global_bg_type_group'
							),
							'global_bg_width'		=> array(
								'name'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-bg-pattern-width',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'	=> 'global_bg_type_pattern',
								'enable-group'	=> 'global_bg_type_group'
							),
							'global_bg_height'		=> array(
								'name'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-bg-pattern-height',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'	=> 'global_bg_type_pattern',
								'enable-group'	=> 'global_bg_type_group'
							),
							'global_bg'		=> array(
								'name'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-bg-image',
								'enabled-id'	=> 'global_bg_type_pattern global_bg_type_image',
								'enable-group'	=> 'global_bg_type_group'
							),
							'global_bg_retina'		=> array(
								'name'	=> __('Background Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-bg-retina-image',
								'desc'	=> __('If you use "Pattern" type then need upload size is 400px * 400px retina support image. ','alterna'),
								'enabled-id'	=> 'global_bg_type_pattern',
								'enable-group'	=> 'global_bg_type_group'
							)
						)
					),
				'global_header_background'		=> array(
						'title' => __('Header Background Setting','alterna'),
						'type' 	=> 'moreline',

						'moreline'	=> array(
							'global_bg_type'		=> array(
								'name'	=> __('Header Background Type','alterna'),
								'type'	=>	'select',
								'property'	=> 'global-header-area-bg-type',
								'default'	=> 0,
								'options'	=> array(0 => 'Pattern', 1 => 'Image', 2 => 'Color'),
								'enable-element' => 'yes',
								'enable-id'		=> '0-global_header_bg_type_pattern:1-global_header_bg_type_image:2-global_header_bg_type_color',
								'enable-group'	=> 'global_header_bg_type_group'
							),
							'global_bg_color'		=> array(
								'name'	=> __('Header Background Color','alterna'),
								'type'	=>	'color',
								'property'	=> 'global-header-area-bg-color',
								'desc'	=> __('When background type select "Color" use it.','alterna'),
								'enabled-id'		=> 'global_header_bg_type_color',
								'enable-group'	=> 'global_header_bg_type_group'
							),
							'global_bg_width'		=> array(
								'name'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-header-area-bg-pattern-width',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'	=> 'global_header_bg_type_pattern',
								'enable-group'	=> 'global_header_bg_type_group'
							),
							'global_bg_height'		=> array(
								'name'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-header-area-bg-pattern-height',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'	=> 'global_header_bg_type_pattern',
								'enable-group'	=> 'global_header_bg_type_group'
							),
							'global_bg'		=> array(
								'name'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-header-area-bg-image',
								'enabled-id'		=> 'global_header_bg_type_pattern global_header_bg_type_image',
								'enable-group'	=> 'global_header_bg_type_group'
							),
							'global_bg_retina'		=> array(
								'name'	=> __('Background Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-header-area-bg-retina-image',
								'desc'	=> __('If you use "Pattern" type then need upload size is 400px * 400px retina support image. ','alterna'),
								'enabled-id'	=> 'global_header_bg_type_pattern',
								'enable-group'	=> 'global_header_bg_type_group'
							)
						)
					),
				'global_page_title_background'		=> array(
						'title' => __('Page Title Background Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'global_bg_type'		=> array(
								'name'	=> __('Page Title Background Type','alterna'),
								'type'	=>	'select',
								'property'	=> 'global-page-title-bg-type',
								'default'	=> 0,
								'options'	=> array(0 => 'Pattern', 1 => 'Image', 2 => 'Color'),
								'enable-element' => 'yes',
								'enable-id'		=> '0-global_title_bg_type_pattern:1-global_title_bg_type_image:2-global_title_bg_type_color',
								'enable-group'	=> 'global_title_bg_type_group'
							),
							'global_bg_color'		=> array(
								'name'	=> __('Page Title Background Color','alterna'),
								'type'	=>	'color',
								'property'	=> 'global-page-title-bg-color',
								'desc'	=> __('When background type select "Color" use it.','alterna'),
								'enabled-id'		=> 'global_title_bg_type_color',
								'enable-group'	=> 'global_title_bg_type_group'
							),
							'global_bg_width'		=> array(
								'name'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-page-title-bg-pattern-width',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'		=> 'global_title_bg_type_pattern',
								'enable-group'	=> 'global_title_bg_type_group'
							),
							'global_bg_height'		=> array(
								'name'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-page-title-bg-pattern-height',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'		=> 'global_title_bg_type_pattern',
								'enable-group'	=> 'global_title_bg_type_group'
							),
							'global_bg'		=> array(
								'name'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-page-title-bg-image',
								'enabled-id'		=> 'global_title_bg_type_pattern global_title_bg_type_image',
								'enable-group'	=> 'global_title_bg_type_group '
							),
							'global_bg_retina'		=> array(
								'name'	=> __('Background Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-page-title-bg-retina-image',
								'desc'	=> __('If you use "Pattern" type then need upload size is 400px * 400px retina support image. ','alterna'),
								'enabled-id'		=> 'global_title_bg_type_pattern',
								'enable-group'	=> 'global_title_bg_type_group '
							)
						)
					),
					'global_content_background'		=> array(
						'title' => __('Content Background Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'global_content_bg_type'		=> array(
								'name'	=> __('Background Image Type','alterna'),
								'type'	=>	'select',
								'property'	=> 'global-content-bg-type',
								'default'	=> 2,
								'options'	=> array(0 => 'Pattern', 1 => 'Image', 2 => 'Color'),
								'enable-element' => 'yes',
								'enable-id'		=> '0-global_content_bg_type_pattern:1-global_content_bg_type_image:2-global_content_bg_type_color',
								'enable-group'	=> 'global_content_bg_type_group'
							),
							'global_content_bg_color'		=> array(
								'name'	=> __('Background Color','alterna'),
								'type'	=>	'color',
								'property'	=> 'global-content-bg-color',
								'desc'	=> __('When background type select "Color" use it.','alterna'),
								'enabled-id'		=> 'global_content_bg_type_color',
								'enable-group'	=> 'global_content_bg_type_group'
							),
							'global_content_bg_width'		=> array(
								'name'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-content-bg-pattern-width',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'		=> 'global_content_bg_type_pattern',
								'enable-group'	=> 'global_content_bg_type_group'
							),
							'global_content_bg_height'		=> array(
								'name'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'property'	=> 'global-content-bg-pattern-height',
								'desc'	=> __('When background type select "Pattern" use it.','alterna'),
								'enabled-id'		=> 'global_content_bg_type_pattern',
								'enable-group'	=> 'global_content_bg_type_group'
							),
							'global_content_bg'		=> array(
								'name'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-content-bg-image',
								'enabled-id'		=> 'global_content_bg_type_pattern global_content_bg_type_image',
								'enable-group'	=> 'global_content_bg_type_group'
							),
							'global_content_bg_retina'		=> array(
								'name'	=> __('Background Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'property'	=> 'global-content-bg-retina-image',
								'desc'	=> __('If you use "Pattern" type then need upload size is 400px * 400px retina support image. ','alterna'),
								'enabled-id'	=> 'global_content_bg_type_pattern',
								'enable-group'	=> 'global_content_bg_type_group'
							)
						)
					),
			
			),
		);
		
	/* Header */
	$page_content[] = array(
				'section' => 'header',
				'icon'	=>	'icon-tasks',
				'name' => __('Header','alterna'),
				'title' => __('Header Setting','alterna'),
				'elements'	=> array(
					'header_style'	=> array(
						'title' => __('Header Style Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
							'header_type'		=> array(
								'name'	=> __('Header Style','alterna'),
								'type'	=>	'select',
								'property'	=> 'header-style-type',
								'default'	=> 0,
								'options'	=> array(0 => 'Bottom Menu Style #1', 1 => 'Right Menu Style #2', 2 => 'All Center Style #3', 3 => 'Right Menu Style #4', 4 => 'Right Menu Style #5'),
								'enable-element' => 'yes',
								'enable-id'		=> '0-pe_header_custom_enable:1-pe_header_custom_enable_1:2-pe_header_custom_enable_2',
								'enable-group'	=> 'pe_header_custom_enable_group',
							),
							'top_banner_enable'	=> array(
									'name'	=> __('Enable Top Banner','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable top banner','alterna'),
									'property' => 'header-banner-enable',
									'enable-element' => 'yes',
									'enable-id'		=> '1-pe_topbanner_enable',
									'enable-group'	=> 'pe_topbanner_enable_group'
								),
							'top_topbar_enable'	=> array(
									'name'	=> __('Enable Topbar','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable topbar','alterna'),
									'property' => 'header-topbar-enable',
									'enable-element' => 'yes',
									'enable-id'		=> '1-pe_topbar_enable',
									'enable-group'	=> 'pe_topbar_enable_group'
								),
							'search_enable'	=> array(
									'name'	=> __('Enable Search Form','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable search form','alterna'),
									'property' => 'header-search-enable'
								),
						)
					),
					'logo_custom'	=> array(
						'title' => __('Custom Logo','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
								'logo_enable_txt'	=> array(
									'name'	=> __('Enable text logo','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable text logo','alterna'),
									'property' => 'logo-txt-enable',
									'enable-element' => 'yes',
									'enable-id'		=> '0-pe_logo_txt_enable',
									'enable-group'	=> 'pe_logo_txt_enable_group'
								),
								'logo_image'	=> array(
									'name'	=> __('Logo image url','alterna'),
									'type' 	=> 'upload',
									'property' => 'logo-image',
									'enabled-id'		=> 'pe_logo_txt_enable',
									'enable-group'	=> 'pe_logo_txt_enable_group'
									),
								'logo_retina_image'	=> array(
									'name'	=> __('Logo retina image @2x','alterna'),
									'type' 	=> 'upload',
									'property' => 'logo-retina-image',
									'desc'	=> __('You need upload a retina logo @2x default logo size. ','alterna'),
									'enabled-id'		=> 'pe_logo_txt_enable',
									'enable-group'	=> 'pe_logo_txt_enable_group'
									),
								'logo_image_width' => array(
									'name'	=> __('Logo image width','alterna'),
									'type' 	=> 'number',
									'property' => 'logo-image-width',
									'after'	=>	'px',
									'enabled-id'		=> 'pe_logo_txt_enable',
									'enable-group'	=> 'pe_logo_txt_enable_group'
									),
								'logo_image_height' => array(
									'name'	=> __('Logo image height','alterna'),
									'type' 	=> 'number',
									'property' => 'logo-image-height',
									'after'	=>	'px',
									'enabled-id'		=> 'pe_logo_txt_enable',
									'enable-group'	=> 'pe_logo_txt_enable_group'
									),
								'logo_image_padding-top' => array(
									'name'	=> __('Logo image padding top','alterna'),
									'type' 	=> 'number',
									'property' => 'logo-image-padding-top',
									'after'	=>	'px',
									'enabled-id'		=> 'pe_logo_txt_enable',
									'enable-group'	=> 'pe_logo_txt_enable_group'
									)
							)
					),
					'fixed_header'	=> array(
						'title' => __('Fixed Header','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
								'fixed_enable'	=> array(
									'name'	=> __('Enable fixed header','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable fixed header','alterna'),
									'property' => 'fixed-enable',
									'enable-element' => 'yes',
									'enable-id'		=> '1-pe_fixed_header_enable',
									'enable-group'	=> 'pe_fixed_header_enable_group'
								),
								'fixed_logo_image'	=> array(
									'name'	=> __('Fixed header logo image url','alterna'),
									'type' 	=> 'upload',
									'property' => 'fixed-logo-image',
									'desc'	=> __('It\'s important logo when scroll window show fixed header! Logo height fixed 44px.','alterna'),
									'enabled-id'		=> 'pe_fixed_header_enable',
									'enable-group'	=> 'pe_fixed_header_enable_group'
									),
								'fixed_logo_retina_image'	=> array(
									'name'	=> __('Fixed header logo retina image @2x','alterna'),
									'type' 	=> 'upload',
									'property' => 'fixed-logo-retina-image',
									'desc'	=> __('You need upload a retina support logo @2x default logo.','alterna'),
									'enabled-id'		=> 'pe_fixed_header_enable',
									'enable-group'	=> 'pe_fixed_header_enable_group'
									),
								'fixed_logo_image_width' => array(
									'name'	=> __('Fixed header logo image width','alterna'),
									'type' 	=> 'number',
									'property' => 'fixed-logo-image-width',
									'after'	=>	'px',
									'enabled-id'		=> 'pe_fixed_header_enable',
									'enable-group'	=> 'pe_fixed_header_enable_group'
									),
								'header_right_area_padding_top' => array(
									'name'	=> __('Fixed Header right area padding from Top','alterna'),
									'type' 	=> 'number',
									'property' => 'fixed-header-right-area-padding-top',
									'after'	=>	'px',
									'desc'	=> __('Support header style #4.','alterna'),
									'enabled-id'		=> 'pe_fixed_header_enable',
									'enable-group'	=> 'pe_fixed_header_enable_group'
									)
							)
					),
					'header_banner_setting'	=> array(
						'title' => __('Header Banner Setting','alterna'),
						'type' 	=> 'moreline',
						'enabled-id'	=> 'pe_topbanner_enable',
						'enable-group'	=> 'pe_topbanner_enable_group',
						'moreline'	=> array(
								'header_banner_id' => array(
									'name'	=> __('ID','alterna'),
									'type'	=>	'input',
									'property' => 'header-banner-id',
									'desc'	=> __('Use it different before banner id','alterna')
									),
								'header_banner_content' => array(
									'name'	=> __('Content','alterna'),
									'type'	=>	'textarea',
									'property' => 'header-banner-content',
									'desc'	=> __('Support HTML format','alterna')
									)
							)
					),
					'header_topbar_setting'	=> array(
						'title' => __('Header Topbar Setting','alterna'),
						'type' 	=> 'moreline',
						'enabled-id'		=> 'pe_topbar_enable',
						'enable-group'	=> 'pe_topbar_enable_group',
						'moreline'	=> array(
							'topbar_type'		=> array(
								'name'	=> __('Topbar Style','alterna'),
								'type'	=>	'select',
								'property'	=> 'header-topbar-style-type',
								'default'	=> 0,
								'options'	=> array(0 => 'Old Text Information Style', 1 => 'NEW Topbar Menu Style')
							),
							'top_topbar_menu_enable'	=> array(
									'name'	=> __('Enable Topbar Left Menu ','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable topbar left area menu','alterna'),
									'property' => 'header-topbar-menu-enable'
								),
							'top_topbar_login_enable'	=> array(
									'name'	=> __('Enable Topbar Login & Cart','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable topbar show login & cart','alterna'),
									'property' => 'header-topbar-login-enable'
								),
							'top_topbar_login_page'	=> array(
								'name'	=> __('Custom Login Page Link','alterna'),
								'type'	=>	'input',
								'property'	=> 'header-topbar-custom-login-page',
								'desc'	=> __('Click login tools turn to which page (Which had used "Login Template" )!','alterna')
							),
							'top_topbar_social_enable'	=> array(
									'name'	=> __('Enable Topbar Social','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable topbar show login & cart','alterna'),
									'property' => 'header-topbar-social-enable'
								),
							'top_topbar_wpml_enable'	=> array(
									'name'	=> __('Enable Topbar Language','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable topbar wpml language switcher','alterna'),
									'property' => 'header-topbar-wpml-enable'
								),
							'top_topbar_message'		=> array(
									'name'	=> __('Topbar Information Content','alterna'),
									'type'	=>	'textarea',
									'property'	=> 'header-alert-message',
									'desc' => __('If you used old topbar style, here input information content (Support HTML)','alterna'),
								)
						)
					),
					'header_style_1_area'	=> array(
						'title' => __('Header Style 1, 2, 3 Custom Area Setting','alterna'),
						'type' 	=> 'moreline',
						'enabled-id'		=> 'pe_header_custom_enable pe_header_custom_enable_1 pe_header_custom_enable_2',
						'enable-group'	=> 'pe_header_custom_enable_group',
						'moreline'	=> array(
							'area_type'		=> array(
								'name'	=> __('Custom Area Show Type','alterna'),
								'type'	=>	'select',
								'property'	=> 'header-right-area-type',
								'default'	=> 0,
								'options'	=> array(0 => 'Default Show Social', 1 => 'Custom Content'),
								'desc'	=> __('If you want to custom content, please choose "Custom". It\'s works for header style 1 right social area and header style 2, 3 social area ','alterna')
							),
							'area_custom_content'	=> array(
								'name'	=> __('Header Custom Area Content','alterna'),
								'type'	=>	'textarea',
								'property'	=> 'header-right-area-content',
								'desc'	=> __('Support HTML CODE for your custom content!','alterna')
							),
						)
					),
					'padding_custom'	=> array(
						'title' => __('Header Padding Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
								'header_right_area_padding_top' => array(
									'name'	=> __('Header right area padding from Top','alterna'),
									'type' 	=> 'number',
									'property' => 'header-right-area-padding-top',
									'after'	=>	'px',
									'desc'	=> __('Support header style #2, #5.','alterna')
									),
								'header_social_padding_left' => array(
									'name'	=> __('Header Social padding from Left','alterna'),
									'type' 	=> 'number',
									'property' => 'header-social-padding-left',
									'after'	=>	'px',
									),
								'header_social_padding_top' => array(
									'name'	=> __('Header Social padding from Top','alterna'),
									'type' 	=> 'number',
									'property' => 'header-social-padding-top',
									'after'	=>	'px',
									)
							)
					)
				)
			);
	
	/* Footer */
	$page_content[] = array(
				'section' => 'footer',
				'icon'	=>	'icon-ellipsis-horizontal',
				'name' => __('Footer','alterna'),
				'title' => __('Footer Setting','alterna'),
				'elements'	=> array(
					'footer_copyright_message'		=> array(
						'name'	=> __('Footer Copyright Setting','alterna'),
						'title'	=> __('Copyright Text','alterna'),
						'type'	=>	'textarea',
						'property'	=> 'footer-copyright-message',
					),
					'footer_link'		=> array(
						'name'	=> __('Footer Link Setting','alterna'),
						'title'	=> __('Link Text','alterna'),
						'type'	=>	'textarea',
						'property'	=> 'footer-link-text',
					),
					'footer_banner_setting'	=> array(
						'title' => __('Footer Banner Setting','alterna'),
						'type' 	=> 'moreline',
						'moreline'	=> array(
								'footer_banner_enable'	=> array(
									'name'	=> __('Enable Bottom Banner','alterna'),
									'type' 	=> 'pc',
									'desc' => __('Check to enable bottom banner','alterna'),
									'property' => 'footer-banner-enable',
									'enable-element' => 'yes',
									'enable-id'		=> '1-pe_footer_banner_enable',
									'enable-group'	=> 'pe_footer_banner_enable_group'
								),
								'footer_banner_id' => array(
									'name'	=> __('ID','alterna'),
									'type'	=>	'input',
									'property' => 'footer-banner-id',
									'desc'	=> __('Use it different before banner id','alterna'),
									'enabled-id'		=> 'pe_footer_banner_enable',
									'enable-group'	=> 'pe_footer_banner_enable_group'
									),
								'footer_banner_content' => array(
									'name'	=> __('Content','alterna'),
									'type'	=>	'textarea',
									'property' => 'footer-banner-content',
									'desc'	=> __('Support HTML format','alterna'),
									'enabled-id'		=> 'pe_footer_banner_enable',
									'enable-group'	=> 'pe_footer_banner_enable_group'
									)
							)
					),
				)
			);
			
	/* Font Setting Page */
	$page_content[] = array(
			'section' => 'font',
			'icon'	=>	'icon-text-width',
			'name' => __('Font','alterna'),
			'title' => __('Font Setting','alterna'),
			'elements'	=> array(
					'font_enable'		=> array(
						'title'	=> __('Enable Font Setting','alterna'),
						'name'	=> __('Enable Custom Font','alterna'),
						'type' 	=> 'pc',
						'property' => 'custom-enable-font',
						'desc'	=> __('Just when enable custom font,then all choose font will run.','alterna'),
						'enable-element' => 'yes',
						'enable-id'		=> '1-pe_custom_font',
						'enable-group'	=> 'pe_custom_font_group'
						),
					'font_setting'	=> array(
						'title'	=> __('Theme Font Setting','alterna'),
						'type'	=>	'moreline',
						'enabled-id'		=> 'pe_custom_font',
						'enable-group'	=> 'pe_custom_font_group',
						'moreline'	=> array(
								'general_font' => array(
									'name'	=> __('General Font','alterna'),
									'type' 	=> 'select',
									'property' => 'custom-general-font',
									'default_option'	=> 'Default: Source Sans Pro',
									'option_array'	=> $google_fonts,
									'desc' => __('Now have 620+ Google web fonts for u choose!','alterna')
									),
								'general_font_weight' => array(
									'name'	=> __('Font Weight','alterna'),
									'type' 	=> 'input',
									'default'	=> '400,400italic,700,700italic',
									'property' => 'custom-general-font-weight',
									'desc' => __('You can check font support style http://google.com/fonts/','alterna')
									),
								'general_font_size' => array(
									'name'	=> '',
									'type' 	=> 'input',
									'input_type'	=> 'number',
									'property' => 'custom-general-font-size',
									'after'	=>	'px'
									),
								'top_nav_font' => array(
									'name'	=> __('Header Menu Font','alterna'),
									'type' 	=> 'select',
									'property' => 'custom-menu-font',
									'default_option'	=> 'Default: Oswald',
									'option_array'	=> $google_fonts
									),
								'top_nav_font_weight' => array(
									'name'	=> __('Font Weight','alterna'),
									'type' 	=> 'input',
									'default'	=> '400',
									'property' => 'custom-menu-font-weight',
									),
								'top_nav_font_size' => array(
									'name'	=> '',
									'type' 	=> 'input',
									'input_type'	=> 'number',
									'property' => 'custom-menu-font-size',
									'after'	=>	'px'
									),
								'title_font' => array(
									'name'	=> __('Title (h1-h6) Font','alterna'),
									'type' 	=> 'select',
									'property' => 'custom-title-font',
									'default_option'	=> 'Default: Open Sans',
									'option_array'	=> $google_fonts
									),
								'title_font_weight' => array(
									'name'	=> __('Font Weight','alterna'),
									'type' 	=> 'input',
									'default'	=> '300,300italic,400,400italic,700,700italic',
									'property' => 'custom-title-font-weight',
									),
								'subset_font' => array(
									'name'	=> __('Google Fonts Subsets','alterna'),
									'type' 	=> 'input',
									'property' => 'google-font-subset',
									'desc' => __('Some of the fonts in the Google Font Directory support multiple scripts (like Latin and Cyrillic for example). Example: "latin,cyrillic" please use "," for more subsets ','alterna')
									),
							)
					),
				)
			);
	
	/* Color Setting Page */
	$page_content[] = array(
			'section' => 'color',
			'icon'	=>	'icon-magic',
			'name' => __('Color','alterna'),
			'title' => __('Color Setting','alterna'),
			'elements'	=> array(
					'enable_colors'		=> array(
						'title'	=> __('Enable Color Setting','alterna'),
						'name'	=> __('Enable Custom Color','alterna'),
						'type' 	=> 'pc',
						'property' => 'custom-enable-color',
						'desc'	=> __('Just when enable custom color,then all choose color will run.','alterna'),
						'enable-element' => 'yes',
						'enable-id'		=> '1-pe_custom_color',
						'enable-group'	=> 'pe_custom_color_group'
						),
					'theme_color'		=> array(
						'title'	=> __('Theme Colors Setting','alterna'),
						'type'	=>	'moreline',
						'enabled-id' 	=> 'pe_custom_color',
						'enable-group'	=> 'pe_custom_color_group',
						'moreline'	=> array(
								'theme_color' => array(
									'name'	=> __('Theme Color','alterna'),
									'type' 	=> 'color',
									'property' => 'theme-color'
									),
								'theme_over_color' => array(
									'name'	=> __('Theme Over Color','alterna'),
									'type' 	=> 'color',
									'property' => 'theme-over-color'
									),
								'general_text_color' => array(
									'name'	=> __('General Text Color','alterna'),
									'type' 	=> 'color',
									'property' => 'custom-general-color',
									'desc'	=> __('General default text color for div,p,span,a color','alterna')
									),
								'a_color' => array(
									'name'	=> __('A default color','alterna'),
									'type' 	=> 'color',
									'property' => 'custom-a-color'
									),
								'h_color' => array(
									'name'	=> __('Title default color','alterna'),
									'type' 	=> 'color',
									'property' => 'custom-h-color',
									'desc'	=> __('h1,h2,h3,h4,h5,h6 default color','alterna')
									),
							),
							
						),
						'theme_menu_color'		=> array(
								'title'	=> __('Header Menu Colors Setting','alterna'),
								'type'	=>	'moreline',
								'enabled-id' 	=> 'pe_custom_color',
								'enable-group'	=> 'pe_custom_color_group',
								'moreline'	=> array(
										'top_menu_bg_color' => array(
											'name'	=> __('Menu Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-menu-background-color',
											'desc'	=> __('Header menu background color','alterna')
											),
										'top_sub_menu_bg_color' => array(
											'name'	=> __('Sub Menu Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-sub-menu-background-color',
											'desc'	=> __('Header sub menu background color','alterna')
											),
										'top_sub_hover_menu_bg_color' => array(
											'name'	=> __('Sub Menu Hover Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-sub-menu-hover-background-color',
											'desc'	=> __('Header sub menu hover background color','alterna')
											),
										'top_sub_hover_menu_border_top_color' => array(
											'name'	=> __('Sub Menu Border Top Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-sub-menu-hover-border-top-color',
											'desc'	=> __('Header sub menu border top color','alterna')
											),
										'top_sub_hover_menu_border_bottom_color' => array(
											'name'	=> __('Sub Menu Border Bottom Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-sub-menu-hover-border-bottom-color',
											'desc'	=> __('Header sub menu border bottom color','alterna')
											),
										'top_menu_font_color' => array(
											'name'	=> __('Menu Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-menu-font-color',
											'desc'	=> __('Header menu font color','alterna')
											),
										'top_hover_menu_font_color' => array(
											'name'	=> __('Menu Hover Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-menu-hover-font-color',
											'desc'	=> __('Header menu over font color','alterna')
											),
										'top_sub_menu_font_color' => array(
											'name'	=> __('Sub Menu Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-sub-menu-font-color',
											'desc'	=> __('Header sub menu font color','alterna')
											),
										'top_sub_hover_menu_font_color' => array(
											'name'	=> __('Sub Menu Hover Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-sub-menu-hover-font-color',
											'desc'	=> __('Header sub menu hover font color','alterna')
											),
									)
							),
						'theme_header_color'		=> array(
								'title'	=> __('Header Area Colors Setting','alterna'),
								'type'	=>	'moreline',
								'enabled-id' 	=> 'pe_custom_color',
								'enable-group'	=> 'pe_custom_color_group',
								'moreline'	=> array(
										'header_top_banner_color' => array(
											'name'	=> __('Top Banner Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-header-banner-bg-color',
											'desc'	=> __('Header top banner area background color','alterna')
											),
										'header_topbar_color' => array(
											'name'	=> __('Topbar Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-header-topbar-bg-color',
											'desc'	=> __('Header topbar area background color','alterna')
											),
										'header_topbar_border_color' => array(
											'name'	=> __('Topbar Border Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-header-topbar-border-color',
											'desc'	=> __('Header topbar area border color','alterna')
											),
										'header_topbar_sub_menu_hover_bg_color' => array(
											'name'	=> __('Topbar Sub Menu Hover Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-header-topbar-sub-menu-hover-bg-color',
											'desc'	=> __('Header topbar area sub menu hover background color','alterna')
											),
										'header_topbar_font_color' => array(
											'name'	=> __('Topbar Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-header-topbar-font-color',
											'desc'	=> __('Header topbar area font color','alterna')
											),
										'header_topbar_font_over_color' => array(
											'name'	=> __('Topbar Hover Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-header-topbar-hover-font-color',
											'desc'	=> __('Header topbar area hover font color','alterna')
											)
									)
							),
						'theme_footer_color'		=> array(
								'title'	=> __('Footer Area Colors Setting','alterna'),
								'type'	=>	'moreline',
								'enabled-id' 	=> 'pe_custom_color',
								'enable-group'	=> 'pe_custom_color_group',
								'moreline'	=> array(
										'footer_txt_color' => array(
											'name'	=> __('Footer Area Text Default Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-text-color',
											'desc'	=> __('Footer area text default color','alterna')
											),
										'footer_a_color' => array(
											'name'	=> __('Footer Area A Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-a-color',
											'desc'	=> __('Footer area a text color','alterna')
											),
										'footer_h_color' => array(
											'name'	=> __('Footer Title Default Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-h-color',

											'desc'	=> __('h1,h2,h3,h4,h5,h6 footer area title text color ','alterna')
											),
										'footer_bg_color' => array(
											'name'	=> __('Footer Area Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-bg-color',
											'desc'	=> __('Footer area background color','alterna')
											),
										'footer_copyright_a_color' => array(
											'name'	=> __('Footer Copyright Area A Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-copyright-a-color',
											'desc'	=> __('Footer copyright area a color','alterna')
											),
										'footer_copyright__a_hover_color' => array(
											'name'	=> __('Footer Copyright Area A Hover Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-copyright-a-hover-color',
											'desc'	=> __('Footer copyright area a hover color','alterna')
											),
										'footer_copyright_color' => array(
											'name'	=> __('Footer Copyright Area Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-copyright-color',
											'desc'	=> __('Footer copyright area background color','alterna')
											),
										'footer_top_banner_color' => array(
											'name'	=> __('Footer Banner Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'custom-footer-banner-bg-color',
											'desc'	=> __('Header footer banner area background color','alterna')
											),
									)
							),
							'theme_fixed_header_color'		=> array(
								'title'	=> __('Fixed Header Area Colors Setting','alterna'),
								'type'	=>	'moreline',
								'enabled-id' 	=> 'pe_custom_color',
								'enable-group'	=> 'pe_custom_color_group',
								'moreline'	=> array(
										'enable_fixed_colors'		=> array(
										'name'	=> __('Enable Fixed Custom Color','alterna'),
										'type' 	=> 'pc',
										'property' => 'fixed-header-enable-color',
										'desc'	=> __('Just when enable custom color,then all fixed header color will run.','alterna'),
										'enable-element' => 'yes',
										'enable-id'		=> '1-pe_custom_fixed_color',
										'enable-group'	=> 'pe_custom_fixed_color_group'
										),
										'fixed_bg_color' => array(
											'name'	=> __('Fixed Area Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-background-color',
											'desc'	=> __('Fixed Header area background color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_sub_menu_bg_color' => array(
											'name'	=> __('Sub Menu Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-sub-menu-background-color',
											'desc'	=> __('Fixed Header sub menu background color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_sub_hover_menu_bg_color' => array(
											'name'	=> __('Sub Menu Hover Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-sub-menu-hover-background-color',
											'desc'	=> __('Fixed Header sub menu hover background color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_menu_font_color' => array(
											'name'	=> __('Menu Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-menu-font-color',
											'desc'	=> __('Fixed Header menu font color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_menu_hover_font_color' => array(
											'name'	=> __('Menu Hover Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-menu-hover-font-color',
											'desc'	=> __('Fixed Header menu hover font color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_sub_menu_font_color' => array(
											'name'	=> __('Sub Menu Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-sub-menu-font-color',
											'desc'	=> __('Fixed Header sub menu font color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_sub_hover_menu_font_color' => array(
											'name'	=> __('Sub Menu Hover Font Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-sub-menu-hover-font-color',
											'desc'	=> __('Fixed Header sub menu hover font color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_search_form_bg_color' => array(
											'name'	=> __('Search Form Background Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-form-background-color',
											'desc'	=> __('Fixed Header search form background color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											),
										'fixed_search_form_border_color' => array(
											'name'	=> __('Search Form Border Color','alterna'),
											'type' 	=> 'color',
											'property' => 'fixed-header-form-border-color',
											'desc'	=> __('Fixed Header search form border color','alterna'),
											'enabled-id'		=> 'pe_custom_fixed_color',
											'enable-group'	=> 'pe_custom_fixed_color_group'
											)
									)
							),
						
				)
			);
	
	/* Enbale Css */
	$page_content[] = array(
			'section' => 'css',
			'icon'	=>	'icon-css3',
			'name' => __('CSS','alterna'),
			'title' => __('Custom Css Setting','alterna'),
			'elements'	=> array(
			'enable_custom_css'		=> array(
						'title'	=> __('Enable Custom CSS','alterna'),
						'type'	=>	'moreline',
						'moreline'	=> array(
								'enable_css' => array(
									'name'	=> __('Enable Custom CSS','alterna'),
									'type' 	=> 'pc',
									'desc' 	=> __('Check here enable custom css for theme','alterna'),
									'property' => 'custom-enable-css',
									'enable-element' => 'yes',
									'enable-id'		=> '1-pe_custom_css',
									'enable-group'	=> 'pe_custom_css_group'
								),
								'custom_css' => array(
									'name'	=> __('Custom CSS','alterna'),
									'type' 	=> 'textarea',
									'codetype' 	=> 'css',
									'property' => 'custom-css-content',
									'enabled-id' 	=> 'pe_custom_css',
									'enable-group'	=> 'pe_custom_css_group'
								),
								'custom_retina_css' => array(
									'name'	=> __('Custom Retina CSS','alterna'),
									'type' 	=> 'textarea',
									'codetype' 	=> 'css',
									'property' => 'custom-css-retina-content',
									'enabled-id' 	=> 'pe_custom_css',
									'enable-group'	=> 'pe_custom_css_group'
								)
							)
						)
					)
			);
	
	/* Enbale Scripts */
	$page_content[] = array(
			'section' => 'scripts',
			'icon'	=>	'icon-code',
			'name' => __('Scripts','alterna'),
			'title' => __('Custom Scripts Setting','alterna'),
			'elements'	=> array(
			'enable_custom_scripts'		=> array(
						'title'	=> __('Enable Custom Scripts','alterna'),
						'type'	=>	'moreline',
						'moreline'	=> array(
								'enable_scripts' => array(
									'name'	=> __('Enable Custom Scripts','alterna'),
									'type' 	=> 'pc',
									'desc' 	=> __('Check here enable custom scripts for theme','alterna'),
									'property' => 'custom-enable-scripts',
									'enable-element' => 'yes',
									'enable-id'		=> '1-pe_custom_scripts',
									'enable-group'	=> 'pe_custom_scripts_group'
								),
								'custom_scripts' => array(
									'name'	=> __('Custom Scripts','alterna'),
									'type' 	=> 'textarea',
									'codetype' 	=> 'javascript',
									'property' => 'custom-scripts-content',
									'enabled-id'		=> 'pe_custom_scripts',
									'enable-group'	=> 'pe_custom_scripts_group'
								)
							)
						)
					)
			);
	
	/* Blog Page */
	$page_content[] = array(
				'section' => 'blog',
				'icon'	=>	'icon-comment',
				'name' => __('Blog','alterna'),
				'title' => __('Blog','alterna'),
				'elements'	=> array(
						'blog_cat_tag' => array(
									'title'	=> __('Blog Show Type, Category, Tags Show Setting','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
													'blog_type' => array(
																	'name'	=> __('Blog Show Type','alterna'),
																	'type' 	=> 'select',
																	'options'	=> array(
																			0 => __('Default: Large Width','alterna'),
																			1 => __('Mid Width','alterna')
																		),
																	'property' => 'blog-show-type',
																	'desc'	=> __('Default global blog posts show type.','alterna'),
													),
													'blog_category_tags' => array(
																	'name'	=> __('Blog Category, Tags Style','alterna'),
																	'type' 	=> 'select',
																	'options'	=> array(
																			0 => __('Default','alterna'),
																			1 => __('Waterfall Flux','alterna')
																		),
																	'property' => 'blog-cat-tags-style',
													),
													'blog_page'	=> array(
																		'name'	=> __('Default Waterfall Flux Page','alterna'),
																		'type' 	=> 'select',
																		'property' => 'blog-waterfall-page',
																		'desc'	=> __('Default blog page for category, tag show style just when you choose "Waterfall Flux" style.','alterna'),
																		'options'	=> alterna_get_all_blog_waterfall_type_pages()
																	)
															)
												),
						'blog_page_top_content'	=> array(
											'title'	=> __('Blog Category, Tag Page Top Area Content','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
													'blog_enable_breadchrumb' => array(
																		'name'	=> __('Category, Tag Page Top Area Content','alterna'),
																		'type'	=>	'pc',
																		'desc' => __('Check to enable show default blog page content for Category, Tags top area content.','alterna'),
																		'property'	=> 'blog-enable-top-content'
																	)
													)
												),
						'blog_breadchrumb'	=> array(
											'title'	=> __('Blog Single Post Breadchrumb Setting','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
													'blog_enable_breadchrumb' => array(
																		'name'	=> __('Category for Single Post Breadchrumb','alterna'),
																		'type'	=>	'pc',
																		'desc' => __('Check to enable show single post breadchrumb with category','alterna'),
																		'property'	=> 'blog-enable-breadchrumb'
																	)
													)
												),
						'blog_author' => array(
											'title'	=> __('Author Information Setting','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
													'blog_author_link' => array(
																		'name'	=> __('Author link type','alterna'),
																		'type' 	=> 'select',
																		'options'	=> array(
																				0 => __('Link to author website','alterna'),
																				1 => __('Link to author page','alterna'),
																			),
																		'desc' => __('Choose post author link type.','alterna'),
																		'property'	=> 'blog-author-link'
																	),
													'blog_enable_author' => array(
																		'name'	=> __('Author information','alterna'),
																		'type'	=>	'pc',
																		'desc' => __('Check to enable single post page show author information','alterna'),
																		'property'	=> 'blog-enable-author'
																	)
													)
												),
						'blog_enable_share' => array(
									'title'	=> __('Post Share Setting','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
															'enable_share' => array(
																	'name'	=> __('Share style type','alterna'),
																	'type' 	=> 'select',
																	'options'	=> array(
																			0 => __('No share','alterna'),
																			1 => __('AddThis share','alterna'),
																			2 => __('Custom share use code','alterna')
																		),
																	'desc'	=> __('Default style for single page share!','alterna'),
																	'property' => 'blog-share-type',
																	'enable-element' => 'yes',
																	'enable-id'		=> '2-pe_blog_share',
																	'enable-group'	=> 'pe_blog_share_group'
																	),
															'custom_share' => array(
																	'name'	=> __('Custom share code','alterna'),
																	'type' 	=> 'textarea',
																	'property' => 'blog-share-code',
																	'desc'	=> __('You can copy your share plugin code into here.','alterna'),
																	'enabled-id'		=> 'pe_blog_share',
																	'enable-group'	=> 'pe_blog_share_group'
																	)
																)
												),
						)
			);
	
	/* Portfolio Page */		
	$page_content[] = array(
				'section' => 'portfolio',
				'icon'	=>	'icon-th-large',
				'name' => __('Portfolio','alterna'),
				'title' => __('Portfolio','alterna'),
				'elements'	=> array(
						'portfolio_page'	=> array(
											'title'	=> __('Default Portfolio Page','alterna'),
											'name'	=> __('Default Portfolio Page','alterna'),
											'type' 	=> 'select',
											'property' => 'portfolio-default-page',
											'desc'	=> __('Default portfolio page for category default show style.','alterna'),
											'options'	=> alterna_get_all_portfolio_type_pages()
										),
						'portfolio_breadchrumb'	=> array(
											'title'	=> __('Portfolio Single Post Breadchrumb Setting','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
													'blog_enable_breadchrumb' => array(
																		'name'	=> __('Category for Single Post Breadchrumb','alterna'),
																		'type'	=>	'pc',
																		'desc' => __('Check to enable show single post breadchrumb with category','alterna'),
																		'property'	=> 'portfolio-enable-breadchrumb'
																	)
													)
												),
						'portfolio_enable_share' => array(
									'title'	=> __('Portfolio Share Setting','alterna'),
											'type'	=>	'moreline',
											'moreline'	=> array(
															'enable_share' => array(
																	'name'	=> __('Share style type','alterna'),
																	'type' 	=> 'select',
																	'options'	=> array(
																			0 => __('No share','alterna'),
																			1 => __('AddThis share','alterna'),
																			2 => __('Custom share use code','alterna')
																		),
																	'desc'	=> __('Default style for single portfolio page share!','alterna'),
																	'property' => 'portfolio-share-type',
																	'enable-element' => 'yes',
																	'enable-id'		=> '2-pe_portfolio_share',
																	'enable-group'	=> 'pe_portfolio_share_group'
																	),
															'custom_share' => array(
																	'name'	=> __('Custom share code','alterna'),
																	'type' 	=> 'textarea',
																	'property' => 'portfolio-share-code',
																	'desc'	=> __('You can copy your share plugin code into here.','alterna'),
																	'enabled-id'		=> 'pe_portfolio_share',
																	'enable-group'	=> 'pe_portfolio_share_group'
																	)
																)
												)
					)
			);
			
	/* Social */
	$page_content[] = array(
				'section' => 'social',
				'icon'	=>	'icon-twitter',
				'name' => __('Socials','alterna'),
				'title' => __('Socials','alterna'),
				'elements'	=> array(
					'social'	=> array(
						'title'	=> __('Social Links with http://','alterna'),
						'type'	=>	'moreline',
						'moreline'	=> array(
								'social_email'	=> array(
										'name'	=> __('Email','alterna'),
										'type' 	=> 'input',
										'property' => 'social-email'
									),
								'social_twitter'	=> array(
										'name'	=> __('Twitter','alterna'),
										'type' 	=> 'input',
										'property' => 'social-twitter'
									),
								'social_facebook'	=> array(
										'name'	=> __('Facebook','alterna'),
										'type' 	=> 'input',
										'property' => 'social-facebook'
									),
								'social_google_plus'	=> array(
										'name'	=> __('Google Plus','alterna'),
										'type' 	=> 'input',
										'property' => 'social-google-plus'
									),
								'social_dribbble'	=> array(
										'name'	=> __('Dribbble','alterna'),
										'type' 	=> 'input',
										'property' => 'social-dribbble'
									),
								'social_pinterest'	=> array(
										'name'	=> __('Pinterest','alterna'),
										'type' 	=> 'input',
										'property' => 'social-pinterest'
									),
								'social_flickr'	=> array(
										'name'	=> __('Flickr','alterna'),
										'type' 	=> 'input',
										'property' => 'social-flickr'
									),
								'social_skype'	=> array(
										'name'	=> __('Skype','alterna'),
										'type' 	=> 'input',
										'property' => 'social-skype'
									),
								'social_youtube'	=> array(
										'name'	=> __('Youtube','alterna'),
										'type' 	=> 'input',
										'property' => 'social-youtube'
									),
								'social_vimeo'	=> array(
										'name'	=> __('Vimeo','alterna'),
										'type' 	=> 'input',
										'property' => 'social-vimeo'
									),
								'social_linkedin'	=> array(
										'name'	=> __('Linkedin','alterna'),
										'type' 	=> 'input',
										'property' => 'social-linkedin'
									),
								'social_digg'	=> array(
										'name'	=> __('Digg','alterna'),
										'type' 	=> 'input',
										'property' => 'social-digg'
									),
								'social_deviantart'	=> array(
										'name'	=> __('Deviantart','alterna'),
										'type' 	=> 'input',
										'property' => 'social-deviantart'
									),
								'social_behance'	=> array(
										'name'	=> __('Behance','alterna'),
										'type' 	=> 'input',
										'property' => 'social-behance'
									),
								'social_forrst'	=> array(
										'name'	=> __('Forrst','alterna'),
										'type' 	=> 'input',
										'property' => 'social-forrst'
									),
								'social_lastfm'	=> array(
										'name'	=> __('Lastfm','alterna'),
										'type' 	=> 'input',
										'property' => 'social-lastfm'
									),
								'social_xing'	=> array(
										'name'	=> __('XING','alterna'),
										'type' 	=> 'input',
										'property' => 'social-xing'
									),
								'social_instagram'	=> array(
										'name'	=> __('Instagram','alterna'),
										'type' 	=> 'input',
										'property' => 'social-instagram'
									),
								'social_stumbleupon'	=> array(
										'name'	=> __('StumbleUpon','alterna'),
										'type' 	=> 'input',
										'property' => 'social-stumbleupon'
									),
								'social_picasa'	=> array(
										'name'	=> __('Picasa','alterna'),
										'type' 	=> 'input',
										'property' => 'social-picasa'
									)
							)
					)
				)
			);
	
	$page_content[] =  array('section' => 'update',	'icon'	=> 'icon-bullhorn','name' => __('Update Log','alterna') ,'title' => __('Update History', 'alterna'),'type'	=> 'update');
				
	$page_content[] =  array('section' => 'import',	'icon'	=> 'icon-retweet','name' => __('Import/Export','alterna') ,'title' => __('Import/Export Options', 'alterna'),'type'	=> 'import');
	
	$page_content[] =  array('section' => 'get_support','icon'	=> 'icon-question-sign','name' => __('Get Support','alterna'),'title' => 'link','type'	=> 'link',	'class'	=>	'light','pagecontent'	=> 'http://support.themefocus.co');
	
	/**
	 * Option Default Value
	 */
	$page_default_property = array(
		
		'theme-update-enable' => 'off',
		'theme-name'		=>	'',
		'theme-purchase-code'	=> '',
		'theme-api'			=>	'',
		'favicon'			=>	$dir.'/img/favicon.png',
		'rss-feed'			=>	"",
		
		'global-responsive' 			=> 0,
		'global-layout'					=>	0,
		'global-mobile-enable'			=>	'on',
		'global-sidebar-layout'			=>	1,
		'global-breakcrumbs-enable'		=>	'off',
		
		'global-bg-type'	=> 0,
		'global-bg-color'	=> '5a5a5a',
		'global-bg-pattern-width' => 100,
		'global-bg-pattern-height' => 100,
		'global-bg-image'	=>	$dir.'/img/bgnoise_lg.png',
		'global-bg-retina-image'	=>	$dir.'/img/bgnoise_lg@2x.png',
		
		'global-content-bg-type'	=> 2,
		'global-content-bg-color'	=> 'ffffff',
		'global-content-bg-pattern-width' => 0,
		'global-content-bg-pattern-height' => 0,
		'global-content-bg-image'	=>	'',
		'global-content-bg-retina-image'	=>	'',
		
		'global-header-area-bg-type'	=> 2,
		'global-header-area-bg-color'	=> 'ffffff',
		'global-header-area-bg-pattern-width' => 0,
		'global-header-area-bg-pattern-height' => 0,
		'global-header-area-bg-image'	=>	'',
		'global-header-area-bg-retina-image'	=>	'',
		
		'global-page-title-bg-type'	=> 0,
		'global-page-title-bg-color'	=> 'f9f9f9',
		'global-page-title-bg-pattern-width' => 297,
		'global-page-title-bg-pattern-height' => 297,
		'global-page-title-bg-image'	=>	$dir.'/img/bright_squares.png',
		'global-page-title-bg-retina-image'	=>	$dir.'/img/bright_squares@2x.png',
		
		'google_analytics-position'	=> 	"0",
		'google_analytics-text'		=>	"",
		
		'shop-per-page'			=>	24,
		'shop-product-search'	=>	'on',
		
		//header
		'header-style-type'			=>	0,
		'header-search-enable'		=>	'on',
		
		'header-banner-enable'		=>	'on',
		'header-banner-id'			=>	'1',
		'header-banner-content'		=>	'Input Content',
		
		'header-topbar-enable'				=>	'on',
		'header-topbar-wpml-enable'			=>	'on',
		'header-topbar-style-type'			=> 0,
		'header-topbar-menu-enable'			=>	'on',
		'header-topbar-login-enable'		=>	'on',
		'header-topbar-custom-login-page'	=>	'',
		'header-topbar-social-enable'		=>	'on',
		'header-topbar-wpml-enable'			=>	'off',
		'header-alert-message'				=>	'',

		'logo-txt-enable' 	=> 	"off",
		'logo-image'		=> 	$dir.'/img/logo.png',
		'logo-retina-image'		=> 	$dir.'/img/logo@2x.png',
		'logo-image-width'	=> 	227,
		'logo-image-height'	=>	60,
		'logo-image-padding-top' => 0,
		
		'fixed-enable'				=>	"on",
		'fixed-logo-image'			=> 	$dir.'/img/fixed-logo.png',
		'fixed-logo-retina-image'	=> 	$dir.'/img/fixed-logo@2x.png',
		'fixed-logo-image-width'	=> 	44,
		'fixed-header-right-area-padding-top'	=> 0,

		'header-right-area-padding-top'	=>	0,
		'header-social-padding-left' => 0,
		'header-social-padding-top' => 14,

		//footer
		'footer-copyright-message'	=> 'Copyright ® 2014 <a href="http://themeforest.net/user/ThemeFocus">ThemeFocus</a>. All rights reserved.',
		'footer-link-text'	=> 'Powered by WordPress.',
		
		'footer-banner-enable'			=>	'off',
		'footer-banner-id'				=>	'1',
		'footer-banner-content'			=>	'Input Content',
		
		//font
		'custom-enable-font'		=>	"off",
		'google-font-subset'		=>	"",
		'custom-general-font'		=>	"0",
		'custom-general-font-size'	=>	"14",
		'custom-menu-font'			=>	"0",
		'custom-menu-font-size'		=>	"13",
		'custom-title-font'			=>	"0",
				
		//color
		'custom-enable-color'				=>	'off',
		'theme-color'						=>	'7AB80E',
		'theme-over-color'					=>	'5b8f00',
		'custom-general-color'				=>	'666666',
		'custom-a-color'					=>	'1c1c1c',
		'custom-h-color'					=>	'3a3a3a',
		
		'custom-menu-background-color'		=>	'0C0C0C',
		'custom-sub-menu-background-color'	=>	'7AB80E',
		'custom-sub-menu-hover-background-color' =>	'0c0c0c',
		'custom-sub-menu-hover-border-top-color'	=>	'ffffff',
		'custom-sub-menu-hover-border-bottom-color'	=>	'0c0c0c',
		'custom-menu-font-color'			=>	'ffffff',
		'custom-menu-hover-font-color'		=>	'7ab80e',
		'custom-sub-menu-font-color'		=>	'000000',
		'custom-sub-menu-hover-font-color' 	=>	'ffffff',
		
		'custom-header-banner-bg-color'					=>	'f7d539',
		'custom-header-topbar-bg-color'					=>	'f2f2f2',
		'custom-header-topbar-border-color'				=>	'e6e6e6',
		'custom-header-topbar-font-color'				=>	'757575',
		'custom-header-topbar-hover-font-color'			=>	'7AB80E',
		'custom-header-topbar-sub-menu-hover-bg-color'	=>	'f7f7f7',
		
		'custom-footer-text-color'				=>	'999999',
		'custom-footer-a-color'					=>	'e7e7e7',
		'custom-footer-h-color'					=>	'ffffff',
		'custom-footer-bg-color'				=>	'404040',
		'custom-footer-copyright-color'     	=>	'0C0C0C',
		'custom-footer-copyright-a-color' 		=> '606060',
		'custom-footer-copyright-a-hover-color' => '7AB80E',
		'custom-footer-banner-bg-color'			=>	'ffffff',
		
		'fixed-header-enable-color'							=>	'off',
		'fixed-header-background-color' 					=>	'0C0C0C',
		'fixed-header-sub-menu-background-color'			=>	'7AB80E',
		'fixed-header-sub-menu-hover-background-color'	 	=>	'0c0c0c',
		'fixed-header-menu-font-color'						=>	'ffffff',
		'fixed-header-menu-hover-font-color'				=>	'ffffff',
		'fixed-header-sub-menu-font-color'					=>	'000000',
		'fixed-header-sub-menu-hover-font-color'			=>	'ffffff',
		'fixed-header-form-background-color'				=>	'323232',
		'fixed-header-form-border-color'					=>	'000344',
		
		//css
		'custom-enable-css'			=>	'off',
		'custom-css-content'		=>	'',
		'custom-css-retina-content'	=>	'',		
		
		//scripts
		'custom-enable-scripts'			=>	'off',
		'custom-scripts-content'		=>	'',
		
		//blog
		'blog-show-type'		=> 0,
		'blog-cat-tags-style'	=> 0,
		'blog-waterfall-page'	=> "",
		'blog-enable-top-content'	=>	'off',
		'blog-enable-breadchrumb'	=>	'off',
		'blog-author-link'		=> 0,
		'blog-enable-author'	=> 	"off",
		'blog-share-type'		=>	0,
		'blog-share-code'		=>	"",
		
		//portfolio 
		'portfolio-default-page'	=> "",
		'portfolio-enable-breadchrumb'	=>	'off',
		'portfolio-share-type'		=>	"",
		'portfolio-share-code'		=>	"",
		
		//social 
		'social-email'		=> "",
		'social-twitter'	=> "" ,
		'social-facebook'	=> "" ,
		'social-google-plus'=>	"" ,
		'social-pinterest'	=>	"",
		'social-github'		=>	"",
		'social-linkedin'	=>	"",
		'social-dribbble'	=>	"",
		'social-flickr'		=>	"",
		'social-skype'		=>	"",
		'social-vimeo'		=>	"",
		'social-digg'		=>	"",
		'social-deviantart'	=>	"",
		'social-behance'	=>	"",
		'social-forrst'		=>	"",
		'social-lastfm'		=>	"",
		'social-xing'		=>	"",
		'social-instagram'		=>	"",
		'social-stumbleupon'		=>	"",
		'social-picasa'		=>	""								
	);
	
	/**
	 * Option Config
	 */
	$optionsConfig = array(
		/* type -> menu,submenu */
		/* page_title,menu_title,capability,menu_slug,function,icon_url,position from 100 */
		'menu'	=> array(
				'type'			=> 'menu',
				'option_name' 	=> 'alterna_options',
				'page_desc'		=> '',
				'page_logo'		=>	'',
				'page_title' 	=> 'Alterna Options',
				'menu_title' 	=> 'Alterna',
				'capability' 	=> 'manage_options',
				'menu_slug'	 	=> 'alterna_options_page',
				'icon_url'		=> get_template_directory_uri().'/img/penguin/penguin-icon.png',
				'position'		=> 99,
				'fun'			=>	'',
				'admin_bar'		=>	true,
				'backpage'		=>	true
			),
		'submenu'	=> array(
				'type'			=> 'submenu',
				'parent_slug' 	=> 'alterna_options_page',
				'option_name' 	=> 'alterna_options',
				'page_desc'		=> __('Welcome to setting alterna theme style!','alterna'),
				'page_logo'		=>	get_template_directory_uri().'/img/penguin/penguin_logo.png',
				'page_logo_url'		=> 'http://themefocus.co/alterna/',
				'page_title' 	=> 'Alterna Option',
				'menu_title' 	=> 'Alterna Options',
				'capability' 	=> 'manage_options',
				'menu_slug'	 	=> 'alterna_options_page',
				'icon_url'		=> get_template_directory_uri().'/img/alterna-icon.png',
				'pages'		 	=> $page_content,
				'pages_default_property'	=> $page_default_property,
				'link'			=>	'http://support.themefocus.co',
				'notifier'		=> "http://themefocus.co/alterna/notifier.xml",
				'update_history'		=> "http://support.themefocus.co/update/?theme=alterna",
				'update_opt'		=> "yes"
			)
	);
	
	// custom metas field
	$metasConfig = array();
	
	// general element field
	$generalConfig = array(
			array(	'name' 	=> 'layout-type',
					'title'	=> __('Layout Type','alterna'),
					'type'	=>	'radio',
					'radios' => array(__('Use Global','alterna'),__('Full Width','alterna'),__('Left Sidebar','alterna'),__('Right Sidebar','alterna')),
					'enable-element'=> 'yes',
					'enable-id'		=> '0-page_layout_type:2-page_layout_type:3-page_layout_type',
					'enable-group'	=> 'page_layout_type_group'
				),
			array(	'name' 	=> 'sidebar-type',
					'title'	=> __('Sidebar','alterna'),
					'type'	=>	'selectname',
					'options' => 'wp_registered_sidebars',
					'enabled-id'	=> 'page_layout_type',
					'enable-group'	=> 'page_layout_type_group'
				),
			array(	'name' 	=> 'title-show',
					'title'	=> __('Show Page Header Title','alterna'),
					'type'	=>	'pc',
					'default'	=> 'on',
					'enable-element'=> 'yes',
					'enable-id'		=> '1-page_title_show',
					'enable-group'	=> 'page_title_show_group'
			),
			array(	'name' 	=> 'title-align',
					'title'	=> __('Title Style','alterna'),
					'type'	=>	'radio',
					'radios' => array('left','center','right'),
					'enabled-id' => 'page_title_show',
					'enable-group'	=> 'page_title_show_group'
				),
			array(	'name' 	=> 'title-content',
					'title'	=> __('Custom Title Content','alterna'),
					'type'	=>	'textarea',
					'enabled-id' => 'page_title_show',
					'enable-group'	=> 'page_title_show_group',
					'desc'	=>	esc_html(__('Support HTML Format content.','alterna')),
				),
			array(	'name' 	=> 'title-breadcrumb',
					'title'	=> __('Show Title Breadcrumb','alterna'),
					'type'	=>	'pc',
					'default'	=> 'on',
					'enabled-id' => 'page_title_show',
					'enable-group'	=> 'page_title_show_group'
				),
			array(	'name' 	=> 'slide-type',
					'title'	=> __('Slider Type','alterna'),
					'type'	=>	'radio',
					'radios' => array('None Slider','Layer Slider','Revolution Slider'),
					'enable-element' => 'yes',
					'enable-id'	=> '1-layer_slide_id:2-rev_slide_id',
					'enable-group'	=> 'layer_slide_group',

				),
			array(	'name' 	=> 'layer-slide-id',
					'title'	=> __('Select Layer Slider','alterna'),
					'type'	=>	'select',
					'options' => penguin_get_layerslider(),
					'enabled-id' => 'layer_slide_id',
					'enable-group'	=> 'layer_slide_group'
				),
			array(	'name' 	=> 'rev-slide-id',
					'title'	=> __('Select Revolution Slider','alterna'),
					'type'	=>	'select',
					'options' => penguin_get_revslider(),
					'enabled-id' => 'rev_slide_id',
					'enable-group'	=> 'layer_slide_group'
				)
		);
		
	$bgConfig = array(
						array(	'name' 	=> 'page-background-type',
								'title'	=> __('Page Background Type','alterna'),
								'type'	=>	'select',
								'section'	=>	'yes',
								'options' => array('Use Global','Pattern','Image','Color'),
								'enable-element' => 'yes',
								'enable-id'	=> '1-page_background_pattern:2-page_background_image:3-page_background_color',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-background-width',
								'title'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'default' => 200,
								'enabled-id' => 'page_background_pattern',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-background-height',
								'title'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'default' => 200,
								'enabled-id' => 'page_background_pattern',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-background-pattern',
								'title'	=> __('Pattern Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_background_pattern',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-background-retina-pattern',
								'title'	=> __('Pattern Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_background_pattern',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-background-img',
								'title'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_background_image',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-background-color',
								'title'	=> __('Background Color','alterna'),
								'type'	=>	'color',
								'default'	=>	'ffffff',
								'enabled-id' => 'page_background_color',
								'enable-group'	=> 'page_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-type',
								'title'	=> __('Page Header Background Type','alterna'),
								'type'	=>	'select',
								'section'	=>	'yes',
								'options' => array('Use Global','Pattern','Image','Color'),
								'enable-element' => 'yes',
								'enable-id'	=> '1-page_header_background_pattern:2-page_header_background_image:3-page_header_background_color',
								'enable-group'	=> 'page_header_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-width',
								'title'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'default' => 0,
								'enabled-id' => 'page_header_background_pattern',
								'enable-group'	=> 'page_header_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-height',
								'title'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'default' => 0,
								'enabled-id' => 'page_header_background_pattern',
								'enable-group'	=> 'page_header_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-pattern',
								'title'	=> __('Pattern Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_header_background_pattern',
								'enable-group'	=> 'page_header_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-retina-pattern',
								'title'	=> __('Pattern Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_header_background_pattern',
								'enable-group'	=> 'page_header_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-img',
								'title'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_header_background_image',
								'enable-group'	=> 'page_header_background_group'
							),
						array(	'name' 	=> 'page-header-area-background-color',
								'title'	=> __('Background Color','alterna'),
								'type'	=>	'color',
								'default'	=>	'ffffff',
								'enabled-id' => 'page_header_background_color',
								'enable-group'	=> 'page_header_background_group'
							),
						
						array(	'name' 	=> 'page-title-background-type',
								'title'	=> __('Page Title Background Type','alterna'),
								'type'	=>	'select',
								'section'	=>	'yes',
								'options' => array('Use Global','Pattern','Image','Color'),
								'enable-element' => 'yes',
								'enable-id'	=> '1-page_title_background_pattern:2-page_title_background_image:3-page_title_background_color',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-title-background-width',
								'title'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'default' => 297,
								'enabled-id' => 'page_title_background_pattern',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-title-background-height',
								'title'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'default' => 297,
								'enabled-id' => 'page_title_background_pattern',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-title-background-pattern',
								'title'	=> __('Pattern Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_title_background_pattern',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-title-background-retina-pattern',
								'title'	=> __('Pattern Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'desc'	=> __('If you use "Pattern" type then need upload size is 400px * 400px retina support image.','alterna'),
								'enabled-id' => 'page_title_background_pattern',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-title-background-img',
								'title'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_title_background_image',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-title-background-color',
								'title'	=> __('Background Color','alterna'),
								'type'	=>	'color',
								'default'	=>	'ffffff',
								'enabled-id' => 'page_title_background_color',
								'enable-group'	=> 'page_title_background_group'
							),
						array(	'name' 	=> 'page-content-background-type',
								'title'	=> __('Page Content Background Type','alterna'),
								'type'	=>	'select',
								'section'	=>	'yes',
								'options' => array('Use Global','Pattern','Image','Color'),
								'enable-element' => 'yes',
								'enable-id'	=> '1-page_content_background_pattern:2-page_content_background_image:3-page_content_background_color',
								'enable-group'	=> 'page_content_background_group'
							),
						array(	'name' 	=> 'page-content-background-width',
								'title'	=> __('Pattern Image Width','alterna'),
								'type'	=>	'number',
								'default' => 200,
								'enabled-id' => 'page_content_background_pattern',
								'enable-group'	=> 'page_content_background_group'
							),
						array(	'name' 	=> 'page-content-background-height',
								'title'	=> __('Pattern Image Height','alterna'),
								'type'	=>	'number',
								'default' => 200,
								'enabled-id' => 'page_content_background_pattern',
								'enable-group'	=> 'page_content_background_group'
							),
						array(	'name' 	=> 'page-content-background-pattern',
								'title'	=> __('Pattern Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_content_background_pattern',
								'enable-group'	=> 'page_content_background_group'
							),
						array(	'name' 	=> 'page-content-background-retina-pattern',
								'title'	=> __('Pattern Retina Image @2x','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_content_background_pattern',
								'enable-group'	=> 'page_content_background_group'
							),
						array(	'name' 	=> 'page-content-background-img',
								'title'	=> __('Background Image','alterna'),
								'type'	=>	'upload',
								'enabled-id' => 'page_content_background_image',
								'enable-group'	=> 'page_content_background_group'
							),
						array(	'name' 	=> 'page-content-background-color',
								'title'	=> __('Background Color','alterna'),
								'type'	=>	'color',
								'default'	=>	'ffffff',
								'enabled-id' => 'page_content_background_color',
								'enable-group'	=> 'page_content_background_group'
							)
				);
	//post
	$metasConfig[] = array( 'id'		=>	'custom-post-setting',
							'type'		=>	'post',
							'priority'	=>	'high',
							'title' 	=>	__('Page Option Setting','alterna'),
							'page_elements'	=> array(
											'element-general' => array(	
													'id'	=>	'custom-post-general',
													'icon'	=>	'icon-gear',
													'title'	=>	__('General','alterna'),
													'fields'	=>	$generalConfig				
											),
											'element-template' => array(	
													'id'	=>	'custom-post-template',
													'icon'	=>	'icon-puzzle-piece',
													'title'	=>	__('Post Options','alterna'),
													'fields'	=>	array(
																array(	'name' 	=> 'gallery-images',
																		'title'	=> __('Gallery Images','alterna'),
																		'type'	=>	'gallery',
																		'postformat'	=> 'gallery'
																),
																array(	'name' 	=> 'video-type',
																		'title'	=> __('Video Type','alterna'),
																		'type'	=>	'select',
																		'options' => array('Youtube','Vimeo','Custom Code'),
																		'postformat'	=> 'video'
																	),
																array(	'name' 	=> 'video-content',
																		'title'	=> __('Video ID or Custom Code','alterna'),
																		'type'	=>	'textarea',
																		'longdesc' => __('Youtube ID e.g. OapE7K5KyG0 "','alterna'),
																		'postformat'	=> 'video'
																	),
																array(	'name' 	=> 'audio-type',
																		'title'	=> __('Audio Type','alterna'),
																		'type'	=>	'select',
																		'options' => array('Soundcloud','Custom Code'),
																		'postformat'	=> 'audio'
																	),
																array(	'name' 	=> 'audio-content',
																		'title'	=> __('Soundcloud Url or Custom Code','alterna'),
																		'type'	=>	'textarea',
																		'longdesc' => __('Soundcloud e.g.  " http://api.soundcloud.com/tracks/38987054 "','alterna'),
																		'postformat'	=> 'audio'
																	),
																array(	'name' 	=> 'show-related-post',
																		'title'	=> __('Show Related Posts','alterna'),
																		'type'	=>	'select',
																		'options' => array('show','hide')
																	),
																array(	'name' 	=> 'show-related-post-number',
																		'title'	=> __('Show Related Posts Number','alterna'),
																		'type'	=>	'input',
																		'input_type'	=> 'number',
																		'default' => '3'
																	)
														)
											),
											'element-background' => array(	
													'id'	=>	'custom-post-background',
													'icon'	=>	'icon-picture',
													'title'	=>	__('Background','alterna'),
													'fields'	=>	$bgConfig
											),
											'element-style' => array(	
														'id'	=>	'custom-post-css-style',
														'icon'	=>	'icon-code',
														'title'	=>	__('Page Custom CSS, Scripts','alterna'),
														'fields'	=>	array(	
																				array(	'name' 	=> 'post-css-style',
																						'title'	=> __('Custom Page CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-css-retina-style',
																						'title'	=> __('Custom Page Retina CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-custom-scripts',
																						'title'	=> __('Custom Page Scripts','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				)
																			)
											)
							)
			);
	
	//page
	$metasConfig[] = array( 
			'id'		=>	'custom-page-setting',
			'type'		=>	'page',
			'priority'	=>	'high',
			'title' 	=>	__('Page Option Setting','alterna'),
			'page_elements'	=> array(
								'element-general' => array(	
										'id'	=>	'custom-page-general',
										'icon'	=>	'icon-gear',
										'title'	=>	__('General','alterna'),
										'fields'	=>	$generalConfig				
								),
								'element-template' => array(	
										'id'	=>	'custom-post-template',
										'icon'	=>	'icon-puzzle-piece',
										'title'	=>	__('Template Options','alterna'),
										'fields'	=>	array(
																array(	'name' 	=> 'blog-show-type',
																		'title'	=> __('Blog Show Type','alterna'),
																		'type'	=>	'select',
																		'template' => 'page-blog',
																		'default' => 0 ,
																		'options' => array(__('Default: Large Width','alterna') , __('Mid Width','alterna'))
																	),
																// Blog with ajax
																array(	'name' 	=> 'blog-ajax-cols-num',
																		'title'	=> __('Blog Waterfall Flux Columns','alterna'),
																		'type'	=>	'select',
																		'template' => 'page-blog-ajax',
																		'default' => 1 ,
																		'options' => array('2 columns' , '3 columns' , '4 columns')
																	),
																array(	'name' 	=> 'blog-ajax-page-num',
																		'title'	=> __('Blog Show Posts Number','alterna'),
																		'type' 	=> 'number',
																		'default' => 10 ,
																		'template' => 'page-blog-ajax page-blog' ,
																		'desc' => __('Also as read more load number.','alterna')
																	),
																array(	'name' 	=> 'blog-ajax-cat',
																		'title'	=> __('Blog Post Show Category','alterna'),
																		'type'	=>	'input',
																		'template' => 'page-blog-ajax page-blog',
																		'desc'	=>	__('Input post category id will just show these category items use "," ','alterna')
																		
																	),
																// Portfolio with ajax
																array(	'name' 	=> 'portfolio-ajax-cols-num',
																		'title'	=> __('Portfolio Waterfall Flux Columns','alterna'),
																		'type'	=>	'select',
																		'template' => 'page-portfolio-ajax',
																		'default' => 1 ,
																		'options' => array('2 columns' , '3 columns' , '4 columns')
																	),
																array(	'name' 	=> 'portfolio-ajax-page-num',
																		'title'	=> __('Portfolio Show Posts Number','alterna'),
																		'type' 	=> 'number',
																		'default' => 10 ,
																		'template' => 'page-portfolio-ajax' ,
																		'desc' => __('Also as read more load number.','alterna')
																	),
																// Portfolio Params for portfolio page
																array(	'name' 	=> 'portfolio-cols-num',
																		'title'	=> __('Portfolio Columns','alterna'),
																		'type'	=>	'select',
																		'template' => 'page-portfolio',
																		'default' => 1 ,
																		'options' => array('2 columns' , '3 columns' , '4 columns')
																	),
																array(	'name' 	=> 'portfolio-page-max-number',
																		'title'	=> __('Portfolio Page Show Max Number','alterna'),
																		'type'	=>	'number',
																		'template' => 'page-portfolio',
																		'default' => '10'
																	),
																array(	'name' 	=> 'portfolio-show-style',
																		'title'	=> __('Portfolio Item Show Style','alterna'),
																		'type'	=>	'select',
																		'template' => 'page-portfolio page-portfolio-ajax',
																		'options' => array('Default (content with category)','Style 2 (Title cover thumbs)' , 'Style 3(Date cover thumbs with title)' , 'Style 4(Popup Big Image)')
																	),
																array(	'name' 	=> 'portfolio-show-filter',
																		'title'	=> __('Portfolio Show Filters','alterna'),
																		'type'	=>	'pc',
																		'template' => 'page-portfolio page-portfolio-ajax',
																		'desc'	=>	__('Check show filters buttons','alterna')
																		
																	),
																array(	'name' 	=> 'portfolio-show-cat',
																		'title'	=> __('Portfolio Show Category','alterna'),
																		'type'	=>	'input',
																		'template' => 'page-portfolio page-portfolio-ajax',
																		'desc'	=>	__('Input portfolio category slug will just show these category items use "," ','alterna')
																		
																	),
																// Google Map for contact page
																array(	'name' 	=> 'map-show',
																		'title'	=> __('Header Map Show','alterna'),
																		'type'	=>	'pc',
																		'template' => 'page-contact',
																		'enable-element' => 'yes',
																		'enable-id'	=> '1-map_show_id',
																		'enable-group'	=> 'map_show_group'
																	),
																array(	'name' 	=> 'map-height',
																		'title'	=> __('Map Height ','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'number',
																		'default' => '320',
																		'desc' =>	__('Header map height','alterna'),
																		'enabled-id' => 'map_show_id',
																		'enable-group'	=> 'map_show_group'
																	),
																array(	'name' 	=> 'map-latlng',
																		'title'	=> __('Map LatLng ','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'input',
																		'desc' =>	__('For Example : 40.716038,-74.080811 ','alterna'),
																		'enabled-id' => 'map_show_id',
																		'enable-group'	=> 'map_show_group'
																	),
																array(	'name' 	=> 'map-address',
																		'title'	=> __('Map Address ','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'textarea',
																		'desc' =>	__('(Support HTML )For Example : Company Name 123 street, New Valley , USA','alterna'),
																		'enabled-id' => 'map_show_id',
																		'enable-group'	=> 'map_show_group'
																	),
																array(	'name' 	=> 'contact-form',
																		'title'	=> __('Default Contact Form','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'select',
																		'options' => array('Use Default Contact Form','Use Form Plugin Replace')
																	),
																array(	'name' 	=> 'contact-recipient',
																		'title'	=> __('Contact Recipient Email','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'input',
																		'desc'	=>	__('It\'s just for default contact form. ','alterna')
																	),
																array(	'name' 	=> 'contact-backsender',
																		'title'	=> __('Sender Email Feedback','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'pc',
																		'desc'	=>	__('If check sender will also get a success email when submit success. ','alterna')
																	),
																array(	'name' 	=> 'form-recaptcha',
																		'title'	=> __('Contact Form Recaptcha','alterna'),
																		'type'	=>	'pc',
																		'template' => 'page-contact',
																		'enable-element' => 'yes',
																		'enable-id'	=> '1-recaptcha_show_id',
																		'enable-group'	=> 'recaptcha_show_group'
																	),
																array(	'name' 	=> 'recaptcha-pub-api',
																		'title'	=> __('Recaptcha Public Key','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'input',
																		'default' => '',
																		'desc' =>	__('<strong>The basic registration form requires</strong> that new users copy text from a "Captcha" image to keep spammers out of the site. You need an account at <a href="http://recaptcha.net/">recaptcha.net</a>. Signing up is FREE and easy. Once you have signed up, come back here and enter the following settings:','alterna'),
																		'enabled-id' => 'recaptcha_show_id',
																		'enable-group'	=> 'recaptcha_show_group'
																	),
																array(	'name' 	=> 'recaptcha-pri-api',
																		'title'	=> __('Recaptcha Private Key','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'input',
																		'enabled-id' => 'recaptcha_show_id',
																		'enable-group'	=> 'recaptcha_show_group'
																	),
																array(	'name' 	=> 'recaptcha-theme',
																		'title'	=> __('Recaptcha Theme','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'selectname',
																		'options'	=> array('white', 'red', 'blackglass', 'clean'),
																		'enabled-id' => 'recaptcha_show_id',
																		'enable-group'	=> 'recaptcha_show_group',
																	),
																array(	'name' 	=> 'recaptcha-lang',
																		'title'	=> __('Recaptcha Language','alterna'),
																		'template' => 'page-contact',
																		'type'	=>	'selectname',
																		'enabled-id' => 'recaptcha_show_id',
																		'enable-group'	=> 'recaptcha_show_group',
																		'options'	=> array('en', 'nl', 'fr', 'de', 'pt', 'ru', 'es', 'tr'),
																	)
														)
											),
											'element-background' => array(	
													'id'	=>	'custom-post-background',
													'icon'	=>	'icon-picture',
													'title'	=>	__('Background','alterna'),
													'fields'	=>	$bgConfig
											),
											'element-style' => array(	
														'id'	=>	'custom-post-css-style',
														'icon'	=>	'icon-code',
														'title'	=>	__('Page Custom CSS, Scripts','alterna'),
														'fields'	=>	array(	
																				array(	'name' 	=> 'post-css-style',
																						'title'	=> __('Custom Page CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-css-retina-style',
																						'title'	=> __('Custom Page Retina CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-custom-scripts',
																						'title'	=> __('Custom Page Scripts','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				)
																			)
											)
							)
			);
			
		
	//portfolio
	$metasConfig[] = array( 'id'		=>	'custom-portfolio-setting',
							'type'		=>	'portfolio',
							'priority'	=>	'high',
							'title' 	=>	__('Page Option Setting','alterna'),
							'page_elements'	=> array(
											'element-general' => array(	
													'id'	=>	'custom-post-general',
													'icon'	=>	'icon-gear',
													'title'	=>	__('General','alterna'),
													'fields'	=>	$generalConfig				
											),
											'element-template' => array(	
													'id'	=>	'custom-post-template',
													'icon'	=>	'icon-puzzle-piece',
													'title'	=>	__('Portfolio Options','alterna'),
													'fields'	=>	array(
																array(	'name' 	=> 'portfolio-type',
																		'title'	=> __('Portfolio Type','alterna'),
																		'type'	=>	'radio',
																		'radios'	=>	array(__('Image','alterna'),__('Gallery','alterna'),__('Video','alterna' )),
																		'enable-element' => 'yes',
																		'enable-id' => '0-portfolio_post_format_image:1-portfolio_post_format_gallery:2-portfolio_post_format_video',
																		'enable-group'	=> 'portfolio_post_format'
																		
																),
																array(	'name' 	=> 'gallery-images',
																		'title'	=> __('Gallery Images','alterna'),
																		'type'	=>	'gallery',
																		'enabled-id' => 'portfolio_post_format_gallery',
																		'enable-group'	=> 'portfolio_post_format'
																),
																array(	'name' 	=> 'video-type',
																		'title'	=> __('Video Type','alterna'),
																		'type'	=>	'radio',
																		'radios'	=>	array('Youtube','Vimeo'),
																		'enabled-id' => 'portfolio_post_format_video',
																		'enable-group'	=> 'portfolio_post_format'
																),
																array(	'name' 	=> 'video-content',
																		'title'	=> __('Video ID','alterna'),
																		'type'	=>	'input',
																		'desc'	=>	__('Youtube Id Example : " OapE7K5KyG0 "','alterna'),
																		'enabled-id' => 'portfolio_post_format_video',
																		'enable-group'	=> 'portfolio_post_format'
																),
																array(	'name' 	=> 'portfolio-client',
																		'title'	=> __('Client','alterna'),
																		'type'	=>	'input'																								
																	),
																array(	'name' 	=> 'portfolio-skills',
																		'title'	=> __('Skills','alterna'),
																		'type'	=>	'input',		
																	),
																array(	'name' 	=> 'portfolio-colors',
																		'title'	=> __('Colors','alterna'),
																		'type'	=>	'input',
																		'desc'	=> __('Please use "," for multiple colors. Example: #ffffff,#000000 ','alterna')											
																	),
																array(	'name' 	=> 'portfolio-system',
																		'title'	=> __('Used System','alterna'),
																		'type'	=>	'input'							
																	),
																array(	'name' 	=> 'portfolio-price',
																		'title'	=> __('Price ','alterna'),
																		'type'	=>	'input'							
																	),
																array(	'name' 	=> 'portfolio-link',
																		'title'	=> __('Link','alterna'),
																		'type'	=>	'input',									
																	),
																array(	'name' 	=> 'portfolio-custom-fields',
																		'title'	=> __('Custom Portfolio Fields','alterna'),
																		'type'	=>	'custom',
																		'fileds' => array('Name','Icon','Value'), 
																		'default'	=>	'',
																		'desc'	=> __('Icon field use fontawesome icon name','alterna')
																),
																array(	'name' 	=> 'show-related-portfolio',
																		'title'	=> __('Show Related Portfolios','alterna'),
																		'type'	=>	'select',
																		'options' => array('show','hide'),
																		'enable-element' => 'yes',
																		'enable-id'	=> '0-related_portfolio',
																		'enable-group'	=> 'related_portfolio_group'
																	),
																array(	'name' 	=> 'show-related-portfolio-style',
																		'title'	=> __('Show Related Portfolios Style','alterna'),
																		'type'	=>	'select',
																		'options' => array('style #1','style #2'),
																		'enabled-id' => 'related_portfolio',
																		'enable-group'	=> 'related_portfolio_group'
																	),
																array(	'name' 	=> 'show-related-portfolio-number',
																		'title'	=> __('Show Related Portfolios Number','alterna'),
																		'type'	=>	'input',
																		'input_type'	=> 'number',
																		'default' => '3',
																		'enabled-id' => 'related_portfolio',
																		'enable-group'	=> 'related_portfolio_group'
																	)
														)
											),
											'element-background' => array(	
													'id'	=>	'custom-post-background',
													'icon'	=>	'icon-picture',
													'title'	=>	__('Background','alterna'),
													'fields'	=>	$bgConfig
											),
											'element-style' => array(	
														'id'	=>	'custom-post-css-style',
														'icon'	=>	'icon-code',
														'title'	=>	__('Page Custom CSS, Scripts','alterna'),
														'fields'	=>	array(	
																				array(	'name' 	=> 'post-css-style',
																						'title'	=> __('Custom Page CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-css-retina-style',
																						'title'	=> __('Custom Page Retina CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-custom-scripts',
																						'title'	=> __('Custom Page Scripts','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				)
																			)
											)
							)
			);
	
	//product
	$metasConfig[] = array( 'id'		=>	'custom-product-setting',
							'type'		=>	'product',
							'priority'	=>	'high',
							'title' 	=>	__('Page Option Setting','alterna'),
							'page_elements'	=> array(
											'element-general' => array(	
													'id'	=>	'custom-post-general',
													'icon'	=>	'icon-gear',
													'title'	=>	__('General','alterna'),
													'fields'	=>	$generalConfig				
											),
											'element-background' => array(	
													'id'	=>	'custom-post-background',
													'icon'	=>	'icon-picture',
													'title'	=>	__('Background','alterna'),
													'fields'	=>	$bgConfig
											),
											'element-style' => array(	
														'id'	=>	'custom-post-css-style',
														'icon'	=>	'icon-code',
														'title'	=>	__('Page Custom CSS, Scripts','alterna'),
														'fields'	=>	array(	
																				array(	'name' 	=> 'post-css-style',
																						'title'	=> __('Custom Page CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-css-retina-style',
																						'title'	=> __('Custom Page Retina CSS','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				),
																				array(	'name' 	=> 'post-custom-scripts',
																						'title'	=> __('Custom Page Scripts','alterna'),
																						'type'	=>	'textarea',
																						'codetype' => 'css'
																				)
																			)
											)
							)
			);
	
	
	$postsConfig = array(
						'portfolio' => array(
											'id'					=>	'portfolio',
											'name'					=>	__('Portfolios','alterna'),
											'menu_name'				=>	__('Portfolios','alterna'),
											'singular_name'			=>	__('Portfolio','alterna'),
											'add_new'				=>	__('Add New','alterna'),
											'add_new_item'			=>	__('Add New','alterna'),
											'edit_item'				=>	__('Edit Portfolio','alterna'),
											'new_item'				=>	__('New Portfolio','alterna'),
											'all_items'				=>	__('All Portfolios','alterna'),
											'view_item'				=>	__('View Portfolio','alterna'),
											'search_items'			=>	__('Search Portfolio','alterna'),
											'not_found'				=>	__('No portfolio found','alterna'),
											'not_found_in_trash'	=>	__('No portfolio found in Trash','alterna'),
											'parent_item_colon'		=>	'',
											'menu_position'			=>	5,
											'rewrite'				=>	'portfolio',
											'rewrite_rule'			=>	'',
											'menu_icon'				=>	'\f180',
											'supports'				=>	array('title', 'editor' , 'thumbnail', 'comments'),
											'categories'			=>	array(
																			'portfolio_cats'	=>	array(
																										'id'			=>	'portfolio_categories',
																										'name'			=>	__( 'Portfolio Categories' ,'alterna'),
																										'menu_name'		=>	__( 'Portfolio Categories' ,'alterna'),
																										'singular_name'	=>	__( 'Portfolio Categories' ,'alterna'),
																										'search_items'	=>	__( 'Search Portfolio Categories' ,'alterna'),
																										'all_items'		=>	__( 'All Portfolio Categories' ,'alterna'),
																										'parent_item'	=>	__( 'Parent Category' ,'alterna'),
																										'parent_item_colon'	=>	__( 'Parent Category:' ,'alterna'),
																										'edit_item'			=>	__( 'Edit Portfolio Category' ,'alterna'),
																										'update_item'		=>	__( 'Update Portfolio Category' ,'alterna'),
																										'add_new_item'		=>	__( 'Add Portfolio Category' ,'alterna'),
																										'new_item_name'		=>	__( 'New Portfolio Category' ,'alterna'),
																										'rewrite'			=>	'',
																										'hierarchical'		=>	true
																									)
																		)
											
									)
					);
	$postsColumnsConfig = array('portfolio' => array(
													'type'		=>	'portfolio',
													'fields'	=>	array(
																		array('id' => 'post-foramt', 'name' => __( 'Post Type' ,'alterna')),
																	)
												)
					);
					
	// start penguin framework for them
	Penguin::$FRAMEWORK_PATH = "/inc/penguin";
	Penguin::$THEME_NAME = "alterna";
	Penguin::start($optionsConfig , $metasConfig , $postsConfig , $postsColumnsConfig, false);
	
	// here need custom return value
	function Penguin_Custom_Posts_Columns_Value($type, $column_name, $id){
		/* start */
		if($type == 'portfolio'){
			if($column_name == 'post-foramt'){
				switch(intval(alterna_get_post_meta_key($column_name, $id))){
					case 0: _e('Image','alterna');break;
					case 1: _e('Gallery','alterna');break;
					case 2: _e('Video','alterna');break;
				}
			}
		}
		/* end */
	}
?>