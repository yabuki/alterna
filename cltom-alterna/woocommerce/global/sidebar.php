<?php
/**
 * Sidebar
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if(is_single()){
	$sidebar_name = get_post_meta(get_the_ID(), 'sidebar-type', true);
	if($sidebar_name == "Global Sidebar" || $sidebar_name == ""){
		dynamic_sidebar('shop');
	}else{
		dynamic_sidebar($sidebar_name);
	}
	return;
}
dynamic_sidebar('shop'); ?>