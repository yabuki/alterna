<?php
/**
 * Author template file.
 *
 * @since alterna 2.4
 */

	get_header();
   
	// index default will use global layout 
	$layout = alterna_get_page_layout('global'); 
	$sidebar_name = '0';
?>
    
	<div id="main" class="container">
    	<div class="row-fluid">
            <?php if($layout == 2) : ?> 
            	<div class="span4"><?php generated_dynamic_sidebar($sidebar_name); ?></div>
            <?php endif; ?>
            
        	<div class="left-side <?php echo $layout == 1 ? 'span12' : 'span8'; ?>">
				<?php
                $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
                ?>
                
            	<h3 class="widget-title"><?php echo __('About','alterna').' '.$curauth->nickname; ?></h3>
                <div class="author-information">
                    <div class="gravatar"><?php echo get_avatar($curauth->ID, 80 ); ?></div>
                    <dl>
                        <dt><?php _e('Website','alterna'); ?></dt>
                        <dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
                        <dt><?php _e('Profile','alterna'); ?></dt>
                        <dd><?php echo $curauth->user_description; ?></dd>
                    </dl>
                </div>
                
            	<h3 class="widget-title"><?php echo __('Posts by','alterna').' '.$curauth->nickname; ?></h3>
                <ul class="sitemap-ul">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <li>
                        <a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php echo get_the_title(); ?>">
                        <?php echo get_the_title(); ?></a>,
                        <?php echo get_the_time('d M Y'); ?> in <?php the_category('&');?>
                    </li>
                <?php endwhile; else: ?>
                    <p><?php _e('No posts by this author.', 'alterna'); ?></p>
                <?php endif; ?>
   				</ul>
                
                <?php alterna_content_pagination('nav-bottom' , 'pagination-centered'); ?>

			</div><!-- #left-side -->
             <?php if($layout == 3) : ?> 
            	<div class="span4"><?php generated_dynamic_sidebar($sidebar_name); ?></div>
            <?php endif; ?>
		</div><!-- #row-fluid -->
    </div><!-- #container -->
        
<?php get_footer(); ?>