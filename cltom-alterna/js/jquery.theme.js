// Description: Alterna, Retina Responsive Theme
"use strict";

jQuery(document).ready(function($) {

/* ----------------------------------------------------
	Functions
---------------------------------------------------- */

// ---------------------------------------
//	init menu
// ---------------------------------------
function menuInit(){
	//add arrow
	$('ul.alterna-nav-menu > li > ul').each(function() {
		$(this).parent().children('a').append('<i class="icon-angle-down"></i>');
		$(this).find('ul').each(function() {
            $(this).parent().children('a').append('<i class="icon-angle-right"></i>');
        });
	});
	//add drop select items
	if($('ul.alterna-nav-menu').length > 0){
		$('ul.alterna-nav-menu').first().find(' > li').each(function() {
			getOptionItem(this,0);
		});
	}
	
	if($('#alterna-nav-menu-select .nav-collapse .nav').find('li.active').length <= 0){
		$($('#alterna-nav-menu-select .nav-collapse .nav').children('li').get(0)).addClass('active');
	}
	
	//$('ul.alterna-nav-menu
	function getOptionItem(params,level){
		var item = $(params).children('a');
		var sub_item = $(params).children('ul');
		var extend_name = '';
		for(var i=0; i<level; i++){
			extend_name += '&nbsp;&nbsp;&nbsp;';
		}
		$('#alterna-nav-menu-select .nav-collapse .nav').append('<li '+ ( $(params).hasClass('current-menu-item') ? 'class="active"' : '') +'><a href="'+ $(item).attr('href')+'">' + extend_name + $(item).html()+'</a></li>'); 

		if(sub_item.length > 0){
			$(sub_item).children('li').each(function(index, element) {
				getOptionItem(element,level+1);
			});
		}
		
	}
	
	//header style 4
	if($('.header-style-4 .alterna-nav-form-icon').length > 0){
		$('.header-style-4 .alterna-nav-form-icon').click(function() {
            if($(this).find('i').hasClass('icon-search')){
				$(this).find('i').removeClass('icon-search');
				$(this).find('i').addClass('icon-remove');
				$('.header-style-4 .menu').addClass('opacity');
				$('.alterna-nav-form-content').addClass('show');
			}else{
				$(this).find('i').removeClass('icon-remove');
				$(this).find('i').addClass('icon-search');
				$('.alterna-nav-form-content').removeClass('show');
				$('.header-style-4 .menu').removeClass('opacity');
			}
        });
	}

	//topbar items
	function getTopbarItem(params,level){
		var item = $(params).children('a');
		var sub_item = $(params).children('ul');
		if(item.length > 0){
			$('#alterna-topbar-select .nav').append('<li><a href="'+ $(item).attr('href')+'">' + $(item).html()+'</a></li>'); 
		}else{
			$('#alterna-topbar-select .nav').append('<li><a>' + $(params).children('span').html()+'</a></li>'); 
		}

		if(sub_item.length > 0){
			$(sub_item).children('li').each(function(index, element) {
				getTopbarItem(element,level+1);
			});
		}
	}
	
	//add drop select items
	$('#header-topbar-left-content > ul > li').each(function() {
		getTopbarItem(this,0);
	});
	
	$('#header-topbar-right-content > ul > li').each(function() {
		getTopbarItem(this,0);
	});
	
	$('#alterna-topbar-select .btn-navbar').click(function() {
		if($('#alterna-topbar-select .collapse').hasClass('open')){
			$('#alterna-topbar-select .collapse').removeClass('open');
		}else{
			$('#alterna-topbar-select .collapse').addClass('open');
		}
    });
}

// ---------------------------------------
//	title line menu
// ---------------------------------------
function titleLineInit(){
	$('.line').each(function() {
		if( $(this).children('.left-line').length === 0 ) {
			$(this).append('<span class="left-line"></span>');
		}
		if( $(this).children('.right-line').length === 0 ) {
			$(this).append('<span class="right-line"></span>');
		}
	});
}

// ---------------------------------------
//	custom title
// ---------------------------------------
function alternaAlertTitleInit() {
	$('.widget_tag_cloud a').tooltip();
	$('.header-social a').tooltip();
	$('.show-tooltip').tooltip();
}

// ---------------------------------------
//	button init
// ---------------------------------------
function buttonsInit(){
	$('.btn-custom').each(function() {
		if( $(this).attr('data-txtcolor') ) {
			$(this).css('color',$(this).attr('data-txtcolor'));
		}
		if( $(this).attr('data-bgcolor') ) {
			$(this).css('background',$(this).attr('data-bgcolor'));
			//$(this).css('border-color',$(this).attr('data-bgcolor'));
		}
		
		if( !$(this).hasClass('disabled') ){
		
			$(this).hover(function(){
				if( $(this).attr('data-txthovercolor') ) {
					$(this).css('color',$(this).attr('data-txthovercolor'));
				}
				if( $(this).attr('data-bghovercolor') ) {
					$(this).css('background',$(this).attr('data-bghovercolor'));
					//$(this).css('border-color',$(this).attr('data-bghovercolor'));
				}
			},function(){
				if( $(this).attr('data-txtcolor') ) {
					$(this).css('color',$(this).attr('data-txtcolor'));
				}
				if( $(this).attr('data-bgcolor') ) {
					$(this).css('background',$(this).attr('data-bgcolor'));
					//$(this).css('border-color',$(this).attr('data-bgcolor'));
				}
				
			});
		}else{
			$(this).click(function() {
                return false;
            });
		}
	});
}

// ---------------------------------------
//	run animation elements
// ---------------------------------------	
function animationRun(){
	// check all single animate element
	$('.animate').each(function(){
		if($(this).hasClass('animated')){
			return false;
		}
		if (checkPosition(this)) {
			$(this).removeClass('animate');
			exAnimate(this);
		}
	});
	
	// check all animate list element
	$('.animate-list').each(function() {
		var items = $(this).find('.animate-item');
		var count = 0;			
		for(var i=0;i<items.length;i++){
			if($(items[i]).hasClass('animating') || $(items[i]).hasClass('animated')){
				if(!$(items[i]).hasClass('animated')){
					count++;
				}else{
					$(items[i]).removeClass('animating');
					$(items[i]).removeClass('animate-item');
				}
				continue;
			}
			if (checkPosition(items[i])) {
				listItemDelayEx(items[i],count);
				count++;
			}
		}			
	});
	
	// list item delay time and execute
	function listItemDelayEx(element,count){
		$(element).addClass('animating').delay(300*count).queue(function(){exAnimate(this);});
	}
	
	// check element position
	function checkPosition(element){
		var imagePos = $(element).offset().top;
		var topOfWindow = $(window).scrollTop();
		var heightOfWindow = $(window).height();
		if (imagePos < topOfWindow+heightOfWindow - 50) {
			return true;
		}
		return false;
	}
	
	// add class for element run animation
	function exAnimate(element){
		var effect = 'fadeIn';
		if($(element).attr('data-effect') && $(element).attr('data-effect') !== ""){
			effect = $(element).attr('data-effect');
		}
		$(element).addClass('animated '+effect);
	}
}
// ---------------------------------------
//	post page more link 
// ---------------------------------------
function postPageMoreLinkInit(){
	var items = $('.post-content');
	
	$(items).each(function(index, element) {
		if($(element).find('.more-link').length > 0) {
			var html = '<p><a class="more-link" href="' + $(element).find('.more-link').attr('href') + '">' + $(element).find('.more-link').html() + '</a></p>';
			$(element).find('.more-link').remove();
			$(element).append(html);
		}
	});
}

// ---------------------------------------
//	single post comment placeholder
// ---------------------------------------
function postCommentPlaceholderInit(){
	//all input files
	$('.placeholding-input input').each(function() {
		$(this).keydown(function() {
			refreshText(this,$(this).attr('value'));
		});
		$(this).focusout(function() {
			refreshText(this,$(this).attr('value'));
		});
	});
	
	$('.placeholding-input textarea').each(function() {
		$(this).keydown(function() {
			refreshText(this,$(this).attr('value'));
		});
		$(this).focusout(function() {
			refreshText(this,$(this).attr('value'));
		});
	});
	
	function refreshText(item,text){
		if(text.length > 0){
			$(item).parent().addClass('have-some');
		}else{
			$(item).parent().removeClass('have-some');
		}
	}
}
// ---------------------------------------
//	Tabs & SideTabs
// ---------------------------------------
function tabsInit(){
		
	/* tabs */
	checkElement(".tabs",tabsBack);
	
	/* side tabs */
	checkElement(".sidetabs",sideTabsBack);
	
	function refreshTabWidth(params){
		$(params).find('.tabs-container .tabs-content').css('width',($(params).width()-30));
	}
	
	/* back fun */
	function tabsBack(params){
		openTabs(params,".tabs-nav li",".tabs-content");
		$(window).resize(function() {
			refreshTabWidth(params);
		});
		refreshTabWidth();
	}
	
	function sideTabsBack(params){
		openTabs(params,".sidetabs-nav li",".sidetabs-content");
	}
	
	function openTabs(params,pname1,pname2){
		var ot_items = $(params).find(pname1);
		var citems = $(params).find(pname2);
		var ot_s1 = 0;
		var ot_sm = ot_items.length;
		var ot_new;
		
		$(ot_items).click(function() {
			if(ot_s1 === $(this).index()) {
				return false;
			}
			
			$(citems[ot_s1]).stop(true,true);
			$(citems[ot_s1]).css("opacity",1);
			
			ot_new = $(this).index();
			
			$(ot_items[ot_s1]).removeClass("current");
			$(ot_items[ot_new]).addClass("current");
			
			if($(citems[ot_s1]) !== null) {
				$(citems[ot_s1]).fadeOut("fast","",runNewTabs);
			}
		});
		
		function runNewTabs(){
			ot_s1 = ot_new;
			showElement(ot_s1,citems);
		}
		
		for(var k=0; k<ot_sm;k++) {
			if(ot_s1 === k){
				if($(ot_items[k]).hasClass("current") === false) {
					$(ot_items[k]).addClass("current");
				}
				showElement(k,citems);
			}else{
				if($(ot_items[k]).hasClass("current") === true)	{
					$(ot_items[k]).removeClass("current");
				}
				hideElement(k,citems);
			}
		}
	}
	
	function showElement(k,citems){
		if($(citems[k]) !== null)	{
			$(citems[k]).fadeIn("fast");
		}
	}
	
	function hideElement(k,citems){
		if($(citems[k]) !== null)	{
			$(citems[k]).fadeOut("fast");
		}
	}
}/* end tabs */

// ---------------------------------------
//	client testimonials
// ---------------------------------------
function testimonialsInit(){
	
	checkElement(".testimonials",openFeedback);

	function openFeedback(params){
		
		var cfb_items = $(params).find(".testimonials-item");
		var cfb_id = 0;
		var cfb_max = cfb_items.length;
		var run = false;
		var intervalObj = null;
		var auto = false;
		var delay = 5000;
		
		$(cfb_items).each(function(index, element) {
			$(element).css("display","none");
		});
		
		function showPrevElement(){
			if(run)	{
				return false;
			}
			run = true;
			closeInterval();
			
			hideElement($(cfb_items[cfb_id]),prevElement);
		}
		
		function showNextElement(){
			if(run) {
				return false;
			}
			run = true;
			closeInterval();
			
			hideElement($(cfb_items[cfb_id]),nextElement);
		}
		
		function prevElement(){
			cfb_id--;
			if(cfb_id < 0)	{
				cfb_id = cfb_max-1;
			}
			showElement($(cfb_items[cfb_id]));
		}
		
		function nextElement(){
			cfb_id++;
			if(cfb_id >= cfb_max)	{
				cfb_id = 0;
			}
			showElement($(cfb_items[cfb_id]));
		}
		
		if(cfb_max <= 1){
			$(params).find(".testimonials-prev").css('display','none');
			$(params).find(".testimonials-next").css('display','none');
		}else{
			$(params).find(".testimonials-prev").click(function() {
				showPrevElement();
			});
			$(params).find(".testimonials-next").click(function() {
				showNextElement();
			});
			// auto play
			if( $(params).hasClass('testimonials-auto') ){
				auto = true;
				delay = parseInt($(params).attr('data-delay'), 5000);
			}
		}
		
		showElement($(cfb_items[cfb_id]));
		
		function showElement(params){
			if($(params).css("display") !== "block"){
				$(params).fadeIn("fast");
			}
			run = false;
			if(auto) { startInterval(); }
		}
		
		//hide text slider elements effect
		function hideElement(params,nextElement){
			if($(params).css("display") === "block"){
				$(params).fadeOut("fast","",nextElement);
			}
		}
		
		function startInterval(){
			intervalObj = setInterval(showNextElement , delay);
		}
		
		function closeInterval(){
			if(intervalObj !== null) { clearInterval(intervalObj); }
			intervalObj = null;
		}
	}
	
}

// ---------------------------------------
//	accordion
// ---------------------------------------
function accordionInit(){
	$('.alterna-accordion .accordion-toggle').click(function() {
		if($(this).hasClass('collapsed')){
			$(this).parent().parent().prevAll().find('.accordion-toggle').addClass('collapsed');
			$(this).parent().parent().nextAll().find('.accordion-toggle').addClass('collapsed');
		}
	});
}
// ---------------------------------------
//	price slider
// ---------------------------------------
function priceSliderInit(){
	
	checkElement(".price-slider",priceSliderContent);
	
	function priceSliderContent(params){
		var index = 0;
		var target = 0;
		var imgs = $(params).find('.price-slider-images .price-img');
		var contents = $(params).find('.price-slider-element');
		var max_items = imgs.length - 1;
		var run = false;
		
		$(window).resize(function() {
			resizePriceSlider();
		});
		
		resizePriceSlider();
		
		function resizePriceSlider(){
			$(params).css('zoom', 1);
			var ps_width = $(params).width();
			var zoom = Number(ps_width/1170);
			
			if ($.browser.msie || $.browser.mozilla || $.browser.opera) {
				if(	ps_width < 940	) {
					$(params).css('height', zoom*360);
					$('.price-slider-content').css('display','none');
					$('.price-slider-btns').css('right','20px');
				}else{
					$(params).css('height', 360);
					$('.price-slider-content').css('display','block');
					$('.price-slider-btns').css('right','280px');
				}
			}else{
				$(params).css('zoom', zoom);
			}
		}
		
		if(max_items <= 0){
			$(params).find('.price-prev').css('display','none');
			$(params).find('.price-next').css('display','none');
		}
		
		$(params).find('.price-prev').click(function() {
			if(run)	{return;}
			run = true;
			target--;
			if(target < 0)	{target = max_items;}
			$(imgs[index]).animate({'left':'100%'},500);
			$(imgs[target]).css('left','-100%').animate({'left':'0%'},500);
			$(contents[index]).css({'display':'block','opacity':'0'}).animate({'opacity':'0'},500,'',showItem);
		});
		
		$(params).find('.price-next').click(function() {
			if(run)	{return;}
			run = true;
			target++;
			if(target > max_items)	{target = 0;}
			$(imgs[index]).animate({'left':'-100%'},500);
			$(imgs[target]).css('left','100%').animate({'left':'0%'},500);
			$(contents[index]).css({'display':'block','opacity':'0'}).animate({'opacity':'0'},500,'',showItem);
		});
		
		$(imgs).css('left','100%');
		$(contents).css('display','none');
		
		function showItem(){
			$(contents[index]).css('display','none');
			index = target;
			$(contents[index]).css({'display':'block','opacity':'0'}).animate({'opacity':'1'},500);
			run = false;
		}
		
		$(imgs[index]).css('left','0%');
		showItem();
	}
	
}
// ---------------------------------------
//	Scroll & Resize Window -----------
// ---------------------------------------
function scrollResizeInit(){
		$("body").append("<a id='back-top' href='#top' ></a>");
		
		$(window).resize(function() {
			if($('.header-fixed-enabled').length > 0)	{fixedHeader();}
		});
		
		$(window).scroll(function() {
			if($('.header-fixed-enabled').length > 0)	{fixedHeader();}
			if($(window).scrollTop() > 200){
				$("#back-top").fadeIn("slow");
			}else{
				$("#back-top").fadeOut("slow");
			}
			animationRun();
			skillsAnimationRun();
		});
		
		// scroll body to 0px on click
		$('#back-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
		
		function fixedHeader(){
			//fixed header 
			if($(window).width() > 979 && $(window).scrollTop() > ($('.header-wrap').outerHeight())) {
				if($('#alterna-nav').length > 0) {
					if(!$('#alterna-nav').hasClass('header-fixed')) {
						$('#alterna-nav').addClass('header-fixed');
						if($('#wpadminbar').length > 0){
							$('#alterna-nav').stop().animate({'top':$('#wpadminbar').height()},500);
						}else{
							$('#alterna-nav').stop().css('top','-100px').animate({'top':'0px'},500);
						}
					}
				}else{
					if(!$('.header-wrap header').hasClass('header-fixed')) {
						$('.header-wrap header').addClass('header-fixed');
						if($('#wpadminbar').length > 0){
							$('.header-wrap header').stop().css('top','-100px').animate({'top':$('#wpadminbar').height()},500);
						}else{
							$('.header-wrap header').stop().css('top','-100px').animate({'top':'0px'},500);
						}
					}
				}
				
			}else{
				if($('#alterna-nav').length > 0) {
					if($('#alterna-nav').hasClass('header-fixed')) {
						$('#alterna-nav').removeClass('header-fixed');
						$('#alterna-nav').stop().css('top','0px');
					}
				}else{
					if($('.header-wrap header').hasClass('header-fixed')) {
						$('.header-wrap header').removeClass('header-fixed');
						$('.header-wrap header').stop().css('top','0px');
					}
				}
				
			}
		}
} 
// ---------------------------------------
//	Touch Hover Effect -----------
// ---------------------------------------
function touchHoverSolve(){
	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
		$(".portfolio-element").each(function() {
			if( $(this).find('.post-tip').length > 0 ){
				if($(this).find('.post-tip .left-link').length >0 ){
					
					var portfolio_link = $($(this).find('.post-tip .left-link').parent()).attr('href');
					if(!portfolio_link) { portfolio_link = $(this).find('.post-tip .left-link a').attr('href'); }
					
					$(this).find('.portfolio-img').wrap('<a style="float:left;" href="'+ portfolio_link +'"></a>');
				}
				$(this).find('.post-tip').remove();
			}
        });
		
		$(".post-img").each(function() {
			if( $(this).find('.post-tip').length > 0  && $(this).find('.no-bg').length === 0 ){
				if($(this).find('.post-tip .left-link').length > 0 ){
					$(this).wrap('<a style="float:left;" href="'+ $($(this).find('.post-tip .left-link').parent()).attr('href') +'"></a>');
				} else if($(this).find('.post-tip .center-link').length > 0 ){
					$(this).wrap('<a style="float:left;" href="'+ $($(this).find('.post-tip .center-link').parent()).attr('href') +'"></a>');
				}
				$(this).find('.post-tip').remove();
			}else if( $(this).find('.post-cover').length >0 ){
				$(this).find('.post-cover').remove();
				$(this).find('h5').remove();
			}
        });
		
		$('.alterna-fl').addClass('touch');
		
		$('#header-topbar-right-content .wpml').click(function() {
            if($(this).hasClass('touch')){
				$(this).removeClass('touch');
			}else{
				$(this).addClass('touch');
			}
        });
	}
}

// ---------------------------------------
//	Features & Services Box Link v2.3 -----------
// ---------------------------------------
function fsboxClickEffect(){
	$('.fsbox-link').click(function() {
		if($(this).attr('data-link') !== ""){
			window.location = $(this).attr('data-link');
		}
    });
}

// ---------------------------------------
//	Alert Modal Banner v2.5
// ---------------------------------------
function alertModalInit(){
	if(navigator.cookieEnabled){
		if($('#alert-modal-banner').length > 0){
			var cookie = getCookie("alterna-modal-banner");
			//show alert modal
			if(cookie === null || cookie !== $('#alert-modal-banner').attr('data-id') ){
				$('#alert-modal-banner').modal('show');
				addCookie("alert-modal-banner", $('#alert-modal-banner').attr('data-id'), 24);
			}else{
				$('#alert-modal-banner').remove();
			}
		}
	}
}
// ---------------------------------------
//	Header Banner v4.0
// ---------------------------------------
function headerBannerInit(){
	if(navigator.cookieEnabled){
		if($('#header-banner').length > 0){
			var cookie = getCookie("alterna-header-banner");
			//show header banner
			if(cookie === null || cookie !== $('#header-banner').attr('data-id') ){
				$('#header-banner').css({marginTop:-($('#header-banner').height() + 1), display:'block'});
				$('#header-banner').animate({marginTop:0},600);
				$('#header-banner .close-btn').click(function() {
					$('#header-banner').animate({marginTop:-($('#header-banner').height() + 1)},600,'',function(){ $('#header-banner').remove(); });
					addCookie("alterna-header-banner", $('#header-banner').attr('data-id') , 24);
					return false;
				});
			}else{
				$('#header-banner').remove();
			}
		}
	}
}
// ---------------------------------------
//	Footer Banner v5.2
// ---------------------------------------
function footerBannerInit(){
	if(navigator.cookieEnabled){
		if($('#footer-banner').length > 0){
			var cookie = getCookie("alterna-footer-banner");
			//show header banner
			if(cookie === null || cookie !== $('#footer-banner').attr('data-id') ){
				$('#footer-banner').css({marginBottom:-($('#footer-banner').height() + 1), display:'block'});
				$('#footer-banner').animate({marginBottom:0},600);
				$('#footer-banner .close-btn').click(function() {
					$('#footer-banner').animate({marginBottom:-($('#footer-banner').height() + 1)},600,'',function(){ $('#footer-banner').remove(); });
					addCookie("alterna-footer-banner", $('#footer-banner').attr('data-id') , 24);
					return false;
				});
			}else{
				$('#footer-banner').remove();
			}
		}
	}
}
// ---------------------------------------
//	skills animation run v6.0
// ---------------------------------------
function skillsAnimationRun(){
	$(".skills").each(function() {
		if($(this).hasClass('skill-animation-complete')){
			return;
		}
		if($(this).children('li').length == $(this).find('.skill-animation-run').length){
			$(this).addClass('skill-animation-complete');
			$(this).find('.skill-animation-run').removeClass('skill-animation-run');
			return;
		}
		
		var items = $(this).children('li');
		var count = 0;
		for(var i=0;i<items.length;i++){
			if($(items[i]).find('.skill-bg').hasClass('.skill-animation-run')){
				continue;
			}
			var pos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var heightOfWindow = $(window).height();
			if (pos < topOfWindow+heightOfWindow - 60) {
				$(items[i]).find('.skill-bg').addClass('skill-animation-run').animate({width:$(items[i]).find('.skill-bg').attr('data-percent')},{duration:(400*count + 500)});
			}
			count++;
		}
	   
	});
}
// ---------------------------------------
//	POST COMMENT CHECK
// ---------------------------------------
function postCommentCheck(){
	checkElement("#comments", postCommentFieldCheck);
	
	function postCommentFieldCheck(params){
		if($(params).find('#comment-alert-error').length > 0){
			$(params).find('#submit').click(function() {
				$(params).find('#comment-alert-error').removeClass('show');
				$(params).find('#comment-alert-error span').removeClass('show');
						
				if($(params).find('.comment-alert-error-name').length > 0){
					if($(params).find('#author').attr('value').length === 0){
						$(params).find('#comment-alert-error').addClass('show');
						$(params).find('.comment-alert-error-name').addClass('show');
						return false;
					}
				}
				if($(params).find('.comment-alert-error-email').length > 0){
					if($(params).find('#email').attr('value').length === 0){
						$(params).find('#comment-alert-error').addClass('show');
						$(params).find('.comment-alert-error-email').addClass('show');
						return false;
					}
					var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
					
					if(!reg.test($(params).find('#email').attr('value'))){
						$(params).find('#comment-alert-error').addClass('show');
						$(params).find('.comment-alert-error-email').addClass('show');
						return false;
					}
				}
				if($(params).find('.comment-alert-error-message').length > 0){
					if($(params).find('#comment').attr('value').length === 0){
						$(params).find('#comment-alert-error').addClass('show');
						$(params).find('.comment-alert-error-message').addClass('show');
						return false;
					}
				}
				return true;
            });
		}
	}
}

// ---------------------------------------
//	COMMON -----------  check element and ex fun
// ---------------------------------------
function checkElement(params,fun){
	var list = $(params);

	if(list.length <= 0) {return false;}
	
	for (var w=0; w<list.length; w++)	{
		fun(list[w]);
	}
}

// ---------------------------------------
//	Cookie ----------- save ,delete ,get cookie
// ---------------------------------------
function addCookie(name,value,hours){
	var str = name + "=" + value; 
	if(hours > 0){
		var date = new Date();
		var ms = hours*3600*1000;
		date.setTime(date.getTime() + ms);
		str += "; expires=" + date.toGMTString();
	}
	document.cookie = str;
}
function getCookie(name){
	var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
	if(arr !== null) {
		return arr[2]; 
	}
	return null;
}


/* ----------------------------------------------------
	Execute
---------------------------------------------------- */

	menuInit();
	titleLineInit();
	alternaAlertTitleInit();
	buttonsInit();
	postPageMoreLinkInit();
	postCommentPlaceholderInit();
	tabsInit();
	testimonialsInit();
	accordionInit();
	priceSliderInit();
	scrollResizeInit();
	touchHoverSolve();
	fsboxClickEffect();
	alertModalInit();
	headerBannerInit();
	footerBannerInit();
	postCommentCheck();
	animationRun();
	skillsAnimationRun();
	
	// flexslide
	$('.alterna-fl').each(function() {
        if($(this).attr('data-delay')){
			$(this).flexslider({slideshow: true , slideshowSpeed:$(this).attr('data-delay') });
		}else{
			$(this).flexslider({slideshow: false });
		}
    });
	
	//carousel auto play v2.5
	$('.carousel-autoplay').each(function() {
        $(this).carousel({interval: $(this).attr('data-delay')});
    });
	
	// isotope filters
	$('.portfolio-filters-cate a').click(function() {
		if($(this).hasClass('active'))	{return false;}
		$(this).addClass('active');
		$(this).parent().prevAll().find('a').removeClass('active');
		$(this).parent().nextAll().find('a').removeClass('active');
		
		var filters = $(this).attr('data-filters');
        $('.portfolio-container').isotope({ filter: filters });
    });
	
	$('.portfolio-container').isotope({
		itemSelector: '.portfolio-element',
		layoutMode : 'fitRows'
	});
	
	$('#post-ajax').isotope({
		itemSelector: '.post-ajax-element',
		layoutMode : 'masonry'
	});
	
	$('.portfolio-container').find('img').load(function() {
        refreshIsotope();
    });
	
	$('#post-ajax').find('img').load(function() {
        refreshIsotope();
    });
	
	//woocommerce
	$('.widget_product_search #searchsubmit').attr('value','');
	
	//fancybox
	if($.fn.fancybox !== null)	{
		$("a[class^='fancyBox']").fancybox();
		$("a[class^='fancybox-thumb']").fancybox({prevEffect	: 'none',nextEffect	: 'none',helpers: {title: {type: 'outside'},thumbs: {width	: 50,height	: 50}}});
	}
	
	// IE8
	function refreshWideBackground(){
		var s_width = ($(window).width() - 940)/2 + 4;
		$('.wide-background').css({'padding-left':s_width + 'px','padding-right': s_width + 'px','margin-left':-s_width+'px'});
	}
	if ( $.browser.msie && $.browser.version < 9) {
		$('.post-img > a').css('float','none');
		$('.wrapper').css('max-width',980);
		if($('body.full-width-layout').length > 0){
			jQuery(window).resize(function() {	refreshWideBackground(); });
			refreshWideBackground();
		}
	}
});

jQuery(window).on("load", function() {
	refreshIsotope();
	jQuery(window).resize(function() {	refreshIsotope(); });
	
});

function refreshIsotope(){
	jQuery('#post-ajax').isotope( 'reLayout' );
	jQuery('.portfolio-container').isotope( 'reLayout' );
}
