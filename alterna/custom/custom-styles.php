<?php
/**
 * Custom Layout,Font,Css
 *
 * @since Alterna 1.0
 */
 
	global $alterna_options,$google_fonts,$google_load_fonts,$google_custom_fonts;
	
	if($google_custom_fonts == null) alterna_get_custom_font();
	
	// Custom Font For google font
 	$general_font 				=  $google_custom_fonts['general_font'];
	$general_font_size 			= '14px';
	$menu_font					= $google_custom_fonts['menu_font'];
	$menu_font_size				= '13px';
	$title_font					= $google_custom_fonts['title_font'];
	
	if(alterna_get_options_key('custom-enable-font') == "on"){
		
		if( alterna_get_options_key('custom-general-font-size') !="")
			$general_font_size = intval($alterna_options['custom-general-font-size']).'px';

		if( alterna_get_options_key('custom-menu-font-size') !="")
			$menu_font_size = intval($alterna_options['custom-menu-font-size']).'px';
	}
	
	// Custom color
	$theme_color						=	'#7ab80e';
	$theme_over_color					=	'#5b8f00';
	$theme_shadow_color					=	'#436a00';
	$custom_general_color				=	'#666666';
	$custom_a_color						=	'#1c1c1c';
	$custom_h_color						=	'#3a3a3a';
	
	$custom_menu_background_color				=	'#0c0c0c';
	$custom_sub_menu_background_color			=	'#7ab80e';
	$custom_sub_menu_hover_background_color		=	'#0c0c0c';
	$custom_menu_border_top_color		=	'#ffffff';
	$custom_menu_border_bottom_color	=	'#0c0c0c';
	$custom_menu_font_color				=	'#ffffff';
	$custom_menu_hover_font_color		=	'#7ab80e';
	
	$custom_sub_menu_font_color			=	'#000000';
	$custom_sub_menu_hover_font_color	=	'#ffffff';
	
	$custom_header_bg_color				=	'#ffffff';
	$custom_header_border_color			=	'#e8e8e8';
	$custom_header_banner_bg_color		=	'#f7d539';
	$custom_header_topbar_bg_color		=	'#f2f2f2';
	$custom_header_topbar_border_color	=	'#e6e6e6';
	$custom_header_topbar_font_color	=	'#757575';
	$custom_header_topbar_hover_font_color	=	'#7ab80e';
	$custom_header_topbar_sub_menu_hover_bg_color	=	'#f7f7f7f';
	
	$custom_content_bg_color			=	'#ffffff';
	
	
	$custom_footer_text_color			=	'#999999';
	$custom_footer_a_color				=	'#e7e7e7';
	$custom_footer_h_color				=	'#ffffff';
	$custom_footer_bg_color				=	'#404040';
	$custom_footer_copyright_a_color	=	'#606060';
	$custom_footer_copyright_a_hover_color	=	'#7ab80e';
	$custom_footer_copyright_bg_color	=	'#0c0c0c';
	$custom_footer_banner_bg_color		=	'#ffffff';
	
	$fixed_header_bg_color				=	'#0C0C0C';
	$fixed_header_sub_menu_bg_color		=	'#7ab80e';
	$fixed_header_sub_menu_hover_bg_color	=	'#0c0c0c';
	$fixed_header_menu_font_color			=	'#ffffff';
	$fixed_header_menu_hover_font_color		=	'#ffffff';
	$fixed_header_sub_menu_font_color		=	'#000000';
	$fixed_header_sub_menu_hover_font_color	=	'#ffffff';
	$fixed_header_form_bg_color				=	'#323232';
	$fixed_header_form_border_color			=	'#000344';

	
	if(alterna_get_options_key('custom-enable-color') == "on"){
		
		if( alterna_get_options_key('theme-color') !="")
			$theme_color=	"#".$alterna_options['theme-color'];
			
		if( alterna_get_options_key('theme-over-color') !="")
			$theme_over_color=	"#".$alterna_options['theme-over-color'];
			
		if( alterna_get_options_key('theme-shadow-color') !="")
			$theme_shadow_color=	"#".$alterna_options['theme-shadow-color'];
			
		if( alterna_get_options_key('custom-general-color') !="")
			$custom_general_color=	"#".$alterna_options['custom-general-color'];
			
		if( alterna_get_options_key('custom-a-color') !="")
			$custom_a_color=	"#".$alterna_options['custom-a-color'];
			
		if( alterna_get_options_key('custom-h-color') !="")
			$custom_h_color=	"#".$alterna_options['custom-h-color'];
			
		if( alterna_get_options_key('custom-menu-background-color') !="")
			$custom_menu_background_color =	"#".$alterna_options['custom-menu-background-color'];
		
		if( alterna_get_options_key('custom-sub-menu-background-color') !="")
			$custom_sub_menu_background_color =	"#".$alterna_options['custom-sub-menu-background-color'];
		
		if( alterna_get_options_key('custom-sub-menu-hover-background-color') !="")
			$custom_sub_menu_hover_background_color =	"#".$alterna_options['custom-sub-menu-hover-background-color'];
			
		if( alterna_get_options_key('custom-sub-menu-hover-border-top-color') !="")
			$custom_menu_border_top_color =	"#".$alterna_options['custom-sub-menu-hover-border-top-color'];
		
		if( alterna_get_options_key('custom-sub-menu-hover-border-bottom-color') !="")
			$custom_menu_border_bottom_color =	"#".$alterna_options['custom-sub-menu-hover-border-bottom-color'];
		
		if( alterna_get_options_key('custom-menu-font-color') !="")
			$custom_menu_font_color =	"#".$alterna_options['custom-menu-font-color'];
			
		if( alterna_get_options_key('custom-menu-hover-font-color') !="")
			$custom_menu_hover_font_color =	"#".$alterna_options['custom-menu-hover-font-color'];

		if( alterna_get_options_key('custom-sub-menu-font-color') !="")
			$custom_sub_menu_font_color =	"#".$alterna_options['custom-sub-menu-font-color'];
		
		if( alterna_get_options_key('custom-sub-menu-hover-font-color') !="")
			$custom_sub_menu_hover_font_color =	"#".$alterna_options['custom-sub-menu-hover-font-color'];
		
		if( alterna_get_options_key('custom-header-banner-bg-color') !="")
			$custom_header_banner_bg_color =	"#".$alterna_options['custom-header-banner-bg-color'];	
		
		if( alterna_get_options_key('custom-header-topbar-bg-color') !="")
			$custom_header_topbar_bg_color =	"#".$alterna_options['custom-header-topbar-bg-color'];	
		
		if( alterna_get_options_key('custom-header-topbar-border-color') !="")
			$custom_header_topbar_border_color =	"#".$alterna_options['custom-header-topbar-border-color'];
			
		if( alterna_get_options_key('custom-header-topbar-font-color') !="")
			$custom_header_topbar_font_color =	"#".$alterna_options['custom-header-topbar-font-color'];	
			
		if( alterna_get_options_key('custom-header-topbar-hover-font-color') !="")
			$custom_header_topbar_hover_font_color = "#".$alterna_options['custom-header-topbar-hover-font-color'];	
			
		if( alterna_get_options_key('custom-header-topbar-sub-menu-hover-bg-color') !="")
			$custom_header_topbar_sub_menu_hover_bg_color = "#".$alterna_options['custom-header-topbar-sub-menu-hover-bg-color'];	
			
			
		if(alterna_get_options_key('custom-content-bg-color') !="") {
			$custom_content_bg_color = "#".$alterna_options['custom-content-bg-color'];	
		}
			
			
		if( alterna_get_options_key('custom-footer-text-color') !="")
			$custom_footer_text_color=	"#".$alterna_options['custom-footer-text-color'];
			
		if( alterna_get_options_key('custom-footer-a-color') !="")
			$custom_footer_a_color=	"#".$alterna_options['custom-footer-a-color'];
		
		if( alterna_get_options_key('custom-footer-h-color') !="")
			$custom_footer_h_color=	"#".$alterna_options['custom-footer-h-color'];
			
		if( alterna_get_options_key('custom-footer-bg-color') !="")
			$custom_footer_bg_color=	"#".$alterna_options['custom-footer-bg-color'];
			
		if( alterna_get_options_key('custom-footer-copyright-a-color') !="")
			$custom_footer_copyright_a_color =	"#".$alterna_options['custom-footer-copyright-a-color'];
		
		if( alterna_get_options_key('custom-footer-copyright-a-hover-color') !="")
			$custom_footer_copyright_a_hover_color =	"#".$alterna_options['custom-footer-copyright-a-hover-color'];
		
		if( alterna_get_options_key('custom-footer-copyright-color') !="")
			$custom_footer_copyright_bg_color =	"#".$alterna_options['custom-footer-copyright-color'];
			
		if( alterna_get_options_key('custom-footer-banner-bg-color') !="")
			$custom_footer_banner_bg_color =	"#".$alterna_options['custom-footer-banner-bg-color'];
			
			
		//fixed header style
		if( alterna_get_options_key('fixed-header-background-color') !="")
			$fixed_header_bg_color =	"#".$alterna_options['fixed-header-background-color'];
			
		if( alterna_get_options_key('fixed-header-sub-menu-background-color') !="")
			$fixed_header_sub_menu_bg_color =	"#".$alterna_options['fixed-header-sub-menu-background-color'];

		if( alterna_get_options_key('fixed-header-sub-menu-hover-background-color') !="")
			$fixed_header_sub_menu_hover_bg_color =	"#".$alterna_options['fixed-header-sub-menu-hover-background-color'];
			
		if( alterna_get_options_key('fixed-header-menu-font-color') !="")
			$fixed_header_menu_font_color =	"#".$alterna_options['fixed-header-menu-font-color'];
			
		if( alterna_get_options_key('fixed-header-menu-hover-font-color') !="")
			$fixed_header_menu_hover_font_color =	"#".$alterna_options['fixed-header-menu-hover-font-color'];
			
		if( alterna_get_options_key('fixed-header-sub-menu-font-color') !="")
			$fixed_header_sub_menu_font_color =	"#".$alterna_options['fixed-header-sub-menu-font-color'];
			
		if( alterna_get_options_key('fixed-header-sub-menu-hover-font-color') !="")
			$fixed_header_sub_menu_hover_font_color =	"#".$alterna_options['fixed-header-sub-menu-hover-font-color'];
			
		if( alterna_get_options_key('fixed-header-form-background-color') !="")
			$fixed_header_form_bg_color =	"#".$alterna_options['fixed-header-form-background-color'];
			
		if( alterna_get_options_key('fixed-header-form-border-color') !="")
			$fixed_header_form_border_color =	"#".$alterna_options['fixed-header-form-border-color'];

	}
	
header("Content-type: text/css; charset: UTF-8");
?>

::-moz-selection { background:<?php echo $theme_color;?>; color: #ffffff; text-shadow: none; }
::selection { background:<?php echo $theme_color;?>; color: #ffffff; text-shadow: none; }

<?php 
if(intval(alterna_get_options_key('global-layout')) != 0) : 
?>    
body {padding:0 !important;}
<?php 
endif;
if(alterna_get_options_key('logo-txt-enable') != "on") : 
?>
.logo a {
    width:<?php echo intval(alterna_get_options_key('logo-image-width')) == 0 ? '280px' : $alterna_options['logo-image-width'].'px'?>;
    height:<?php echo intval(alterna_get_options_key('logo-image-height')) == 0 ? '60px' : $alterna_options['logo-image-height'].'px'?>;
    margin-top:<?php echo intval(alterna_get_options_key('logo-image-padding-top')) == 0 ? 0 : $alterna_options['logo-image-padding-top'].'px'?>;
    background-image:url(<?php echo alterna_get_options_key('logo-image') == "" ? "../img/logo.png" : $alterna_options['logo-image'];?>);
    background-size: <?php echo intval(alterna_get_options_key('logo-image-width')) == 0 ? '280px' : $alterna_options['logo-image-width'].'px'?> <?php echo intval(alterna_get_options_key('logo-image-height')) == 0 ? '60px' : $alterna_options['logo-image-height'].'px'?>;
}
<?php 
endif; 
if(alterna_get_options_key('fixed-enable') == "on") : 
?>
.fixed-logo a {
    width:<?php echo intval(alterna_get_options_key('fixed-logo-image-width')) == 0 ? '44px' : $alterna_options['fixed-logo-image-width'].'px'?>;
    height:44px;
    background-image:url(<?php echo alterna_get_options_key('fixed-logo-image') == "" ? "../img/fixed-logo.png" : $alterna_options['fixed-logo-image'];?>);
    background-size: <?php echo intval(alterna_get_options_key('fixed-logo-image-width')) == 0 ? '44px' : $alterna_options['fixed-logo-image-width'].'px'?> 44px;
}
<?php 
endif; 
if(intval(alterna_get_options_key('global-layout')) == 0) : 
	if(intval(alterna_get_options_key('global-bg-type')) == 0) : 
?>
    body {
        background-size:<?php echo alterna_get_options_key('global-bg-pattern-width') == "" ? "200" : $alterna_options['global-bg-pattern-width'];?>px <?php echo alterna_get_options_key('global-bg-pattern-height') == "" ? "200" : $alterna_options['global-bg-pattern-height'];?>px;
        background-repeat: repeat;
        background-image:url(<?php echo alterna_get_options_key('global-bg-image') == "" ? "../img/wild_oliva.png" : $alterna_options['global-bg-image'];?>);
     }
<?php 
	elseif(intval(alterna_get_options_key('global-bg-type')) == 1 && alterna_get_options_key('global-bg-image') != "") : 
?>
    body { 
        background: url(<?php echo alterna_get_options_key('global-bg-image'); ?>) no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
<?php 
	elseif(intval(alterna_get_options_key('global-bg-type')) == 2 && alterna_get_options_key('global-bg-color') != "") : 
?>
    body {
        background-color:#<?php echo alterna_get_options_key('global-bg-color'); ?>;
    }
<?php 
	endif;
	?>
    /* global wrap style */
	.wrapper {margin: <?php echo alterna_get_options_key('global-layout-boxed-padding');?>px auto;}
<?php
endif;
?> 
.header-social-container {
    margin-top:<?php echo intval(alterna_get_options_key('header-social-padding-top')).'px'; ?>;
    margin-right:<?php echo intval(alterna_get_options_key('header-social-padding-left')).'px'; ?>;
}

header.header-fixed .header-style-4 .alterna-header-right-container {margin-top:<?php echo intval(alterna_get_options_key('fixed-header-right-area-padding-top')).'px'; ?>;}
.header-style-2  .alterna-header-right-container,
.header-style-5  .alterna-header-right-container {margin-top:<?php echo intval(alterna_get_options_key('header-right-area-padding-top')).'px'; ?>;}

<?php if(intval(alterna_get_options_key('global-content-bg-type')) == 0) { ?>
.content-wrap {
    background-size:<?php echo alterna_get_options_key('global-content-bg-pattern-width');?>px <?php echo alterna_get_options_key('global-content-bg-pattern-height') ;?>px;
    background-repeat: repeat;
    background-image:url(<?php echo alterna_get_options_key('global-content-bg-image');?>);
 }
<?php 
} elseif(intval(alterna_get_options_key('global-content-bg-type')) == 1 && alterna_get_options_key('global-content-bg-image') != "") {
?>
.content-wrap { 
    background: url(<?php echo alterna_get_options_key('global-content-bg-image'); ?>) no-repeat center center fixed; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
<?php 
} elseif(intval(alterna_get_options_key('global-content-bg-type')) == 2 && alterna_get_options_key('global-content-bg-color') != "") {
?>
.content-wrap {
    background-color:#<?php echo alterna_get_options_key('global-content-bg-color'); ?>;
}
<?php } ?>


<?php if(intval(alterna_get_options_key('global-header-area-bg-type')) == 0) { ?>
#alterna-header {
    background-size:<?php echo alterna_get_options_key('global-header-area-bg-pattern-width') == "" ? "100" : $alterna_options['global-header-area-bg-pattern-width'];?>px <?php echo alterna_get_options_key('global-header-area-bg-pattern-height') == "" ? "100" : $alterna_options['global-header-area-bg-pattern-height'];?>px;
    background-repeat: repeat;
    background-image:url(<?php echo alterna_get_options_key('global-header-area-bg-image') == "" ? "" : $alterna_options['global-header-area-bg-image'];?>);
 }
<?php }elseif(intval(alterna_get_options_key('global-header-area-bg-type')) == 1 && alterna_get_options_key('global-header-area-bg-image') != "") {?>
#alterna-header { 
    background: url(<?php echo alterna_get_options_key('global-header-area-bg-image'); ?>) repeat center center scroll; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
<?php }elseif(intval(alterna_get_options_key('global-header-area-bg-type')) == 2 && alterna_get_options_key('global-header-area-bg-color') != "") {?>
#alterna-header {
    background-color:#<?php echo alterna_get_options_key('global-header-area-bg-color'); ?>;
}
<?php } ?>

/* page title background */
<?php if(intval(alterna_get_options_key('global-page-title-bg-type')) == 0) { ?>
#page-header {
    background-size:<?php echo alterna_get_options_key('global-page-title-bg-pattern-width') == "" ? "297" : $alterna_options['global-page-title-bg-pattern-width'];?>px <?php echo alterna_get_options_key('global-page-title-bg-pattern-height') == "" ? "297" : $alterna_options['global-page-title-bg-pattern-height'];?>px;
    background-repeat: repeat;
    background-image:url(<?php echo alterna_get_options_key('global-page-title-bg-image') == "" ? "../img/bright_squares.png" : $alterna_options['global-page-title-bg-image'];?>);
 }
<?php }elseif(intval(alterna_get_options_key('global-page-title-bg-type')) == 1 && alterna_get_options_key('global-page-title-bg-image') != "") {?>
#page-header { 
    background: url(<?php echo alterna_get_options_key('global-page-title-bg-image'); ?>) repeat center center scroll; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
<?php }elseif(intval(alterna_get_options_key('global-page-title-bg-type')) == 2 && alterna_get_options_key('global-page-title-bg-color') != "") {?>
#page-header {
    background-color:#<?php echo alterna_get_options_key('global-page-title-bg-color'); ?>;
}
<?php } ?>


<?php 
if(alterna_get_options_key('custom-enable-font') == "on") : 
?>
/* 	----------------------------------------------------------------------------------------------	
										A - CUSTOM THEME FONT																												
	----------------------------------------------------------------------------------------------	*/
    
    body {
    	font-family:<?php echo $general_font;?>,"Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size:<?php echo $general_font_size; ?>;
    }

	h1,h2,h3,h4,h5,h6 {font-family:<?php echo $title_font; ?>,Arial,Helvetica,sans-serif;}
	
    .alterna-nav-menu li a , #alterna-nav-menu-select .nav a {
    	font-size: <?php echo $menu_font_size;?>;
       	font-family: <?php echo $menu_font;?>,sans-serif;
    }
    
    .post-meta, 
    .post-meta a , 
    .comment-meta , 
    .comment-meta a ,
    .search-post-mate, 
    .search-post-mate a {font-family:<?php echo $title_font; ?>,Arial,Helvetica,sans-serif;}
    
<?php endif; ?>

<?php if( alterna_get_options_key('custom-enable-color') == "on") : ?>	

/* 	----------------------------------------------------------------------------------------------	
										CUSTOM THEME COLOR																												
	----------------------------------------------------------------------------------------------	*/


h1,h2,h3,h4,h5,h6 {color:<?php echo $custom_h_color;?>;}
body {color: <?php echo $custom_general_color;?>;}
a {color: <?php echo $custom_a_color;?>;}
a:hover {color: <?php echo $theme_color;?>;}

/* global wrap style */
.footer-content {border-top: 6px <?php echo $theme_color;?> solid;background:<?php echo $custom_footer_bg_color;?>;}

	textarea:focus,
    input[type="text"]:focus,
    input[type="password"]:focus,
    input[type="datetime"]:focus,
    input[type="datetime-local"]:focus,
    input[type="date"]:focus,
    input[type="month"]:focus,
    input[type="time"]:focus,
    input[type="week"]:focus,
    input[type="number"]:focus,
    input[type="email"]:focus,
    input[type="url"]:focus,
    input[type="search"]:focus,
    input[type="tel"]:focus,
    input[type="color"]:focus,
    .uneditable-input:focus {
      border: 1px solid <?php echo $theme_color; ?> !important;
    }
    
/* 	----------------------------------------------------------------------------------------------	
										B - LAYOUT																													
	----------------------------------------------------------------------------------------------	*/
/* = Header Style
-------------------------------------------------------------- */
/* header banner */
#header-banner {background:<?php echo $custom_header_banner_bg_color; ?>;}

/* header alert message */
#header-topbar, .header-top-content {background: <?php echo $custom_header_topbar_bg_color; ?>;}
#header-topbar-left-content > ul > li, #header-topbar-right-content > ul > li {border-right:1px <?php echo $custom_header_topbar_border_color; ?> solid;}
#header-topbar ul li ul {background: <?php echo $custom_header_topbar_bg_color; ?>;border:1px <?php echo $custom_header_topbar_border_color; ?> solid;}
#header-topbar li ul li a, #header-topbar li ul li span {border-bottom:1px <?php echo $custom_header_topbar_border_color; ?> solid;}
#header-topbar span, #header-topbar a {color:<?php echo $custom_header_topbar_font_color; ?>;}
#header-topbar a:hover, #header-topbar a:hover .amount {color:<?php echo $custom_header_topbar_hover_font_color; ?>;}
#header-topbar li ul li a:hover {background:<?php echo $custom_header_topbar_sub_menu_hover_bg_color; ?>;}

/* header menu , search form */

#alterna-nav {background:<?php echo $custom_menu_background_color; ?>;border-bottom:4px <?php echo $theme_color; ?> solid;}

.alterna-nav-menu > li:hover, 
.header-style-2 .alterna-nav-menu > li:hover ,
.header-style-3 .alterna-nav-menu > li:hover {background:<?php echo $theme_color; ?>;}

.alterna-nav-menu > li.current-menu-item ,
.alterna-nav-menu > li.current-menu-ancestor {background:<?php echo $theme_color; ?>;}

.header-style-2 .alterna-nav-menu > li.current-menu-item , 
.header-style-2 .alterna-nav-menu > li.current-menu-ancestor,
.header-style-3 .alterna-nav-menu > li.current-menu-item , 
.header-style-3 .alterna-nav-menu > li.current-menu-ancestor {background:none;}

.alterna-nav-menu .sub-menu {background:<?php echo $custom_sub_menu_background_color; ?>;}
.alterna-nav-menu .sub-menu li {border-bottom:1px <?php echo $custom_menu_border_bottom_color; ?> dotted;}

.alterna-nav-menu > li.current-menu-item:hover  , 
.alterna-nav-menu > li.current-menu-ancestor:hover ,
.alterna-nav-menu > li.current-menu-item:hover .sub-menu, 
.alterna-nav-menu > li.current-menu-ancestor:hover .sub-menu {background:<?php echo $theme_color; ?>;}
.alterna-nav-menu .sub-menu li:hover {background:<?php echo $custom_sub_menu_hover_background_color; ?>;}

.header-style-2 .alterna-nav-menu > li.current-menu-item > a , 
.header-style-3 .alterna-nav-menu > li.current-menu-item > a , 
.header-style-2 .alterna-nav-menu > li.current-menu-ancestor > a ,
.header-style-3 .alterna-nav-menu > li.current-menu-ancestor > a{ border-bottom:<?php echo $theme_color; ?> solid 1px;}

#alterna-nav-menu-select .nav a ,
.alterna-nav-menu li a, 
.header-style-2 .alterna-nav-menu li a, 
.header-style-3 .alterna-nav-menu li a,
.header-style-4 .alterna-nav-menu li a,
.header-style-5 .alterna-nav-menu li a {color:<?php echo $custom_menu_font_color; ?>;}

.alterna-nav-menu > li:hover > a, 
.header-style-2 .alterna-nav-menu > li:hover > a,
.header-style-3 .alterna-nav-menu > li:hover > a,
.header-style-4 .alterna-nav-menu > li:hover > a,
.header-style-5 .alterna-nav-menu > li:hover > a {color:<?php echo $custom_menu_hover_font_color; ?>;}

.alterna-nav-menu .sub-menu li a, 
.header-style-2 .alterna-nav-menu .sub-menu li a, 
.header-style-3 .alterna-nav-menu .sub-menu li a,
.header-style-4 .alterna-nav-menu .sub-menu li a {color:<?php echo $custom_sub_menu_font_color; ?>;}

.alterna-nav-menu .sub-menu li:hover > a,
.header-style-2 .alterna-nav-menu .sub-menu li:hover > a,
.header-style-3 .alterna-nav-menu .sub-menu li:hover > a,
.header-style-4 .alterna-nav-menu .sub-menu li:hover > a {color:<?php echo $custom_sub_menu_hover_font_color; ?>;}

/* style 4 */
.header-style-4 .alterna-nav-menu > li.current-menu-item > a,
.header-style-4 .alterna-nav-menu > li.current-menu-ancestor > a{border-bottom:2px solid <?php echo $theme_color; ?>;color:<?php echo $theme_color; ?>;}
.header-style-4 .alterna-nav-menu > li:hover > a {border-bottom:2px solid <?php echo $theme_color; ?>;background: <?php echo $custom_menu_background_color; ?>;}
.header-style-4 .alterna-nav-menu .sub-menu {border-top:2px solid <?php echo $theme_color; ?>;}
.header-style-4 .alterna-nav-menu .sub-menu li {background:<?php echo $custom_sub_menu_background_color; ?>;border-top: <?php echo $custom_menu_border_top_color; ?> 1px solid;
border-bottom: <?php echo $custom_menu_border_bottom_color; ?> 1px solid;}
.header-style-4 .alterna-nav-menu .sub-menu li:hover {background:<?php echo $custom_sub_menu_hover_background_color; ?>;}
.header-style-4 .alterna-nav-form-content .searchform .sf-s:focus {border: 1px <?php echo $theme_color; ?> solid;}
#alterna-nav-menu-select {background:<?php echo $custom_menu_background_color; ?>;}
#alterna-nav-menu-select .nav .active a, #alterna-nav-menu-select .nav a:hover {background:<?php echo $theme_color; ?>;}

/* style 5 */
#alterna-header.header-style-2
#alterna-header.header-style-3,
#alterna-header.header-style-4,
#alterna-header.header-style-5 {border-bottom:<?php echo $custom_header_border_color; ?> solid 1px;}
.header-style-5 .alterna-nav-menu > li {background:none;}
.header-style-5 .alterna-nav-menu .sub-menu,
.header-style-5 .alterna-nav-menu > li.current-menu-item:hover .sub-menu, 
.header-style-5 .alterna-nav-menu > li.current-menu-ancestor:hover .sub-menu {background:<?php echo $custom_sub_menu_background_color; ?>;}
.header-style-5 .alterna-nav-menu .sub-menu li {border-bottom: 1px <?php echo $custom_menu_border_bottom_color; ?> solid;}
/* header form */
.searchform .sf-searchsubmit {border: 1px solid <?php echo $theme_color; ?>;background-color: <?php echo $theme_color; ?>;}


<?php if(alterna_get_options_key('fixed-header-enable-color') == "on"){ ?>
/* fixed header custom css */
#alterna-nav.header-fixed {background: <?php echo $fixed_header_bg_color;?>;}
#alterna-nav.header-fixed .alterna-nav-menu .sub-menu {background: <?php echo $fixed_header_sub_menu_bg_color;?>;}
#alterna-nav.header-fixed .alterna-nav-menu .sub-menu li:hover {background:<?php echo $fixed_header_sub_menu_hover_bg_color;?>;}
#alterna-nav.header-fixed .alterna-nav-menu li a {color: <?php echo $fixed_header_menu_font_color;?>;}

#alterna-nav.header-fixed .alterna-nav-menu > li.current-menu-item > a , 
#alterna-nav.header-fixed .alterna-nav-menu > li.current-menu-ancestor > a ,
#alterna-nav.header-fixed .alterna-nav-menu > li:hover > a {color: <?php echo $fixed_header_menu_hover_font_color;?>;}

#alterna-nav.header-fixed .alterna-nav-menu .sub-menu li a {color: <?php echo $fixed_header_sub_menu_font_color;?>;}
#alterna-nav.header-fixed .alterna-nav-menu .sub-menu li:hover a {color: <?php echo $fixed_header_sub_menu_hover_font_color;?>;}

#alterna-nav.header-fixed .searchform .sf-s {background: <?php echo $fixed_header_form_bg_color;?>;border: 1px <?php echo $fixed_header_form_border_color;?> solid;}

<?php } ?>


/* = Footer Style
-------------------------------------------------------------- */
.footer-content {color: <?php echo $custom_footer_text_color;?>;}
.footer-content a {color: <?php echo $custom_footer_a_color;?>;}
.footer-content a:hover {color: <?php echo $theme_color;?>;}
.footer-top-content h1 , 
.footer-top-content h2 ,
.footer-top-content h3 ,
.footer-top-content h4 ,
.footer-top-content h5, 
.footer-top-content h6 {color:<?php echo $custom_footer_h_color; ?>;}

.footer-bottom-content {border-top: none;background-color: <?php echo $custom_footer_copyright_bg_color; ?>;}

.footer-content .footer-copyright a {color: <?php echo $custom_footer_copyright_a_color;?>;}
.footer-content .footer-copyright  a:hover {color: <?php echo $custom_footer_copyright_a_hover_color;?>;}

/* footer banner */
#footer-banner {background:<?php echo $custom_footer_banner_bg_color; ?>;}

/* = Other Element Style
-------------------------------------------------------------- */
.left-line {background:<?php echo $theme_color;?>;}
#back-top:hover {background-color:<?php echo $theme_color;?>;}
/* 	----------------------------------------------------------------------------------------------	
										C - PAGE STYLES																											
	----------------------------------------------------------------------------------------------	*/

/* = Post , Blog
-------------------------------------------------------------- */
.post-date-type .post-type {background: <?php echo $theme_color;?>;}
.single-post .entry-tags .post-tags-icon {background: <?php echo $theme_color;?>;	}
div.left-link:hover ,div.right-link:hover ,div.center-link:hover {background: <?php echo $theme_color;?>;}
.post-quote-entry {background:<?php echo $theme_color;?>;}
.comment-list .comment-reply-link {background: <?php echo $theme_color;?>;}
.post-ajax-element .post-type {line-height: 10px;background: <?php echo $theme_color;?>;}
.post-meta a:hover , .comment-meta a:hover ,.search-post-mate a:hover {color:<?php echo $theme_color;?> !important;}

/* = Portfolio Style
-------------------------------------------------------------- */
.portfolio-filters-cate li a:hover {color: <?php echo $theme_color;?>;border: 1px solid <?php echo $theme_color;?>;}
.portfolio-wrap .post-tip .entry-title {color: <?php echo $theme_color;?>;}
.post-tip-info .portfolio-client {color: <?php echo $theme_color;?>;}
.portfolio-element.portfolio-style-1:hover .portfolio-content {border-bottom: 1px <?php echo $theme_color;?> solid;}
.portfolio-element:hover .portfolio-content .entry-title {color: <?php echo $theme_color;?>;}

/* 	----------------------------------------------------------------------------------------------	
										D. WIDGETS																										
	----------------------------------------------------------------------------------------------	*/
.shortcode-post-entry.blog-shortcode-style-2 .date,
.shortcode-post-entry.blog-shortcode-style-4 .date {background: <?php echo $theme_color;?>;}

.shortcode-post-entry.blog-shortcode-style-2 .post-comments a:hover,
.shortcode-post-entry.blog-shortcode-style-4 .post-comments a:hover {color:<?php echo $theme_color;?>;}

.shortcode-post-entry.blog-shortcode-style-3 .post-type {background: <?php echo $theme_color;?>;}

/* = Portfolio Widget
-------------------------------------------------------------- */
.sidebar-portfolio-recent.icon-style .post-type {background: <?php echo $theme_color;?>;}

/* = Blog Widget
-------------------------------------------------------------- */
.sidebar-blog-recent.icon-style .post-type {background: <?php echo $theme_color;?>;}
.sidebar-blog-recent .entry-meta a:hover {color:<?php echo $theme_color;?>;}

.sidebar-blog-recent.big-thumbs-style .post-tip .link {background: <?php echo $theme_color;?>;}

.sidebar-blog-recent.big-thumbs-style .post-tip .link,
.sidebar-portfolio-recent.big-thumbs-style .post-tip .link {background: <?php echo $theme_color;?>;}

.btn-theme-t {color:<?php echo $theme_color;?>;}
.btn-theme {background:<?php echo $theme_color;?>;}
.btn-theme:hover {background:<?php echo $theme_over_color;?>;}
.float-btn.btn-theme {box-shadow: 0 4px 0 <?php echo $theme_shadow_color;?>;}
.float-btn.btn-theme:active {box-shadow: 0 3px 0 <?php echo $theme_shadow_color;?>;}

/* ------- 5. Search Form  ------- */
.sidebar-searchform input[type=submit] ,
.widget_product_search #searchform input[type=submit]{
	background-color: <?php echo $theme_color;?>;
}

.footer-top-content .widget_archive ul li a:hover , 
.footer-top-content .widget_login ul li a:hover ,
.footer-top-content .widget_categories ul li a:hover ,
.footer-top-content .widget_product_categories ul li a:hover ,
.footer-top-content .widget_nav_menu ul li a:hover ,
.footer-top-content .widget_alternaportfoliocategorywidget ul li a:hover ,
.footer-top-content .widget_recent_entries ul li a:hover ,
.footer-top-content .widget_pages  ul li a:hover ,
.footer-top-content .widget_recent_comments ul li:hover ,
.footer-top-content .widget_meta ul li a:hover ,
.footer-top-content .widget_links ul li a:hover ,
.footer-top-content .widget_tag_cloud .tagcloud a:hover {
	background: <?php echo $theme_color;?>;
}

.footer-top-content .widget_recent_comments ul li a:hover {
	color:<?php echo $theme_over_color;?> !important;
}

/* 	----------------------------------------------------------------------------------------------	
										E - EXTRAS																					
	----------------------------------------------------------------------------------------------	*/

.tabs .tabs-nav li.current {border-bottom: 1px <?php echo $theme_color;?> solid;}
.sidetabs.left .sidetabs-nav li.current {border-right: 1px solid <?php echo $theme_color;?>;}
.sidetabs.right .sidetabs-nav li.current {border-left: 1px solid <?php echo $theme_color;?>;}
.map-info-window.black a:hover {color:<?php echo $theme_color;?>;}

/* = Plugin
-------------------------------------------------------------- */

/* ------- 1. Flexslider  ------- */
.flexslider.alterna-fl .flex-direction-nav a:hover {background-color: <?php echo $theme_color;?>;}

/* Bootstrap */
.pagination>li>a:hover, .pagination>li>a:focus {color:<?php echo $theme_color;?>;border: 1px solid <?php echo $theme_color;?>;}

.wpcf7 .wpcf7-submit {background: <?php echo $theme_color; ?>;}
.wpcf7 .wpcf7-submit:hover {background:<?php echo $theme_over_color; ?>;}

.woocommerce div.product span.price,.woocommerce-page div.product span.price,.woocommerce #content div.product span.price,.woocommerce-page #content div.product span.price,.woocommerce div.product p.price,.woocommerce-page div.product p.price,.woocommerce #content div.product p.price,.woocommerce-page #content div.product p.price {
	color: <?php echo $theme_color; ?>;
}
.woocommerce div.product .woocommerce-tabs ul.tabs li.active,.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active,.woocommerce #content div.product .woocommerce-tabs ul.tabs li.active,.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active {
	border-top-color: <?php echo $theme_color; ?>;
}
.woocommerce ul.products li.product .price,.woocommerce-page ul.products li.product .price {
	color: <?php echo $theme_color; ?>;
}
.woocommerce a.button.added:before,.woocommerce-page a.button.added:before,.woocommerce button.button.added:before,.woocommerce-page button.button.added:before,.woocommerce input.button.added:before,.woocommerce-page input.button.added:before,.woocommerce #respond input#submit.added:before,.woocommerce-page #respond input#submit.added:before,.woocommerce #content input.button.added:before,.woocommerce-page #content input.button.added:before {
	color: <?php echo $theme_color; ?>;
}
.woocommerce a.button:hover,.woocommerce-page a.button:hover,.woocommerce button.button:hover,.woocommerce-page button.button:hover,.woocommerce input.button:hover,.woocommerce-page input.button:hover,.woocommerce #respond input#submit:hover,.woocommerce-page #respond input#submit:hover,.woocommerce #content input.button:hover,.woocommerce-page #content input.button:hover {
	background:<?php echo $theme_color; ?>;
}
.woocommerce a.button.alt,.woocommerce-page a.button.alt,.woocommerce button.button.alt,.woocommerce-page button.button.alt,.woocommerce input.button.alt,.woocommerce-page input.button.alt,.woocommerce #respond input#submit.alt,.woocommerce-page #respond input#submit.alt,.woocommerce #content input.button.alt,.woocommerce-page #content input.button.alt {
	background:<?php echo $theme_color; ?>;
}
.woocommerce .star-rating span,.woocommerce-page .star-rating span {
	color: <?php echo $theme_color; ?>;
}
.woocommerce ul.cart_list li ins,.woocommerce-page ul.cart_list li ins ,.woocommerce ul.product_list_widget li ins ,.woocommerce-page ul.product_list_widget li ins{
	color:<?php echo $theme_color; ?>;
}
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle {
	background:<?php echo $theme_color; ?>;
    border: 1px solid <?php echo $theme_color; ?>;
}

<?php endif; ?>

@media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
only screen and (-moz-min-device-pixel-ratio: 1.5),
only screen and (-o-min-device-pixel-ratio: 3/2),
only screen and (min-device-pixel-ratio: 1.5) {
<?php if(alterna_get_options_key('logo-txt-enable') != "on" && alterna_get_options_key('logo-retina-image') != "") { ?>
.logo a { background-image:url(<?php echo alterna_get_options_key('logo-retina-image');?>); }
<?php } ?>
<?php if(alterna_get_options_key('fixed-enable') == "on" && alterna_get_options_key('fixed-logo-retina-image') != "") { ?>
.fixed-logo a { background-image:url(<?php echo alterna_get_options_key('fixed-logo-retina-image');?>); }
<?php } ?> 
<?php if(intval(alterna_get_options_key('global-layout')) == 0 && intval(alterna_get_options_key('global-bg-type')) == 0 && alterna_get_options_key('global-bg-retina-image') != "") {?>
body { background-image:url(<?php echo alterna_get_options_key('global-bg-retina-image');?>); }
<?php }?>
<?php if(intval(alterna_get_options_key('global-content-bg-type')) == 0 && alterna_get_options_key('global-content-bg-retina-image') != "") {?>
.content-wrap { background-image:url(<?php echo alterna_get_options_key('global-content-bg-retina-image');?>); }
<?php }?>
<?php if(intval(alterna_get_options_key('global-header-area-bg-type')) == 0 && alterna_get_options_key('global-header-area-bg-retina-image') != "") { ?>
#alterna-header { background-image:url(<?php echo alterna_get_options_key('global-header-area-bg-retina-image');?>);}
<?php }?>
<?php if(intval(alterna_get_options_key('global-page-title-bg-type')) == 0 && alterna_get_options_key('global-page-title-bg-retina-image') != "") { ?>
#page-header { background-image:url(<?php echo alterna_get_options_key('global-page-title-bg-retina-image');?>);}
<?php }?>

}