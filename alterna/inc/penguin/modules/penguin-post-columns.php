<?php

/**
	Penguin Framework

	Copyright (c) 2009-2014 ThemeFocus

	@url http://penguin.themefocus.co
	@package Penguin
	@version 4.0
**/


class PenguinPostColumns {
	
	public $posts;
	function PenguinPostColumns($posts = array()){
		
		$this->posts = $posts;
		
		if(count($posts) > 0){
			foreach($this->posts as $post){
				new PenguinPostColumn( $post['type'], $post['fields']);
			}
		}
		
	}
}

class PenguinPostColumn {
	
	public $type;
	public $fields;
	
	function PenguinPostColumn($type,$fields = array()){
		
		$this->type 	= $type;
		$this->fields 	= $fields;
		if(count($fields) > 0){
			add_filter('manage_'.$this->type.'_posts_columns', array($this,'penguin_posts_column_views'));
			add_action('manage_'.$this->type.'_posts_custom_column', array($this,'penguin_posts_custom_column_views'), 5, 2);
		}
	}
	
	// show columns fileds
	function penguin_posts_column_views($defaults){
		foreach($this->fields as $field){
			$defaults[$field['id']] = __($field['name'],Penguin::$THEME_NAME);
		}
		return $defaults;
	}
	
	// get columns value
	function penguin_posts_custom_column_views($column_name, $id){
		if(function_exists('Penguin_Custom_Posts_Columns_Value')){
			Penguin_Custom_Posts_Columns_Value($this->type, $column_name, $id);
		}
	}
}


?>