<?php

/**
	Penguin Framework

	Copyright (c) 2009-2014 ThemeFocus

	@url http://penguin.themefocus.co
	@package Penguin
	@version 5.0
**/

if ( ! function_exists( 'penguin_generate_options_css' ) ) :
/**
 * Generate Options CSS
 */
function penguin_generate_options_css() {
	$options_update_name = Penguin::$THEME_NAME.'_options_update';
	
	//get theme update history
	$options_update = get_option($options_update_name);
	
	//get theme version
	$theme_old_version = get_option($options_update_name.'-ver');
	$theme_data = wp_get_theme();
	if(!$theme_old_version){
		$theme_old_version = $theme_data['Version'];
		update_option($options_update_name.'-ver', $theme_data['Version']);
	}
	
	/** Define some vars **/
	$uploads = wp_upload_dir();
	$css_dir = get_template_directory() . '/custom/';
	
	/** Save on different directory if on multisite **/
	if(is_multisite()) {
		$uploads_dir = trailingslashit($uploads['basedir']);
	} else {
		$uploads_dir = $css_dir;
	}
	
	$update_data;
	
	if(isset($options_update['update'])){
		if($options_update['update'] == 'yes'){
			if(version_compare($theme_data['Version'], $theme_old_version, '>')){
				update_option($options_update_name.'-ver', $theme_data['Version']);
			}else{
				if (file_exists($uploads_dir . Penguin::$THEME_NAME.'-styles.css')) {
					return;
				}
			}
		}
		$update_data = array('update'=>'yes','version'=> (intval($options_update['version']) + 1) );
	}else{
		$update_data = array('update'=>'yes','version'=> 0 );
	}

	/** Capture CSS output **/
	ob_start();
	require($css_dir . 'custom-styles.php');
	$css = ob_get_clean();
	
	/** Write to options.css file **/
	WP_Filesystem();
	global $wp_filesystem;
	if ( ! $wp_filesystem->put_contents( $uploads_dir .  Penguin::$THEME_NAME.'-styles.css', $css, 0644) ) {
		return true;
	}
	update_option($options_update_name ,$update_data);
}
endif;

if ( ! function_exists( 'penguin_add_image_size' ) ) :
/**
 * Add custom image size for feature image crop
 */
function penguin_add_image_size($name,$w,$h,$crop = true){
	add_image_size( $name, $w, $h, $crop );
	update_option($name.'_size_w', $w);
	update_option($name.'_size_h', $h);
	update_option($name.'_crop', $crop ? 1 : 0);
}
endif;

if ( ! function_exists( 'penguin_string_limit_words' ) ) :
/**
 * Get limit words
 */
function penguin_string_limit_words($str, $limit = 18 , $need_end = false) {
	$words = explode(' ', $str, ($limit + 1));
	if(count($words) > $limit) {
		array_pop($words);
		array_push($words,'...');
	}
	return implode(' ', $words);
}
endif;

if ( ! function_exists( 'penguin_hex2RGB' ) ) :
/**
 * Get common hex to rgb
 *
 */
function penguin_hex2RGB($color) {
	if ($color[0] == '#'){
        $color = substr($color, 1);
	}

    if (strlen($color) == 6){
        list($r, $g, $b) = array($color[0].$color[1],$color[2].$color[3],$color[4].$color[5]);
	}
    elseif (strlen($color) == 3){
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
	}else{
        return false;
	}

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return array('r' => $r, 'g' => $g, 'b' => $b);
}
endif;

if ( ! function_exists( 'penguin_add_param_for_link' ) ) :
/**
 * get custom param link
 */
function penguin_add_param_for_link($link,$param){
	if(!strpos($link,'?')){
		return $link.'?'.$param;
	}
	return $link.'&amp;'.$param;
}
endif;

//========================================================
//  	PLUGIN FUNCTION METHODS
//========================================================

/**
 * Get option  for layerslider
 */
function penguin_get_layerslider(){
	$layerslider_slides = array();
	$layerslider_slides[0] = __('Select a slider',Penguin::$THEME_NAME);
	
	 // Get WPDB Object
    global $wpdb;
 
    // Table name
    $table_name = $wpdb->prefix . "layerslider";

	$sql = "show tables like '$table_name'";
	
	$table = $wpdb->get_var($sql);

	// have no rev slider 
	if($table != $table_name) return $layerslider_slides;
 
    // Get sliders
    $sliders = $wpdb->get_results( "SELECT * FROM $table_name
                                        WHERE flag_hidden = '0' AND flag_deleted = '0'
                                        ORDER BY date_c ASC LIMIT 100" );
 
    // Iterate over the sliders
    foreach($sliders as $key => $item) {
 		$layerslider_slides[$item->id] = '#'.$item->id . ' - ' .$item->name;
    }
	
	return $layerslider_slides;
}

/**
 * Get option  for revslider
 */
function penguin_get_revslider(){
	$revslider_slides = array();
	$revslider_slides[0] = __('Select a slider',Penguin::$THEME_NAME);
	
	 // Get WPDB Object
    global $wpdb;

    // Table name
    $table_name = $wpdb->prefix . "revslider_sliders";
	
	$sql = "show tables like '$table_name'";
	
	$table = $wpdb->get_var($sql);

	// have no rev slider 
	if($table != $table_name) return $revslider_slides;
	
    // Get sliders
    $sliders = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY id LIMIT 100" );
 
    // Iterate over the sliders
    foreach($sliders as $key => $item) {
 		$revslider_slides[$item->id] = '#'.$item->id . ' - ' .$item->title;
    }
	
	return $revslider_slides;
}

/**
 * Get option  for wpml
 */
function penguin_get_wpml_switcher(){
	if (function_exists( 'icl_get_languages' ) && function_exists('icl_disp_language')) {
		$languages = icl_get_languages('skip_missing=1');
		
		if(!empty($languages)){
			
			$lang_active = '';
			$lang_list = '<ul class="sub-menu">';
			
			foreach($languages as $l){

				if($l['active']){ $lang_active = '<a href="#"><i class="fa fa-globe"></i>'.$l['native_name'].'</a>';				}
				$lang_list .= '<li>';

				if(!$l['active']){
					$lang_list .= '<a href="'.$l['url'].'">';
				}else{
					$lang_list .=  '<span>';
				}
				
				if($l['country_flag_url']){
					$lang_list .= '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';				
				}
				$lang_list .=  $l['native_name'];
			
				if(!$l['active']){
					$lang_list .= '</a>';
				}else{
					$lang_list .=  '</span>';
				}
				$lang_list .= '</li>';
			}
			$lang_list .= '</ul>';
			
			return '<li class="wpml">'.$lang_active.$lang_list.'</li>';
		}
	}
	return '';
}

// function to display number of posts.
function penguin_get_post_meta_count($postID, $count_type = 0, $count_name_bool = true){
	
	if($count_type == 1) {
		$count_key = 'votes_count';
	}else{
		$count_key = 'post_views_count';
	}
	
    $count = get_post_meta($postID, $count_key, true);
	
    if($count == ''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');

        return '0';
    }
	
    return $count;
}
 
// function to count views.
function penguin_set_post_view($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// set add post like count
function penguin_post_like(){
	$nonce = $_POST['nonce'];

    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        die ( 'Busted!');
		
	if(isset($_POST['post_like'])){
		$ip = $_SERVER['REMOTE_ADDR'];
		$post_id = $_POST['post_id'];
		
		$meta_IP = get_post_meta($post_id, "voted_IP");
		if(isset($meta_IP[0]) && is_array($meta_IP[0])){
			$voted_IP = $meta_IP[0];
		}else{
			$voted_IP = array();
		}
		
		$meta_count = get_post_meta($post_id, "votes_count", true);

		if(!penguin_post_already_vote($post_id)){
			$voted_IP[$ip] = time();
			update_post_meta($post_id, "voted_IP", $voted_IP);
			update_post_meta($post_id, "votes_count", ++$meta_count);
			
			echo $meta_count;
		}else{
			echo "already";
		}
	}
	exit;
}

// check post had vote
function penguin_post_already_vote($post_id){
	
	$meta_IP = get_post_meta($post_id, "voted_IP");
	
	if(!isset($meta_IP[0])){
		return false;
	}
	
	$voted_IP = $meta_IP[0];
	
	if(!is_array($voted_IP)){
		$voted_IP = array();
	}

	$ip = $_SERVER['REMOTE_ADDR'];
	
	if(in_array($ip, array_keys($voted_IP))){
		return true;
	}
	return false;
}

// get post like link
function penguin_get_post_like_link($post_id){
	if( !penguin_post_already_vote($post_id) ){
		return 'rate';
	}
	return '';
}