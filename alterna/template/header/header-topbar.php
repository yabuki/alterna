<?php
/**
 * Header Topbar
 * 
 * @since alterna 7.0
 */

if(alterna_get_options_key('header-topbar-enable') == "on") {
	if(intval(alterna_get_options_key('header-topbar-style-type')) == 0) { 
?>
    <div class="header-top-content">
        <div class="container">
            <div class="header-information">
                <div class="header-custom-message">
                <?php echo do_shortcode(alterna_get_options_key('header-topbar-message'));  ?>
                </div>
            </div>
        </div>
    </div>
	<?php }else{ ?>
    <div id="header-topbar">
        <div class="container">
        <?php if(alterna_get_options_key('header-topbar-menu-enable') == "on") { ?>
            <div id="header-topbar-left-content">
                <?php 
                    $alterna_topbar_menu = array(
                        'theme_location'  	=> 'alterna_topbar_menu',
                        'container'			=> '',
                        'menu_class'    	=> 'alterna-topbar-menu',
                        'fallback_cb'	  	=> 'alterna_show_setting_topbar_menu'
                    ); 
                    wp_nav_menu($alterna_topbar_menu);
                ?>
            </div>
        <?php } ?>
            <div id="header-topbar-right-content">
                <ul>
                <?php
                if(alterna_get_options_key('header-topbar-login-enable') == "on"){
                    if (class_exists( 'woocommerce' )) {
                        get_template_part('woocommerce/topbar-content');
                    }else if( alterna_get_options_key('header-topbar-login-enable') == "on" && alterna_get_options_key('header-topbar-custom-login-page') != ""){
                ?>
                    <li>
                    <?php if ( is_user_logged_in() ) { ?>
                        <a href="<?php echo alterna_get_options_key('header-topbar-custom-login-page'); ?>" title="<?php _e( 'View your account', 'alterna' ); ?>"><?php
                            global $current_user;
                            get_currentuserinfo();
                            if($current_user->user_firstname){
                                echo __('Welcome, ','alterna') . $current_user->user_firstname;
                            }elseif($current_user->display_name){
                                echo __('Welcome, ','alterna') . $current_user->display_name;
                            }
                            ?></a>&nbsp;&nbsp;<?php _e('|','alterna'); ?>&nbsp;&nbsp;<a href="<?php echo wp_logout_url(get_permalink()); ?>"><?php _e('Log out','alterna'); ?></a>
                    <?php }else{ ?>
                        <a class="wc-login-in" href="<?php echo alterna_get_options_key('header-topbar-custom-login-page'); ?>" title="<?php _e( 'Login / Register', 'alterna' ); ?>"><?php _e( 'Login / Register', 'alterna' ); ?></a>
                    <?php } ?>
                    </li>
                <?php
                    }
                }
                if(alterna_get_options_key('header-topbar-social-enable') == "on"){
                    echo alterna_get_social_list('',true);
                }
                if(alterna_get_options_key('rss-feed') != ""){
                    echo '<li class="social"><a href="'.alterna_get_options_key('rss-feed').'"><i class="fa fa-rss"></i></a></li>';
                }
                if(alterna_get_options_key('header-topbar-wpml-enable') == "on"){
                    echo penguin_get_wpml_switcher();
                }
                ?>
                </ul>
            </div>
            <div id="alterna-topbar-nav" class="navbar">
                <div id="alterna-topbar-select" class="navbar-inverse">
                    <button type="button" class="btn btn-navbar collapsed"><span class="fa fa-bars"></span></button>
                    <div class="collapse"><ul class="nav"></ul></div>
                </div>
            </div>
        </div>
    </div>
<?php } 
}
?>