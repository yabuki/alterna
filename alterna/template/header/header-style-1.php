<?php
/**
 * Header Style 1
 * 
 * @since alterna 7.0
 */
?>
<header>
    <div id="alterna-header" class="header-style-1 <?php echo alterna_get_options_key('fixed-enable') == "on" ? "header-fixed-enabled" : "";?>">
        <div class="container">
            <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php 
					if(alterna_get_options_key('logo-txt-enable')  == "on"){
						echo '<h2>'.get_bloginfo( 'name' ).'</h2>';
					}
				?></a>
            </div>
            <?php if(intval(alterna_get_options_key('header-right-area-type')) == 0) { ?>
            <div class="header-social-container">
                <ul class="inline alterna-social header-social">
                <?php
                    echo alterna_get_social_list();
                    if(alterna_get_options_key('rss-feed') != ""){
                        echo '<li><a title="'.__('rss','alterna').'" href="'.alterna_get_options_key('rss-feed').'" class="alterna-icon-rss"></a></li>';
                    }
                ?>
                </ul>
            </div>
            <?php }else{ ?>
            <div class="header-custom-container">
                <?php echo do_shortcode(alterna_get_options_key('header-right-area-content')); ?>
            </div>
            <?php } ?>
        </div>
    </div>

    <!-- mobile show drop select menu -->
    <div id="alterna-drop-nav" class="navbar">
        <div id="alterna-nav-menu-select" class="navbar-inverse">
            <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                <span class="fa fa-bars"></span>
             </button>
             <div class="nav-collapse collapse"><ul class="nav"></ul></div>
        </div>
    </div>

    <!-- menu & search form -->
    <nav id="alterna-nav" class="<?php echo (alterna_get_options_key('header-search-enable') == "on") ? "show-search" :""; ?>">
        <div class="container">
			<?php if(alterna_get_options_key('fixed-enable') == "on"){ ?>
            <div class="fixed-logo">
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"></a>
            </div>
        	<?php } ?>
			<?php 
                $alterna_main_menu = array(
                    'theme_location'  	=> 'alterna_menu',
                    'container_class'	=> 'alterna-nav-menu-container',
                    'menu_class'    	=> 'alterna-nav-menu',
                    'fallback_cb'	  	=> 'alterna_show_setting_primary_menu'
                ); 
                wp_nav_menu($alterna_main_menu);
            ?>
        	<?php if(alterna_get_options_key('header-search-enable') == "on") { ?>
            <div class="alterna-nav-form-container">
                <div class="alterna-nav-form">
                    <form role="search" class="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                       <div>
                            <?php if (class_exists( 'woocommerce' ) && alterna_get_options_key('shop-product-search') == "on") { ?>
                                <input class="sf-type" name="post_type" type="hidden" value="product" />
                            <?php } ?>
                            <input class="sf-s" name="s" type="text" placeholder="<?php _e('Search','alterna'); ?>" />
                            <input class="sf-searchsubmit" type="submit" value="" />
                       </div>
                    </form>
                </div>
            </div>
        <?php } ?>
        </div>
    </nav>
</header>